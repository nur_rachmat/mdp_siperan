<?php if($this->session->flashdata('simpan_ok')){ ?>
    <div class="alert alert-success alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <?=$this->session->flashdata('simpan_ok');?>
    </div>
<?php } ?>
<?php if($this->session->flashdata('simpan_ggl')){ ?>
    <div class="alert alert-danger alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <?=$this->session->flashdata('simpan_ggl');?>
    </div>
<?php } ?>
<ul class="nav nav-tabs">

    <li <?=(isset($active) && $active == 'stmik') ? 'class="active"' : '';?>><a href="<?=site_url('keuangan/pembiayaan/stmik');?>">KAS STMIK</a></li>
    <li <?=(isset($active) && $active == 'stie') ? 'class="active"' : '';?>><a href="<?=site_url('keuangan/pembiayaan/stie');?>">KAS STIE </a></li>
    <li><a href="<?=site_url('keuangan/pembiayaan/stie');?>"><i class="glyphicon glyphicon-compressed"> </i> Rekap Pembiayaan (Per Akun)</a></li>

</ul>
<div class="claerfix">&nbsp;</div>
<?php if($level != "Ketua") { ?>
<a href="<?=site_url('keuangan/tambah_pembiayaan');?>"><button type="submit" class="btn btn-primary"><i class="icon-plus icon-white"></i> Input Pembiayaan</button></a>
<?php  } ?>
<div class="claerfix">&nbsp;</div>
<div class="alert alert-success" role="alert">
    <h4>
        <i class="icon-tasks"></i> Pembiayaan dari Yayasan
        <span class="label label-danger pull-right"> <?=count($dt_pembiayaan);?> Pembiayaan</span>
    </h4>
</div>


<table id="table-pembiayaan" class="table" data-paging="true"></table>



<!-- Modal -->
<div class="modal fade bs-example-modal-lg" id="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Detail Usulan</h4>
            </div>
            <div class="modal-body">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade bs-example-modal-lg" id="modal-reklain" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Transfer Ke Rekening Lain</h4>
            </div>
            <form class="form-horizontal" method="post" action="">
                <div class="modal-body">
                    <div class="form-group">
                        <label for="recipient-name" class="col-sm-2 control-label text-left">No. Ref / ID:</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" name="ref-no" id="ref-no" readonly>
                        </div>
                        <div class="col-sm-2">
                            <input type="text" class="form-control" name="idusulan" id="idusulan" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="col-sm-2 control-label">Data Penerima</label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" required name="recipient-name" placeholder="Nama Penerima">
                        </div>
                        <div class="col-sm-5">
                         <input type="number" class="form-control" required name="recipient-rek" placeholder="Nomor Rekening ">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="message-text" class="col-sm-2 control-label">Pesan:</label>
                        <div class="col-sm-10">
                            <textarea class="form-control" required name="message-text"></textarea>
                        </div>
                    </div>

                    <p class="bg-warning text-danger text-center">
                        Harap perhatikan kembali Data Penerima dana sebelum mengirimkan permintaan Transfer Rekening.<br/>
                        Pastikan Data yang dimasukkan benar karena proses yang telah dilakukan tidak dapat dibatalkan lagi.
                    </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary" id="btn_transfer">Kirim</button>
                </div>
            </form>
        </div>
    </div>
</div>