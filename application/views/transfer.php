<?php if($this->session->flashdata('simpan_ok')){ ?>
    <div class="alert alert-success alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <?=$this->session->flashdata('simpan_ok');?>
    </div>
<?php } ?>

<div class="claerfix">&nbsp;</div>
<div class="alert alert-success" role="alert">
    <h4>
        <i class="icon-tasks"></i> Permohonan Transfer Dana
        <span class="label label-danger pull-right"> <?=count($dt_usulan);?> Permohonan</span>
    </h4>
</div>


<table class="table table-striped footable">
    <thead>
        <tr>
            <th>No. Ref</th>
            <th data-breakpoints="xs sm" data-type="date">Tgl Usul</th>
            <th>Pengusul</th>
            <?=(isset($active) && $active == 'transfer') ? '<th>Acc Yayasan</th>' : '';?>
            <th>Nilai</th>
            <th data-breakpoints="xs sm" data-type="html">Kas</th>

            <th data-breakpoints="xs sm" data-type="html">Keterangan</th>
            <th data-breakpoints="xs sm" data-type="html">Ke</th>
        </tr>
    </thead>
    <?php
    if(count($dt_usulan) > 0){
        echo "<tbody>";
        foreach($dt_usulan as $row){
            echo "<tr>";
            $no_ref = ($row['no_ref'] =='') ? "00" : $row['no_ref'];
            echo "<td>".$no_ref."</td>";
            echo "<td>".date('d-m-Y H:i:s', strtotime($row['tgl_usul']))."</td>";
            $user = explode("@", $row['user']);
            echo "<td>".$user[0]."</td>";
            echo (isset($active) && $active == 'transfer') ? "<td>".date('d-m-Y H:i:s', strtotime($row['tgl_rev']))."</td>" : '';

            if($row['rekening'] === '1'){
                $kas = "<span class='label label-info'>STMIK</span>";
            }elseif($row['rekening'] == '2'){
                $kas = "<span class='label label-success'>STIE</span>";
            }elseif($row['rekening'] == '3'){
                $kas = "<span class='label label-default'>Bersama</span>";
            }else{
                $kas = "<span class='label label-danger'>N/A</span>";
            }
            
            echo "<td title='".Terbilang($row['nilai'])." Rupiah'>Rp ".number_format($row['nilai'], 2, ",", ".")."</td>";
            //echo "<td>".$row['nilai']."</td>";
            echo "<td>".$kas."</td>";



            
            $ket = strip_tags(htmlspecialchars_decode($row['keterangan']), "<img>");
            $k_img = preg_replace("/<img[^>]+\>/i", " [ <b>image</b> ] ", $ket);

            $link_more = " [ <a data-toggle='modal' data-target='#modal' href='#' data-href='".site_url('peran/detail_usulan/'.$row['id'])."' ><i class='glyphicon glyphicon-eye-open'></i> detail</a> ]";
            echo "<td>".substr($k_img, 0, 100).((strlen($k_img) > 100) ? $link_more : "")."</td>";

            $Qke = "SELECT * FROM t_transfer WHERE no_ref = ?";
            $Rke = $this->db->query($Qke, array($row['no_ref']))->row_array();
            if(isset($Rke['no_ref'])){
                echo "<td>Nama Penerima : ".$Rke['nama_penerima'].
                    "<br/>Rek Penerima : ". $Rke['rek_penerima'].
                    "<br/>Pesan : ". $Rke['pesan'].

                    "</td>";
            }else{
                echo "<td>".(($row['dana'] == '') ? $kas : strtoupper($row['dana']))."</td>";
            }


            $act_batal = " <a href='".site_url('peran/batal_usulan/'.$row['id'])."'><button class='btn btn-warning btn-sm '><i class='glyphicon glyphicon-remove-circle'></i> Batal</button></a>";
            $act_ubah = " <a href='".site_url('peran/ubah_usulan/'.$row['id'])."'><button class='btn btn-default btn-sm '><i class='glyphicon glyphicon-edit'></i> Ubah</button></a>";
            $act_minta = " <a href='".site_url('peran/minta_dana/'.$row['id'])."'><button class='btn btn-success btn-sm '><i class='glyphicon glyphicon-transfer'></i> Minta Dana</button></a>";
            if($row['status'] == 0){
                $status = "<span class='label label-info lbl-sm'>Telah Diajukan</span> ";
                $act_batal = $act_batal . " ". $act_ubah;
            }elseif($row['status'] == 1){
                $status = "<span class='label label-success lbl-sm'>Disetujui</span>";
                $act_batal = $act_minta;
            }elseif($row['status'] == 3){
                $status = "<span class='label label-danger lbl-sm'>Ditolak</span>";
                $act_batal = '';
            }elseif($row['status'] == 4){
                $status = "<span class='label label-warning lbl-sm'>Dibatalkan</span>";
                $act_batal = '';
            }elseif($row['status'] == 5){
                $status = "<span class='label label-success lbl-sm'>Disetujui</span>";
                $act_batal = "[ diajukan ke bendahara ]";

            }else{
                $status = "<span class='label label-default lbl-sm'>Tidak Diketahui</span>";
                $act_batal = '';
            }

            //echo "<td >".$status."</td>";
            /*if(!isset($active)) {
                echo "<td class='text-center'>" . $act_batal . "</td>";
            }*/
            echo "</tr>";

        }
        echo "</tbody>";
    }


    ?>



</table>

<!-- Modal -->
<!--<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">

    </div>
</div>-->

<div class="modal fade bs-example-modal-lg" id="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Detail Usulan</h4>
            </div>
            <div class="modal-body">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>