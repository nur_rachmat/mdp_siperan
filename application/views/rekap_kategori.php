<div class="alert alert-info no-print" role="alert">
    <h4><i class="glyphicon glyphicon-file"></i> Rekap Nota Berdasarkan Account (Kategori)</h4>
</div>

<form method="post" action="<?=site_url('peran/daftar_kategori')?>" class="form-inline no-print">
    <div class="form-group">
        <div class="input-group col-sm-2">
            <select class="form-control" id="inKas" name="kas">
                <option value="1" <?=(($this->input->post('kas') == 1) ? "selected" : "");?>>STMIK</option>
                <option value="2" <?=(($this->input->post('kas') == 2) ? "selected" : "");?>>STIE</option>
            </select>
        </div>

        <!--<label for="inTgl" class="col-sm-1 control-label text-right">Kas</label>-->
        <label for="reportrange" class="col-sm-1 control-label text-right">Periode</label>
        <div class="input-group col-sm-3">
            <div class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i> </div>
            <input type="text" value="<?=(($this->input->post('periode')) ? $this->input->post('periode') : "");?>" name="periode" id="reportrange" class="form-control">
        </div>

        <label for="inPcash" class="col-sm-1 control-label text-right">Account (Kategori)</label>
        <div class="input-group col-sm-4">
            <div class="input-group-addon"><i class="glyphicon glyphicon-briefcase"></i></div>
            <select id="inPcash" name="kategori" required class="form-control select2-allow-clear select2">
                <optgroup label="Semua Account">
                    <option value="semua">Semua</option>
                </optgroup>
                <optgroup label="Account(Kategori)">
                    <?php
                    $dt_account = $this->Siperan_Model->get_dt_account();
                    if(count($dt_account)){
                        foreach ($dt_account as $acc) {
                            if($this->input->post('kategori')){
                                $kat = $this->input->post('kategori');
                                $sel = ($kat == $acc['kd_account']) ? 'selected' : '';
                                echo "<option value=\"".$acc['kd_account']."\" ".$sel.">".$acc['kd_account']." - ".$acc['account']."</option>";
                            }else{
                                echo "<option value=\"".$acc['kd_account']."\">".$acc['kd_account']." - ".$acc['account']."</option>";
                            }

                        }
                    }
                    ?>
                </optgroup>
            </select>
        </div>

        <div class="input-group col-sm-12">
            <button type="submit" name="cari" value="Cari" class="btn btn-info">Cari</button>&nbsp;
            <button type="submit" name="print" formtarget="_blank" value="Excel" class="btn btn-success"> <i class="fa fa-file-excel-o"></i> Export to Excel</button>
        </div>
    </div>
</form>
<div class="clearfix">&nbsp;</div>
<?php
if($this->input->post('cari')){
    $kode = $this->input->post('kategori', true);

    $periode = $this->input->post('periode', true);
    $kas = $this->input->post('kas', true);

    $prd = explode("-", $periode);

    if($kode != 'semua'){
        $sql = "SELECT  t_nota.*, t_account.account as account FROM t_nota
            inner join t_account on t_nota.kd_account = t_account.kd_account
            inner join t_pcash on t_nota.pcash = t_pcash.id
            where t_nota.kd_account = ? and (t_nota.pcash is not null or t_nota.pcash != '')
            AND (DATE_FORMAT( t_pcash.tgl_pcash,  '%Y-%m-%d' ) >= DATE_FORMAT( ?,  '%Y-%m-%d' )
            and DATE_FORMAT( t_pcash.tgl_pcash,  '%Y-%m-%d' ) <= DATE_FORMAT( ?,  '%Y-%m-%d' ))
            AND t_pcash.kas_owner = ?
            ORDER BY t_nota.tgl_nota ASC";
        $dt_pettycash = $this->db->query($sql, array($kode, $prd[0], $prd[1], $kas))->result_array();
    }else{
        $dt_account = $this->Siperan_Model->get_dt_account();
        foreach ($dt_account as $acc) {
            $kodek = $acc['kd_account'];
            $sql = "SELECT  t_nota.*, t_account.account as account FROM t_nota
            inner join t_account on t_nota.kd_account = t_account.kd_account
            inner join t_pcash on t_nota.pcash = t_pcash.id
            where t_nota.kd_account = ? and (t_nota.pcash is not null or t_nota.pcash != '')
            AND (DATE_FORMAT( t_pcash.tgl_pcash,  '%Y-%m-%d' ) >= DATE_FORMAT( ?,  '%Y-%m-%d' )
            and DATE_FORMAT( t_pcash.tgl_pcash,  '%Y-%m-%d' ) <= DATE_FORMAT( ?,  '%Y-%m-%d' ))
            AND t_pcash.kas_owner = ?
            ORDER BY t_nota.tgl_nota ASC";
            $dt_pettycash[$kodek] = $this->db->query($sql, array($kodek, $prd[0], $prd[1], $kas))->result_array();
        }
    }

    if($kode != 'semua'){
        if(count($dt_pettycash) > 0){
            $pc = $this->Siperan_Model->get_account($kode);
            echo '<p class="alert alert-success text-center"><strong>Daftar Nota Berdasarkan Account (Kategori) '.$pc.' ('.$kode.')</strong></p>';

        }else{
            $dt_pettycash = array();
        }
    }else{
        foreach ($dt_account as $acc) {
            $kodek = $acc['kd_account'];
            if(count($dt_pettycash[$kodek]) > 0){
                $pc = $this->Siperan_Model->get_account($kodek);
                echo '<p class="alert alert-success text-center"><strong>Daftar Nota Berdasarkan Account (Kategori) '.$pc.' ('.$kodek.')</strong></p>';
            ?>
                <table class="table table-striped footable" id="foo-modal">
                    <thead>
                        <tr>
                            <th>No. Ref</th>
                            <th>Tgl. Nota</th>
                            <th>Petty Cash</th>
                            <th data-breakpoints="xs sm md lg" data-type="html">Keterangan</th>
                            <th data-breakpoints="xs sm md lg" data-type="html">Nota</th>
                            <th>Nominal</th>
                        </tr>
                    </thead>

                    <?php
                    if(count($dt_pettycash[$kodek]) > 0){
                        echo "<tbody>";
                        $total_nilai = 0;
                        foreach($dt_pettycash[$kodek] as $row){
                            echo "<tr>";
                            echo "<td>".$row['no_ref']."</td>";
                            echo "<td>".date("d-M-Y", strtotime($row['tgl_nota']))."</td>";
                            $pc = $this->Siperan_Model->get_pcash($row['pcash']);
                            echo "<td>".$pc['kode_pcash']."</td>";
                            echo "<td class=\"det-keet\">".htmlspecialchars_decode($row['keterangan'], ENT_QUOTES)."</td>";
                            echo "<td>".(($row['f_nota'] == '') ? 'Tidak Ada' : '<a href="'.base_url('uploads/nota/'.$row['f_nota']).'" target="_blank">Nota</a>')."</td>";
                            echo "<td title='".Terbilang($row['nilai'])." Rupiah'>Rp ".number_format($row['nilai'], 2, ",", ".")."</td>";
                            echo "</tr>";
                            $total_nilai = $row['nilai'] + $total_nilai;
                        }
                        echo "</tbody>";
                    }
                    ?>
                </table>
                <?php if(count($dt_pettycash[$kodek]) > 0) :  ?>
                    <h4> Total Nominal : <span class="pull-right"> Rp <?=number_format($total_nilai, 2, ",", ".");?></span></h4>
                    <span class="text-muted" style="color:darkred;"><?=Terbilang($total_nilai);?> Rupiah</span>
                <?php endif; ?>
                <hr/>
            <?php
            }
        }
    }

    if($kode != 'semua'){
?>
        <table class="table table-striped footable" id="foo-modal">
            <thead>
                <tr>
                    <th>No. Ref</th>
                    <th>Tgl. Nota</th>
                    <th>Petty Cash</th>
                    <th data-breakpoints="xs sm md lg" data-type="html">Keterangan</th>
                    <th data-breakpoints="xs sm md lg" data-type="html">Nota</th>
                    <th>Nominal</th>
                </tr>
            </thead>

            <?php
            if(count($dt_pettycash) > 0){
                echo "<tbody>";
                $total_nilai = 0;
                foreach($dt_pettycash as $row){
                    echo "<tr>";
                    echo "<td>".$row['no_ref']."</td>";
                    echo "<td>".date("d-M-Y", strtotime($row['tgl_nota']))."</td>";
                    $pc = $this->Siperan_Model->get_pcash($row['pcash']);
                    echo "<td>".$pc['kode_pcash']."</td>";
                    echo "<td class=\"det-keet\">".htmlspecialchars_decode($row['keterangan'], ENT_QUOTES)."</td>";
                    echo "<td>".(($row['f_nota'] == '') ? 'Tidak Ada' : '<a href="'.base_url('uploads/nota/'.$row['f_nota']).'" target="_blank">Nota</a>')."</td>";
                    echo "<td title='".Terbilang($row['nilai'])." Rupiah'>Rp ".number_format($row['nilai'], 2, ",", ".")."</td>";
                    echo "</tr>";
                    $total_nilai = $row['nilai'] + $total_nilai;
                }
                echo "</tbody>";
            }
            ?>
        </table>
        <?php if(count($dt_pettycash) > 0) :  ?>
            <h4> Total Nominal : <span class="pull-right"> Rp <?=number_format($total_nilai, 2, ",", ".");?></span></h4>
            <span class="text-muted"><?=Terbilang($total_nilai);?> Rupiah</span>
        <?php endif; ?>
    <?php } ?>

<?php } ?>

<script src="<?php echo base_url(); ?>data_table/js/jquery.js"></script>
<script src="<?php echo base_url(); ?>assets/footable/js/footable.js"></script>
<script>
    jQuery(function($){
        $('#foo-rekap').footable({
            "useParentWidth": true,
            "paging": {
                "enabled": true
            },
            "filtering": {
                "enabled": true
            },
            "sorting": {
                "enabled": true
            }
        });
    });
</script>

<?php $show  = false; if($show){ ?>
<div class="well">
    <h4>Petunjuk penarikan data menggunakan SAPI (SIPERAN API)</h4>
    <ol>
        <ul>
            <li>Data response berupa JSON dengan format Seperti berikut : <br/>
                <a href ="<?=base_url('sapi/pettycash');?>" target="_blank"><span class="glyphicon glyphicon-check"></span> Json Petty Cash</a><br/>
                <ul>
                    <li>Link ambil semua data <code><?=site_url('sapi/pettycash');?></code></li>
                    <li>Link filter berdasar Tanggal Nota <code><?=site_url('sapi/pettycash/f_tgl/YYYY-MM-DD');?></code></li>
                    <li>Link filter berdasar Kode PettyCash <code><?=site_url('sapi/pettycash/f_pcash/KODEPETTYCASH');?></code></li>
                    <li>Link filter berdasar Tanggal dan Kas <code><?=site_url('sapi/pettycash/f_tglkas/YYYY-MM-DD/KODE_KAS');?></code></li>
                </ul>
                <a href ="<?=base_url('sapi/nota');?>" target="_blank"><span class="glyphicon glyphicon-check"></span>  Json Nota</a><br/>
                <ul>
                    <li>Link ambil semua data <code><?=site_url('sapi/nota');?></code></li>
                    <li>Link filter berdasar Tanggal Nota <code><?=site_url('sapi/nota/f_tgl/YYYY-MM-DD');?></code></li>
                    <!--<li>Link filter berdasar KAS <code><?/*=site_url('sapi/nota/f_kas/KODE_KAS');*/?></code></li>
                    <li>Link filter berdasar Tanggal dan Kas <code><?/*=site_url('sapi/nota/f_tglkas/YYYY-MM-DD/KODE_KAS');*/?></code></li>-->
                </ul>
                <a href ="<?=base_url('sapi/pettycash_data');?>" target="_blank"><span class="glyphicon glyphicon-check"></span> Json Petty Cash + Nota</a><br/>
                <ul>
                    <li>Link ambil semua data <code><?=site_url('sapi/pettycash_data');?></code></li>
                    <li>Link filter berdasar Tanggal Nota <code><?=site_url('sapi/pettycash_data/f_tgl/YYYY-MM-DD');?></code></li>
                    <li>Link filter berdasar Kode PettyCash <code><?=site_url('sapi/pettycash_data/f_pcash/KODEPETTYCASH');?></code></li>
                   <!-- <li>Link filter berdasar KAS <code><?/*=site_url('sapi/pettycash_data/f_kas/KODE_KAS');*/?></code></li>
                    <li>Link filter berdasar Tanggal dan Kas <code><?/*=site_url('sapi/pettycash_data/f_tglkas/YYYY-MM-DD/KODE_KAS');*/?></code></li>-->
                </ul>
                <!--<a href ="<?/*=base_url('sapi/per_pettycash');*/?>" target="_blank"><span class="glyphicon glyphicon-check"></span> Json Petty Cash </a><br/>-->

                <a href ="<?=base_url('sapi/nota_account');?>" target="_blank"><span class="glyphicon glyphicon-check"></span> Json Nota Per Account</a><br/>
                <ul>
                    <li>Link ambil semua data <code><?=site_url('sapi/nota_account');?></code></li>
                    <li>Link filter berdasar Account<code><?=site_url('sapi/nota_account/f_acc/KODE_ACCOUNT');?></code></li>
                </ul>
                <a href ="<?=base_url('sapi/account');?>" target="_blank"><span class="glyphicon glyphicon-check"></span> Json Account</a><br/>
            </li>

            <li>Format Tanggal yaitu YYYY-MM-DD, contoh tanggal hari ini <strong><?=date('Y-m-d');?></strong></li>
            <li>KODE_KAS yaitu <strong>1</strong> untuk STMIK dan <strong>2</strong> untuk STIE</li>
            <li>Comma delimiter pad KODE_ACCOUNT harus diraubah menjadi <strong>Titik (.)</strong> sehingga <strong>610,901</strong> menjadi <strong>610.901</strong></li>
        </ul>
    </ol>
</div>
<?php } ?>