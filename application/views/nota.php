
<ul class="nav nav-tabs">
    <li <?=(!isset($active) ||  $active == 'tambahnota') ? 'class="active"' : '';?>><a href="<?=site_url('peran/nota');?>">Buat Nota</a></li>
    <li <?=(isset($active) && $active == 'notakosong') ? 'class="active"' : '';?>><a href="<?=site_url('peran/nota/notakosong');?>">Nota Kosong</a></li>
    <li <?=(isset($active) && $active == 'created') ? 'class="active"' : '';?>><a href="<?=site_url('peran/nota/created');?>">Nota Dibuat</a></li>
</ul>
<div class="claerfix">&nbsp;</div>
<?php if(isset($act) && $act == 'buat_nota') : ?>
<a href="<?=site_url('peran/nota');?>"><button type="button" class="btn btn-primary"><i class="icon-plus icon-white"></i> Buat Nota Berdasarkan Usulan</button></a>
<?php endif; ?>
<div class="claerfix">&nbsp;</div>


<?php if($this->session->flashdata('simpan_ok')){ ?>
    <div class="alert alert-success alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <?=$this->session->flashdata('simpan_ok');?>
    </div>
<?php } ?>



<?php
if(isset($active) && $active == 'created'):
    // Tab Nota Dibuat / Cari Nota yang telah dibuat (berdasarkan tgl nota dan kas)
    $this->load->view('modules/v_carinota');
elseif(isset($active) && $active == 'notakosong'):
    // Tab Nota Kosong
    $this->load->view('modules/v_notakosong');
elseif(isset($active) && $active == 'tambahnota'):
    // Tab Nota Kosong
    $this->load->view('modules/v_tambahnota');
else:
    // Tab Buat Nota
?>
    <div class="alert alert-success" role="alert">
        <i class="icon-tasks"></i> Buat Nota berdasarkan Usulan yang telah disetujui
    </div>
    <div class="row">
        <div class="col-md-12">
            <?php if(!isset($act) || ($act != 'edit_nota' && $act != 'edit_notakosong')) : ?>
                <form method="post" action="<?=site_url('peran/buat_nota')?>" class="form-horizontal has-success">
                    <div class="form-group">
                        <label for="inTgl" class="col-sm-1 control-label">Tgl Nota</label>
                        <div class="input-group col-sm-4">
                            <div class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></div>
                            <input type="text" name="tgl_nota" <?php if(isset($lock) && $lock == true){ echo "readonly  value='".date("d-m-Y", strtotime($tgl_nota))."'  class=\"form-control\""; }else{ echo  " class=\"form-control datepicker\" value=\"".date("d-m-Y")."\""; }  ?> id="inTgl" >
                        </div>

                        <label for="nota_kas" class="col-sm-1 control-label"> Kas</label>

                            <?php if(isset($lock) && $lock == true) :  $arr_nk = array('', 'STMIK', 'STIE'); ?>
                            <div class="input-group col-sm-6">
                                <input type="text" name="nota_kas" <?php if(isset($lock) && $lock == true){ echo "readonly  value='".$arr_nk[$nota_kas]."'"; }?> id="nota_kas" class="form-control " style="background-color: #DFF0D8 !important;" >
                            </div>
                                <?php else : ?>
                            <div class="input-group col-sm-4">
                                <select class="form-control" id="nota_kas" name="nota_kas">
                                    <option value="1" <?=(($this->input->post('kas') == 1) ? "selected" : "");?>>STMIK</option>
                                    <option value="2" <?=(($this->input->post('kas') == 2) ? "selected" : "");?>>STIE</option>
                                </select>
                            </div>
                            <?php endif; ?>

                        <?php if(!isset($act) && $act != 'buat_nota') : ?>
                            <div class="input-group col-sm-2">
                                <button type="submit" class="btn btn-info" ><span class="glyphicon glyphicon-plus-sign"></span></button>
                            </div>
                        <?php else : ?>
                            <!--<div class="input-group col-sm-2">
                                <a href="<?/*=site_url('peran/nota');*/?>">
                                    <button type="button" class="btn btn-info" ><span class="glyphicon glyphicon-plus-sign"></span></button>
                                </a>
                            </div>-->
                        <?php endif; ?>
                    </div>
                </form>
            <?php endif; ?>

            <?php if($this->session->flashdata('simpan_ggl')){ ?>
                <div class="alert alert-danger alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <?=$this->session->flashdata('simpan_ggl');?>
                </div>
            <?php } ?>

            <?php
            // Form Buat Nota (berdasarkan nota kosong)
            if(isset($act) && $act == 'buat_nota') :
                $this->load->view('modules/v_buatnota');
            endif;
            ?>

            <?php
            // Form Edit/Ubah Nota
            if(isset($act) && $act == 'edit_nota') :
                $this->load->view('modules/v_editnota');
            endif;
            ?>


            <?php if(isset($act) && $act == 'edit_notakosong') :
                $this->load->view('modules/v_formeditnotakosong');
            endif; ?>
        </div>

        <?php
        //Tampilkan list nota pada halaman buat nota
        if(isset($act) && ($act == 'buat_nota'  || $act == 'buat_nota' )) :
            $this->load->view('modules/v_listnota');
        endif;
        ?>
    </div>

<?php endif; ?>

<!-- Modal -->
<div class="modal fade bs-example-modal-lg" id="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Detail Usulan</h4>
            </div>
            <div class="modal-body">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>


<div class="modal fade bs-example-modal-lg" id="modal_dt_ref" tabindex="-1" role="dialog" aria-labelledby="myModalLabel_ref">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel_ref">Data Usulan</h4>
            </div>
            <div class="modal-body">

            </div>
            <div class="modal-footer">

                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>