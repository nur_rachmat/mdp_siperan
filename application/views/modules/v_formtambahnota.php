<form method="post" enctype="multipart/form-data" id="form_nota"  action="<?=site_url('peran/simpan_nota/back')?>" class="form-horizontal">
    <input type="hidden" name="no_ref" id="no_ref" value="">

    <div class="form-group">
        <label for="inTgl" class="col-sm-1 control-label">Tgl Nota</label>
        <div class="input-group col-sm-3">
            <div class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></div>
            <input type="text" name="tgl_nota" class="form-control datepicker" value="<?=date("d-m-Y");?>" id="inTgl" >
        </div>

        <label for="nota_kas" class="col-sm-1 control-label"> Kas</label>
        <?php
        $arr_nk = array('', 'STMIK', 'STIE'); ?>
        <div class="input-group col-sm-7">
            <input type="hidden" name="nota_kas" readonly  value="<?=$kas;?>">
            <input type="text" name="kas" readonly  value="<?=$arr_nk[$kas];?>" id="nota_kas" class="form-control " style="background-color: #DFF0D8 !important;" >
        </div>
    </div>
    <div class="form-group has-warning">
        <label for="inPcash" class="col-sm-1 control-label">Petty Cash</label>
            <div class="input-group col-sm-3">
                <div class="input-group-addon"><i class="glyphicon glyphicon-briefcase"></i></div>
                <input type="hidden" readonly  name="kd_pcash" required class="form-control" value="<?=$kd_pcash;?>">

                <?php
                $pettycah = "";
                $dt_pcash = $this->Siperan_Model->get_dt_pcash($kas);
                if(count($dt_pcash)){
                    foreach ($dt_pcash as $pcash) {
                        if($pcash['lock'] == 'T' && $pcash['id'] == $kd_pcash) {
                            $pettycah = $pcash['kode_pcash'];
                            ?>
                            <input type="hidden" name="pettycash" required value="<?= $pcash['id']; ?>">
                            <input type="text" readonly id="inPcash" name="kode_pettycash" required class="form-control" value="<?= $pcash['kode_pcash']; ?>">
                            <?php
                        }
                    }
                }
                ?>


            </div>


            <label for="inAccount" class="col-sm-1 control-label">Usulan</label>

            <div class="col-sm-7">
                <div class="input-group  select2-bootstrap-prepend">
                    <span class="input-group-btn">
                        <button class="btn btn-default" type="button" data-select2-open="single-prepend-text">
                            <span class="glyphicon glyphicon-search"></span>
                        </button>
                    </span>
                    <select id="single-prepend-text" name="id_nota" required class="form-control select2-allow-clear select2no_ref">
                        <option></option>
                        <optgroup label="Usulan">
                            <?php
                            $dt_noref = $this->Siperan_Model->get_dt_noref2($kd_pcash, $kas);
                            if(count($dt_noref)){
                                foreach ($dt_noref as $acc) {
                                    echo "<option value=\"".$acc['id_nota']."\" title=\"".$acc['no_ref']."\" data-account=\"".$acc['account']."\" data-nilai=\"".$acc['nilai']."\"  data-keterangan=\"".$acc['keterangan']."\" >#".date('d-M-Y', strtotime($acc['tgl_usul'])). " # " . $acc['no_ref']. " # " .strip_tags(htmlspecialchars_decode($acc['keterangan'], ENT_QUOTES))."</option>";
                                }
                            }
                            ?>
                        </optgroup>
                    </select>
                    <div class="input-group-btn">
                        <?php
                        $atts = array(
                            'width'      => '800',
                            'height'     => '600',
                            'scrollbars' => 'yes',
                            'status'     => 'yes',
                            'resizable'  => 'yes',
                            'screenx'    => '0',
                            'screeny'    => '0'
                        );

                        $l_pop = "<button type=\"button\" class=\"btn btn-default\" aria-label=\"Help\"><span class=\"glyphicon glyphicon-list-alt\"></span></button>";

                        echo "<a data-toggle='modal' data-target='#modal_dt_ref' href='#' data-href='".site_url('peran/pop_usulan')."' >";
                        echo $l_pop;
                        echo "</a>";
                        //echo anchor_popup('peran/pop_usulan', $l_pop, $atts);
                        ?>

                    </div>
                </div>
            </div>
    </div>
    <hr style="border-color: #468847;"/>

    <div class="form-group has-success collapse" id="kokom">
        <span id="helpket" class="help-block col-sm-12">Keterangan Usulan</span>
        <span id="helpnilai" style="color: #468847 !important;" class="help-block  col-sm-12">Nilai</span>
    </div>

    <div class="alert alert-info" role="alert">
        <i class="icon-tasks"></i> Buat Nota | Pilih berdasarkan item Usulan atau tambahkan data secara manual.
    </div>



    <div class="form-group">
        <label for="inAccount" class="col-sm-1 control-label">Account</label>
        <div class="col-sm-5">
            <div class="input-group select2-bootstrap-prepend">
                <span class="input-group-btn">
                    <button class="btn btn-default" type="button" data-select2-open="single-prepend-text">
                        <span class="glyphicon glyphicon-search"></span>
                    </button>
                </span>
                <select id="single-prepend-text" name="account" required class="form-control select2-allow-clear select2">
                    <option></option>
                    <optgroup label="Master Account">
                        <?php
                        $dt_account = $this->Siperan_Model->get_dt_account();
                        if(count($dt_account)){
                            foreach ($dt_account as $acc) {
                                echo "<option value=\"".$acc['kd_account']."\">".$acc['account']."</option>";
                            }
                        }
                        ?>
                    </optgroup>
                </select>
            </div>
        </div>
        <label for="inNilai" class="col-sm-1 control-label">Nota</label>
        <div class="input-group col-sm-5">
            <input type="file" name="nota">
        </div>
        <span class="help-block col-sm-offset-7 col-sm-5">Nota dapat berupa gambar atau file pdf</span>
    </div>


    <div class="form-group">
        <label for="inKeterangan" class="col-sm-1 control-label">Keterangan</label>
        <div class="input-group col-sm-11">
            <textarea class="form-control" name="keterangan" id="defaultketnota" rows="3"></textarea>
        </div>
    </div>

    <div class="form-group">
        <span style="color: #468847 !important;" class="help-block col-sm-offset-1 col-sm-11">Nilai berikut adalah total nilai dari usulan yang diajukan. Ubah sesuai nilai NOTA</span>
        <label for="inNilai" class="col-sm-1 control-label">Nilai</label>

        <div class="input-group col-sm-11">
            <div class="input-group-addon">Rp </div>
            <input type="number" name="nilai" min="1" id="inNilai" class="form-control" required>
            <div class="input-group-addon">,00</div>
        </div>
        <span id="helpSatuan" class="help-block col-sm-offset-1 col-sm-11"></span>
    </div>

    <div class="form-group">
        <div class="col-sm-offset-1 col-sm-11">
            <button type="submit" name="update" value="Simpan" data-loading-text="Simpan Nota..." class="btn btn-primary" autocomplete="off">Simpan</button>
        </div>
    </div>
</form>


<div class="alert alert-danger" role="alert">
    <span class="pull-left" ><i class="icon-tasks"></i> Kunci Nota dan Petty Cash</span>
    <?php
    $act_lock = " <a href='".site_url('peran/lock_pcash/'.$kd_pcash.'/'.$pettycah)."' class='pull-right'><button class='btn btn-warning btn-sm '><i class='glyphicon glyphicon-lock'></i> Kunci</button></a>";
    echo $act_lock;
    ?>
    <div class="clearfix"></div>
</div>