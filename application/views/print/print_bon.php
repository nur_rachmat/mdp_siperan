<?php
$dt = "data_{$p}";
$vard = ${$dt};
?>
<style type="text/css">
    td{padding: 5px;}
    th{padding: 2px; background-color: #999999;}
</style>
<table width="100%">
    <tr>
        <td width="25%">
            <img src="<?=base_url('logo/logo'.$kas.'.png');?>" width="75%">
        </td>
        <td style="text-align: center;">

        </td>
    </tr>
    <tr>
        <td colspan="2" style="text-align: center;">
            <h3 style="text-decoration: underline;">LAPORAN BON, PENDAPATAN dan PENGELUARAN BIAYA KEGIATAN</h3>
        </td>
    </tr>

</table>
<table width="100%" border="1" cellspacing="4" cellspacing="4" style="border-collapse: collapse; padding: 5px;">
    <tr height="50">
        <th >Keterangan</th>
        <th width="15%">Jumlah (Rp)</th>
    </tr>

    <tr>
        <td >BON</td>
        <td>&nbsp;</td>
    </tr>

    <?php
    if(isset($dt_bon['id'])){
        echo "<tr>";
        echo "<td style='padding-left: 25px;'>".str_replace(PHP_EOL,"<br>",$dt_bon['keterangan'])."</td>";

        echo "<td align='right'>".number_format($dt_bon['nilai'], 0, ",", ".")."</td>";
        echo "</tr>";

    }
    ?>


    <tr>
        <td>TOTAL BON</td>
        <td><?php echo (isset($dt_bon['id'])) ? number_format($dt_bon['nilai'], 0, ",", ".") : "";?></td>
    </tr>


    <tr>
        <td >PENGELUARAN</td>
        <td>&nbsp;</td>
    </tr>

    <?php
    $total_nilai = 0;
    $sisa = 0;
    if(count($item_bon) > 0){
        $i = 1;

        foreach($item_bon as $row){
            echo "<tr>";
            echo "<td style='padding-left: 25px;'>".str_replace(PHP_EOL,"<br>",$row['keterangan'])."</td>";
            echo "<td align='right'>".number_format($row['nilai'], 0, ",", ".")."</td>";
            echo "</tr>";
            $total_nilai = $row['nilai'] + $total_nilai;
        }
        $sisa = $dt_bon['nilai']-$total_nilai;
    }
    ?>

    <tr>
        <td>TOTAL PENGELUARAN</td>
        <td><?php echo (isset($total_nilai)) ? number_format($total_nilai, 0, ",", ".") : 0;?></td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>

    <tr>
        <td>SISA DANA BON</td>
        <td><?php echo isset($sisa) ? number_format($sisa, 0, ",", ".")  : 0;?></td>
    </tr>


</table>
<table width="100%">
    <tr>
        <td></td>
        <td>Palembang, <?=date("d-M-Y");?></td>
    </tr>
    <tr>
        <td>Diketahui Oleh,</td>
        <td>Dibuat Oleh,</td>
    </tr>
    <tr>
        <td height="50"></td>
        <td></td>
    </tr>
    <tr>



        <td>
            <u>Johannes Petrus, S.Kom., M.T.I</u><br/>
            Ketua</td>

        <td>
            <u><?=$bak;?></u><br/>
            Ka. BAK</td>
    </tr>

</table>

