<?php
$dt = "data_{$p}";
$vard = ${$dt};
?>
<style type="text/css">
    td{padding: 5px;}
    th{padding: 10px; background-color: #999999;}
</style>
<table width="100%">
    <tr>
        <td>
            <img src="<?=base_url('logo/logo'.$kas.'.png');?>" width="75%">
        </td>
        <td style="text-align: right;">
            <h3>YAYASAN MULTI DATA PALEMBANG<br/>
            BUKTI KAS KELUAR</h3>
        </td>
    </tr>
    <tr>
        <td width="50%">
            <table  style="font-weight: bold;">
                <tr>
                    <td>Nomor</td>
                    <td>:</td>
                    <td><?=$pcash['kode_pcash'];?> / <?=$kas;?></td>
                </tr>
            </table>
        </td>

        <td width="50%">
            <table style="font-weight: bold;">
                <tr>
                    <td>Tanggal</td>
                    <td>:</td>
                    <td><?=date("d-M-Y", strtotime($pcash['tgl_pcash']));?></td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<table width="100%" border="1" cellspacing="4" cellspacing="4" style="border-collapse: collapse; padding: 5px;">
    <tr height="50">
        <th style="">No.</th>
        <th>Keterangan</th>
        <th width="15%">Jumlah (Rp)</th>
    </tr>
    <?php
    if(count($vard) > 0){
        $i = 1;
        $total_nilai = 0;
        foreach($vard as $row){
            echo "<tr>";

            echo "<td align='center'>".$i++."</td>";
            echo "<td >".htmlspecialchars_decode($row['keterangan'], ENT_QUOTES)."</td>";
            echo "<td align='right'>".number_format($row['nilai'], 0, ",", ".")."</td>";
            echo "</tr>";
            $total_nilai = $row['nilai'] + $total_nilai;
        }
    }
    ?>
    <tr style="font-weight: bold;">
        <td align="center" colspan="2">Total Dibayar</td>
        <td align="right"><?=number_format($total_nilai, 0, ",", ".");?></td>
    </tr>
</table>
<p><strong>Terbilang : #<?=Terbilang($total_nilai)." Rupiah";?></strong> #</p>
<table width="100%">
    <tr>
        <td>Diterima Oleh,</td>
        <td>Disiapkan Oleh,</td>
        <td>Diketahui Oleh,</td>
        <td>Disetujui Oleh,</td>
    </tr>
    <tr>
        <td height="50"></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td>(..................................)</td>
        <td>
            <u><?=$pk2;?></u><br/>
            Pembantu Ketua II</td>
        <td><u>(..................................)</u><br/>
        Bendahara</td>
        <td>
            <u>Johannes Petrus, S.Kom., M.T.I</u><br/>
        Ketua</td>
    </tr>

</table>
