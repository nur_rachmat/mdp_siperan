<?php
/**
 * Created by PhpStorm.
 * User: Njr
 * Date: 5/14/2016
 * Time: 8:51 AM
 */
function akses_usulan(){
    return array('Administrasi', 'SuperAdmin', 'Ketua');
}

function akses_nota(){
    return array('Administrasi', 'SuperAdmin');
}

function akses_pcash(){
    return akses_nota();
}

function akses_transfer(){
    return array('Bendahara', 'SuperAdmin');
}

function akses_persetujuan(){
    return array('Yayasan', 'SuperAdmin', 'Ketua');
}

function cek_akses_nota(){
    $ci =& get_instance();
    return in_array($ci->session->userdata('admin_level'), akses_nota());
}
function cek_akses_persetujuan(){
    $ci =& get_instance();
    return in_array($ci->session->userdata('admin_level'), akses_persetujuan());
}


function Terbilang($satuan){
    $huruf = array ("", "Satu", "Dua", "Tiga", "Empat", "Lima", "Enam",
        "Tujuh", "Delapan", "Sembilan", "Sepuluh","Sebelas");
    if ($satuan < 12)
        if($satuan != "")
            return " ".$huruf[$satuan];
        else
            return " ";
    elseif ($satuan < 20)
        return Terbilang($satuan - 10)." Belas";
    elseif ($satuan < 100)
        return Terbilang($satuan / 10)." Puluh".Terbilang($satuan % 10);
    elseif ($satuan < 200)
        return "Seratus".Terbilang($satuan - 100);
    elseif ($satuan < 1000)
        return Terbilang($satuan / 100)." Ratus".Terbilang($satuan % 100);
    elseif ($satuan < 2000)
        return "Seribu".Terbilang($satuan - 1000);
    elseif ($satuan < 1000000)
        return Terbilang($satuan / 1000)." Ribu".Terbilang($satuan % 1000);
    elseif ($satuan < 1000000000)
        return Terbilang($satuan / 1000000)." Juta".Terbilang($satuan % 1000000);
    elseif ($satuan < 1000000000000)
        return Terbilang($satuan / 1000000000)." Miliar".Terbilang($satuan % 1000000);
    elseif ($satuan < 1000000000000000)
        return Terbilang($satuan / 1000000000000)." Triliun".Terbilang($satuan % 1000000);
    elseif ($satuan >= 1000000000000000)
        return "Angka terlalu Besar";
}
