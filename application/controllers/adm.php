<?php if (!defined('BASEPATH')) exit ('No direct script access allowed');

class Adm extends CI_Controller
{
	public $folder_upload = "assets/uploads";
    	//public $folder_upload_foto_guru = "assets/uploads/foto/guru";

    public function __construct()
    {
        parent::__construct();
		
        $this->tahun = date('Y');
        
		if ($this->session->userdata('admin_valid') == FALSE && $this->session->userdata('admin_user') == "") {
			redirect("welcome");
		}
        
        $this->load->library('grocery_CRUD');
    }

    public function index($act=null)
    {
        
		if($this->session->userdata('admin_level') == "UPT"){
			//redirect("adm/getPC","refresh");
			$data['act_pro']	= $act;
			$this->load->view('header');
			$this->load->view('sop',$data);
			$this->load->view('footer');
		}else{
			//redirect("adm/getAgenda/calender","refresh");
			$data['act_pro']	= $act;
			$this->load->view('header');
			$this->load->view('sop',$data);
			$this->load->view('footer');
			
		}
    }
	
	public function getAula()
    {
        $this->load->view('header');
		
		echo '<br /><br /><br />
		<ul class="nav nav-tabs">
		  <li class="active"><a href="#">Kalender</a></li>';
		  //($this->session->userdata('admin_level')=='Admin')or
		  if(($this->session->userdata('admin_level')=='SuperAdmin')or($this->session->userdata('admin_level')=='KaUPT')){
			echo '<li><a href="'.base_url().'index.php/adm/getAgenda/belum/add">Tambah Agenda</a></li>';
		  }
		  echo '<li><a href="'.base_url().'index.php/adm/index/aula">Prosedur Peminjaman Aula</a></li>';
		echo '</ul>
		';
		$this->load->view('scheduleaula');
		$this->load->view('footer');
    }
	
	public function getLaporan($pil=NULL)
	{
		$this->load->view('header');
		
		$actb = "";
		$acts = "";
		if($pil=="prestasi"){
			$st = "PRESTASI";
			$actb = "class='active'";
		}else if($pil=="agenda"){
			$st= "AGENDA";	
			$acts = "class='active'";
		}else{
			redirect("adm","refresh");
		}
		
		echo '<br /><br /><br />
		<ul class="nav nav-tabs">
		  <li '.$acts.'><a href="'.base_url().'index.php/adm/getLaporan/agenda">Agenda</a></li>
		  <li '.$actb.'><a href="'.base_url().'index.php/adm/getLaporan/prestasi">Prestasi</a></li>
		</ul>
		';
		
		if($pil=='prestasi'){
			$this->load->view('laporanprestasi');
		}else if($pil=='agenda'){
			$this->load->view('laporanagenda');
		}
		$this->load->view('footer');
	}	
	
	public function password()
    {
        $this->load->view('header');
		$this->load->view('password');
		$this->load->view('footer');
    }
	
	
	
	public function ubahpassword() {
		$id_user		= $this->session->userdata('admin_user');
		//var post
		$p1				= sha1($this->input->post('p1'));
		$p2				= sha1($this->input->post('p2'));
		$p3				= sha1($this->input->post('p3'));
		
		$cek_password_lama	= $this->db->query("SELECT PASSWORD FROM t_pengguna WHERE EMAIL = '$id_user'")->row();
		
		$cek_password_lama	= $this->db->query("SELECT PASSWORD FROM t_pengguna WHERE EMAIL = '$id_user'")->row();
			//echo 
			
			if ($cek_password_lama->PASSWORD != $p1) {
				$this->session->set_flashdata('k_passwod', '<div id="alert" class="alert alert-error">Password Lama tidak sama</div>');
				redirect('adm/password');
			} else if ($p2 != $p3) {
				$this->session->set_flashdata('k_passwod', '<div id="alert" class="alert alert-error">Password Baru 1 dan 2 tidak cocok</div>');
				redirect('adm/password');
			} else {
				$this->db->query("UPDATE t_pengguna SET password = '$p3' WHERE EMAIL = '$id_user'");
				$this->session->set_flashdata('k_passwod', '<div id="alert" class="alert alert-success">Password berhasil diperbaharui</div>');
				redirect('adm/password');
			}
		redirect('adm/password');
	}
  	
	public function getPengguna($kampus=NULL)
    {
	$this->load->view('header');
        try
        {
            //create crud object
            $crud = new grocery_CRUD();

            //theme
            $crud->set_theme('datatables');

            //fitur set
            $crud->unset_print()
                ->set_language("indonesian")
				->unset_export();

            //set table
            $crud->set_table('t_pengguna');
            
            //set primary key
            $crud->set_primary_key("ID");

            //set columns
            $crud->columns("EMAIL","LEVEL");
            
            //display
            $crud->display_as('EMAIL', 'NAMA PENGGUNA');
			$crud->display_as('PASSWORD', 'KATA KUNCI');

            //field required
            $crud->required_fields("EMAIL,PASSWORD,LEVEL");
            
			//callback
            $crud->callback_before_insert(array($this, 'encrypt_password_before_insert_callback'));
            $crud->callback_edit_field('PASSWORD', array($this, 'set_password_input_to_empty'));
            $crud->callback_add_field('PASSWORD', array($this, 'set_password_input_to_empty'));
            $crud->callback_before_update(array($this, 'encrypt_password_before_update_callback'));

            //subject table
            $crud->set_subject('PENGGUNA');

            //render
            $output = $crud->render();
            $this->load->view('crud_profil', $output);
        }
        catch (exception $e)
        {
            show_error($e->getMessage() . ' --- ' . $e->getTraceAsString());
        }
	$this->load->view('footer');
    }
	
	
    function encrypt_password_before_insert_callback($post_array)
    {
        $post_array['PASSWORD'] = sha1($post_array['PASSWORD']);
        return $post_array;
    }

    function encrypt_password_before_update_callback($post_array, $primary_key)
    {
        //Encrypt password only if is not empty. Else don't change the password to an empty field
        if (!empty($post_array['PASSWORD']))
        {
            $post_array['PASSWORD'] = sha1($post_array['PASSWORD']);
        } else
        {
            unset($post_array['PASSWORD']);
        }

        return $post_array;
    }

    function set_password_input_to_empty()
    {
        return "<input type='password' name='PASSWORD'  value='' />";
    }

public function getPrestasi($kampus=NULL)
    {
		$this->load->view('header');
		if($kampus=="stmik"){
			$nm_tabel = "t_prestasi_stmik";
		}else if($kampus=="stie"){
			$nm_tabel = "t_prestasi_stie";	
		}else{
			redirect("adm","refresh");
		}
		
        try
        {
            //create crud object
            $crud = new grocery_CRUD();

            //theme
            $crud->set_theme('datatables');

            //fitur set
            $crud->set_language("indonesian");
	    
			if (($this->session->userdata('admin_level')=='User')){
            	$crud->unset_delete();
				$crud->unset_edit();
            }
			if (($this->session->userdata('admin_level')=='Admin')){
				$crud->unset_delete();
				$crud->unset_edit();
            }
            if (($this->session->userdata('admin_level')=='Ketua')){
            	$crud->unset_delete();
				$crud->unset_edit();
            }
            
            //set table
            $crud->set_table("$nm_tabel");
            
            //set primary key
            $crud->set_primary_key("ID");

            //set columns
            $crud->columns("NM_PRESTASI","TEMPAT","TINGKAT","KATEGORI_PRESTASI","KATEGORI_PESERTA","NM_PESERTA","PRODI","TGL_LOMBA");
            
            //unset fields
            $crud->unset_fields("TGL_ENTRI");
			
			$crud->change_field_type('DIINPUT_OLEH','invisible');
			$crud->change_field_type('DIUPDATE_OLEH','invisible');
            
              //display
            $crud->display_as('NM_PRESTASI', 'Prestasi')
            			->display_as('TEMPAT', 'Tempat')
            			->display_as('TINGKAT', 'Tingkat')
            			->display_as('KATEGORI_PRESTASI', 'Bidang')
            			->display_as('KATEGORI_PESERTA', 'Kategori Peserta')
            			->display_as('NM_PESERTA', 'Peserta')
            			->display_as('PRODI', 'Prodi')
            			->display_as('TGL_LOMBA', 'Tanggal');


            //field required
            $crud->required_fields("NM_PRESTASI","TEMPAT","TINGKAT","KATEGORI_PRESTASI","KATEGORI_PESERTA","NM_PESERTA","PRODI","TGL_LOMBA");
            
            //upload
            $crud->set_field_upload('FILE1', $this->folder_upload."/$kampus/prestasi");
            $crud->set_field_upload('FILE2', $this->folder_upload."/$kampus/prestasi");
			$crud->set_field_upload('FILE3', $this->folder_upload."/$kampus/prestasi");
			
			$crud->callback_before_insert(array($this,'diinput_oleh_insert_callback'));
			$crud->callback_before_update(array($this,'diupdate_oleh_insert_callback'));
            //subject table
            $crud->set_subject("PRESTASI ".strtoupper($kampus));

            //render
            $output = $crud->render();
            $this->load->view('crud_profil', $output);
        }
        catch (exception $e)
        {
            show_error($e->getMessage() . ' --- ' . $e->getTraceAsString());
        }
		
	$this->load->view('footer');
    }
	
	
	
	public function getAgenda($status)
    {
		$this->load->view('header');
		$acta = "";
		$actb = "";
		$acts = "";
		$actba = "";
		if($status=="belum"){
			$st = "BELUM";
			$actb = "class='active'";
		}else if($status=="selesai"){
			$st= "SELESAI";	
			$acts = "class='active'";
		}else if($status=="batal"){
			$st= "BATAL";	
			$actba = "class='active'";
		}else if($status=="calender"){
			$st= "KALENDER";	
			$acta = "class='active'";
		}else{
			redirect("adm","refresh");
		}
		
		echo '<br /><br /><br />
		<ul class="nav nav-tabs">
		  <li '.$acta.'><a href="'.base_url().'index.php/adm/getAgenda/calender">Kalender</a></li>
		  <li '.$actb.'><a href="'.base_url().'index.php/adm/getAgenda/belum">Belum</a></li>
		  <li '.$acts.'><a href="'.base_url().'index.php/adm/getAgenda/selesai">Selesai</a></li>
		  <li '.$actba.'><a href="'.base_url().'index.php/adm/getAgenda/batal">Batal</a></li>
		</ul>
		';
		
		if($status=="calender"){
			echo '<iframe src="http://i-mpian.mdp.ac.id/schedule/11e9ba26c91a2db7452a5ef9ff7051e19b6b377a1.php" style="position:fixed; top:100px; left:0px; bottom:0px; right:0px; width:100%; height:86%; border:none; margin:0; padding:0; overflow:hidden; z-index:999999;">
			Browser Anda tidak mendukung iframe
			</iframe>';
		}else{
		
				try
				{
					//create crud object
					$crud = new grocery_CRUD();

					//theme
					$crud->set_theme('datatables');

					//fitur set
					$crud->set_language("indonesian");
				
					if (($this->session->userdata('admin_level')=='User')){
						$crud->unset_delete();
						$crud->unset_edit();
						$crud->unset_add();
					}
					if (($this->session->userdata('admin_level')=='Admin')){
						$crud->unset_delete();
						//$crud->unset_edit();
					}
					if (($this->session->userdata('admin_level')=='Ketua')){
						$crud->unset_delete();
						$crud->unset_edit();
					}
					
					//set table
					$crud->where('STATUS',"$st");
					$crud->set_table("t_agenda");
					
					
					//set primary key
					$crud->set_primary_key("ID_AGENDA");

					//set columns
					$crud->columns("DARI","SAMPAI","INSTANSI","NM_AGENDA","RESCHEDULE","NM_PELAKSANA","TEMPAT_PELAKSANAAN","STATUS");
					
					//invisible fields
					$crud->change_field_type('TANGGAL_INPUT','invisible');
					$crud->change_field_type('DIINPUT_OLEH','invisible');
					$crud->change_field_type('DIUPDATE_OLEH','invisible');
					
					//unset fields
					//$crud->unset_fields("PERALATAN");
					
					//display
					$crud->display_as('INSTANSI', 'Nama Sekolah/Instansi')
								->display_as('NM_AGENDA', 'Nama Agenda/Pelatihan')
								->display_as('NM_PELAKSANA', 'Pelaksana/Prodi')
								->display_as('PERALATAN', 'Peralatan yang Diperlukan')
								->display_as('PENANGGUNG_JAWAB', 'Penanggung Jawab')
								->display_as('CP', 'No. HP PJ')
								->display_as('DARI', 'Dari')
								->display_as('RESCHEDULE', 'Reschedule')
								->display_as('SAMPAI', 'Sampai')
								->display_as('TEMPAT_PELAKSANAAN', 'Tempat Kegiatan <br />(Jika dilaksanakan di aula, mohon diisi <b>Aula MDP</b>)')
								->display_as('KATEGORI', 'Kategori')
								->display_as('FILEPROPOSAL', 'Laporan Pelaksanaan')
								->display_as('FILEABSEN', 'File Surat Tugas')
								->display_as('FILEFOTO1', 'Foto 1')
								->display_as('FILEFOTO2', 'Foto 2')
								->display_as('FILEFOTO3', 'Foto 3')
								->display_as('KETERANGAN', 'Keterangan')
								->display_as('STATUS', 'Status Agenda')
								->display_as('DIINPUT_OLEH', 'Diinput Oleh')
								->display_as('DIUPDATE_OLEH', 'Diupdate Oleh')
								->display_as('TANGGAL_INPUT', 'Tanggal Input')
								;
								
					//field required
					$crud->required_fields("INSTANSI","NM_AGENDA","PERALATAN","DARI","SAMPAI","STATUS","TEMPAT_PELAKSANAAN","KATEGORI");
					
					//$crud->callback_before_insert(array($this,'tanggal_input_insert_callback'));
					$crud->callback_before_insert(array($this,'diinput_oleh_insert_callback'));
					$crud->callback_before_update(array($this,'diupdate_oleh_insert_callback'));
					
					//upload
					$crud->set_field_upload('FILEPROPOSAL', $this->folder_upload."/agenda/proposal");
					$crud->set_field_upload('FILEABSEN', $this->folder_upload."/agenda/absen");
					$crud->set_field_upload('FILEFOTO1', $this->folder_upload."/agenda/foto");
					$crud->set_field_upload('FILEFOTO2', $this->folder_upload."/agenda/foto");
					$crud->set_field_upload('FILEFOTO3', $this->folder_upload."/agenda/foto");
					

					//subject table
					$crud->set_subject("AGENDA KEGIATAN :: Tanda (*) wajib diisi. ");
					
					$tempat= strtolower($this->input->post('TEMPAT_PELAKSANAAN'));
					if ($tempat=='aula mdp'){
						$crud->callback_after_insert(array($this,'sendEmailAgendaAula'));
						$crud->callback_after_update(array($this,'sendEmailAgendaAulaUpdate'));
					}
					elseif ($tempat=='aula'){
						$crud->callback_after_insert(array($this,'sendEmailAgendaAula'));
						$crud->callback_after_update(array($this,'sendEmailAgendaAulaUpdate'));
					}
					else{
						$crud->callback_after_insert(array($this,'sendEmailAgenda'));
						$crud->callback_after_insert(array($this,'sendEmailAgendaUpdate'));
					}
					
					
					//render
					$output = $crud->render();
					$this->load->view('crud_profil', $output);
				}
				catch (exception $e)
				{
					show_error($e->getMessage() . ' --- ' . $e->getTraceAsString());
				}
		}
	$this->load->view('footer');
    }
	
	// AGENDA AULA
	function sendEmailAgendaAula($post_array,$primary_key)
	{
		$this->load->library('email');
		$config['protocol'] = 'mail';
		$config['mailtype'] = 'html';
		$config['charset'] = 'utf-8';
		$config['wordwrap'] = TRUE;
		$this->email->initialize($config);
			
		$Nama=$this->session->userdata('admin_user');
		$DARI=$this->input->post('DARI');
		$SAMPAI= $this->input->post('SAMPAI');
		$INSTANSI= $this->input->post('INSTANSI');
		$NM_AGENDA= $this->input->post('NM_AGENDA');
		$RESCHEDULE= $this->input->post('RESCHEDULE');
		$NM_PELAKSANA= $this->input->post('NM_PELAKSANA');
		$PERALATAN= $this->input->post('PERALATAN');
		$STATUS= $this->input->post('STATUS');
		$PIC= $this->input->post('PENANGGUNG_JAWAB');
		
		$isi= "<html>
		<body>
			<table>
				<tr>
					<td>Agenda</td>
					<td>:</td>
					<td>$NM_AGENDA</td>
				</tr>
				
				<tr>
					<td>Dari</td>
					<td>:</td>
					<td>$DARI</td>
				</tr>
				<tr>
					<td>Sampai</td>
					<td>:</td>
					<td>$SAMPAI</td>
				</tr>
				<tr>
					<td>Instansi</td>
					<td>:</td>
					<td>$INSTANSI</td>
				</tr>
				<tr>
					<td>Reschedule</td>
					<td>:</td>
					<td>$RESCHEDULE</td>
				</tr>
				<tr>
					<td>Nama Pelaksana</td>
					<td>:</td>
					<td>$NM_PELAKSANA</td>
				</tr>
				<tr>
					<td>PIC</td>
					<td>:</td>
					<td>$PIC</td>
				</tr>
				<tr>
					<td>Peralatan</td>
					<td>:</td>
					<td>$PERALATAN</td>
				</tr>
				<tr>
					<td>Status</td>
					<td>:</td>
					<td>$STATUS</td>
				</tr>
			</table>
		</body>
		</html>";
		
		$this->email->from($Nama);
		//$this->email->to('haiyunizar@staff.mdp.ac.id');		
		//$this->email->cc($Nama);		
		$email=$this->db->query("select * from t_pengguna WHERE NOTIFIKASI = 'Y'");
		$to="";
		foreach($email->result_array() as $value){
			$to.=$value['EMAIL']." ,";
		}
		$this->email->to($to);	
		
		$this->email->subject('Agenda Aula MDP '.$NM_AGENDA);
		$this->email->message($isi);
 
		$this->email->send();
		return true;
	}
	
	function sendEmailAgendaAulaUpdate($post_array,$primary_key)
	{
		$this->load->library('email');
		$config['protocol'] = 'mail';
		$config['mailtype'] = 'html';
		$config['charset'] = 'utf-8';
		$config['wordwrap'] = TRUE;
		$this->email->initialize($config);
		
		$Nama=$this->session->userdata('admin_user');
		$DARI=$this->input->post('DARI');
		$SAMPAI= $this->input->post('SAMPAI');
		$INSTANSI= $this->input->post('INSTANSI');
		$NM_AGENDA= $this->input->post('NM_AGENDA');
		$RESCHEDULE= $this->input->post('RESCHEDULE');
		$NM_PELAKSANA= $this->input->post('NM_PELAKSANA');
		$PERALATAN= $this->input->post('PERALATAN');
		$STATUS= $this->input->post('STATUS');
		$PIC= $this->input->post('PENANGGUNG_JAWAB');
		
		$isi= "<html>
		<body>
			<table>
				<tr>
					<td>Agenda</td>
					<td>:</td>
					<td>$NM_AGENDA</td>
				</tr>
				<tr>
					<td>Dari</td>
					<td>:</td>
					<td>$DARI</td>
				</tr>
				<tr>
					<td>Sampai</td>
					<td>:</td>
					<td>$SAMPAI</td>
				</tr>
				<tr>
					<td>Instansi</td>
					<td>:</td>
					<td>$INSTANSI</td>
				</tr>
				<tr>
					<td>Reschedule</td>
					<td>:</td>
					<td>$RESCHEDULE</td>
				</tr>
				<tr>
					<td>Nama Pelaksana</td>
					<td>:</td>
					<td>$NM_PELAKSANA</td>
				</tr>
				<tr>
					<td>PIC</td>
					<td>:</td>
					<td>$PIC</td>
				</tr>
				<tr>
					<td>Peralatan</td>
					<td>:</td>
					<td>$PERALATAN</td>
				</tr>
				<tr>
					<td>Status</td>
					<td>:</td>
					<td>$STATUS</td>
				</tr>
				<tr>
					<td>Diinput oleh</td>
					<td>:</td>
					<td>$Nama</td>
				</tr>
			</table>
		</body>
		</html>";
		
		$this->email->from($Nama);
		//$this->email->to('haiyunizar@staff.mdp.ac.id');		
		//$this->email->cc($Nama);		
		$email=$this->db->query("select * from t_pengguna WHERE NOTIFIKASI = 'Y'");
		$to="";
		foreach($email->result_array() as $value){
			$to.=$value['EMAIL']." ,";
		}
		$this->email->to($to);	
		
		$this->email->subject('Perubahan Agenda Aula MDP '.$NM_AGENDA);
		$this->email->message($isi);
 
		$this->email->send();
		return true;
	}
	
	function sendEmailAgenda($post_array,$primary_key)
	{
		$this->load->library('email');
		$config['protocol'] = 'mail';
		$config['mailtype'] = 'html';
		$config['charset'] = 'utf-8';
		$config['wordwrap'] = TRUE;
		$this->email->initialize($config);
			
		$Nama=$this->session->userdata('admin_user');
		$DARI=$this->input->post('DARI');
		$SAMPAI= $this->input->post('SAMPAI');
		$INSTANSI= $this->input->post('INSTANSI');
		$NM_AGENDA= $this->input->post('NM_AGENDA');
		$RESCHEDULE= $this->input->post('RESCHEDULE');
		$NM_PELAKSANA= $this->input->post('NM_PELAKSANA');
		$PERALATAN= $this->input->post('PERALATAN');
		$STATUS= $this->input->post('STATUS');
		$PIC= $this->input->post('PENANGGUNG_JAWAB');
		
		$isi= "<html>
		<body>
			<table>
				<tr>
					<td>Agenda</td>
					<td>:</td>
					<td>$NM_AGENDA</td>
				</tr>
				<tr>
					<td>Dari</td>
					<td>:</td>
					<td>$DARI</td>
				</tr>
				<tr>
					<td>Sampai</td>
					<td>:</td>
					<td>$SAMPAI</td>
				</tr>
				<tr>
					<td>Instansi</td>
					<td>:</td>
					<td>$INSTANSI</td>
				</tr>
				<tr>
					<td>Reschedule</td>
					<td>:</td>
					<td>$RESCHEDULE</td>
				</tr>
				<tr>
					<td>Nama Pelaksana</td>
					<td>:</td>
					<td>$NM_PELAKSANA</td>
				</tr>
				<tr>
					<td>PIC</td>
					<td>:</td>
					<td>$PIC</td>
				</tr>
				<tr>
					<td>Peralatan</td>
					<td>:</td>
					<td>$PERALATAN</td>
				</tr>
				<tr>
					<td>Status</td>
					<td>:</td>
					<td>$STATUS</td>
				</tr>
				<tr>
					<td>Diinput oleh</td>
					<td>:</td>
					<td>$Nama</td>
				</tr>
			</table>
		</body>
		</html>";
		
		$this->email->from($Nama);
		//diubah
		//$this->email->cc('indrawan@mdp.ac.id');	
		$email=$this->db->query("select * from t_pengguna WHERE NOTIFIKASI = 'Y'");
		$to="";
		foreach($email->result_array() as $value){
			$to.=$value['EMAIL']." ,";
		}
		$this->email->to($to);	
		$this->email->subject('Agenda '.$NM_AGENDA);
		$this->email->message($isi);
 
		$this->email->send();
		return true;
	}
	
	function sendEmailAgendaUpdate($post_array,$primary_key)
	{
		$this->load->library('email');
		$config['protocol'] = 'mail';
		$config['mailtype'] = 'html';
		$config['charset'] = 'utf-8';
		$config['wordwrap'] = TRUE;
		$this->email->initialize($config);
			
		$Nama=$this->session->userdata('admin_user');
		$DARI=$this->input->post('DARI');
		$SAMPAI= $this->input->post('SAMPAI');
		$INSTANSI= $this->input->post('INSTANSI');
		$NM_AGENDA= $this->input->post('NM_AGENDA');
		$RESCHEDULE= $this->input->post('RESCHEDULE');
		$NM_PELAKSANA= $this->input->post('NM_PELAKSANA');
		$PERALATAN= $this->input->post('PERALATAN');
		$STATUS= $this->input->post('STATUS');
		
		$isi= "<html>
		<body>
			<table>
				<tr>
					<td>Agenda</td>
					<td>:</td>
					<td>$NM_AGENDA</td>
				</tr>
				<tr>
					<td>Dari</td>
					<td>:</td>
					<td>$DARI</td>
				</tr>
				<tr>
					<td>Sampai</td>
					<td>:</td>
					<td>$SAMPAI</td>
				</tr>
				<tr>
					<td>Instansi</td>
					<td>:</td>
					<td>$INSTANSI</td>
				</tr>
				<tr>
					<td>Reschedule</td>
					<td>:</td>
					<td>$RESCHEDULE</td>
				</tr>
				<tr>
					<td>Nama Pelaksana</td>
					<td>:</td>
					<td>$NM_PELAKSANA</td>
				</tr>
				<tr>
					<td>Status</td>
					<td>:</td>
					<td>$PERALATAN</td>
				</tr>
				<tr>
					<td>Status</td>
					<td>:</td>
					<td>$STATUS</td>
				</tr>
				<tr>
					<td>Diinput oleh</td>
					<td>:</td>
					<td>$Nama</td>
				</tr>
			</table>
		</body>
		</html>";
		
		$this->email->from($Nama);
		//diubah
		//$this->email->cc('indrawan@mdp.ac.id');	
		$email=$this->db->query("select * from t_pengguna WHERE NOTIFIKASI = 'Y'");
		$to="";
		foreach($email->result_array() as $value){
			$to.=$value['EMAIL']." ,";
		}
		$this->email->to($to);	
		$this->email->subject('Perubahan Agenda '.$NM_AGENDA);
		$this->email->message($isi);
 
		$this->email->send();
		return true;
	}
	
     
	public function getMobil($status)
    {
		$this->load->view('header');
		$acta = "";
		$actb = "";
		if($status=="list"){
			$st = "DAFTAR PEMINJAMAN MOBIL";
			$actb = "class='active'";
		}else if($status=="calender"){
			$st= "KALENDER";	
			$acta = "class='active'";
		}
		
		echo '<br /><br /><br />
		<ul class="nav nav-tabs">
		  <li '.$acta.'><a href="'.base_url().'index.php/adm/getMobil/calender">Kalender</a></li>
		  <li '.$actb.'><a href="'.base_url().'index.php/adm/getMobil/list">Daftar Peminjaman Mobil</a></li>
		  <li><a href="http://i-mpian.mdp.ac.id/index.php/adm/index/mobil">Prosedur Penggunaan</a></li>
		</ul>
		';
		
		if($status=="calender"){
			echo '<iframe src="http://i-mpian.mdp.ac.id/schedule/11e9ba26c91a2db7452a5ef9ff7051e19b6b377a2.php" style="position:fixed; top:100px; left:0px; bottom:0px; right:0px; width:100%; height:86%; border:none; margin:0; padding:0; overflow:hidden; z-index:999999;">
			Browser Anda tidak mendukung iframe
			</iframe>';
		}else{
				try
				{
					//create crud object
					$crud = new grocery_CRUD();

					//theme
					$crud->set_theme('datatables');

					//fitur set
					$crud->set_language("indonesian");
				
					if (($this->session->userdata('admin_level')<>'SuperAdmin')){
						$crud->unset_delete();
					}
					if (($this->session->userdata('admin_level')=='Ketua')){
						$crud->unset_edit();
					}
					
					//set table
					$crud->set_table("t_mobil");
					
					//set primary key
					$crud->set_primary_key("ID_MOBIL");

					//set columns
					$crud->columns("NAMA_PENGGUNA","PLAT_MOBIL","DARI","SAMPAI","KEPERLUAN","KEMBALI","KETERANGAN");
					
					//invisible fields
					$crud->change_field_type('TANGGAL_INPUT','invisible');
					$crud->change_field_type('DIINPUT_OLEH','invisible');
					$crud->change_field_type('TANGGAL','invisible');
					
					//display
					$crud->display_as('NAMA_PENGGUNA', 'Nama Peminjam')
								->display_as('PLAT_MOBIL', 'Plat Mobil')
								->display_as('DARI', 'Dari')
								->display_as('SAMPAI', 'Sampai')
								->display_as('KEPERLUAN', 'Untuk Keperluan')
								->display_as('KEMBALI', 'Pengembalian')
								->display_as('KETERANGAN', 'Pengisian BBM / Lainnya')
								;

					//field required
					$crud->required_fields("NAMA_PENGGUNA","PLAT_MOBIL","DARI","SAMPAI","KEPERLUAN");
					
					//$crud->callback_before_insert(array($this,'tanggal_input_insert_callback'));
					$crud->callback_before_insert(array($this,'diinput_oleh_insert_callback'));
					$crud->callback_before_update(array($this,'diupdate_oleh_insert_callback'));
					
					//subject table
					$crud->set_subject("PEMINJAMAN MOBIL");
					
					$crud->callback_after_insert(array($this,'sendEmailMobil'));

					//render
					$output = $crud->render();
					$this->load->view('crud_profil', $output);
				}
				catch (exception $e)
				{
					show_error($e->getMessage() . ' --- ' . $e->getTraceAsString());
				}
		}
		$this->load->view('footer');
    }
	
	
	function sendEmailMobil($post_array,$primary_key)
	{
		$this->load->library('email');
		$config['protocol'] = 'mail';
		$config['mailtype'] = 'html';
		$config['charset'] = 'utf-8';
		$config['wordwrap'] = TRUE;
		$this->email->initialize($config);
		
		$sender= $this->session->userdata('admin_user');
		$Nama=$this->input->post('NAMA_PENGGUNA');
		$Plat= $this->input->post('PLAT_MOBIL');
		$Dari= $this->input->post('DARI');
		$Sampai= $this->input->post('SAMPAI');
		$Untuk= $this->input->post('KEPERLUAN');
		$Pengembalian= $this->input->post('KEMBALI');
		$Keterangan= $this->input->post('KETERANGAN');
		
		$isi= "<html>
		<body>
			<table>
				<tr>
					<td>Nama Peminjam</td>
					<td>:</td>
					<td>$Nama</td>
				</tr>
				<tr>
					<td>Plat Mobil</td>
					<td>:</td>
					<td>$Plat</td>
				</tr>
				<tr>
					<td>Dari</td>
					<td>:</td>
					<td>$Dari</td>
				</tr>
				<tr>
					<td>Sampai</td>
					<td>:</td>
					<td>$Sampai</td>
				</tr>
				<tr>
					<td>Untuk Keperluan</td>
					<td>:</td>
					<td>$Untuk</td>
				</tr>
				<tr>
					<td>Pengembalian</td>
					<td>:</td>
					<td>$Pengembalian</td>
				</tr>
				<tr>
					<td>Keterangan</td>
					<td>:</td>
					<td>$Keterangan</td>
				</tr>
			</table>
		</body>
		</html>";
		
		$this->email->from($sender);
		
		$to="";
		$email=$this->db->query("select * from t_pengguna WHERE NOTIFIKASI_MOBIL = 'Y'");
		foreach($email->result_array() as $value){
			$to.=$value['EMAIL']." ,";
		}
		$this->email->to($to);	
		//$this->email->to('haiyunizar@staff.mdp.ac.id');	
		//$this->email->cc($sender);	
		
		$this->email->subject('Peminjaman Mobil '.$Plat);
		$this->email->message($isi);
 
		$this->email->send();
		return true;
	}
	
	
	public function getDesign()
    {
		$this->load->view('header');
				
		
		echo '<br /><br /><br />
		<ul class="nav nav-tabs">
		  <li class="active"><a href="#">Buat Sertifikat</a></li>
		  <li><a href="'.base_url().'index.php/adm/getDataSertifikat">Data Sertifikat</a></li>
		  <li><a href="'.base_url().'index.php/adm/getDataDesign">Design</a></li>
		 
		</ul>
		';
		
		$this->load->view('FormAddDesign');
		$this->load->view('footer');
    }
	
	public function getSimpanSertifikat(){
		$STMIK	 		 = $this->input->post('STMIK',TRUE);
		$AMIK	 		 = $this->input->post('AMIK',TRUE);
		$STIE			 = $this->input->post('STIE',TRUE);
		$SERTIFIKAT		 = $this->input->post('sertifikat',TRUE);
		$STRF			 = $this->input->post('lainnya',TRUE);
		$ISI			 = $this->input->post('isi',TRUE);
		$KETUA	  		 = $this->input->post('Ketua',TRUE);
		$PENYELENGGARA	 = $this->input->post('Penyelenggara',TRUE);
		$OTHER1		  	 = $this->input->post('other1',TRUE);
		$TANDA		  	 = $this->input->post('tanda',TRUE);
		$TANGGAL   	 	 = $this->input->post('tanggal',TRUE);
		$DESIGN 		 = $this->input->post('Design',TRUE);
		$CETAK			 = $this->input->post('Cetak',TRUE);
		$OLEH			 = $this->session->userdata('admin_user');
		
		If(empty($STMIK) AND empty($STIE) AND empty($AMIK)){
			$this->session->set_flashdata('k_passwod', '<div id="alert" class="alert alert-error">Institusi Harap di Pilih</div>');
			redirect('adm/getDesign');
		}
		elseif(empty($ISI)){
			$this->session->set_flashdata('k_passwod', '<div id="alert" class="alert alert-error">Isi Kepentingan Sertifikat belum diisi</div>');
			redirect('adm/getDesign');
		}
		elseif(empty($KETUA) AND empty($PENYELENGGARA) AND empty($OTHER1)){
			$this->session->set_flashdata('k_passwod', '<div id="alert" class="alert alert-error">Di Tanda Tangani Harap di Pilih</div>');
			redirect('adm/getDesign');
		}
		
		elseif(empty($TANGGAL)){
			$this->session->set_flashdata('k_passwod', '<div id="alert" class="alert alert-error">TanggalPenerbitan Harap diiisi</div>');
			redirect('adm/getDesign');
		}
		
		elseif(empty($DESIGN) AND empty($CETAK)){
			$this->session->set_flashdata('k_passwod', '<div id="alert" class="alert alert-error">Jenis Permintaan Harap di Pilih</div>');
			redirect('adm/getDesign');
		}
		else{
			
			If (empty($STMIK)){$STMIK ="-";}
		
			If (empty($AMIK)){$AMIK ="-";}
		
			If (empty($STIE)){$STIE ="-";}
		
			$i.=$STMIK.', '.$AMIK.', '.$STIE;
		
		
			If (empty($KETUA)){$KETUA ="-";}
			If (empty($PENYELENGGARA)){$PENYELENGGARA ="-";}
			
			If (!empty($OTHER1)){$OTHER1='Other-'.$TANDA;}
		
			$TTD.=$KETUA.', '.$PENYELENGGARA.', '.$OTHER1;
		
		
			If (empty($DESIGN)){$DESIGN ="-";}
		
			If (empty($CETAK)){$CETAK ="-";}
		
			$JENIS.= $DESIGN.', '.$CETAK;
		
		if ($SERTIFIKAT=="other"){
			$STR='Other-'.$STRF;
			$data =array(
				'INTRUKSI' => $i,
				'SERTIFIKAT' => $STR,
				'ISI' => $ISI,
				'TTD' => $TTD,
				'TANGGAL' => $TANGGAL,
				'JENIS' => $JENIS,
				'DIINPUT_OLEH' => $OLEH
		
			);
		
			$this->db->insert('Sertifikat', $data); 
			$this->sendSertifikat();
			echo "<script>alert('Data Tersimpan.');</script>";
			echo "<meta http-equiv='refresh' content='0; url=".base_url()."index.php/adm/getDataSertifikat'>";	
			
		}
		else{
			$data =array(
				'INTRUKSI' => $i,
				'SERTIFIKAT' => $SERTIFIKAT,
				'ISI' => $ISI,
				'TTD' => $TTD,
				'TANGGAL' => $TANGGAL,
				'JENIS' => $JENIS,
				'DIINPUT_OLEH' => $OLEH
		
			);
		
			$this->db->insert('Sertifikat', $data); 
			$this->sendSertifikat();
			echo "<script>alert('Data Tersimpan.');</script>";
			echo "<meta http-equiv='refresh' content='0; url=".base_url()."index.php/adm/getDataSertifikat'>";
			
		}
		
		}
			
	}
	
	
	function sendSertifikat($post_array,$primary_key)
	{
		$this->load->library('email');
		$config['protocol'] = 'mail';
		$config['mailtype'] = 'html';
		$config['charset'] = 'utf-8';
		$config['wordwrap'] = TRUE;
		$this->email->initialize($config);
		
		$STMIK	 		 = $this->input->post('STMIK',TRUE);
		$AMIK	 		 = $this->input->post('AMIK',TRUE);
		$STIE			 = $this->input->post('STIE',TRUE);
		$SERTIFIKAT		 = $this->input->post('sertifikat',TRUE);
		$STRF			 = $this->input->post('lainnya',TRUE);
		$ISI			 = $this->input->post('isi',TRUE);
		$KETUA	  		 = $this->input->post('Ketua',TRUE);
		$PENYELENGGARA	 = $this->input->post('Penyelenggara',TRUE);
		$OTHER1		  	 = $this->input->post('other1',TRUE);
		$TANDA		  	 = $this->input->post('tanda',TRUE);
		$TANGGAL   	 	 = $this->input->post('tanggal',TRUE);
		$DESIGN 		 = $this->input->post('Design',TRUE);
		$CETAK			 = $this->input->post('Cetak',TRUE);
		$OLEH			 = $this->session->userdata('admin_user');
		
		
		If (empty($STMIK)){$STMIK ="-";}
		
		If (empty($AMIK)){$AMIK ="-";}
		
		If (empty($STIE)){$STIE ="-";}
		
		$i.=$STMIK.', '.$AMIK.', '.$STIE;
		
		If (empty($KETUA)){$KETUA ="-";}
		
		If (empty($PENYELENGGARA)){$PENYELENGGARA ="-";}
			
		If (!empty($OTHER1)){$OTHER1='Other-'.$TANDA;}
		
		$TTD.=$KETUA.', '.$PENYELENGGARA.', '.$OTHER1;
		
		If (empty($DESIGN)){$DESIGN ="-";}
		
		If (empty($CETAK)){$CETAK ="-";}
		
		$JENIS.= $DESIGN.', '.$CETAK;
			
		$Nama=$this->session->userdata('admin_user');
		
		if ($SERTIFIKAT=="other"){
			$SERTIFIKAT='Other-'.$STRF;}
		
		$SUB=Explode('-',$SERTIFIKAT);
				$isi= "<html>
		<body>
			<table>
				<tr>
					<td>Intruksi Penyelenggara</td>
					<td>:</td>
					<td>$i</td>
				</tr>
				<tr>
					<td>Sertifikat</td>
					<td>:</td>
					<td>$SUB[1]</td>
				</tr>
				<tr>
					<td>Isi Kepentingan Sertifikat</td>
					<td>:</td>
					<td>$ISI</td>
				</tr>
				<tr>
					<td>Di Tanda Tangani Oleh</td>
					<td>:</td>
					<td>$TTD</td>
				</tr>
				<tr>
					<td>Tanggal Penerbitan Sertifikat</td>
					<td>:</td>
					<td>$TANGGAL</td>
				</tr>
				<tr>
					<td>Jenis Permintaan</td>
					<td>:</td>
					<td>$JENIS</td>
				</tr>
				<tr>
					<td>Diinput oleh</td>
					<td>:</td>
					<td>$OLEH</td>
				</tr>
			</table>
		</body>
		</html>";
		
		$this->email->from($Nama);
		$this->email->to('widi@mdp.ac.id, uyi@mdp.ac.id, arafik@staff.mdp.ac.id');		
		$this->email->cc($nama);		
		$SUB=Explode('-',$SERTIFIKAT);
		$this->email->subject('Sertifikat '.$SUB[1]);
		$this->email->message($isi);
 
		$this->email->send();
		return true;
	}
	

		
	public function getDataSertifikat($status,$id)
    {
		$this->load->view('header');
		
		echo '<br /><br /><br />
		<ul class="nav nav-tabs">
		  <li><a href="'.base_url().'index.php/adm/getDesign">Buat Sertifikat</a></li>
		  <li class="active"><a href="'.base_url().'index.php/adm/getDataSertifikat">Data Sertifikat</a></li>
		  <li><a href="'.base_url().'index.php/adm/getDataDesign">Design</a></li>
		 
		</ul>
		';		
		
        try
        {
            //create crud object
            $crud = new grocery_CRUD();

            //theme
            $crud->set_theme('datatables');

            //fitur set
            $crud->set_language("indonesian");
			$crud->unset_add();
	    
			if (($this->session->userdata('admin_level')=='User')){
            	$crud->unset_delete();
				$crud->unset_edit();
				$crud->unset_add();
            }
			if (($this->session->userdata('admin_level')=='Admin')){
				$crud->unset_delete();
				$crud->unset_edit();
				$crud->unset_add();
            }
            if (($this->session->userdata('admin_level')=='Ketua')){
            	$crud->unset_delete();
				$crud->unset_edit();
				$crud->unset_add();
            }
			if (($this->session->userdata('admin_level')=='UPT')or($this->session->userdata('admin_level')=='UPT')){
            	$crud->unset_delete();
				$crud->unset_edit();
				$crud->unset_add();
            }				
            if (($this->session->userdata('admin_level')=='SuperAdmin')){
            	//$crud->unset_delete();
				$crud->unset_edit();
				
				$crud->add_action('Edit','','adm/getEditSertifikat','ui-button-icon-primary ui-icon ui-icon-pencil');
            }
			
			
            
            //set table
            $crud->set_table("sertifikat");
            
            //set primary key
            $crud->set_primary_key("ID");

            //set columns
            $crud->columns("ID","INTRUKSI", "SERTIFIKAT", "ISI", "TTD", "TANGGAL", "JENIS");
            
            //unset fields
			//$crud->unset_fields("DIINPUT_OLEH", "Jabatan", "TANGGAL_INPUT");
            
            //display
			$crud->display_as('ID', 'No.')
				->display_as('INTRUKSI', 'INTRUKSI')
				->display_as('SERTIFIKAT', 'SERTIFIKAT')
				->display_as('ISI', 'ISI KEPENTINGAN')
				->display_as('JENIS', 'JENIS PERMINTAAN'); 
				
			
			//field required
            //$crud->required_fields("Keperluan", "Jenis_design", "Deadline", "Ingin");
            
			$crud->callback_before_insert(array($this,'diinput_oleh_insert_callback'));
			
			//invisible fields
			//$crud->change_field_type('TANGGAL','invisible');
			//$crud->change_field_type('DIINPUT_OLEH','invisible');
			
			//menghilangkan Texteditor
			//$crud->unset_texteditor('Keperluan');
			
	
            //subject table
            $crud->set_subject("SERTIFIKAT");
			
            //render
            $output = $crud->render();
            $this->load->view('crud_profil', $output);
        }
        catch (exception $e)
        {
            show_error($e->getMessage() . ' --- ' . $e->getTraceAsString());
        }
		
	$this->load->view('footer');
    }
	
	
	public function getEditSertifikat($id){
		echo '<br /><br /><br />
		<ul class="nav nav-tabs">
		  <li><a href="'.base_url().'index.php/adm/getDesign">Buat Sertifikat</a></li>
		  <li class="active"><a href="'.base_url().'index.php/adm/getDataSertifikat">Data Sertifikat</a></li>
		  <li><a href="'.base_url().'index.php/adm/getDataDesign">Design</a></li>
		 
		</ul>
		';		
		
		//$data['id']=$id;
		$data['hasil']=$this->db->query("select * from sertifikat where ID='$id'");
			$this->load->helper('form');
			$this->load->view('header');
			$this->load->view('FormEditDesign',$data);
			$this->load->view('footer');
	}
	
	
	public function getUbahSertifikat(){
		$ID 	 		 = $this->input->post('ID',TRUE);
		$STMIK	 		 = $this->input->post('STMIK',TRUE);
		$AMIK	 		 = $this->input->post('AMIK',TRUE);
		$STIE			 = $this->input->post('STIE',TRUE);
		$SERTIFIKAT		 = $this->input->post('sertifikat',TRUE);
		$STRF			 = $this->input->post('lainnya',TRUE);
		$ISI			 = $this->input->post('isi',TRUE);
		$KETUA	  		 = $this->input->post('Ketua',TRUE);
		$PENYELENGGARA	 = $this->input->post('Penyelenggara',TRUE);
		$OTHER1		  	 = $this->input->post('other1',TRUE);
		$TANDA		  	 = $this->input->post('tanda',TRUE);
		$TANGGAL   	 	 = $this->input->post('tanggal',TRUE);
		$DESIGN 		 = $this->input->post('Design',TRUE);
		$CETAK			 = $this->input->post('Cetak',TRUE);
		
		
			
		If(empty($STMIK) AND empty($STIE) AND empty($AMIK)){
			$this->session->set_flashdata('k_passwod', '<div id="alert" class="alert alert-error">Institusi Harap di Pilih</div>');
			redirect('adm/getDesign');
		}
		elseif(empty($ISI)){
			$this->session->set_flashdata('k_passwod', '<div id="alert" class="alert alert-error">Isi Kepentingan Sertifikat belum diisi</div>');
			redirect('adm/getDesign');
		}
		elseif(empty($KETUA) AND empty($PENYELENGGARA) AND empty($OTHER1)){
			$this->session->set_flashdata('k_passwod', '<div id="alert" class="alert alert-error">Di Tanda Tangani Harap di Pilih</div>');
			redirect('adm/getDesign');
		}
		
		elseif(empty($TANGGAL)){
			$this->session->set_flashdata('k_passwod', '<div id="alert" class="alert alert-error">TanggalPenerbitan Harap diiisi</div>');
			redirect('adm/getDesign');
		}
		
		elseif(empty($DESIGN) AND empty($CETAK)){
			$this->session->set_flashdata('k_passwod', '<div id="alert" class="alert alert-error">Jenis Permintaan Harap di Pilih</div>');
			redirect('adm/getDesign');
		}
		else{
			
			If (empty($STMIK)){$STMIK ="-";}
		
			If (empty($AMIK)){$AMIK ="-";}
		
			If (empty($STIE)){$STIE ="-";}
		
			$i.=$STMIK.', '.$AMIK.', '.$STIE;
		
		
			If (empty($KETUA)){$KETUA ="-";}
			If (empty($PENYELENGGARA)){$PENYELENGGARA ="-";}
			
			If (!empty($OTHER1)){$OTHER1='Other-'.$TANDA;}
		
			$TTD.=$KETUA.', '.$PENYELENGGARA.', '.$OTHER1;
		
		
			If (empty($DESIGN)){$DESIGN ="-";}
		
			If (empty($CETAK)){$CETAK ="-";}
		
			$JENIS.= $DESIGN.', '.$CETAK;
			
			if ($SERTIFIKAT=="other"){$SERTIFIKAT='Other-'.$STRF;}
			
			//SEND EMAIL
			$this->load->library('email');
			$config['protocol'] = 'mail';
			$config['mailtype'] = 'html';
			$config['charset'] = 'utf-8';
			$config['wordwrap'] = TRUE;
			$this->email->initialize($config);
			
			$Nama=$this->session->userdata('admin_user');
			$SUB=Explode('-',$SERTIFIKAT);
			$isi= "<html>
			<body>
			<table>
				
				<tr>
					<td>ID</td>
					<td>:</td>
					<td>$ID</td>
				</tr>
				<tr>
					<td>Intruksi Penyelenggara</td>
					<td>:</td>
					<td>$i</td>
				</tr>
				<tr>
					<td>Sertifikat</td>
					<td>:</td>
					<td>$SUB[1]</td>
				</tr>
				<tr>
					<td>Isi Kepentingan Sertifikat</td>
					<td>:</td>
					<td>$ISI</td>
				</tr>
				<tr>
					<td>Di Tanda Tangani Oleh</td>
					<td>:</td>
					<td>$TTD</td>
				</tr>
				<tr>
					<td>Tanggal Penerbitan Sertifikat</td>
					<td>:</td>
					<td>$TANGGAL</td>
				</tr>
				<tr>
					<td>Jenis Permintaan</td>
					<td>:</td>
					<td>$JENIS</td>
				</tr>
			</table>
			</body>
			</html>";
			
			$data =array(
				'INTRUKSI' => $i,
				'SERTIFIKAT' => $SERTIFIKAT,
				'ISI' => $ISI,
				'TTD' => $TTD,
				'TANGGAL' => $TANGGAL,
				'JENIS' => $JENIS
		
			);
		
		
			
			$this->db->where('ID', $ID);
			$this->db->Update('sertifikat', $data); 
			
			
			
			$this->email->from($Nama);
			$this->email->to('widi@mdp.ac.id');		
			$this->email->cc($Nama);		
			
			$this->email->subject('Revisi Sertifikat '.$SUB[1]);
			$this->email->message($isi);
 
			$this->email->send();
						
			
			echo "<script>alert('Data $ID Telah di Ubah.');</script>";
			echo "<meta http-equiv='refresh' content='0; url=".base_url()."index.php/adm/getDataSertifikat'>";	
				
		}
			
	}
	
	
	public function getDataDesign()
    {
		$this->load->view('header');
		
		echo '<br /><br /><br />
		<ul class="nav nav-tabs">
		  <li><a href="'.base_url().'index.php/adm/getDesign">Buat Sertifikat</a></li>
		  <li><a href="'.base_url().'index.php/adm/getDataSertifikat">Data Sertifikat</a></li>
		  <li class="active"><a href="#">Design</a></li>
		 
		</ul>
		';
		
		
        try
        {
            //create crud object
            $crud = new grocery_CRUD();

            //theme
            $crud->set_theme('datatables');

            //fitur set
            $crud->set_language("indonesian");
	    
			if (($this->session->userdata('admin_level')=='User')){
            	$crud->unset_delete();
				$crud->unset_edit();
            }
			if (($this->session->userdata('admin_level')=='Admin')){
				$crud->unset_delete();
				$crud->unset_edit();
            }
            if (($this->session->userdata('admin_level')=='Ketua')){
            	$crud->unset_delete();
				$crud->unset_edit();
            }
            //set table
            $crud->set_table("design");
            
            //set primary key
            $crud->set_primary_key("id");

            //set columns
            $crud->columns("DIINPUT_OLEH", "Keperluan", "Jenis_design", "Deadline", "Ingin", "Catatan", "TANGGAL_INPUT");
            
            //unset fields
			//$crud->unset_fields("DIINPUT_OLEH", "Jabatan", "TANGGAL_INPUT");
            
              //display
			$crud->display_as('DIINPUT_OLEH', 'Nama Lengkap')
            			->display_as('TINGKAT', 'Tingkat')
            			->display_as('Keperluan', 'Keperluan Design untuk')
            			->display_as('Jenis_design', 'Jenis Design')
            			->display_as('Deadline', 'Deadline')
            			->display_as('Ingin', 'Ingin dikerjakan oleh')
            			->display_as('Catatan', 'Catatan')
            			->display_as('TANGGAL_INPUT', 'Tanggal Entry');

			 //field required
			$crud->required_fields("Keperluan", "Jenis_design", "Deadline", "Ingin");
            
			$crud->callback_before_insert(array($this,'diinput_oleh_insert_callback'));
			$crud->callback_after_insert(array($this,'sendEmail'));
			
			//invisible fields
			$crud->change_field_type('TANGGAL_INPUT','invisible');
			$crud->change_field_type('DIINPUT_OLEH','invisible');
			
			//menghilangkan Texteditor
			//$crud->unset_texteditor('Keperluan');
			
	
            //subject table
            $crud->set_subject("Design");
			
			
			//$crud->callback_add_field('Jenis_design',array($this,'add_field_callback_1'));
			
            //render
            $output = $crud->render();
            $this->load->view('crud_profil', $output);
        }
        catch (exception $e)
        {
            show_error($e->getMessage() . ' --- ' . $e->getTraceAsString());
        }
		
	$this->load->view('footer');
    }
	
	/*
	function add_field_callback_1()
{
	return ' 
	<input type="radio" name="Jenis_design" value="Banner" checked/> Banner <br/>
	<input type="radio" name="Jenis_design" value="Backdrop" /> Backdrop <br/>
	<input type="radio" name="Jenis_design" value="Poster" /> Poster <br/>
	<input type="radio" name="Jenis_design" value="Spanduk" /> Spanduk <br/>
	<input type="radio" name="Jenis_design" value="Brosur" /> Brosur <br/>
	<input type="radio" name="Jenis_design" value="Flyyer" /> Flyer <br/>
	<input type="radio" name="Jenis_design" value="Sertifikat" /> Sertifikat <br/>
	<input type="radio" name="Jenis_design" value="Sticker" /> Sticker<br/>
	<input type="radio" name="Jenis_design" /> Other <input type="text" name="Jenis_design" weight=10 />';
}
	*/
	
	function sendEmail($post_array,$primary_key)
	{
		$this->load->library('email');
		$config['protocol'] = 'mail';
		$config['mailtype'] = 'html';
		$config['charset'] = 'utf-8';
		$config['wordwrap'] = TRUE;
		$this->email->initialize($config);
		
		
			
		
		$Nama=$this->session->userdata('admin_user');
		$sub= $this->input->post('Jenis_design');
		$keperluan= strip_tags($this->input->post('Keperluan'));
		$Design= $this->input->post('Jenis_design');
		$Deadline= $this->input->post('Deadline');
		$Ingin= $this->input->post('Ingin');
		$Catatan= strip_tags($this->input->post('Catatan'));
		//$Oleh= $this->input->post('DIINPUT_OLEH');
		
				$isi= "<html>
		<body>
			<table>
				<tr>
					<td>Diinput oleh</td>
					<td>:</td>
					<td>$Nama</td>
				</tr>
				<tr>
					<td>Kepeluan design untuk</td>
					<td>:</td>
					<td>$keperluan</td>
				</tr>
				<tr>
					<td>Design</td>
					<td>:</td>
					<td>$Design</td>
				</tr>
				<tr>
					<td>Deadline</td>
					<td>:</td>
					<td>$Deadline</td>
				</tr>
				<tr>
					<td>Ingin dikerjakan oleh</td>
					<td>:</td>
					<td>$Ingin</td>
				</tr>
			</table>
		</body>
		</html>";
		
		$this->email->from($Nama);
		$this->email->to('widi@mdp.ac.id');		
		
		$this->email->subject('Design '.$sub);
		$this->email->message($isi);
 
		$this->email->send();
		return true;
	}
	
	function diinput_oleh_insert_callback($post_array)
    {
        $post_array['DIINPUT_OLEH'] = $this->session->userdata('admin_user');
		$post_array['TANGGAL_INPUT'] = date('Y-m-d');
        return $post_array;
    }
	
	function diupdate_oleh_insert_callback($post_array)
    {
        $post_array['DIUPDATE_OLEH'] = $this->session->userdata('admin_user');
        return $post_array;
    }
    
    public function logout(){
        $this->session->sess_destroy();
		redirect('welcome');
    }
	
	public function getPC()
    {
        $this->load->view('header');
		echo '<br /><br /><br />
		<ul class="nav nav-tabs">
		  
		  <li class="active"><a href="'.base_url().'index.php/adm/getPC">PC</a></li>
		  <li><a href="'.base_url().'index.php/adm/getLCD">LCD</a></li>
		  <li><a href="'.base_url().'index.php/adm/getPerawatanlain">Catalyst, etc</a></li>
		  <li><a href="'.base_url().'index.php/adm/getKontrol">Kontrol Bahan Bakar Genset</a></li>
		  <li><a href="'.base_url().'index.php/adm/getTambahSolar">Penambahan Stok Solar</a></li>
		  <li><a href="'.base_url().'index.php/adm/getIsiGenset">Pengisian Solar ke Genset ke Genset</a></li>
		  <li><a href="'.base_url().'index.php/adm/getServis">Service Rutin</a></li>
		 
		</ul>
		';
		$data["lantai"]	= $this->db->query("select * from t_ruang order by KD_LANTAI, NM_RUANG asc");
		
		$this->load->view("pc", $data);
		$this->load->view('footer');
    }
	
	public function addPC()
    {
        $this->load->view('header');
		$ruang 	= $this->input->post('ruang', TRUE);
		$data["ruang"] 	= $ruang;
		$data["pc"]		= $this->db->query("select * from t_komputer where RUANG='$ruang' AND JENIS != 'LAIN' AND JENIS != 'LCD' order by KDKOMPUTER asc");
		$data["cat"]		= $this->db->query("select * from t_komputer where RUANG='$ruang' AND JENIS = 'LAIN' order by KDKOMPUTER asc");
		$data["bulan"]	= $this->db->query("select * from t_bulan order by KD_BULAN asc");
		$data["tahun"]	= $this->db->query("select * from t_tahun order by KD_TAHUN desc");
		
		$this->load->view("pc-detail", $data);
		$this->load->view('footer');
    }
	
	public function addPC_act()
    {
        $this->load->view('header');
		$statuscek	= $this->input->post('statuscek',TRUE);
		$bulan	= $this->input->post('bulan',TRUE);
		$tahun	= $this->input->post('tahun',TRUE);
		$ruang	= $this->input->post('ruang',TRUE);
		$jumlah	= $this->input->post('jumlah',TRUE);
		$jumlahlain	= $this->input->post('jumlahlain',TRUE);
		$sj=0; $sjl=0;
		
		//echo "<br /><br /><br /><br /><br /><br /><br />";
		//echo "$ruang $jumlah";
		
		if($statuscek=="hari"){
			$addquery = " and TANGGAL='".date('Y-m-d')."'";
		}else{
			$addquery = "";
		}
		
		if($jumlah > 0){
			$cek	= $this->db->query("select * from t_perawatan where RUANG='$ruang' and BULAN='$bulan' and tahun='$tahun' $addquery");
			if ($cek->num_rows() > 0) {
				echo "<script>alert('Data perawatan ruang $ruang sudah pernah diinput. Silakan pilih ruang lain.');</script>";
			}else{
				$this->db->trans_begin();
				
				$data_perawatan	= array();
				for($i=1;$i<=$jumlah;$i++){
					
					$data_perawatan[$i]['BULAN']		= $bulan;
					$data_perawatan[$i]['TAHUN']		= $tahun;
					$data_perawatan[$i]['RUANG']		= $ruang;
					$data_perawatan[$i]['ID_KOMPUTER']	= ($_POST['pc'.$i] == "" ? "N" : $_POST['pc'.$i]);
					$data_perawatan[$i]['CPU']			= ($_POST['cpu'.$i] == "" ? "N" : $_POST['cpu'.$i]);
					$data_perawatan[$i]['LCD']			= ($_POST['lcd'.$i] == "" ? "N" : $_POST['lcd'.$i]);
					$data_perawatan[$i]['KB']			= ($_POST['keyboard'.$i] == "" ? "N" : $_POST['keyboard'.$i]);
					$data_perawatan[$i]['MS']			= ($_POST['mouse'.$i] == "" ? "N" : $_POST['mouse'.$i]);
					$data_perawatan[$i]['LAN']			= ($_POST['kabel'.$i] == "" ? "N" : $_POST['kabel'.$i]);
					$data_perawatan[$i]['MEJA']			= ($_POST['meja'.$i] == "" ? "N" : $_POST['meja'.$i]);
					$data_perawatan[$i]['KURSI']		= ($_POST['kursi'.$i] == "" ? "N" : $_POST['kursi'.$i]);
					$data_perawatan[$i]['KETERANGAN']	= ($_POST['keterangan'.$i] == "" ? "" : $_POST['keterangan'.$i]);
					$data_perawatan[$i]['DIINPUT_OLEH']	= $this->session->userdata('admin_user');
					$data_perawatan[$i]['TANGGAL']		= date('Y-m-d');
				}
				$this->db->insert_batch('t_perawatan', $data_perawatan);
				$sj = 1;
			}
		}

		if($jumlahlain > 0){
			$cek	= $this->db->query("select * from t_perawatan_lain where RUANG='$ruang' and BULAN='$bulan' and tahun='$tahun' $addquery");
			if ($cek->num_rows() > 0) {
				echo "<script>alert('Data perawatan ruang $ruang sudah pernah diinput. Silakan pilih ruang lain.');</script>";
			}else{
				$this->db->trans_begin();
				
				$data_perawatanlain	= array();
				for($i=1;$i<=$jumlahlain;$i++){
					
					$data_perawatanlain[$i]['BULAN']		= $bulan;
					$data_perawatanlain[$i]['TAHUN']		= $tahun;
					$data_perawatanlain[$i]['RUANG']		= $ruang;
					$data_perawatanlain[$i]['ID_KOMPUTER']	= ($_POST['pclain'.$i] == "" ? "N" : $_POST['pclain'.$i]);
					$data_perawatanlain[$i]['PERALATAN']	= ($_POST['peralatanlain'.$i] == "" ? "N" : $_POST['peralatanlain'.$i]);
					$data_perawatanlain[$i]['KETERANGAN']	= ($_POST['keteranganlain'.$i] == "" ? "" : $_POST['keteranganlain'.$i]);
					$data_perawatanlain[$i]['DIINPUT_OLEH']		= $this->session->userdata('admin_user');
					$data_perawatanlain[$i]['TANGGAL']			= date('Y-m-d');
				}
				$this->db->insert_batch('t_perawatan_lain', $data_perawatanlain);
				$sjl = 1;
			}
		}
		if(($sj==1)and($sjl==1)){
			echo "<script>alert('Data perawatan ruang $ruang sudah disimpan.');</script>";
		}else if(($sj==1)or($sjl==1)){
			echo "<script>alert('Data perawatan ruang $ruang sudah disimpan.');</script>";
		}else{
		
		}
		echo "<meta http-equiv='refresh' content='0; url=".base_url()."index.php/adm/getPC'>";
		$this->load->view('footer');
    }
	
	public function getPerawatan()
    {
		$this->load->view('header');
		echo '<br /><br /><br />
		<ul class="nav nav-tabs">
		  
		  <li class="active"><a href="'.base_url().'index.php/adm/getPC">PC</a></li>
		  <li><a href="'.base_url().'index.php/adm/getLCD">LCD</a></li>
		  <li><a href="'.base_url().'index.php/adm/getPerawatanlain">Catalyst, etc</a></li>
		  <li><a href="'.base_url().'index.php/adm/getKontrol">Kontrol Bahan Bakar Genset</a></li>
		  <li><a href="'.base_url().'index.php/adm/getTambahSolar">Penambahan Stok Solar</a></li>
		  <li><a href="'.base_url().'index.php/adm/getIsiGenset">Pengisian Solar ke Genset</a></li>
		  <li><a href="'.base_url().'index.php/adm/getServis">Service Rutin</a></li>
		  
		</ul>
		';
        try
        {
            //create crud object
            $crud = new grocery_CRUD();

            //theme
            $crud->set_theme('datatables');

            //fitur set
            $crud->set_language("indonesian");
			$crud->unset_add();
	    
			if (($this->session->userdata('admin_level')=='User')){
            	$crud->unset_delete();
				$crud->unset_edit();
				$crud->unset_add();
            }
			if (($this->session->userdata('admin_level')=='Admin')){
				$crud->unset_delete();
				$crud->unset_edit();
				$crud->unset_add();
            }
            if (($this->session->userdata('admin_level')=='Ketua')){
            	$crud->unset_delete();
				$crud->unset_edit();
				$crud->unset_add();
            }
			if (($this->session->userdata('admin_level')=='UPT')or($this->session->userdata('admin_level')=='UPT')){
            	$crud->unset_delete();
				$crud->unset_edit();
				$crud->unset_add();
            }
			
            
            //set table
            $crud->set_table("t_perawatan");
            
            //set primary key
            $crud->set_primary_key("ID_PERAWATAN");

            //set columns
            $crud->columns("BULAN", "TAHUN", "RUANG", "ID_KOMPUTER", "CPU", "LCD", "KB", "MS", "LAN", "MEJA", "KURSI", "KETERANGAN", "DIINPUT_OLEH", "TANGGAL");
            
            //unset fields
			//$crud->unset_fields("DIINPUT_OLEH", "Jabatan", "TANGGAL_INPUT");
            
            //display
			$crud->display_as('ID_KOMPUTER', 'ID KOMP')
				->display_as('MS', 'MOUSE')
				->display_as('KB', 'KEYBOARD')
				->display_as('LCD', 'LCD MONITOR'); 
			
			//field required
            //$crud->required_fields("Keperluan", "Jenis_design", "Deadline", "Ingin");
            
			$crud->callback_before_insert(array($this,'diinput_oleh_insert_callback'));
			
			//invisible fields
			//$crud->change_field_type('TANGGAL','invisible');
			//$crud->change_field_type('DIINPUT_OLEH','invisible');
			
			//menghilangkan Texteditor
			//$crud->unset_texteditor('Keperluan');
			
	
            //subject table
            $crud->set_subject("Perawatan PC");
			
            //render
            $output = $crud->render();
            $this->load->view('crud_profil', $output);
        }
        catch (exception $e)
        {
            show_error($e->getMessage() . ' --- ' . $e->getTraceAsString());
        }
		
	$this->load->view('footer');
    }
	
	public function getPerawatanlain()
    {
		$this->load->view('header');
		echo '<br /><br /><br />
		<ul class="nav nav-tabs">
		  <li><a href="'.base_url().'index.php/adm/getPC">PC</a></li>
		  <li><a href="'.base_url().'index.php/adm/getLCD">LCD</a></li>
		  <li class="active"><a href="#">Catalyst, etc</a></li>
		  <li><a href="'.base_url().'index.php/adm/getKontrol">Kontrol Bahan Bakar Genset</a></li>
		  <li><a href="'.base_url().'index.php/adm/getTambahSolar">Penambahan Stok Solar</a></li>
		  <li><a href="'.base_url().'index.php/adm/getIsiGenset">Pengisian Solar ke Genset</a></li>
		  <li><a href="'.base_url().'index.php/adm/getServis">Service Rutin</a></li>
		  
		  
		</ul>
		';
        try
        {
            //create crud object
            $crud = new grocery_CRUD();

            //theme
            $crud->set_theme('datatables');

            //fitur set
            $crud->set_language("indonesian");
			$crud->unset_add();
	    
			if (($this->session->userdata('admin_level')=='User')){
            	$crud->unset_delete();
				$crud->unset_edit();
				$crud->unset_add();
            }
			if (($this->session->userdata('admin_level')=='Admin')){
				$crud->unset_delete();
				$crud->unset_edit();
				$crud->unset_add();
            }
            if (($this->session->userdata('admin_level')=='Ketua')){
            	$crud->unset_delete();
				$crud->unset_edit();
				$crud->unset_add();
            }
			if (($this->session->userdata('admin_level')=='UPT')or($this->session->userdata('admin_level')=='UPT')){
            	$crud->unset_delete();
				$crud->unset_edit();
				$crud->unset_add();
            }
			
            
            //set table
            $crud->set_table("t_perawatan_lain");
            
            //set primary key
            $crud->set_primary_key("ID_PERAWATAN");

            //set columns
            $crud->columns("BULAN", "TAHUN", "RUANG", "ID_KOMPUTER", "PERALATAN", "KETERANGAN", "DIINPUT_OLEH", "TANGGAL");
            
            //unset fields
			//$crud->unset_fields("DIINPUT_OLEH", "Jabatan", "TANGGAL_INPUT");
            
            //display
			$crud->display_as('ID_KOMPUTER', 'ID ALAT')
				->display_as('PERALATAN', 'KONDISI')
				->display_as('TANGGAL', 'TGL INPUT'); 
			
			//field required
            //$crud->required_fields("Keperluan", "Jenis_design", "Deadline", "Ingin");
            
			$crud->callback_before_insert(array($this,'diinput_oleh_insert_callback'));
			
			//invisible fields
			//$crud->change_field_type('TANGGAL','invisible');
			//$crud->change_field_type('DIINPUT_OLEH','invisible');
			
			//menghilangkan Texteditor
			//$crud->unset_texteditor('Keperluan');
			
            //subject table
            $crud->set_subject("Perawatan Catalyst, etc");
			
            //render
            $output = $crud->render();
            $this->load->view('crud_profil', $output);
        }
        catch (exception $e)
        {
            show_error($e->getMessage() . ' --- ' . $e->getTraceAsString());
        }
		
	$this->load->view('footer');
    }
	
	
	public function getKontrol()
    {
		$this->load->view('header');
		
		echo '<br /><br /><br />
		<ul class="nav nav-tabs">
		   <li><a href="'.base_url().'index.php/adm/getPC">PC</a></li>
		  <li><a href="'.base_url().'index.php/adm/getLCD">LCD</a></li>
		  <li><a href="'.base_url().'index.php/adm/getPerawatanlain">Catalyst, etc</a></li>
		  <li class="active"><a href="'.base_url().'index.php/adm/getKontrol">Kontrol Bahan Bakar Genset</a></li>
		  <li><a href="'.base_url().'index.php/adm/getTambahSolar">Penambahan Stok Solar</a></li>
		  <li><a href="'.base_url().'index.php/adm/getIsiGenset">Pengisian Solar ke Genset</a></li>
		  <li><a href="'.base_url().'index.php/adm/getServis">Service Rutin</a></li>
		 
		</ul>
				
		';		
		
		$data["bulan"]	= $this->db->query("select * from t_bulan order by KD_BULAN asc");
		
		
		$this->load->view("cetakkontrol", $data);
		
        try
        {
            //create crud object
            $crud = new grocery_CRUD();

            //theme
            $crud->set_theme('datatables');

            //fitur set
            $crud->set_language("indonesian");
			//$crud->unset_add();
	    
			if (($this->session->userdata('admin_level')=='User')){
            	$crud->unset_delete();
				$crud->unset_edit();
				$crud->unset_add();
            }
			if (($this->session->userdata('admin_level')=='Admin')){
				$crud->unset_delete();
				$crud->unset_edit();
				$crud->unset_add();
            }
            if (($this->session->userdata('admin_level')=='Ketua')){
            	$crud->unset_delete();
				$crud->unset_edit();
				$crud->unset_add();
            }
			if (($this->session->userdata('admin_level')=='UPT')or($this->session->userdata('admin_level')=='UPT')){
            	$crud->unset_delete();
				$crud->unset_edit();
            }
			
			
            
            //set table
            $crud->set_table("kontrol_genset");
            
            //set primary key
            $crud->set_primary_key("ID_KONTROL");

            //set columns
			
            $crud->columns("ID_KONTROL","TGL_KONTROL", "BERISI", "KOSONG", "AIR_RADIATOR", "rutin_hidup", "rutin_mati", "t_rutin_hidup", "t_rutin_mati", "CATATAN");
            
            //unset fields
			$crud->unset_fields("JML_SOLAR");
            
            //display
			$crud->display_as('ID_KONTROL', 'No.')
				->display_as('TGL_KONTROL', 'Tanggal Kontrol')
				->display_as('JML_SOLAR', 'Jumlah Solar')
				->display_as('BERISI', 'Dirigen yang Berisi')
				->display_as('KOSONG', 'Dirigen yang Kosong')
				->display_as('AIR_RADIATOR', 'Air Radiator')
				->display_as('t_rutin_hidup', 'Tidak Rutin Hidup')
				->display_as('t_rutin_mati', 'Tidak Rutin Mati')
				->display_as('CATATAN', 'Catatan')
				->display_as('TANGGAL_INPUT', 'Tanggal Entry'); 
				
			$crud->field_type('rutin_hidup','time')
				 ->field_type('rutin_mati','time')
				 ->field_type('t_rutin_hidup','time')
				 ->field_type('t_rutin_mati','time');
			//field required
            $crud->required_fields("TGL_KONTROL", "BERISI", "KOSONG", "AIR_RADIATOR", "rutin_hidup", "rutin_mati");
            
			$crud->callback_before_insert(array($this,'diinput_oleh_insert_callback'));
			
			//invisible fields
			$crud->change_field_type('TANGGAL_INPUT','invisible');
			$crud->change_field_type('DIINPUT_OLEH','invisible');
			
			//menghilangkan Texteditor
			//$crud->unset_texteditor('Keperluan');
			
	
            //subject table
            $crud->set_subject("Kontrol Genset");
			
            //render
            $output = $crud->render();
            $this->load->view('crud_profil', $output);
        }
        catch (exception $e)
        {
            show_error($e->getMessage() . ' --- ' . $e->getTraceAsString());
        }
		
	$this->load->view('footer');
    }
	
	public function cetakkontrol()
    {
		$bln=$this->input->post('bulan',TRUE);
		$thn=$this->input->post('tahun',TRUE);
		
		$NM_BULAN	= $this->db->query("select NM_BULAN from t_bulan where KD_BULAN=$bln")->result_array();
		foreach($NM_BULAN as $BULAN){
				$data['bulan']=$BULAN['NM_BULAN'];
		}
		
		$data['tahun']=$this->input->post('tahun',TRUE);
		$data["control"]	= $this->db->query("select * from kontrol_genset where MID(TGL_KONTROL,6,2) LIKE '%$bln' AND MID( TGL_KONTROL, 1, 4 ) LIKE '%$thn' ");
		
		$this->load->view("laporan", $data);
		
	}
	
	
	public function getTambahSolar()
    {
		$this->load->view('header');
		
		echo '<br /><br /><br />
		<ul class="nav nav-tabs">
		   <li><a href="'.base_url().'index.php/adm/getPC">PC</a></li>
		  <li><a href="'.base_url().'index.php/adm/getLCD">LCD</a></li>
		  <li><a href="'.base_url().'index.php/adm/getPerawatanlain">Catalyst, etc</a></li>
		  <li><a href="'.base_url().'index.php/adm/getKontrol">Kontrol Bahan Bakar Genset</a></li>
		  <li class="active"><a href="#">Penambahan Stok Solar</a></li>
		  <li><a href="'.base_url().'index.php/adm/getIsiGenset">Pengisian Solar ke Genset</a></li>
		  <li><a href="'.base_url().'index.php/adm/getServis">Service Rutin</a></li>
		 
		</ul>
		';		
		
        try
        {
            //create crud object
            $crud = new grocery_CRUD();

            //theme
            $crud->set_theme('datatables');

            //fitur set
            $crud->set_language("indonesian");
			//$crud->unset_add();
	    
			if (($this->session->userdata('admin_level')=='User')){
            	$crud->unset_delete();
				$crud->unset_edit();
				$crud->unset_add();
            }
			if (($this->session->userdata('admin_level')=='Admin')){
				$crud->unset_delete();
				$crud->unset_edit();
				$crud->unset_add();
            }
            if (($this->session->userdata('admin_level')=='Ketua')){
            	$crud->unset_delete();
				$crud->unset_edit();
				$crud->unset_add();
            }
			if (($this->session->userdata('admin_level')=='UPT')or($this->session->userdata('admin_level')=='UPT')){
            	$crud->unset_delete();
				$crud->unset_edit();
            }
			
			
            
            //set table
            $crud->set_table("penambahan_stok");
            
            //set primary key
            $crud->set_primary_key("ID_STOK");

            //set columns
            $crud->columns("ID_STOK","JML_DIRIGEN", "TGL_PEMBELIAN", "JAM_PAKAI" ,"CATATAN");
            
            //unset fields
			//$crud->unset_fields("DIINPUT_OLEH", "Jabatan", "TANGGAL_INPUT");
            
            //display
			$crud->display_as('ID_STOK', 'No.')
				->display_as('JML_DIRIGEN', 'Jumlah Beli Solar ')
				->display_as('TGL_PEMBELIAN', 'Tanggal Pembelian')
				->display_as('TANGGAL_INPUT', 'Tanggal Entry'); 
				
			
			//upload
				$crud->set_field_upload('FILE_1', $this->folder_upload."/TambahSolar");
				$crud->set_field_upload('FILE_2', $this->folder_upload."/TambahSolar");
			
			
			//field required
            $crud->required_fields("JML_DIRIGEN", "TGL_PEMBELIAN", "JAM_PAKAI" ,"CATATAN");
            
			$crud->callback_before_insert(array($this,'diinput_oleh_insert_callback'));
			
			//invisible fields
			$crud->change_field_type('TANGGAL_INPUT','invisible');
			$crud->change_field_type('DIINPUT_OLEH','invisible');
			
			//menghilangkan Texteditor
			//$crud->unset_texteditor('Keperluan');
			
	
            //subject table
            $crud->set_subject("Penambahan Stok Solar");
			
            //render
            $output = $crud->render();
            $this->load->view('crud_profil', $output);
        }
        catch (exception $e)
        {
            show_error($e->getMessage() . ' --- ' . $e->getTraceAsString());
        }
		
	$this->load->view('footer');
    }
	
	public function getIsiGenset()
    {
		$this->load->view('header');
		
		echo '<br /><br /><br />
		<ul class="nav nav-tabs">
		   <li><a href="'.base_url().'index.php/adm/getPC">PC</a></li>
		  <li><a href="'.base_url().'index.php/adm/getLCD">LCD</a></li>
		  <li><a href="'.base_url().'index.php/adm/getPerawatanlain">Catalyst, etc</a></li>
		  <li><a href="'.base_url().'index.php/adm/getKontrol">Kontrol Bahan Bakar Genset</a></li>
		  <li><a href="'.base_url().'index.php/adm/getTambahSolar">Penambahan Stok Solar</a></li>
		  <li class="active"><a href="#">Pengisian Solar ke Genset</a></li>
		  <li><a href="'.base_url().'index.php/adm/getServis">Service Rutin</a></li>
		 
		</ul>
		';		
		
        try
        {
            //create crud object
            $crud = new grocery_CRUD();

            //theme
            $crud->set_theme('datatables');

            //fitur set
            $crud->set_language("indonesian");
			//$crud->unset_add();
	    
			if (($this->session->userdata('admin_level')=='User')){
            	$crud->unset_delete();
				$crud->unset_edit();
				$crud->unset_add();
            }
			if (($this->session->userdata('admin_level')=='Admin')){
				$crud->unset_delete();
				$crud->unset_edit();
				$crud->unset_add();
            }
            if (($this->session->userdata('admin_level')=='Ketua')){
            	$crud->unset_delete();
				$crud->unset_edit();
				$crud->unset_add();
            }
			if (($this->session->userdata('admin_level')=='UPT')or($this->session->userdata('admin_level')=='UPT')){
            	$crud->unset_delete();
				$crud->unset_edit();
            }
			
			
            
            //set table
            $crud->set_table("isi_tangki");
            
            //set primary key
            $crud->set_primary_key("ID_ISI");

            //set columns
            $crud->columns("ID_ISI", "TGL_ISI","JML_ISI", "CATATAN");
            
            //unset fields
			//$crud->unset_fields("DIINPUT_OLEH", "Jabatan", "TANGGAL_INPUT");
            
            //display
			$crud->display_as('ID_ISI', 'No.')
				->display_as('JML_ISI', 'Jumlah Isi ke Genset ')
				->display_as('TGL_ISI', 'Tanggal Isi')
				->display_as('TANGGAL_INPUT', 'Tanggal Entry'); 
				
			$crud->set_field_upload('FILE', $this->folder_upload."/UPT/ISI TANKI");
				
			
			//field required
            $crud->required_fields("TGL_ISI","JML_ISI");
            
			$crud->callback_before_insert(array($this,'diinput_oleh_insert_callback'));
			
			//invisible fields
			$crud->change_field_type('TANGGAL_INPUT','invisible');
			$crud->change_field_type('DIINPUT_OLEH','invisible');
			
			//menghilangkan Texteditor
			//$crud->unset_texteditor('Keperluan');
			
	
            //subject table
            $crud->set_subject("Isi Tangki Genset");
			
            //render
            $output = $crud->render();
            $this->load->view('crud_profil', $output);
        }
        catch (exception $e)
        {
            show_error($e->getMessage() . ' --- ' . $e->getTraceAsString());
        }
		
	$this->load->view('footer');
    }
	
	public function getServis()
    {
		$this->load->view('header');
		
		echo '<br /><br /><br />
		<ul class="nav nav-tabs">
		  <li><a href="'.base_url().'index.php/adm/getPC">PC</a></li>
		  <li><a href="'.base_url().'index.php/adm/getLCD">LCD</a></li>
		  <li><a href="'.base_url().'index.php/adm/getPerawatanlain">Catalyst, etc</a></li>
		  <li><a href="'.base_url().'index.php/adm/getKontrol">Kontrol Bahan Bakar Genset</a></li>
		  <li><a href="'.base_url().'index.php/adm/getTambahSolar">Penambahan Stok Solar</a></li>
		  <li><a href="'.base_url().'index.php/adm/getIsiGenset">Pengisian Solar ke Genset</a></li>
		  <li class="active"><a href="#">Service Rutin</a></li>
		</ul>
		';		
		
        try
        {
            //create crud object
            $crud = new grocery_CRUD();

            //theme
            $crud->set_theme('datatables');

            //fitur set
            $crud->set_language("indonesian");
			//$crud->unset_add();
	    
			if (($this->session->userdata('admin_level')=='User')){
            	$crud->unset_delete();
				$crud->unset_edit();
				$crud->unset_add();
            }
			if (($this->session->userdata('admin_level')=='Admin')){
				$crud->unset_delete();
				$crud->unset_edit();
				$crud->unset_add();
            }
            if (($this->session->userdata('admin_level')=='Ketua')){
            	$crud->unset_delete();
				$crud->unset_edit();
				$crud->unset_add();
            }
			if (($this->session->userdata('admin_level')=='UPT')or($this->session->userdata('admin_level')=='UPT')){
            	$crud->unset_delete();
				$crud->unset_edit();
            }
			
			
            
            //set table
            $crud->set_table("service_rutin");
            
            //set primary key
            $crud->set_primary_key("ID_SERVICE");

            //set columns
            $crud->columns("ID_SERVICE", "TANGGAL_INPUT", "GANTI_OLI","FILTER_SOLAR", "FILTER_OLI", "AIR_PENGHILANG_ASAP", "DRUM", "MESIN_CELUP_GENSET", "MESIN_CELUP_SUBSITENG", "LAMPU_INDIKATOR_PLN", "LAMPU_EMERGENCY", "LAMPU_TL", "MCB_PLN", "MCB_dalam_GENSET", "SWITCH_PLN", "CATATAN");
            
            //unset fields
			//$crud->unset_fields("DIINPUT_OLEH", "Jabatan", "TANGGAL_INPUT");
            
            //display
			$crud->display_as('ID_SERVICE', 'No.')
				 ->display_as('TANGGAL_INPUT', 'Tanggal Entry'); 
				
			
			//field required
            $crud->required_fields("GANTI_OLI","FILTER_SOLAR", "FILTER_OLI", "AIR_PENGHILANG_ASAP", "DRUM", "MESIN_CELUP_GENSET", "MESIN_CELUP_SUBSITENG", "LAMPU_INDIKATOR_PLN", "LAMPU_EMERGENCY", "LAMPU_TL", "MCB_PLN", "MCB_dalam_GENSET", "SWITCH_PLN");
            
			$crud->callback_before_insert(array($this,'diinput_oleh_insert_callback'));
			
			//invisible fields
			$crud->change_field_type('TANGGAL_INPUT','invisible');
			$crud->change_field_type('DIINPUT_OLEH','invisible');
			//menghilangkan Texteditor
			//$crud->unset_texteditor('Keperluan');
			
	
            //subject table
            $crud->set_subject("Service Rutin");
			
            //render
            $output = $crud->render();
            $this->load->view('crud_profil', $output);
        }
        catch (exception $e)
        {
            show_error($e->getMessage() . ' --- ' . $e->getTraceAsString());
        }
		
	$this->load->view('footer');
    }
	
	public function getFeedback()
    {
		$this->load->view('header');
		
		try
		{
			//create crud object
			$crud = new grocery_CRUD();

			//theme
			$crud->set_theme('datatables');

			//fitur set
			$crud->set_language("indonesian");
		
			if (($this->session->userdata('admin_level')<>'SuperAdmin')){
				$crud->unset_delete();
				$crud->unset_export();
				$crud->unset_print();
				$crud->unset_edit();
				$crud->unset_read();
			}
			
			//set table
			$crud->set_table("t_feedback");
			
			//set primary key
			$crud->set_primary_key("ID_FB");

			//set columns
			$crud->columns("CATATAN","TANGGAL");
			
			//invisible fields
			$crud->change_field_type('TANGGAL','invisible');
			$crud->change_field_type('DIINPUT_OLEH','invisible');
			
			//display
			$crud->display_as('CATATAN', 'Saran/kritik untuk i-mpian');

			//field required
			$crud->required_fields("CATATAN");
			
			$crud->callback_before_insert(array($this,'tanggal_input_insert_callback'));
			$crud->callback_before_insert(array($this,'diinput_oleh_insert_callback'));
			
			//subject table
			$crud->set_subject(" :: Feedback dari Bapak/Ibu sangat membantu pengembangan i-mpian lebih baik lagi :) ");

			//render
			$output = $crud->render();
			$this->load->view('crud_profil', $output);
		}
		catch (exception $e)
		{
			show_error($e->getMessage() . ' --- ' . $e->getTraceAsString());
		}
		
		$this->load->view('footer');
    }
	
	public function getLCD()
    {
		$this->load->view('header');
		echo '<br /><br /><br />
		<ul class="nav nav-tabs">
		  <li><a href="'.base_url().'index.php/adm/getPC">PC</a></li>
		  <li class="active"><a href="'.base_url().'index.php/adm/getLCD">LCD</a></li>
		  <li><a href="'.base_url().'index.php/adm/getPerawatanlain">Catalyst, etc</a></li>
		  <li><a href="'.base_url().'index.php/adm/getKontrol">Kontrol Bahan Bakar Genset</a></li>
		  <li><a href="'.base_url().'index.php/adm/getTambahSolar">Penambahan Stok Solar</a></li>
		  <li><a href="'.base_url().'index.php/adm/getIsiGenset">Pengisian Solar ke Genset</a></li>
		  <li><a href="'.base_url().'index.php/adm/getServis">Service Rutin</a></li>
		</ul>
		';	
		try
		{
			//create crud object
			$crud = new grocery_CRUD();

			//theme
			$crud->set_theme('datatables');

			//fitur set
			$crud->set_language("indonesian");
		
			if (($this->session->userdata('admin_level')=='User')){
            	$crud->unset_delete();
				$crud->unset_edit();
				$crud->unset_add();
            }
			if (($this->session->userdata('admin_level')=='Admin')){
				$crud->unset_delete();
				$crud->unset_edit();
				$crud->unset_add();
            }
            if (($this->session->userdata('admin_level')=='Ketua')){
            	$crud->unset_delete();
				$crud->unset_edit();
				$crud->unset_add();
            }
			if ($this->session->userdata('admin_level')=='UPT'){
            	$crud->unset_delete();
				$crud->unset_edit();
				//$crud->unset_add();
            }
			if ($this->session->userdata('admin_level')=='KaUPT'){
            	$crud->unset_delete();
				//$crud->unset_edit();
				//$crud->unset_add();
            }
			//set table
			$crud->set_table("t_lcd");
			
			//set primary key
			$crud->set_primary_key("ID_LCD");

			//set columns
			//$crud->columns("CATATAN","TANGGAL");
			
			//invisible fields
			$crud->change_field_type('TANGGAL_INPUT','invisible');
			$crud->change_field_type('DIINPUT_OLEH','invisible');
			
			//display
			//$crud->display_as('CATATAN', 'Saran/kritik untuk i-mpian');
			
			$crud->set_relation('KDKOMPUTER','t_komputer','KDKOMPUTER',array('JENIS' => 'LCD'));
			
			//field required
			$crud->required_fields("KDKOMPUTER,JAM_PAKAI");
			
			$crud->callback_before_insert(array($this,'tanggal_input_insert_callback'));
			$crud->callback_before_insert(array($this,'diinput_oleh_insert_callback'));
			
			//subject table
			$crud->set_subject("LCD");

			//render
			$output = $crud->render();
			$this->load->view('crud_profil', $output);
		}
		catch (exception $e)
		{
			show_error($e->getMessage() . ' --- ' . $e->getTraceAsString());
		}
		
		$this->load->view('footer');
    }
	
	public function getMasterPC()
    {
		$this->load->view('header');
		
		try
		{
			//create crud object
			$crud = new grocery_CRUD();

			//theme
			$crud->set_theme('datatables');

			//fitur set
			$crud->set_language("indonesian");
		
			if (($this->session->userdata('admin_level')<>'SuperAdmin')){
            	$crud->unset_delete();
				$crud->unset_edit();
				$crud->unset_add();
            }
			
			//set table
			$crud->set_table("t_komputer");
			
			//set primary key
			$crud->set_primary_key("KDKOMPUTER");

			$crud->set_relation('RUANG','t_ruang','NM_RUANG');
			
			//field required
			$crud->required_fields("KDKOMPUTER,JENIS,LANTAI,RUANG");
			
			
			//subject table
			$crud->set_subject("MASTER PC");

			//render
			$output = $crud->render();
			$this->load->view('crud_profil', $output);
		}
		catch (exception $e)
		{
			show_error($e->getMessage() . ' --- ' . $e->getTraceAsString());
		}
		
		$this->load->view('footer');
    }
	
	public function getMasterRuang()
    {
		$this->load->view('header');
		
		try
		{
			//create crud object
			$crud = new grocery_CRUD();

			//theme
			$crud->set_theme('datatables');

			//fitur set
			$crud->set_language("indonesian");
		
			if (($this->session->userdata('admin_level')<>'SuperAdmin')){
            	$crud->unset_delete();
				$crud->unset_edit();
				$crud->unset_add();
            }
			
			//set table
			$crud->set_table("t_ruang");
			
			//set primary key
			$crud->set_primary_key("NM_RUANG");

			//$crud->set_relation('RUANG','t_ruang','NM_RUANG');
			
			//field required
			$crud->required_fields("NM_RUANG,KD_LANTAI");
			
			
			//subject table
			$crud->set_subject("MASTER RUANG");

			//render
			$output = $crud->render();
			$this->load->view('crud_profil', $output);
		}
		catch (exception $e)
		{
			show_error($e->getMessage() . ' --- ' . $e->getTraceAsString());
		}
		
		$this->load->view('footer');
    }
	
	public function getApp()
    {
		$this->load->view('header');
		
		try
		{
			//create crud object
			$crud = new grocery_CRUD();

			//theme
			$crud->set_theme('datatables');

			//fitur set
			$crud->set_language("indonesian");
		
			if (($this->session->userdata('admin_level')<>'SuperAdmin')){
            	$crud->unset_delete();
				$crud->unset_edit();
				$crud->change_field_type('TANGGAL_SELESAI','invisible');
				$crud->change_field_type('STATUS','invisible');
				$crud->change_field_type('CATATAN_PENGUJIAN','invisible');
				$crud->change_field_type('TANGGAL_PENGUJIAN','invisible');
				
            }
			
			//set table
			$crud->set_table("t_app");
			
			//set primary key
			$crud->set_primary_key("ID_APP");

			//$crud->columns("NAMA_APP","DEKSKRIPSI","AKSES","TIPE","PUBLISH");
			
			//display
			$crud->display_as('NAMA_APP', 'APLIKASI')
			->display_as('DEKSKRIPSI', 'DESKRIPSI')
			->display_as('PUBLISH', 'TANGGAL PENGGUNAAN');
			
			//field required
			$crud->required_fields("NAMA_APP","DEKSKRIPSI","AKSES","TIPE","PUBLISH");
			
			//invisible fields
			$crud->change_field_type('TANGGAL_INPUT','invisible');
			$crud->change_field_type('DIINPUT_OLEH','invisible');
			
			
			
			$crud->callback_before_insert(array($this,'tanggal_input_insert_callback'));
			$crud->callback_before_insert(array($this,'diinput_oleh_insert_callback'));
			
			
			//subject table
			$crud->set_subject("FORM PEMESANAN APLIKASI");

			//render
			$output = $crud->render();
			$this->load->view('crud_profil', $output);
		}
		catch (exception $e)
		{
			show_error($e->getMessage() . ' --- ' . $e->getTraceAsString());
		}
		
		$this->load->view('footer');
    }
	
}