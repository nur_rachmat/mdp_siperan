<!--<div class="alert alert-danger" role="alert">Grafik dalam proses perbaikan</div>-->
<script src="<?php echo base_url();?>assets/js/highchart/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/js/highchart/highcharts.js" type="text/javascript"></script>
<script type="text/javascript">
	var chart1; var chart2; //stmik
	var chart3; var chart4; //stie
	var chart5; var chart6; //mdpits
	var chart7; var chart8; //kopertis
	var chart9; var chart10; //umum
   //STMIK
   
    $(document).ready(function() {
      chart1 = new Highcharts.Chart({
         chart: {
            renderTo: 'container',
            type: 'column'
         },   
         title: {
            text: 'Grafik Agenda per Bulan'
         },
         xAxis: {
            categories: ['JAN', 'FEB' , 'MAR' , 'APR' , 'MEI' , 'JUN' , 'JUL' , 'AGU' , 'SEP' , 'OKT' , 'NOV' , 'DES']
         },
         yAxis: {
            title: {
               text: 'Jumlah agenda yang diinput'
            }
         },
            series:             
            [
            <?php 
            $sql   = "SELECT * FROM v_agenda_bulan where INSTANSI='STMIK/AMIK' order by tahun asc";
            $query = mysql_query( $sql )  or die(mysql_error());
            while( $ret = mysql_fetch_array( $query ) ){
            	 $tahun = $ret['TAHUN']; 
		 $jan = $ret['JAN'];
		 $feb = $ret['FEB'];
		 $mar = $ret['MAR'];
		 $apr = $ret['APR'];
		 $mei = $ret['MEI'];
		 $jun = $ret['JUN'];
		 $jul = $ret['JUL'];
		 $agu = $ret['AGU'];
		 $sep = $ret['SEP'];
		 $okt = $ret['OKT'];
		 $nov = $ret['NOV'];
		 $des = $ret['DES'];
		 
                  ?>
                  {
                      name: '<?php echo $tahun; ?>',
                      data: [<?php echo $jan; ?>,<?php echo $feb; ?>,<?php echo $mar; ?>,<?php echo $apr; ?>,<?php echo $mei; ?>,<?php echo $jun; ?>,<?php echo $jul; ?>,<?php echo $agu; ?>,<?php echo $sep; ?>,<?php echo $okt; ?>,<?php echo $nov; ?>,<?php echo $des; ?>],
             		dataLabels: {
                enabled: true,
                rotation: 0,
                color: '#FFFFFF',
                align: 'right',
                x: -2,
                y: 20,
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif',
                    textShadow: '0 0 3px black'
                }
            }
                  },
            <?php } ?>
            ]
      });
   });	
   
   $(document).ready(function() {
      chart2 = new Highcharts.Chart({
         chart: {
            renderTo: 'container2',
            type: 'column'
         },   
         title: {
            text: 'Grafik Agenda per Tahun'
         },
         xAxis: {
            categories: ['Agenda']
         },
         yAxis: {
            title: {
               text: 'Jumlah agenda yang diinput'
            }
         },
         series:             
            [
            	{
                      name: 'Tahun',
                      data: [
            <?php 
            $sql   = "SELECT * FROM v_agenda_tahun order by tahun asc";
            $query = mysql_query( $sql )  or die(mysql_error());
            while( $ret = mysql_fetch_array( $query ) ){
            	 	$tahun = $ret['TAHUN']; 
			$total = $ret['TSTMIK'];    
                  ?>
                  
                      [<?php echo $tahun; ?>, <?php echo $total; ?>],
            <?php } ?>
            
             		],
             		dataLabels: {
                enabled: true,
                rotation: 0,
                color: '#FFFFFF',
                align: 'right',
                x: -2,
                y: 20,
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif',
                    textShadow: '0 0 3px black'
                }
            }
             }]
      });
   });
   
   //STIE
   $(document).ready(function() {
      chart3 = new Highcharts.Chart({
         chart: {
            renderTo: 'container3',
            type: 'column'
         },   
         title: {
            text: 'Grafik Agenda per Bulan '
         },
         xAxis: {
            categories: ['JAN', 'FEB' , 'MAR' , 'APR' , 'MEI' , 'JUN' , 'JUL' , 'AGU' , 'SEP' , 'OKT' , 'NOV' , 'DES']
         },
         yAxis: {
            title: {
               text: 'Jumlah agenda yang diinput'
            }
         },
             series:             
            [
            <?php 
            $sql   = "SELECT * FROM v_agenda_bulan where INSTANSI='STIE' order by tahun asc";
            $query = mysql_query( $sql )  or die(mysql_error());
            while( $ret = mysql_fetch_array( $query ) ){
            	 $tahun = $ret['TAHUN']; 
		 $jan = $ret['JAN'];
		 $feb = $ret['FEB'];
		 $mar = $ret['MAR'];
		 $apr = $ret['APR'];
		 $mei = $ret['MEI'];
		 $jun = $ret['JUN'];
		 $jul = $ret['JUL'];
		 $agu = $ret['AGU'];
		 $sep = $ret['SEP'];
		 $okt = $ret['OKT'];
		 $nov = $ret['NOV'];
		 $des = $ret['DES'];
		 
                  ?>
                  {
                      name: '<?php echo $tahun; ?>',
                      data: [<?php echo $jan; ?>,<?php echo $feb; ?>,<?php echo $mar; ?>,<?php echo $apr; ?>,<?php echo $mei; ?>,<?php echo $jun; ?>,<?php echo $jul; ?>,<?php echo $agu; ?>,<?php echo $sep; ?>,<?php echo $okt; ?>,<?php echo $nov; ?>,<?php echo $des; ?>],
             		dataLabels: {
                enabled: true,
                rotation: 0,
                color: '#FFFFFF',
                align: 'right',
                x: -2,
                y: 20,
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif',
                    textShadow: '0 0 3px black'
                }
            }
                  },
            <?php } ?>
            ]
      });
   });
   $(document).ready(function() {
      chart4 = new Highcharts.Chart({
         chart: {
            renderTo: 'container4',
            type: 'column'
         },   
         title: {
            text: 'Grafik Agenda per Tahun'
         },
         xAxis: {
            categories: ['Agenda']
         },
         yAxis: {
            title: {
               text: 'Jumlah agenda yang diinput'
            }
         },
         series:             
            [
            	{
                      name: 'Tahun',
                      data: [
            <?php 
            $sql   = "SELECT * FROM v_agenda_tahun order by tahun asc";
            $query = mysql_query( $sql )  or die(mysql_error());
            while( $ret = mysql_fetch_array( $query ) ){
            	 	$tahun = $ret['TAHUN']; 
			$total = $ret['TSTIE'];    
                  ?>
                  
                      [<?php echo $tahun; ?>, <?php echo $total; ?>],
            <?php } ?>
            
             		],
             		dataLabels: {
                enabled: true,
                rotation: 0,
                color: '#FFFFFF',
                align: 'right',
                x: -2,
                y: 20,
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif',
                    textShadow: '0 0 3px black'
                }
            }
             }]
      });
   });
   
   
   
   //MDP IT SUPERSTORE
   $(document).ready(function() {
      chart5 = new Highcharts.Chart({
         chart: {
            renderTo: 'container5',
            type: 'column'
         },   
        title: {
            text: 'Grafik Agenda per Bulan '
         },
         xAxis: {
            categories: ['JAN', 'FEB' , 'MAR' , 'APR' , 'MEI' , 'JUN' , 'JUL' , 'AGU' , 'SEP' , 'OKT' , 'NOV' , 'DES']
         },
         yAxis: {
            title: {
               text: 'Jumlah agenda yang diinput'
            }
         },
             series:             
            [
            <?php 
            $sql   = "SELECT * FROM v_agenda_bulan where INSTANSI='MDP IT SUPERSTORE' order by tahun asc";
            $query = mysql_query( $sql )  or die(mysql_error());
            while( $ret = mysql_fetch_array( $query ) ){
            	 $tahun = $ret['TAHUN']; 
		 $jan = $ret['JAN'];
		 $feb = $ret['FEB'];
		 $mar = $ret['MAR'];
		 $apr = $ret['APR'];
		 $mei = $ret['MEI'];
		 $jun = $ret['JUN'];
		 $jul = $ret['JUL'];
		 $agu = $ret['AGU'];
		 $sep = $ret['SEP'];
		 $okt = $ret['OKT'];
		 $nov = $ret['NOV'];
		 $des = $ret['DES'];
		 
                  ?>
                  {
                      name: '<?php echo $tahun; ?>',
                      data: [<?php echo $jan; ?>,<?php echo $feb; ?>,<?php echo $mar; ?>,<?php echo $apr; ?>,<?php echo $mei; ?>,<?php echo $jun; ?>,<?php echo $jul; ?>,<?php echo $agu; ?>,<?php echo $sep; ?>,<?php echo $okt; ?>,<?php echo $nov; ?>,<?php echo $des; ?>],
             		dataLabels: {
                enabled: true,
                rotation: 0,
                color: '#FFFFFF',
                align: 'right',
                x: -2,
                y: 20,
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif',
                    textShadow: '0 0 3px black'
                }
            }
                  },
            <?php } ?>
            ]
      });
   });	
   
   
   $(document).ready(function() {
      chart6 = new Highcharts.Chart({
         chart: {
            renderTo: 'container6',
            type: 'column'
         },   
         title: {
            text: 'Grafik Agenda per Tahun'
         },
         xAxis: {
            categories: ['Agenda']
         },
         yAxis: {
            title: {
               text: 'Jumlah agenda yang diinput'
            }
         },
         series:             
            [
            	{
                      name: 'Tahun',
                      data: [
            <?php 
            $sql   = "SELECT * FROM v_agenda_tahun order by tahun asc";
            $query = mysql_query( $sql )  or die(mysql_error());
            while( $ret = mysql_fetch_array( $query ) ){
            	 	$tahun = $ret['TAHUN']; 
			$total = $ret['TMDPIT'];    
                  ?>
                  
                      [<?php echo $tahun; ?>, <?php echo $total; ?>],
            <?php } ?>
            
             		],
             		dataLabels: {
                enabled: true,
                rotation: 0,
                color: '#FFFFFF',
                align: 'right',
                x: -2,
                y: 20,
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif',
                    textShadow: '0 0 3px black'
                }
            }
             }]
      });
   });
   
   
   //KOPERTIS
   $(document).ready(function() {
      chart7 = new Highcharts.Chart({
         chart: {
            renderTo: 'container7',
            type: 'column'
         },   
        title: {
            text: 'Grafik Agenda per Bulan '
         },
         xAxis: {
            categories: ['JAN', 'FEB' , 'MAR' , 'APR' , 'MEI' , 'JUN' , 'JUL' , 'AGU' , 'SEP' , 'OKT' , 'NOV' , 'DES']
         },
         yAxis: {
            title: {
               text: 'Jumlah agenda yang diinput'
            }
         },
             series:             
            [
            <?php 
            $sql   = "SELECT * FROM v_agenda_bulan where INSTANSI='KOPERTIS' order by tahun asc";
            $query = mysql_query( $sql )  or die(mysql_error());
            while( $ret = mysql_fetch_array( $query ) ){
            	 $tahun = $ret['TAHUN']; 
		 $jan = $ret['JAN'];
		 $feb = $ret['FEB'];
		 $mar = $ret['MAR'];
		 $apr = $ret['APR'];
		 $mei = $ret['MEI'];
		 $jun = $ret['JUN'];
		 $jul = $ret['JUL'];
		 $agu = $ret['AGU'];
		 $sep = $ret['SEP'];
		 $okt = $ret['OKT'];
		 $nov = $ret['NOV'];
		 $des = $ret['DES'];
		 
                  ?>
                  {
                      name: '<?php echo $tahun; ?>',
                      data: [<?php echo $jan; ?>,<?php echo $feb; ?>,<?php echo $mar; ?>,<?php echo $apr; ?>,<?php echo $mei; ?>,<?php echo $jun; ?>,<?php echo $jul; ?>,<?php echo $agu; ?>,<?php echo $sep; ?>,<?php echo $okt; ?>,<?php echo $nov; ?>,<?php echo $des; ?>],
             		dataLabels: {
                enabled: true,
                rotation: 0,
                color: '#FFFFFF',
                align: 'right',
                x: -2,
                y: 20,
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif',
                    textShadow: '0 0 3px black'
                }
            }
                  },
            <?php } ?>
            ]
      });
   });	
   
   
    $(document).ready(function() {
      chart8 = new Highcharts.Chart({
         chart: {
            renderTo: 'container8',
            type: 'column'
         },   
         title: {
            text: 'Grafik Agenda per Tahun'
         },
         xAxis: {
            categories: ['Agenda']
         },
         yAxis: {
            title: {
               text: 'Jumlah agenda yang diinput'
            }
         },
         series:             
            [
            	{
                      name: 'Tahun',
                      data: [
            <?php 
            $sql   = "SELECT * FROM v_agenda_tahun order by tahun asc";
            $query = mysql_query( $sql )  or die(mysql_error());
            while( $ret = mysql_fetch_array( $query ) ){
            	 	$tahun = $ret['TAHUN']; 
			$total = $ret['TKOPERTIS'];    
                  ?>
                  
                      [<?php echo $tahun; ?>, <?php echo $total; ?>],
            <?php } ?>
            
             		],
             		dataLabels: {
                enabled: true,
                rotation: 0,
                color: '#FFFFFF',
                align: 'right',
                x: -2,
                y: 20,
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif',
                    textShadow: '0 0 3px black'
                }
            }
             }]
      });
   });
   
   
   //UMUM
   $(document).ready(function() {
      chart9 = new Highcharts.Chart({
         chart: {
            renderTo: 'container9',
            type: 'column'
         },   
        title: {
            text: 'Grafik Agenda per Bulan '
         },
         xAxis: {
            categories: ['JAN', 'FEB' , 'MAR' , 'APR' , 'MEI' , 'JUN' , 'JUL' , 'AGU' , 'SEP' , 'OKT' , 'NOV' , 'DES']
         },
         yAxis: {
            title: {
               text: 'Jumlah agenda yang diinput'
            }
         },
             series:             
            [
            <?php 
            $sql   = "SELECT * FROM v_agenda_bulan where INSTANSI='UMUM' order by tahun asc";
            $query = mysql_query( $sql )  or die(mysql_error());
            while( $ret = mysql_fetch_array( $query ) ){
            	 $tahun = $ret['TAHUN']; 
		 $jan = $ret['JAN'];
		 $feb = $ret['FEB'];
		 $mar = $ret['MAR'];
		 $apr = $ret['APR'];
		 $mei = $ret['MEI'];
		 $jun = $ret['JUN'];
		 $jul = $ret['JUL'];
		 $agu = $ret['AGU'];
		 $sep = $ret['SEP'];
		 $okt = $ret['OKT'];
		 $nov = $ret['NOV'];
		 $des = $ret['DES'];
		 
                  ?>
                  {
                      name: '<?php echo $tahun; ?>',
                      data: [<?php echo $jan; ?>,<?php echo $feb; ?>,<?php echo $mar; ?>,<?php echo $apr; ?>,<?php echo $mei; ?>,<?php echo $jun; ?>,<?php echo $jul; ?>,<?php echo $agu; ?>,<?php echo $sep; ?>,<?php echo $okt; ?>,<?php echo $nov; ?>,<?php echo $des; ?>],
             		dataLabels: {
                enabled: true,
                rotation: 0,
                color: '#FFFFFF',
                align: 'right',
                x: -2,
                y: 20,
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif',
                    textShadow: '0 0 3px black'
                }
            }
                  },
            <?php } ?>
            ]
      });
   });	
   
    $(document).ready(function() {
      chart10 = new Highcharts.Chart({
         chart: {
            renderTo: 'container10',
            type: 'column'
         },   
         title: {
            text: 'Grafik Agenda per Tahun'
         },
         xAxis: {
            categories: ['Agenda']
         },
         yAxis: {
            title: {
               text: 'Jumlah agenda yang diinput'
            }
         },
         series:             
            [
            	{
                      name: 'Tahun',
                      data: [
            <?php 
            $sql   = "SELECT * FROM v_agenda_tahun order by tahun asc";
            $query = mysql_query( $sql )  or die(mysql_error());
            while( $ret = mysql_fetch_array( $query ) ){
            	 	$tahun = $ret['TAHUN']; 
			$total = $ret['TUMUM'];    
                  ?>
                  
                      [<?php echo $tahun; ?>, <?php echo $total; ?>],
            <?php } ?>
            
             		],
             		dataLabels: {
                enabled: true,
                rotation: 0,
                color: '#FFFFFF',
                align: 'right',
                x: -2,
                y: 20,
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif',
                    textShadow: '0 0 3px black'
                }
            }
             }]
      });
   });
   
</script>

<ul class="nav nav-tabs">
    <li class="active"><a href="#stmik-tab" data-toggle="tab">STMIK/AMIK<i class="fa"></i></a></li>
    <li><a href="#stie-tab" data-toggle="tab">STIE<i class="fa"></i></a></li>
        <li><a href="#mdpits-tab" data-toggle="tab">MDP IT SUPERSTORE<i class="fa"></i></a></li>
    <li><a href="#kopertis-tab" data-toggle="tab">KOPERTIS<i class="fa"></i></a></li>
    <li><a href="#umum-tab" data-toggle="tab">UMUM<i class="fa"></i></a></li>
</ul>


<div class="tab-content">
	<div class="tab-pane active" id="stmik-tab">
	   <div id='container2'></div>
	   <div id='container'></div>
	</div>
	
	<div class="tab-pane" id="stie-tab">
	   <div id='container4' style='width:1120px'></div>
	   <div id='container3' style='width:1120px'></div>
	   
	</div>
	<div class="tab-pane" id="mdpits-tab">
	   <div id='container6' style='width:1120px'></div>
	   <div id='container5' style='width:1120px'></div>
	   
	</div>
	<div class="tab-pane" id="kopertis-tab">
	   <div id='container8' style='width:1120px'></div>
	   <div id='container7' style='width:1120px'></div>
	   
	</div>
	<div class="tab-pane" id="umum-tab">
	   <div id='container10' style='width:1120px'></div>
	   <div id='container9' style='width:1120px'></div>
	   
	</div>
</div>