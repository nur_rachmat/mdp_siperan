<?php if($this->session->flashdata('simpan_ok')){ ?>
    <div class="alert alert-success alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <?=$this->session->flashdata('simpan_ok');?>
    </div>
<?php } ?>
<?php if($this->session->flashdata('simpan_ggl')){ ?>
    <div class="alert alert-danger alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <?=$this->session->flashdata('simpan_ggl');?>
    </div>
<?php } ?>

<div class="claerfix">&nbsp;</div>
<?php if($level != "Ketua") { ?>
<a href="<?=site_url('keuangan/input_bon');?>"><button type="submit" class="btn btn-primary"><i class="icon-plus icon-white"></i> Input Item Bon</button></a>
<a href="<?=site_url('keuangan/input_pendapatan');?>"><button type="submit" class="btn btn-warning"><i class="icon-plus icon-white"></i> Input Item Pendapatan</button></a>
<?php  } ?>
<div class="claerfix">&nbsp;</div>
<div class="alert alert-success" role="alert">
    <h4>
        <i class="icon-tasks"></i> Bon dan Pendapatan
        <span class="label label-danger pull-right"> <?php echo $dt_bp;?> Bon dan Pendapatan</span>
    </h4>
</div>


<table id="table-bp" class="table" data-paging="true"></table>



<!-- Modal -->
<div class="modal fade bs-example-modal-lg" id="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Detail Usulan</h4>
            </div>
            <div class="modal-body">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
