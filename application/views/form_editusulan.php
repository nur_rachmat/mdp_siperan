<?php if($this->session->flashdata('simpan_ggl')){ ?>
    <div class="alert alert-danger alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <?=$this->session->flashdata('simpan_ggl');?>
    </div>
<?php } ?>

<div class="alert alert-success no-print" role="alert">
    <h4><i class="glyphicon glyphicon-pencil"></i> <?=ucfirst($act);?> Usulan</h4>
</div>

<div class="row">
    <div class="col-md-12">
        <?php echo $this->session->flashdata("e_usulan");?>
        <form method="post" id="form_usulan" action="<?=site_url('peran/simpan_usulan')?>" class="form-horizontal">
            <input type="hidden" name="id_usulan" value="<?=$dt_edit['id'];?>" >
            <div class="form-group">
                <label for="inTgl" class="col-sm-3 control-label">Tanggal Usulan</label>
                <div class="input-group col-sm-9">
                    <div class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i> </div>
                    <input type="text" name="tgl_usul" id="inTgl" class="form-control" readonly value="<?=date("Y-m-d", strtotime($dt_edit['tgl_usul']));?>">
                </div>
            </div>

            <div class="form-group has-success">
                <label for="inPengusul" class="col-sm-3 control-label">Pengusul</label>
                <div class="input-group col-sm-9">
                    <div class="input-group-addon"><i class="glyphicon glyphicon-user"></i> </div>
                    <input type="text" name="pengusul" id="inPengusul" class="form-control" readonly value="<?=$this->session->userdata('admin_user')?>">
                    <input type="hidden" name="id_pengusul" value="<?=$dt_edit['pengusul'];?>">
                </div>
            </div>

            <!--<div class="form-group">
                <label for="inAccount" class="col-sm-3 control-label">Account</label>
                <div class="col-sm-9">
                    <div class="input-group  select2-bootstrap-prepend">
						<span class="input-group-btn">
                            <button class="btn btn-default" type="button" data-select2-open="single-prepend-text">
                                <span class="glyphicon glyphicon-search"></span>
                            </button>
						</span>
                        <select id="single-prepend-text" name="account" class="form-control select2-allow-clear select2">
                            <option></option>
                            <optgroup label="Master Account">
                                <?php
/*                                if(count($dt_account)){
                                    foreach ($dt_account as $acc) {
                                        echo "<option value=\"".$acc['kd_account']."\" ".(($dt_edit['kd_account'] == $acc['kd_account']) ? 'selected' : '').">".$acc['account']."</option>";
                                    }
                                }
                                */?>
                            </optgroup>
                        </select>

                        <div class="input-group-btn">
                            <button type="button" class="btn btn-default" aria-label="Help"><span class="glyphicon glyphicon-list-alt"></span></button>
                        </div>

                        <div class="input-group-btn">
                            <button type="button" class="btn btn-success" aria-label="Help"><span class="glyphicon glyphicon-plus"></span></button>
                        </div>
                    </div>
                </div>

            </div>-->

            <div class="form-group">
                <label for="inNilai" class="col-sm-3 control-label">Nilai</label>
                <div class="input-group col-sm-9">
                    <div class="input-group-addon">Rp </div>
                    <input type="number" name="nilai" min="1" id="inNilai" class="form-control" value="<?=$dt_edit['nilai'];?>" required>
                    <div class="input-group-addon">,00</div>
                </div>
                <span id="helpSatuan" class="help-block col-sm-offset-3 col-sm-9"><?=Terbilang($dt_edit['nilai']);?></span>
            </div>
            <div class="form-group">
                <label for="inKeterangan" class="col-sm-3 control-label">Keterangan</label>
                <div class="input-group col-sm-9">
                    <!--<div id="summernote"><?/*=($dt_edit['keterangan']) ? htmlspecialchars_decode($dt_edit['keterangan'], ENT_QUOTES) : '';*/?></div>-->

                    <div class="form-control" id="inKeterangan" required rows="3"></div>
                    <input type="hidden" class="form-control" name="keterangan" id="inKetHTML"  value="<?=($dt_edit['keterangan']) ? $dt_edit['keterangan'] : '';?>" required>
                    <input type="hidden" class="form-control" name="<?=$act;?>" value="<?=$act;?>" required>

                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-offset-3 col-sm-9">
                    <input type="submit" class="btn btn-primary" name="btn_ubah" value="Update Usulan" id="btnAjukan" data-loading-text="Mengirim Usulan...">
                    <!--<button type="submit" name="<?/*=$act;*/?>" value="ubah" id="btnAjukan" data-loading-text="Mengirim Usulan..." class="btn btn-primary" autocomplete="off">Update Usulan</button>-->
                    <a href="<?=site_url('peran');?>" class="btn btn-success">Kembali</a>
                </div>
            </div>
        </form>
    </div>
</div>
