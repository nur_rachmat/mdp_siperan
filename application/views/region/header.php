<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="author" content="Njr">
        <meta name="author" content="rachmat.nur91@staff.mdp.ac.id">

        <meta name="google-signin-scope" content="profile email">
        <meta name="google-signin-client_id" content="685791212141-4etrat3m7q9kr9hhicigthdp97cuilvs.apps.googleusercontent.com">
        <script src="https://apis.google.com/js/platform.js?onload=onLoad" async defer></script>
        <script>
            function onLoad() {
                gapi.load('auth2', function() {
                    gapi.auth2.init();
                });
            }
        </script>

        <title>SIPERAN | Sistem Persetujuan Keuangan</title>
        <link rel="stylesheet" href="<?php echo base_url(); ?>aset/css/bootstrap.css">

        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/footable/css/footable.bootstrap.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/font-awesome/font-awesome.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>data_table/css/dataTables.bootstrap.min.css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>aset/js/jquery/jquery-ui.css" />
        <style type="text/css">
            .navbar-static-top {
                margin-bottom: 19px;
            }
            .lbl-sm{
                padding: 8px 12px;
                line-height: 1.5;
                font-size: 12px;
            }
            @media print
            {
                .no-print, .no-print *
                {
                    display: none !important;
                }
                .well{
                    border: none;
                }
                .alert{
                    border:none;
                }
            }
            td.det-keet img {
                max-width: 700px;
                width: 100%;
            }
            .btn{
                padding: 6px 12px !important;
            }

        </style>

        <!-- include summernote css/js-->
        <link href="<?php echo base_url(); ?>summernote/summernote.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/select2/css/select2.min.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/select2/css/select2-bootstrap.css" rel="stylesheet">

        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/daterangepicker/daterangepicker.css" />

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
	</head>
	
	<body>
        <nav class="navbar navbar-inverse navbar-static-top">
            <div class="container">
                <div class="navbar-header">
                    <span class="navbar-brand"><strong style="font-family: verdana;">SIPERAN</strong></span>
                    <button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#navbar-main">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>
                <div class="navbar-collapse collapse" id="navbar-main">
                    <ul class="nav navbar-nav">
                        <?php $act = $this->uri->segment(2); $level = $this->session->userdata('admin_level');?>

                        <li <?php if($act==''){echo "class='active'";}?> ><a href="<?php echo site_url('peran'); ?>"><i class="icon-home icon-white"> </i> Home</a></li>

                        <?php if($level == 'Administrasi' || $level == 'SuperAdmin' || $level == 'Ketua') {?>
                        <li <?php if($act=='usulan'){echo "class=active";}?>><a href="<?php echo site_url('peran/usulan'); ?>"><i class="icon-tasks icon-white"> </i> Usulan</a></li>
                        <?php } ?>

                        <?php if($level == 'Administrasi' || $level == 'SuperAdmin') {?>
                            <li <?php if($act=='pcash'){echo "class=active";}?>><a href="<?php echo site_url('peran/pcash'); ?>"><i class="glyphicon glyphicon-briefcase icon-white"> </i> Petty Cash</a></li>
                        <?php } ?>

                        <?php if($level == 'Administrasi' || $level == 'SuperAdmin') {?>
                            <li <?php if($act=='nota'){echo "class=active";}?>><a href="<?php echo site_url('peran/nota'); ?>"><i class="glyphicon glyphicon-list-alt icon-white"> </i> Nota</a></li>

                        <?php } ?>



                        <?php if($level == 'Administrasi' || $level == 'Bendahara' || $level == 'SuperAdmin' ) {?>
                            <li <?php if($act=='transfer'){echo "class=active";}?>><a href="<?php echo site_url('peran/transfer'); ?>"><i class="glyphicon glyphicon-transfer"> </i> Permohonan Transfer</a></li>
                        <?php } ?>

                        <?php if($level == 'Yayasan' || $level == 'SuperAdmin'  || $level == 'Ketua' ) {?>
                        <li <?php if($act=='persetujuan'){echo "class=active";}?>><a href="<?php echo site_url('peran/persetujuan'); ?>/"><i class="icon-ok-sign icon-white"> </i> Persetujuan</a></li>
                        <?php } ?>

                        <li <?php if($act=='daftar_usulan'){echo "class=active";}?> class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#" id="themes"><i class="icon-file icon-white"></i> Laporan <span class="caret"></span></a>
                            <ul class="dropdown-menu" aria-labelledby="themes">
                                <li><a tabindex="-1" href="<?php echo site_url('peran/daftar_usulan'); ?>"><i class="icon-list-alt icon-black"> </i> Rekap Usulan</a></li>
                                <li><a tabindex="-1" href="<?php echo site_url('peran/daftar_pettycash'); ?>"><i class="glyphicon glyphicon-briefcase"> </i> Petty Cash</a></li>
                                <li><a tabindex="-1" href="<?php echo site_url('peran/daftar_kategori'); ?>"><i class="glyphicon glyphicon-compressed"> </i> Account (Kategori)</a></li>
                                <!--<li><a tabindex="-1" href="<?php /*echo site_url('peran/rekap_nilaithe'); */?>"><i class="icon-list icon-black"> </i> Rekap Nilai (yang disetujui)</a></li>-->
                            </ul>
                        </li>
                    </ul>

                    <ul class="nav navbar-nav navbar-right">
                        <li>
                            <a href="#"><i class="icon-user icon-white"></i>
                                <?php echo $this->session->userdata('admin_user');?> </a>
                        </li>
                        <li><a href="<?php echo site_url('peran/password'); ?>"><i class="icon-lock icon-white"> </i> Ubah Password</a></li>
                        <li><a href="<?php echo site_url('peran/logout'); ?>" onclick="signOut();"><i class="icon-circle-arrow-right icon-white"> </i> Logout</a></li>
                    </ul>
                </div>
            </div>
        </nav>
        <?php if($act==''){ ?>
        <div class="container">
            <div class="starter-template">
                <h1>SIPERAN</h1>
                <p class="lead">Sitem Persetujuan Keuangan ini ditujukan bagi Bagian Keuangan Kampus untuk mempermudah melakukan pengusulan keuangan.</p>
            </div>
        </div><!-- /.container -->
        <?php } ?>
		<div class="container">
			<div class="row">
				<div class="col-md-12">
                    <div class="well well-large">