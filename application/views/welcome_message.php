<!DOCTYPE html>
<html>
<head>
    <head>
		<title>SIPERAN | Sistem Persetujuan Keuangan</title>
		<meta name="google-signin-scope" content="profile email">
		<meta name="google-signin-client_id" content="685791212141-4etrat3m7q9kr9hhicigthdp97cuilvs.apps.googleusercontent.com">
		<script src="https://apis.google.com/js/platform.js" async defer></script>

		<link rel="stylesheet" href="<?php echo base_url(); ?>aset/css/bootstrap.css" media="screen">
		<link rel="stylesheet" href="<?php echo base_url(); ?>aset/js/jquery/jquery-ui.css" />

		<script src="<?php echo base_url(); ?>aset/js/jquery.min.js"></script>
		<script src="<?php echo base_url(); ?>aset/js/bootstrap.min.js"></script>
		<script src="<?php echo base_url(); ?>aset/js/jquery/jquery-ui.js"></script>
	
	</head>
	<body style="margin-bottom: 0px; background-color:rgba(0,0,255,0.3);">
		<div id="body" style="margin-top:15%; margin-left:10%;" >
			<div class="row">
				<div class="col-md-4 col-sm-10 col-xs-10">
					<div class="panel panel-default">
						<div class="panel-body">
							<h2>Login SIPERAN</h2>
							<form action="<?=site_url('welcome/login');?>" method="post">
								<?=$this->session->flashdata("k")?>
								<div class="input-group input-group-lg" style="margin-bottom:10px">
									<span class="input-group-addon"></span>
									<input type="text" name="u" class="form-control" placeholder="Username" autocomplete="off" autofocus required>
								</div>
								<div class="input-group input-group-lg" style="margin-bottom:10px">
									<span class="input-group-addon"></span>
									<input type="password" name="p" class="form-control" autocomplete="off" placeholder="Password" required>
								</div>
								<div class="row">
									<div class="col-xs-4"><input type="submit" class="btn btn-danger" value="Login"></div>
									<!--<div class="col-xs-2"><p>atau</p></div>
									<div class="col-xs-5"><div class="g-signin2" data-onsuccess="onSignIn"></div></div>-->
								</div>
								<br/>

								<!--<div id="msg_login"></div>-->

								<script>
									var js_base_url = function(){ return "<?php echo base_url();?>"; }
									var path = "<?=site_url('welcome/google_login');?>";
									function onSignIn(googleUser) {
										// Useful data for your client-side scripts:
										var profile = googleUser.getBasicProfile();
										console.log("ID: " + profile.getId()); // Don't send this directly to your server!
										console.log("Name: " + profile.getName());
										console.log("Image URL: " + profile.getImageUrl());
										console.log("Email: " + profile.getEmail());

										// The ID token you need to pass to your backend:
										var id_token = googleUser.getAuthResponse().id_token;
										console.log("ID Token: " + id_token);
										$.post( path, { email : profile.getEmail() }, function( data ) {
											if(data.login == true){
												$("#msg_login").css('display','block').addClass('alert-success').html("Login diterima. Memuat ...");
												$(location).attr('href', js_base_url() + data.msg);
											}else{
												$("#msg_login").html(data.msg);
												signOut();
											}
										}, "json" );

									};

									function signOut() {
										var auth2 = gapi.auth2.getAuthInstance();
										auth2.signOut().then(function () {
											console.log('User signed out.');
										});
									}
								</script>
							</form>
						</div>
						<div class="panel-footer">&copy; 2015 UPT Sistem Informasi</div>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>