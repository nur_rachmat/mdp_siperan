<?php
/**
 * Created by PhpStorm.
 * User: Njr
 * Date: 12/11/2015
 * Time: 1:40 PM
 */
if (!defined('BASEPATH')) exit ('No direct script access allowed');
class Sapi extends CI_Controller
{
    public $folder_upload = "assets/uploads";
    public $tahun = "";

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('akses_helper');
        $this->load->model('Sapi_Model');
        $this->load->library('mailsender', array('smtp_user' => 'siperan@mdp.ac.id', 'smtp_pass' => 'xiper4n2015'));
        $this->tahun = date('Y');
    }

    function index(){

    }

    public function nota($jenis = "all", $filter = "", $filter2 = ""){
        $kas = array('1'=>'STMIK', '2'=>'STIE');
        $data = array();
        if($jenis == "f_tgl") {
            $dt_usulan = $this->Sapi_Model->get_dt_nota($filter);
        }elseif($jenis == "f_kas"){
            $dt_usulan = $this->Sapi_Model->get_dt_nota("",$filter);
        }elseif($jenis == "f_tglkas"){
            $dt_usulan = $this->Sapi_Model->get_dt_nota($filter, $filter2);
        }else{
            $dt_usulan = $this->Sapi_Model->get_dt_nota();
        }

        if(count($dt_usulan) > 0){
            $data['status'] = 'success';
            $datanota = array();
            foreach($dt_usulan as $key=>$val){
                if($val['lock'] == "Y") {
                    $datanota[] = array(
                        'id_nota' => $val['id_nota'],
                        'no_ref' => $val['no_ref'],
                        'tgl_pengajuan' => $val['tgl_buatnota'],
                        'tgl_nota' => $val['tgl_nota'],
                        'account' => $val['kd_account'],
                        'ket_account' => $val['account'],
                        'isi_nota' => $val['keterangan'],
                        'nominal' => $val['nilai'],
                        'kas' =>  $val['nota_kas'],
                        'ket_kas' => $kas[$val['nota_kas']],
                        'url_nota' => 'http://www.mdp.ac.id/siperan/uploads/nota/',
                        'file_nota' => $val['f_nota'],
                        'pembuat' => $this->Sapi_Model->get_accounter($val['accounter']),

                    );
                }
            }
            $data['data']['nota']   = $datanota;
        }else{
            $data['status'] = 'failed';
            $data['msg'] = 'Nota tidak ditemukan silakan gunakan mode filter lainnya';
            $data['data'] = array();
        }

        $cb = $this->input->get('data', TRUE);
        if(substr($cb,0,5) === 'jsonp'){
            $this->output
                ->set_content_type('application/json')
                ->set_output("jsonpdata(".json_encode($data).")");
        }else{
            //echo $cb;
            $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));
        }


    }

    public function pettycash($jenis = "all", $filter = "", $filter2 = ""){
        $kas = array('1'=>'STMIK', '2'=>'STIE');
        $data = array();
        if($jenis == "f_tgl") {
            $dt_usulan = $this->Sapi_Model->get_dt_pcash($filter);
        }elseif($jenis == "f_pcash"){
            $dt_usulan = $this->Sapi_Model->get_dt_pcash_fpcash($filter);
        }elseif($jenis == "f_kas"){
            $dt_usulan = $this->Sapi_Model->get_dt_pcash("",$filter);
        }elseif($jenis == "f_tglkas"){
            $dt_usulan = $this->Sapi_Model->get_dt_pcash($filter, $filter2);
        }else {
            $dt_usulan = $this->Sapi_Model->get_dt_pcash();
        }
        if(count($dt_usulan) > 0){
            $data['status'] = 'success';
            $datanota = array();
            foreach($dt_usulan as $key=>$val){
                if($val['lock'] == "Y") {
                    $datanota[] = array(
                        'id' => $val['id'],
                        'kode_pcash' => $val['kode_pcash'],
                        'tgl_pcash' => $val['tgl_pcash'],
                        'keterangan' => $val['keterangan'],
                        'kas' =>  $val['keterangan'],
                        'ket_kas' => $kas[ $val['kas_owner']],
                        'nominal' => $val['nilai'],
                        'pembuat' => $this->Sapi_Model->get_accounter($val['accounter']),

                    );
                }
            }
            $data['data']['pettycash'] = $datanota;
        }else{
            $data['status'] = 'failed';
            $data['msg'] = 'Petty Cash tidak ditemukan silakan gunakan mode filter lainnya';
            $data['data'] = array();
        }
       // if (!$this->input->is_ajax_request()) {

            $cb = $this->input->get('data', TRUE);
            if(substr($cb,0,5) === 'jsonp'){
                $this->output
                    ->set_content_type('application/json')
                    ->set_output("jsonpdata(".json_encode($data).")");
            }else{
                //echo $cb;
                $this->output
                    ->set_content_type('application/json')
                    ->set_output(json_encode($data));
            }
       // }




    }

    public function account($get = "all"){
        $data = array();
        $dt_account = $this->Sapi_Model->get_dt_account();
        if(count($dt_account) > 0){
            $data['status'] = 'success';
            /*$dataacc = array();
            foreach($dt_account as $key=>$val){
                    $dataacc[] = array(
                        'id' => $val['id'],
                        'kode_pcash' => $val['kode_pcash'],
                        'tgl_pcash' => $val['tgl_pcash'],
                        'keterangan' => $val['keterangan'],
                        'kas' =>  $val['keterangan'],
                        'ket_kas' => $kas[ $val['kas_owner']],
                        'nominal' => $val['nilai'],
                        'pembuat' => $this->Sapi_Model->get_accounter($val['accounter']),

                    );
            }*/
            $data['data']['account']   = $dt_account;
        }

        $cb = $this->input->get('data', TRUE);
        if(substr($cb,0,5) === 'jsonp'){
            $this->output
                ->set_content_type('application/json')
                ->set_output("jsonpdata(".json_encode($data).")");
        }else{
            //echo $cb;
            $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));
        }


    }

    public function nota_account($jenis = "all", $filter = ""){
        $kas = array('1'=>'STMIK', '2'=>'STIE');
        $data = array();
        if($jenis == "f_acc") {
            $filter = str_replace(".",",", $filter);
            $dt_account = $this->Sapi_Model->get_group_account($filter);
        }else{
            $dt_account = $this->Sapi_Model->get_group_account();
        }

        if(count($dt_account) > 0){
            $data['status'] = 'success';
            $datanc = array();
            foreach($dt_account as $keys=>$val){
                $datanc[$keys]['account'] = $dt_account[$keys];
                $dt_usulan = $this->Sapi_Model->get_dt_nota("","",$val['kd_account']);
                $datanota = array();
                foreach($dt_usulan as $key=>$val){
                    if($val['lock'] == "Y") {
                        $datanota[] = array(
                            'id_nota' => $val['id_nota'],
                            'no_ref' => $val['no_ref'],
                            'tgl_pengajuan' => $val['tgl_buatnota'],
                            'tgl_nota' => $val['tgl_nota'],
                            'account' => $val['kd_account'],
                            'ket_account' => $val['account'],
                            'isi_nota' => $val['keterangan'],
                            'nominal' => $val['nilai'],
                            'kas' =>  $val['nota_kas'],
                            'ket_kas' => $kas[$val['nota_kas']],
                            'url_nota' => 'http://www.mdp.ac.id/siperan/uploads/nota/',
                            'file_nota' => $val['f_nota'],
                            'pembuat' => $this->Sapi_Model->get_accounter($val['accounter']),

                        );
                    }
                }
                $datanc[$keys]['nota'] = $datanota;

            }
            $data['data'] = $datanc;
            /*$dataacc = array();
            foreach($dt_account as $key=>$val){
                    $dataacc[] = array(
                        'id' => $val['id'],
                        'kode_pcash' => $val['kode_pcash'],
                        'tgl_pcash' => $val['tgl_pcash'],
                        'keterangan' => $val['keterangan'],
                        'kas' =>  $val['keterangan'],
                        'ket_kas' => $kas[ $val['kas_owner']],
                        'nominal' => $val['nilai'],
                        'pembuat' => $this->Sapi_Model->get_accounter($val['accounter']),

                    );
            }*/

        }



        $cb = $this->input->get('data', TRUE);
        if(substr($cb,0,5) === 'jsonp'){
            $this->output
                ->set_content_type('application/json')
                ->set_output("jsonpdata(".json_encode($data).")");
        }else{
            //echo $cb;
            $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));
        }


    }

    public function pettycash_data($jenis = "all", $filter = "", $filter2 = ""){
        $kas = array('1'=>'STMIK', '2'=>'STIE');

        if($jenis == "f_tgl") {
            $dt_pcash = $this->Sapi_Model->get_dt_pcash($filter);
        }elseif($jenis == "f_kas") {

            $dt_pcash = $this->Sapi_Model->get_dt_pcash("", $filter);
        }elseif($jenis == "f_pcash"){
            $dt_pcash = $this->Sapi_Model->get_dt_pcash_fpcash($filter);

        }elseif($jenis == "f_kode"){
            $dt_pcash = $this->Sapi_Model->get_dt_pcash($filter, $filter2);
        }else{
            $dt_pcash = $this->Sapi_Model->get_dt_pcash();
        }
        $data = array();

        if(count($dt_pcash) > 0){
            $data['status'] = 'success';
            $datapc = array();
            foreach($dt_pcash as $key=>$val){
                if($val['lock'] == "Y") {
                    $datapcash = array(
                        'id' => $val['id'],
                        'kode_pcash' => $val['kode_pcash'],
                        'tgl_pcash' => $val['tgl_pcash'],
                        'keterangan' => $val['keterangan'],
                        'kas' =>  $val['keterangan'],
                        'ket_kas' => $kas[$val['kas_owner']],
                        'nominal' => $val['nilai'],
                        'pembuat' => $this->Sapi_Model->get_accounter($val['accounter']),
                    );

                    $datapc[$key]['pettycash'] = $datapcash;

                    $dt_nota = $this->Sapi_Model->get_dt_nota("",$val['id'],"");
                    //echo $this->db->last_query();
                    $datanota = array();
                    foreach($dt_nota as $keys=>$val){
                        if($val['lock'] == "Y") {
                            $datanota[] = array(
                                'id_nota' => $val['id_nota'],
                                'no_ref' => $val['no_ref'],
                                'tgl_pengajuan' => $val['tgl_buatnota'],
                                'tgl_nota' => $val['tgl_nota'],
                                'account' => $val['kd_account'],
                                'ket_account' => $val['account'],
                                'isi_nota' => $val['keterangan'],
                                'nominal' => $val['nilai'],
                                'kas' =>  $val['nota_kas'],
                                'ket_kas' => $kas[$val['nota_kas']],
                                'url_nota' => 'http://www.mdp.ac.id/siperan/uploads/nota/',
                                'file_nota' => $val['f_nota'],
                                'pembuat' => $this->Sapi_Model->get_accounter($val['accounter']),
                            );
                        }
                    }
                    $datapc[$key]['nota'] = $datanota;
                }
            }

            $data['data'] = $datapc;
        }



        $cb = $this->input->get('data', TRUE);
        if(substr($cb,0,5) === 'jsonp'){
            $this->output
                ->set_content_type('application/json')
                ->set_output("jsonpdata(".json_encode($data).")");
        }else{
            //echo $cb;
            $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));
        }


    }

}