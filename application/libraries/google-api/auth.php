<?php
require_once 'src/Google/autoload.php';

session_start();

$client = new Google_Client();
$client->setAuthConfigFile('client_secrets.json');
$client->setAccessType('offline');
//$client->addScope(Google_Service_Directory);
$client->addScope("https://www.googleapis.com/auth/admin.directory.group.member");
$client->addScope("https://www.googleapis.com/auth/admin.directory.group");

if (isset($_SESSION['access_token']) && $_SESSION['access_token']) {
  // Exchange authorization code for an access token.
  /*if ($client->isAccessTokenExpired()) {
     $client->refreshToken($client->getRefreshToken());
    //$_SESSION['access_token'] = $client->getRefreshToken();
  }*/
  
  //print_r($client->getAccessToken());
  $client->setAccessToken($_SESSION['access_token']);  
  
  
  $service = new Google_Service_Directory($client);
  $member = new Google_Service_Directory_Members();
  
  $list = $service->members->listMembers("berita_simponi@mdp.ac.id", array('maxResults'=>5));
  print_r($list['members']);
} else {
  $redirect_uri = 'http://' . $_SERVER['HTTP_HOST'] . '/google-api/oauth2callback.php';
  header('Location: ' . filter_var($redirect_uri, FILTER_SANITIZE_URL));
}
?>