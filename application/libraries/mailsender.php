<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Mailsender
{
    public $ci      = null;
    public $subject = '';
    public $body    = '';
    public $mail_to = '';
    public $mail_from = '';
    public $mail_cc = '';
    public $smtp_user = '';
    public $smtp_pass = '';

    public function __construct($config)
    {
        $this->ci =& get_instance();  // call CI
        $this->smtp_user = $config['smtp_user'];
        $this->smtp_pass = $config['smtp_pass'];
    }

    function set_vars($data = array())
    {
        if(is_array($data))
        {
            isset($data['mail_from']) ? $this->mail_from = $data['mail_from'] : '';
            isset($data['subject']) ? $this->subject = $data['subject'] : '';
            isset($data['body']) ? $this->body = $data['body'] : '';
            isset($data['mail_to']) ? $this->mail_to = $data['mail_to'] : '';
            isset($data['mail_cc']) ? $this->mail_cc = $data['mail_cc'] : $data['mail_from'];
        }
    }

    // Mail Sender
    function send_mail()
    {
        $this->ci->load->library('email');

        $config['protocol'] = 'smtp';
        $config['smtp_host'] = 'ssl://smtp.gmail.com';
        $config['smtp_port'] = '465';
        $config['smtp_timeout'] = '500';
        $config['smtp_user'] = $this->smtp_user;
        $config['smtp_pass'] = $this->smtp_pass;
        $config['newline'] = "\r\n";
        $config['validation'] = TRUE; // bool whether to validate email or not

        $config['charset'] = 'utf-8';
        $config['wordwrap'] = TRUE;
        $config['mailtype'] = 'html';

        $subject = $this->subject;
        $pesan  = $this->body;
        $to     = $this->mail_to;

        ///$this->ci->email->clear();
        $user = explode("@", $this->mail_from);
        $this->ci->email->initialize($config);
        $this->ci->email->from($this->smtp_user, 'SIPERAN');
        $this->ci->email->reply_to($this->mail_from, $user[0]);
        $this->ci->email->to($to);
        $this->ci->email->cc($this->mail_cc);

        $this->ci->email->subject($subject);
        $this->ci->email->message($pesan);

        if ($this->ci->email->send()) {
            //return true;
            return $this->ci->email->print_debugger();
        } else {
            return $this->ci->email->print_debugger();
        }
    }
}