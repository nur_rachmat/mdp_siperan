                </div>
            </div>
        </div>
    </div>

    <footer class="footer">
        <div class="container">
            <p class="text-muted">&copy; SIPERAN 2015 - Sistem Persetujuan Keuangan</p>
        </div>
    </footer>
    <script src="<?php echo base_url(); ?>data_table/js/jquery.js"></script>
    <script src="<?php echo base_url(); ?>assets/footable/js/footable.js"></script>
    <script src="<?php echo base_url(); ?>aset/js/jquery/jquery-ui.js"></script>
    <script>var base_url = "<?=base_url();?>";</script>
    <script src="<?php echo base_url(); ?>aset/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/moment.min.js"></script>
    <!--<script src="<?php /*echo base_url(); */?>data_table/js/jquery.dataTables.min.js"></script>
    <script src="<?php /*echo base_url(); */?>data_table/js/dataTables.bootstrap.min.js"></script>-->

    <script type="text/javascript" src="<?php echo base_url(); ?>assets/daterangepicker/daterangepicker.js"></script>

    <?php
    $seq = $this->uri->segment(2, 0);
    $seq3 = $this->uri->segment(3);
    if($seq == 'tambah_usulan' || $seq == 'buat_nota'  || $seq == 'ubah_nota' || $seq == 'daftar_pettycash' ||  $seq == 'daftar_kategori' || $seq == 'nota' || $seq == 'pcash' || $seq == 'tambah_pembiayaan' || $this->uri->segment(1, 0) == 'keuangan') :
    ?>
        <script src="<?php echo base_url(); ?>assets/select2/js/select2.min.js"></script>
        <script>

            $( ".select2" ).select2( {
                theme: "bootstrap",
                placeholder: "Pilih Account",
                allowClear : true
            } );

            $( ".select2_pcash" ).select2( {
                theme: "bootstrap",
                placeholder: "Pilih Petty Cash",
                allowClear : true
            } );

            $( ".select2no_ref" ).select2( {
                theme: "bootstrap",
                placeholder: "Pilih Usulan",
                allowClear : true
            } );

            $( "button[data-select2-open]" ).click( function() {
                $( "#" + $( this ).data( "select2-open" ) ).select2( "open" );
            });

            var $eventSelect = $(".select2no_ref");

            $eventSelect.on("select2:unselect", function (e) {
                $("#kokom").collapse('hide');

                //log("select2:select", e);
            });

            $eventSelect.on("select2:select", function (e) {
                log("select2:select", e);
            });
            //$eventSelect.on("select2:unselect", function (e) { log("select2:unselect", e); });

            function log (name, evt) {
                if (!evt) {
                    var args = "{}";
                } else {

                    var args = JSON.stringify(evt.params, function (key, value) {
                        if (value && value.nodeName) return "[DOM node]";
                        if (value instanceof $.Event) return "[$.Event]";
                        return value;
                    });


                   /* console.log(args);
                    var dtref = $(evt.relatedTarget); // Button that triggered the modal
                    var idnota = dtref.data('account'); // Extract info from data-* attributes
                    console.log(idnota);*/
                    var ref_noref = evt.params.data.title;
                    var ref = evt.params.data.id;
                    $.getJSON( "<?=base_url();?>peran/ref_data/" + ref + "/" + ref_noref , function( jdata ) {
                        if(jdata.result == true){
                            var data = jdata.data;
                            //$("#vaccount").val(data.kd_account);

                            $(".select2").val(data.kd_account).trigger("change");

                            /*if(data.kd_account == '' || data.kd_account == undefined){
                                $("#vaccount").prop('readonly', false);
                            }else{
                                $("#vaccount").prop('readonly', true);
                            }*/
                            //$("#id_nota").val(data.id_nota);
                            $("#no_ref").val(data.no_ref);
                            $("#helpket").html("<strong>#"+data.tgl_usulan + "</strong> | "+ data.keterangan_clean);
                            $("#defaultketnota").text(data.keterangannota_dec);
                            $("#helpnilai").html("Rp "+ data.nominal);
                            $("#inNilai").val(data.nilai).trigger('keyup');

                            $("#kokom").collapse('show');


                        }
                    });
                }

            }
        </script>
    <?php
    endif;
    ?>
    <script src="<?php echo base_url(); ?>summernote/summernote.js"></script>

    <?php if($seq == 'usulan') : ?>
        <script>
            $(function	() {
                var status = "<?=$status;?>";
                $('#table-usulandiajukan').footable({
                    "useParentWidth": true,
                    "paging": {
                        "enabled": true,
                        "countFormat": "{CP} dari {TP}"
                    },
                    "filtering": {
                        "enabled": true
                    },
                    "sorting": {
                        "enabled": true
                    },
                    "columns": $.Deferred(function(d){
                        setTimeout(function(){
                            $.get(base_url + 'peran/usulandiajukan_json/columns/'+ status).then(d.resolve, d.reject);
                        }, 3000);
                    }),
                    "rows": $.get(base_url + 'peran/usulandiajukan_json/rows/'+ status)
                });
            });
        </script>
    <?php endif; ?>

    <?php if($seq == 'nota') : ?>
        <script>
            $(function	() {
                $('#table-notakosong').footable({
                    "useParentWidth": true,
                    "paging": {
                        "enabled": true,
                        "countFormat": "{CP} dari {TP}"
                    },
                    "filtering": {
                        "enabled": true
                    },
                    "sorting": {
                        "enabled": true
                    },
                    "columns": $.Deferred(function(d){
                        setTimeout(function(){
                            $.get(base_url + 'peran/nota_json/columns').then(d.resolve, d.reject);
                        }, 3000);
                    }),
                    "rows": $.get(base_url + 'peran/nota_json/rows')
                });
            });
        </script>
    <?php endif; ?>

    <?php if($seq == 'pembiayaan' || $this->uri->segment(1, 0) == 'keuangan') : ?>
        <script>
            $(function	() {
                var status = "<?=$status;?>";
                //var kas = "<?=$status;?>";
                $('#table-pembiayaan').footable({
                    "useParentWidth": true,
                    "paging": {
                        "enabled": true,
                        "countFormat": "{CP} dari {TP}"
                    },
                    "filtering": {
                        "enabled": true
                    },
                    "sorting": {
                        "enabled": true
                    },
                    "columns": $.Deferred(function(d){
                        setTimeout(function(){
                            $.get(base_url + 'keuangan/pembiayaan_json/columns/'+ status).then(d.resolve, d.reject);
                        }, 3000);
                    }),
                    "rows": $.get(base_url + 'keuangan/pembiayaan_json/rows/'+ status)
                });

                $('#table-bp').footable({
                    "useParentWidth": true,
                    "paging": {
                        "enabled": true,
                        "countFormat": "{CP} dari {TP}"
                    },
                    "filtering": {
                        "enabled": true
                    },
                    "sorting": {
                        "enabled": true
                    },
                    "columns": $.Deferred(function(d){
                        setTimeout(function(){
                            $.get(base_url + 'keuangan/bon_pendapatan_json/columns/'+ status).then(d.resolve, d.reject);
                        }, 3000);
                    }),
                    "rows": $.get(base_url + 'keuangan/bon_pendapatan_json/rows/'+ status)
                });
            });
        </script>
    <?php endif; ?>

    <script>
        $(function	() {
            //$('[data-toggle="tooltip"]').tooltip();


            $('#inKeterangan, .inKeterangan').summernote({
                height: 100,                 // set editor height
                minHeight: null,             // set minimum height of editor
                maxHeight: null,             // set maximum height of editor
                callbacks: {
                    onImageUpload: function (files) {
                        // upload image to server and create imgNode...
                        data = new FormData();
                        data.append("file", files[0]);
                        $.ajax({
                            data: data,
                            type: "POST",
                            url: "<?=site_url('peran/saveimage');?>",
                            cache: false,
                            contentType: false,
                            processData: false,
                            success: function(url) {
                                $('#inKeterangan, .inKeterangan').summernote('insertImage', url);
                                $('#inKeterangan, .inKeterangan').summernote('createLink', {
                                    text: 'File Attachment',
                                    url: url,
                                    newWindow: true
                                });
                            }
                        });

                    },
                    onPaste: function(e) {
                        console.log('Called event paste'+ e);
                    },
                    onKeyup: function(e) {
                        //var trg = jQuery(e.target).attr('id');
                        var id = $(this).attr('id');
                        console.log('Key up'+ e.target.id + " " + " " + id);

                        // $("#inKetHTML, .inKetHTML1").val($(this).summernote('code'));
                        $(".inKetHTML"+id).val($(this).summernote('code'));
                    }
                }
            });

            $('#inKeterangan, .inKeterangan').summernote('code', $("#inKetHTML").val());


            $('.datepicker').datepicker({ dateFormat: 'dd-mm-yy' });

            /*$('#dataTable, #dataTable2')
                .removeClass( 'display' )
                .addClass('table table-striped table-bordered table-responsive');*/

            $("input[type=checkbox]").click(function(){
                if($('input[name="setuju[]"]:checked').length > 0){
                    $("#btn_setuju").prop('disabled', false);
                }else{
                    $("#btn_setuju").prop('disabled', true);
                }

                if($('input[name="lock[]"]:checked').length > 0){
                    $("#btn_lock").prop('disabled', false);
                }else{
                    $("#btn_lock").prop('disabled', true);
                }
            });

            $("#check_all").click(function() {
                if ($("#check_all").prop('checked')) {
                    $("input[name='setuju[]']").prop('checked', true);
                    $("#btn_setuju").prop('disabled', false);
                }else{
                    $("input[name='setuju[]']").prop('checked', false);
                    $("#btn_setuju").prop('disabled', true);
                }
            });

            $("#check_all_lock").click(function() {
                if ($("#check_all_lock").prop('checked')) {
                    $("input[name='lock[]']").prop('checked', true);
                    $("#btn_lock").prop('disabled', false);
                }else{
                    $("input[name='lock[]']").prop('checked', false);
                    $("#btn_lock").prop('disabled', true);
                }
            });

            $('#inKeterangan').change();
            $('#inKeterangan').summernote('focus');

            $('#btnAjukan').on('submit', function (event) {

                $(this).button('loading');
                $('#form_usulan :input').prop('readonly', true);
                var nilai = $('#inNilai').val();
                if(nilai.trim()){
                    return false;
                }

                if ($('#inKeterangan').summernote('isEmpty')) {
                    alert("Keterangan harus diisi");
                    return false;
                }

                // business logic...
                //$btn.button('reset')
            });

            $('#inNilai').on('keyup', function () {
                $('#helpSatuan').html(Terbilang($(this).val()));
            });

            $('.footable').footable({
                "useParentWidth": true,
                "paging": {
                    "enabled": true,
                    "countFormat": "{CP} dari {TP}"
                },
                "filtering": {
                    "enabled": true
                },
                "sorting": {
                    "enabled": true
                },
                "columns": {
                    "type": "html"
                }
            });

        <?php if($seq == 'pcash' && $seq3 == 'buat') : ?>

            $('#nota_kas').change(function(){
                var kode_stmik = '<?=$kode_stmik;?>';
                var kode_stie = '<?=$kode_stie;?>';
                if($('#nota_kas').val() == '1'){
                    $('#inKode').val(kode_stmik);
                }else{
                    $('#inKode').val(kode_stie);
                }

            });
        <?php endif; ?>

            /*"columns": $.Deferred(function(d){
             setTimeout(function(){
             $.get(base_url + 'peran/nota_json/columns').then(d.resolve, d.reject);
             }, 3000);
             }),*/

            /*$('#foo-rekap').footable({
                "useParentWidth": true,
                "paging": {
                    "enabled": true
                },
                "filtering": {
                    "enabled": true
                },
                "sorting": {
                    "enabled": true
                }
            });*/



            $('#modal').on('show.bs.modal', function (e) {
                // do something...
                var button = $(e.relatedTarget); // Button that triggered the modal
                var link = button.data('href'); // Extract info from data-* attributes
                //alert(button + " " + link);
                var modal = $(this);
                modal.find('.modal-body').load(link);
            });

            $('#modal_dt_ref').on('show.bs.modal', function (e) {
                // do something...
                var button = $(e.relatedTarget); // Button that triggered the modal
                var link = button.data('href'); // Extract info from data-* attributes
                //alert(button + " " + link);
                var modal = $(this);
                modal.find('.modal-body').load(link);
                $('.footable').trigger('footable_resize');
            });

            $('#modal-reklain').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget); // Button that triggered the modal
                var recipient = button.data('noref'); // Extract info from data-* attributes
                var idusulan = button.data('idusulan'); // Extract info from data-* attributes
                // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
                // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
                var modal = $(this);
                //modal.find('.modal-title').text('New message to ' + recipient)
                //$("#ref-no").val(recipient);
                //alert(recipient);
                var base_url = "<?=base_url();?>";

                modal.find('.modal-body #ref-no').val(recipient);
                modal.find('.modal-content form').prop('action', base_url + 'peran/minta_dana/'+idusulan+'/reklain');
                modal.find('.modal-body #idusulan').val(idusulan);
            });
            <?php $tanggal = explode("-", $this->input->post('periode')); ?>
            var _start = "<?=(($this->input->post('periode')) ? $tanggal[0] : "");?>";
            var _end = "<?=(($this->input->post('periode')) ? $tanggal[1] : "");?>";
            var start   = (_start == "") ? moment().subtract(29, 'days') :  _start;
            var end     = (_end == "") ? moment() : _end;

            function cb(start, end) {
                //$('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
            }

            $('#reportrange').daterangepicker({
                locale: {
                    format: 'YYYY/MM/DD'
                },
                startDate: start,
                endDate: end,
                ranges: {
                    'Hari Ini': [moment(), moment()],
                    'Kemarin': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    '7 Hari Terakhir': [moment().subtract(6, 'days'), moment()],
                    '30 Hari Terakhir': [moment().subtract(29, 'days'), moment()],
                    'Bulan Ini': [moment().startOf('month'), moment().endOf('month')],
                    'Bulan Lalu': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                }
            }, cb);

            cb(start, end);

        });

        function Terbilang(satuan){
            var huruf = [" ", "Satu", "Dua", "Tiga", "Empat", "Lima", "Enam",
                "Tujuh", "Delapan", "Sembilan", "Sepuluh","Sebelas"];
            if (satuan < 12)
                return " " + huruf[Math.floor(satuan)];
            else if (satuan < 20)
                return Terbilang(satuan - 10) + " Belas";
            else if (satuan < 100)
                return Terbilang(satuan / 10) + " Puluh" + Terbilang(satuan % 10);
            else if (satuan < 200)
                return "Seratus" + Terbilang(satuan - 100);
            else if (satuan < 1000)
                return Terbilang(satuan / 100) + " Ratus" + Terbilang(satuan % 100);
            else if (satuan < 2000)
                return "Seribu" + Terbilang(satuan - 1000);
            else if (satuan < 1000000)
                return Terbilang(satuan / 1000) + " Ribu" + Terbilang(satuan % 1000);
            else if (satuan < 1000000000)
                return Terbilang(satuan / 1000000) + " Juta" + Terbilang(satuan % 1000000);
            else if (satuan >= 1000000000)
                return "Angka terlalu Besar";
            else
                return " ";
        }

        function signOut() {
            var auth2 = gapi.auth2.getAuthInstance();
            auth2.signOut().then(function () {
                console.log('User signed out.');
            });
        }

    </script>

</body>

</html>