<?php

/**
 * Created by PhpStorm.
 * User: Njr
 * Date: 7/28/2017
 * Time: 3:44 PM
 */
if (!defined('BASEPATH')) exit ('No direct script access allowed');
class Keuangan extends CI_Controller
{
    public $folder_upload = "assets/uploads";
    public $tahun = "";
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('akses_helper');
        $this->load->model('Siperan_Model');
        $this->load->model('Pembiayaan_Model');
        $this->load->model('Bon_Model');
        $this->load->model('Item_bon_model');
        //$this->load->model('Bon_Model');
        $this->load->library('mailsender', array('smtp_user'=>'siperan@mdp.ac.id', 'smtp_pass'=>'xiper4n2015'));
        $this->tahun = date('Y');

        if ($this->session->userdata('admin_valid') == FALSE && $this->session->userdata('admin_user') == ""){
            redirect("welcome");
        }
    }

    public function index()
    {
        redirect('peran', 'refresh');
    }

    public function pembiayaan($status = null)
    {
        $kas = array('stmik'=>'1', 'stie'=>'2');
        $level = $this->session->userdata('admin_level');

        $status = isset($kas[$status]) ? $status : null;

        if(is_null($status)) {
            $data['dt_pembiayaan'] = $this->Pembiayaan_Model->get_all_pembiayaan();
        }else{
            $data['active'] = $status;
            $data['dt_pembiayaan'] = $this->Pembiayaan_Model->get_all_pembiayaan_where(array('kas'=>$kas[$status]));
        }

        $data['status'] = $status;
        $this->load->view('region/header');
        $this->load->view('keuangan/pembiayaan', $data);
        $this->load->view('region/footer');
    }

    public function tambah_pembiayaan()
    {
        $this->load->library('form_validation');

        $this->form_validation->set_rules('kd_account','Account','required');
        $this->form_validation->set_rules('keterangan','Keterangan','required');
        $this->form_validation->set_rules('nilai','Nilai','required');
        $this->form_validation->set_rules('tgl_nota','Tgl Nota','required');

        if($this->form_validation->run())
        {
            $ket = $this->input->post('keterangan');

            if($_FILES['nota']['name']){
                $nota =  $this->saveNota();
            }else{
                $nota['status'] = true;
                $nota['msg']    = 'nofile';
            }

            $params = array(
                'kd_account' => $this->input->post('kd_account'),
                'kas' => $this->input->post('kas'),
                'no_ref' => $this->input->post('no_ref'),
                'nilai' => $this->input->post('nilai'),
                'tgl_nota' => $tgl_nota= date("Y-m-d", strtotime($this->input->post('tgl_nota'))),
                'tgl_input' => date('Y-m-d H:i:s'),
                'accounter' => $this->session->userdata('admin_id'),
                'keterangan' => htmlspecialchars($ket, ENT_QUOTES),
            );

            if($nota['status'] == true){
                if($nota['msg'] == 'nofile'){
                    $params = $params;
                }else{
                    $params = array_merge($params, array('nota' => $nota['msg']));
                }
            }else{
                $this->session->set_flashdata('simpan_ggl', 'Gagal menyimpan item prmbiayaan. <br/><br/>Note : '.$this->db->_error_message() .'<br/>'.$nota['msg']);
            }


            $this->session->set_flashdata('simpan_ok', 'Item pembiayaan berhasil disimpan');




            $pembiayaan_id = $this->Pembiayaan_Model->add_pembiayaan($params);
            redirect('keuangan/pembiayaan', 'refresh');
        }
        else
        {
            $data['dt_account'] = $this->Siperan_Model->get_dt_account();
            $this->load->view('region/header');
            $this->load->view('keuangan/tambah_pembiayaan', $data);
            $this->load->view('region/footer');
        }
    }

    public function pembiayaan_json($jenis, $kas = null){
        $level = $this->session->userdata('admin_level');

        if($jenis == 'columns'){

            $data = array(
                array("name"=>"no","title"=>"No"),
                array("name"=>"tgl_nota","title"=>"Tgl Nota","type"=>"date", "breakpoints"=>"xs sm"),

                array("name"=>"kd_account","title"=>"Account", "breakpoints"=>"xs sm"),
                array("name"=>"kas","title"=>"Kas", "breakpoints"=>"xs sm"),
                array("name"=>"keterangan","title"=>"Keterangan",  "breakpoints"=>"xs sm"),
                array("name"=>"nilai_nota","title"=>"Nilai"),
                array("name"=>"aksi","title"=>"Aksi","type"=>"html","breakpoints"=>"xs")
            );


            $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));
        }else{
            $data = array();

            if(is_null($kas)) {
                $dt_pembiayaan = $this->Pembiayaan_Model->get_all_pembiayaan();
            }else{
                $kas_b = array('stmik'=>'1', 'stie'=>'2');
                $level = $this->session->userdata('admin_level');

                $kas = isset($kas_b[$kas]) ? $kas_b[$kas] : null;
                $dt_pembiayaan = $this->Pembiayaan_Model->get_all_pembiayaan_where(array('kas'=>$kas));
            }

            foreach($dt_pembiayaan as $key=>$val){
                $data[$key]['no'] = $key+1;

                $ket = strip_tags(htmlspecialchars_decode($val['keterangan']), "<img>");
                //$k_img = preg_replace("/<img[^>]+\>/i", " [ <b>image</b> ] ", $ket);
                //$link_more = " [ <a data-toggle='modal' data-target='#modal' href='#' data-href='".site_url('keuangan/detail_pembiayaan/'.$val['id_pembiayaan'])."' ><i class='glyphicon glyphicon-eye-open'></i> detail</a> ]";
                //$data[$key]['keterangan'] = strip_tags(htmlspecialchars_decode($val['keterangan']));
                $data[$key]['keterangan'] = str_replace(PHP_EOL,"<br>",$val['keterangan']);
                $data[$key]['keterangan'] .= (($val['nota'] == '') ? '' : '<br/><a href="'.base_url('uploads/nota_keuangan/'.$val['nota']).'" target="_blank">Nota</a>');

                $data[$key]['kd_account'] = $val['kd_account'];
                if($val['kas'] === '1'){
                    $kas = "<span class='label label-info'>STMIK</span>";
                }elseif($val['kas'] == '2'){
                    $kas = "<span class='label label-success'>STIE</span>";
                }else{
                    $kas = "<span class='label label-danger'>N/A</span>";
                }
                $data[$key]['kas'] = $kas;

                $act_ubah = " <a href='".site_url('keuangan/ubah_pembiayaan/'.$val['id_pembiayaan'])."'><button class='btn btn-success btn-sm '><i class='glyphicon glyphicon-edit'></i> Ubah</button></a>";

                $act_hapus = " <a href='".site_url('keuangan/hapus_pembiayaan/'.$val['id_pembiayaan'])."'><button class='btn btn-danger btn-sm '><i class='glyphicon glyphicon-remove'></i> Hapus</button></a>";



                $data[$key]['nilai_nota'] = "Rp ".number_format($val['nilai'], 2, ",", ".");
                $data[$key]['tgl_input'] = date('d-m-Y', strtotime($val['tgl_input']));
                $data[$key]['tgl_nota'] = date('d-m-Y', strtotime($val['tgl_nota']));
                $data[$key]['aksi'] = $act_ubah .$act_hapus;

            }

            $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));
        }

    }

    public function hapus_pembiayaan($id_pembiayaan)
    {
        $pembiayaan = $this->Pembiayaan_Model->get_pembiayaan($id_pembiayaan);

        // check if the pembiayaan exists before trying to delete it
        if(isset($pembiayaan['id_pembiayaan']))
        {
            $this->Pembiayaan_Model->delete_pembiayaan($id_pembiayaan);
            $this->session->set_flashdata('simpan_ggl', 'Pembiayaan telah dihapus!');
            redirect('keuangan/pembiayaan');
        }
        else
            show_error('The pembiayaan you are trying to delete does not exist.');
    }

    /*BON dan Pembiayaan*/
    public function bon_pendapatan_json($jenis, $kas = null, $status=null){
        $level = $this->session->userdata('admin_level');

        if($jenis == 'columns'){

            $data = array(
                array("name"=>"no","title"=>"No"),
                array("name"=>"tgl_bon","title"=>"Tgl Bon","type"=>"date", "breakpoints"=>"xs sm"),

                array("name"=>"kd_account","title"=>"Account", "breakpoints"=>"xs sm"),
                array("name"=>"status","title"=>"Status", "breakpoints"=>"xs sm"),
                array("name"=>"kas","title"=>"Kas", "breakpoints"=>"xs sm"),
                array("name"=>"unit","title"=>"Unit", "breakpoints"=>"xs sm"),
                array("name"=>"keterangan","title"=>"Keterangan",  "breakpoints"=>"xs sm"),
                array("name"=>"nilai_nota","title"=>"Nilai"),
                array("name"=>"aksi","title"=>"Aksi","type"=>"html","breakpoints"=>"xs")
            );


            $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));
        }else{
            $data = array();

            if(is_null($kas)) {
                $dt_pembiayaan = $this->Bon_Model->get_all_t_bon();
            }else{
                $kas_b = array('stmik'=>'1', 'stie'=>'2');
                $level = $this->session->userdata('admin_level');

                $kas = isset($kas_b[$kas]) ? $kas_b[$kas] : null;
                $dt_pembiayaan = $this->Pembiayaan_Model->get_all_pembiayaan_where(array('kas'=>$kas, 'status'=>$status ));
            }

            $this->load->model('Unit_model');

            foreach($dt_pembiayaan as $key=>$val){
                $data[$key]['no'] = $key+1;


                $data[$key]['kd_account'] = $val['kd_account'];
                if($val['kas'] === '1'){
                    $kas = "<span class='label label-info'>STMIK</span>";
                }elseif($val['kas'] == '2'){
                    $kas = "<span class='label label-success'>STIE</span>";
                }else{
                    $kas = "<span class='label label-danger'>N/A</span>";
                }
                $data[$key]['status'] = ($val['status'] == 'bon') ? "<span class='label label-info'>BON</span>" : "<span class='label label-success'>Pendapatan</span>";
                $data[$key]['kas'] = $kas;


                $unit =  $this->Unit_model->get_unit($val['unit']);
                $data[$key]['unit'] = isset($unit['nama_unit']) ? $unit['nama_unit'] : 'N/A' ;
                $data[$key]['keterangan'] = str_replace(PHP_EOL,"<br>",$val['keterangan']);
                $data[$key]['keterangan'] .= (($val['bon'] == '') ? '' : '<br/><a href="'.base_url('uploads/nota_keuangan/'.$val['bon']).'" target="_blank">Nota</a>');

                //$act_ubah = " <a href='".site_url('keuangan/ubah_'.$val['status'].'/'.$val['id'])."'><button class='btn btn-success btn-sm '><i class='glyphicon glyphicon-edit'></i> Ubah</button></a>";
                $act_ubah = "";
                $act_hapus = " <a href='".site_url('keuangan/hapus_'.$val['status'].'/'.$val['id'])."'><button class='btn btn-danger btn-sm '><i class='glyphicon glyphicon-remove'></i> Hapus</button></a>";
                $act_item = " <a href='".site_url('keuangan/input_'.$val['status'].'/'.$val['id'].'#item')."'><button class='btn btn-info btn-sm '><i class='glyphicon glyphicon-expand'></i> Lihat Item</button></a>";
                $act_print = " <a target='_blank' href='".site_url('keuangan/cetak/'.$val['status'].'/'.$val['id'])."'><button class='btn btn-primary btn-sm '><i class='glyphicon glyphicon-print'></i> Print</button></a>";



                $data[$key]['nilai_nota'] = "Rp ".number_format($val['nilai'], 2, ",", ".");
                $data[$key]['tgl_input'] = date('d-m-Y', strtotime($val['tgl_buat']));
                $data[$key]['tgl_bon'] = date('d-m-Y', strtotime($val['tgl_bon']));
                $data[$key]['aksi'] = $act_item. $act_ubah .$act_hapus. $act_print;

            }

            $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));
        }

    }

    public function lihat_bp()
    {

        $data['status'] = null;
        $data['dt_bp'] = $this->Bon_Model->get_all_t_bon_count();


        $this->load->view('region/header');
        $this->load->view('keuangan/bon_pendapatan', $data);
        $this->load->view('region/footer');
    }

    public function buat_bon()
    {
        $this->load->library('form_validation');

        $this->form_validation->set_rules('tgl_bon','Tgl Bon','required');
        $this->form_validation->set_rules('nilai','Nilai','required|greater_than[0]');
        $this->form_validation->set_rules('kd_account','Kd Account','required');
        $this->form_validation->set_rules('kas','Kas','required');
        $this->form_validation->set_rules('keterangan','Keterangan','required');

        if($this->form_validation->run())
        {
            if($_FILES['nota']['name']){
                $nota =  $this->saveNota();
            }else{
                $nota['status'] = true;
                $nota['msg']    = 'nofile';
            }

            $params = array(
                'status' => 'bon',
                'kas' => $this->input->post('kas'),
                'unit' => $this->input->post('unit'),
                'tgl_bon' => date("Y-m-d", strtotime($this->input->post('tgl_bon'))),
                'nilai' => $this->input->post('nilai'),
                'accounter' => $this->session->userdata('admin_id'),
                'kd_account' =>  $this->input->post('kd_account'),
                'keterangan' => htmlspecialchars($this->input->post('keterangan'), ENT_QUOTES)
            );

            $do_simpan = false;
            if($nota['status'] == true){
                if($nota['msg'] == 'nofile'){
                    $params = $params;
                }else{
                    $params = array_merge($params, array('bon' => $nota['msg']));
                }
                $do_simpan = true;
            }else{
                $this->session->set_flashdata('simpan_ggl', 'Gagal mengupload nota bon/pendapatan. <br/><br/>Note : '.$this->db->_error_message() .'<br/>'.$nota['msg']);
            }

            if($do_simpan == true){
                $this->session->set_flashdata('simpan_ok', 'Master Bon berhasil disimpan');

                $t_bon_id = $this->Bon_Model->add_t_bon($params);
            }


            redirect('keuangan/lihat_bp');
        }
        else
        {
            $this->load->model('Unit_model');
            $data['all_unit'] = $this->Unit_model->get_all_unit();

            $data['dt_account'] = $this->Siperan_Model->get_dt_account();
            $data['dt_bp'] = $this->Bon_Model->get_all_t_bon();
            $data['active'] = 'bon';
            $this->load->view('region/header');
            $this->load->view('keuangan/tambah_bp', $data);
            $this->load->view('region/footer');
        }
    }

    public function buat_pendapatan()
    {
        $this->load->library('form_validation');

        $this->form_validation->set_rules('tgl_bon','Tgl Bon','required');
        $this->form_validation->set_rules('nilai','Nilai','required|greater_than[0]');
        $this->form_validation->set_rules('kd_account','Kd Account','required');
        $this->form_validation->set_rules('kas','Kas','required');
        $this->form_validation->set_rules('keterangan','Keterangan','required');

        if($this->form_validation->run())
        {
            $params = array(
                'status' => 'pendapatan',
                'kas' => $this->input->post('kas'),
                'unit' => $this->input->post('unit'),
                'tgl_bon' => date("Y-m-d", strtotime($this->input->post('tgl_bon'))),
                'nilai' => $this->input->post('nilai'),
                'accounter' => $this->session->userdata('admin_id'),
                'kd_account' =>  $this->input->post('kd_account'),
                'keterangan' => htmlspecialchars($this->input->post('keterangan'), ENT_QUOTES)
            );

            $this->session->set_flashdata('simpan_ok', 'Master Pendapatan berhasil disimpan');

            $t_bon_id = $this->Bon_Model->add_t_bon($params);
            redirect('keuangan/input_pendapatan/'.$t_bon_id);
        }
        else
        {

            $this->load->model('Unit_model');
            $data['all_unit'] = $this->Unit_model->get_all_unit();

            $data['dt_account'] = $this->Siperan_Model->get_dt_account();
            $data['dt_bp'] = $this->Bon_Model->get_all_t_bon();
            $data['active'] = 'pendapatan';
            $this->load->view('region/header');
            $this->load->view('keuangan/tambah_bp', $data);
            $this->load->view('region/footer');
        }
    }

    public function hapus_bon($id_bon = null)
    {
        $pembiayaan = $this->Bon_Model->get_t_bon($id_bon);

        // check if the pembiayaan exists before trying to delete it
        if(isset($pembiayaan['id']))
        {
            $this->Bon_Model->delete_t_bon($id_bon);
            $this->session->set_flashdata('simpan_ggl', 'Master Bon/Pendapatan telah dihapus!');
            redirect('keuangan/lihat_bp');
        }
        else
            show_error('The Bon/Pendapatan you are trying to delete does not exist.');
    }

    public function hapus_pendapatan($id_bon = null)
    {
        $this->hapus_bon($id_bon);
    }


    /*Item Bon*/
    public function input_bon($id_bon = null, $data = array())
    {
        $this->load->library('form_validation');

        $this->form_validation->set_rules('kd_account','Kd Account','required');
        $this->form_validation->set_rules('tgl_bon','Tgl Bon','required');
        $this->form_validation->set_rules('nilai','Nilai','required');
        $this->form_validation->set_rules('keterangan','Keterangan','required');

        if($this->form_validation->run())
        {

            if($_FILES['nota']['name']){
                $nota =  $this->saveNota();
            }else{
                $nota['status'] = true;
                $nota['msg']    = 'nofile';
            }

            $params = array(
                'id_bon' => $this->input->post('id_bon'),
                'kd_account' => $this->input->post('kd_account'),
                'kas' => $this->input->post('kas'),
                'unit' => $this->input->post('unit'),
                'tgl_bon' => date("Y-m-d", strtotime($this->input->post('tgl_bon'))),
                'nilai' => $this->input->post('nilai'),
                'accounter' => date("Y-m-d", strtotime($this->input->post('tgl_bon'))),
                'keterangan' => htmlspecialchars($this->input->post('keterangan'), ENT_QUOTES),
            );

            $do_simpan = false;
            if($nota['status'] == true){
                if($nota['msg'] == 'nofile'){
                    $params = $params;
                }else{
                    $params = array_merge($params, array('bon' => $nota['msg']));
                }
                $do_simpan = true;
            }else{
                $this->session->set_flashdata('simpan_ggl', 'Gagal mengupload nota bon/pendapatan. <br/><br/>Note : '.$this->db->_error_message() .'<br/>'.$nota['msg']);
            }

            if($do_simpan == true){
                $this->session->set_flashdata('simpan_ok', 'Item Bon/Pendapatan berhasil ditambahkan');

                $item_bon_id = $this->Item_bon_model->add_item_bon($params);
            }

            redirect('keuangan/input_bon/'.$id_bon, "refresh");
        }
        else
        {
            $this->load->model('Bon_model');
            $data['all_t_bon'] = $this->Bon_model->get_all_t_bon();

            $this->load->model('Unit_model');
            $data['all_unit'] = $this->Unit_model->get_all_unit();

            $data['dt_account'] = $this->Siperan_Model->get_dt_account();
            $data['id_bon'] = (is_null($id_bon)) ? null : $id_bon;
            $data['dt_bon'] = (is_null($id_bon)) ? null : $this->Bon_Model->get_t_bon($id_bon);
            $data['item_bon'] = (is_null($id_bon)) ? null : $this->Item_bon_model->get_all_item_bon_where(array('id_bon' => $id_bon));

            $data['active'] = (isset($data['active']) && $data['active'] != 'bon') ? $data['active'] :  'bon';

            $this->load->view('region/header');
            $this->load->view('keuangan/input_item_bp', $data);
            $this->load->view('region/footer');
        }
    }

    public function input_pendapatan($id_bon = null){
        $data['active'] ='pendapatan';
        $this->input_bon($id_bon, $data);
    }


    public function cetak($data= 'bon', $id='')
    {
        $datas['p'] = $data;
        $bon =$this->Bon_Model->get_t_bon($id);
        if($data == "bon"){
            $datas['dt_'.$data] = $this->Bon_Model->get_t_bon($id);
            $datas['item_'.$data] = $this->Item_bon_model->get_all_item_bon_where(array('id_bon' => $id));
        }

        if($data == "pendapatan"){
            $datas['dt_'.$data] = $this->Bon_Model->get_t_bon($id);
            $datas['item_'.$data] = $this->Item_bon_model->get_all_item_bon_where(array('id_bon' => $id));
        }



        //$datas['pk2'] = ($kas == '1') ? "Yulistia, S.Kom., M.T.I." : "Megawati, SE., M.Si";
        $datas['bak'] = "Kathryn Sugara, SE., M.Si";
        $datas['kas'] = ($bon['kas'] == '1') ? "STMIK" : "STIE";
        $this->load->view('print/print_'.$data, $datas);
    }

    /*
     * Editing a item_bon
     */
    function edit($id)
    {
        // check if the item_bon exists before trying to edit it
        $data['item_bon'] = $this->Item_bon_model->get_item_bon($id);

        if(isset($data['item_bon']['id']))
        {
            $this->load->library('form_validation');

            $this->form_validation->set_rules('kd_account','Kd Account','required');
            $this->form_validation->set_rules('tgl_bon','Tgl Bon','required');
            $this->form_validation->set_rules('nilai','Nilai','required');
            $this->form_validation->set_rules('keterangan','Keterangan','required');

            if($this->form_validation->run())
            {
                $params = array(
                    'id_bon' => $this->input->post('id_bon'),
                    'kd_account' => $this->input->post('kd_account'),
                    'kas' => $this->input->post('kas'),
                    'unit' => $this->input->post('unit'),
                    'tgl_bon' => $this->input->post('tgl_bon'),
                    'nilai' => $this->input->post('nilai'),
                    'accounter' => $this->input->post('accounter'),
                    'keterangan' => $this->input->post('keterangan'),
                    'bon' => $this->input->post('bon'),
                    'tgl_buat' => $this->input->post('tgl_buat'),
                );

                $this->Item_bon_model->update_item_bon($id,$params);
                redirect('item_bon/index');
            }
            else
            {
                $this->load->model('Bon_model');
                $data['all_t_bon'] = $this->Bon_model->get_all_t_bon();

                $this->load->model('Unit_model');
                $data['all_unit'] = $this->Unit_model->get_all_unit();

                $data['_view'] = 't_item_bon/edit';
                $this->load->view('layouts/main',$data);
            }
        }
        else
            show_error('The item_bon you are trying to edit does not exist.');
    }

    /*
     * Deleting item_bon
     */
    function hapus_item($id)
    {
        $item_bon = $this->Item_bon_model->get_item_bon($id);

        // check if the item_bon exists before trying to delete it
        if(isset($item_bon['id']))
        {
            $this->Item_bon_model->delete_item_bon($id);
            $this->session->set_flashdata('simpan_ggl', 'Item Bon/Pendapatan telah dihapus!');

            redirect($this->agent->referrer(), 'refresh');
        }
        else
            show_error('The item_bon you are trying to delete does not exist.');
    }

    /*
     * Editing a pembiayaan
     */
    function ubah_pembiayaan($id_pembiayaan)
    {
        // check if the pembiayaan exists before trying to edit it
        $data['pembiayaan'] = $this->Pembiayaan_Model->get_pembiayaan($id_pembiayaan);

        if(isset($data['pembiayaan']['id_pembiayaan']))
        {
            $this->load->library('form_validation');

            $this->form_validation->set_rules('kd_account','Account','required');
            $this->form_validation->set_rules('keterangan','Keterangan','required');
            $this->form_validation->set_rules('nilai','Nilai','required');
            $this->form_validation->set_rules('tgl_nota','Tgl Nota','required');

            if($this->form_validation->run())
            {
                $ket = $this->input->post('keterangan');
                $params = array(
                    'kd_account' => $this->input->post('kd_account'),
                    'kas' => $this->input->post('kas'),
                    'nilai' => $this->input->post('nilai'),
                    'tgl_nota' => $this->input->post('tgl_nota'),
                    'nota' => $this->input->post('nota'),
                    'accounter' => $this->session->userdata('admin_id'),
                    'keterangan' => htmlspecialchars($ket, ENT_QUOTES),
                );

                if($_FILES['nota']['name']){
                    $nota =  $this->saveNota();
                }else{
                    $nota['status'] = true;
                    $nota['msg']    = 'nofile';
                }

                if($nota['status'] == true){
                    if($nota['msg'] == 'nofile'){
                        $params = $params;
                    }else{
                        $params = array_merge($params, array('nota' => $nota['msg']));
                    }
                }else{
                    $this->session->set_flashdata('simpan_ggl', 'Gagal mengubah item pembiayaan. <br/><br/>Note : '.$this->db->_error_message() .'<br/>'.$nota['msg']);
                }

                $this->Pembiayaan_Model->update_pembiayaan($id_pembiayaan,$params);
                $this->session->set_flashdata('simpan_ok', 'Item pembiayaan berhasil diubah');
                redirect('keuangan/pembiayaan', 'refresh');
            }
            else
            {
                $data['dt_account'] = $this->Siperan_Model->get_dt_account();
                $this->load->view('region/header');
                $this->load->view('keuangan/ubah_pembiayaan', $data);
                $this->load->view('region/footer');
            }
        }
        else
            show_error('The pembiayaan you are trying to edit does not exist.');
    }


    public function saveNota()
    {

        $path = 'uploads/nota_keuangan/';
        is_dir($path) || mkdir($path, '0777', true);
        $config['upload_path'] = $path;
        $config['allowed_types'] = 'jpg|png|gif|pdf|doc|docx';
        $config['max_size']	= '102400';
        $config['remove_spaces'] = 'true';

        $this->load->library('upload', $config);

        if ($this->upload->do_upload('nota'))
        {
            $upload_data = $this->upload->data();
            return array('status'=>true ,'msg' => $upload_data['file_name']);
        }else{
            $gagal= $this->upload->display_errors();
            return array('status'=>false ,'msg' => strip_tags($gagal));
        }

    }
}