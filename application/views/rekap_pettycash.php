<div class="alert alert-info no-print" role="alert">
    <h4><i class="glyphicon glyphicon-file"></i> Petty Cash STMIK dan STIE MDP</h4>
</div>

<form method="post" action="<?=site_url('peran/daftar_pettycash')?>" class="form-inline no-print">
    <div class="form-group">
        <!--<label for="inTgl" class="col-sm-1 control-label text-right">Kas</label>-->

        <label for="inTgl" class="col-sm-2 control-label text-right">Petty Cash</label>
        <div class="input-group col-sm-8">
            <div class="input-group-addon"><i class="glyphicon glyphicon-briefcase"></i></div>
            <select id="inPcash" name="pettycash" required class="form-control select2-allow-clear select2_pcash">
                <optgroup label="Petty Cash">
                    <?php
                    $dt_pcash = $this->Siperan_Model->get_dt_pcash('Y');
                    if(count($dt_pcash)){
                        $ko = array('1'=>'STMIK', '2'=>'STIE');
                        foreach ($dt_pcash as $pcash) {
                                echo "<option value=\"".$pcash['id']."\" >".date('d-m-Y', strtotime($pcash['tgl_pcash']))." # ".$pcash['kode_pcash']." # ".$ko[$pcash['kas_owner']]." # Rp ".number_format($pcash['nilai'], 2, ",", ".")."</option>";
                        }
                    }
                    ?>
                </optgroup>
            </select>
        </div>


        <div class="col-sm-1">
            <button type="submit" name="cari" value="Cari" class="btn btn-info">Cari</button>
        </div>
    </div>
</form>
<div class="clearfix">&nbsp;</div>
<?php
if($this->input->post()){
    $kode = $this->input->post('pettycash', true);

    $dt_pettycash = $this->Siperan_Model->get_nota_pcash($kode);
    /*
    $this->load->view('pop_nota_pcash', $data);*/

    if(count($dt_pettycash) > 0){
        /*
        $dt_pcash = $res->result_array();*/
        /*if($dari != $sampai){
            //echo $this->db->last_query();
            echo '<p class="alert alert-success text-center"><strong>Daftar Usulan Keuangan '.$s_kas.' Tanggal '. date('d-m-Y', strtotime($dari)) .' s.d '. date('d-m-Y', strtotime($sampai)) ."</strong></p>";
        }else{
            //echo $this->db->last_query();
            echo '<p class="alert alert-success text-center"><strong>Daftar Usulan Keuangan '.$s_kas.' Tanggal '. date('d-m-Y', strtotime($dari)) ."</strong></p>";
        }*/
        $pc = $this->Siperan_Model->get_pcash($kode);
        echo '<p class="alert alert-success text-center"><strong>Daftar Nota Berdasarkan Kode Petty Cash '.$pc['kode_pcash'].'</strong></p>';

    }else{
       // echo $this->db->last_query();
        $dt_pettycash = array();
    }
}
?>


<table class="table table-striped footable" id="foo-modal">
    <thead>
    <tr>
        <th>No. Ref</th>
        <th>Tgl. Nota</th>
        <th>Account</th>

        <th data-breakpoints="xs sm md lg" data-type="html">Keterangan</th>
        <th data-breakpoints="xs sm md lg" data-type="html">Nota</th>
        <th>Nominal</th>
    </tr>
    </thead>

    <?php
    if(count($dt_pettycash) > 0){
        echo "<tbody>";
        $total_nilai = 0;
        foreach($dt_pettycash as $row){
            echo "<tr>";

            echo "<td>".$row['no_ref']."</td>";
            echo "<td>".date("d-M-Y", strtotime($row['tgl_nota']))."</td>";
            echo "<td>".$row['kd_account']. " - ".$row['account']."</td>";



            echo "<td class=\"det-keet\">".htmlspecialchars_decode($row['keterangan'], ENT_QUOTES)."</td>";

            echo "<td>".(($row['f_nota'] == '') ? 'Tidak Ada' : '<a href="'.base_url('uploads/nota/'.$row['f_nota']).'" target="_blank">Nota</a>')."</td>";
            echo "<td title='".Terbilang($row['nilai'])." Rupiah'>Rp ".number_format($row['nilai'], 2, ",", ".")."</td>";
            echo "</tr>";
            $total_nilai = $row['nilai'] + $total_nilai;
        }
        echo "</tbody>";
    }
    ?>
</table>
<?php if(count($dt_pettycash) > 0) :  ?>
    <h4> Total Nominal : <span class="pull-right"> Rp <?=number_format($total_nilai, 2, ",", ".");?></span></h4>
    <span class="text-muted"><?=Terbilang($total_nilai);?> Rupiah</span>
<?php endif; ?>

<script src="<?php echo base_url(); ?>data_table/js/jquery.js"></script>
<script src="<?php echo base_url(); ?>assets/footable/js/footable.js"></script>
<script>
    jQuery(function($){
        $('#foo-rekap').footable({
            "useParentWidth": true,
            "paging": {
                "enabled": true
            },
            "filtering": {
                "enabled": true
            },
            "sorting": {
                "enabled": true
            }
        });
    });
</script>

<?php $show  = false; if($show){ ?>
<div class="well">
    <h4>Petunjuk penarikan data menggunakan SAPI (SIPERAN API)</h4>
    <ol>
        <ul>
            <li>Data response berupa JSON dengan format Seperti berikut : <br/>
                <a href ="<?=base_url('sapi/pettycash');?>" target="_blank"><span class="glyphicon glyphicon-check"></span> Json Petty Cash</a><br/>
                <ul>
                    <li>Link ambil semua data <code><?=site_url('sapi/pettycash');?></code></li>
                    <li>Link filter berdasar Tanggal Nota <code><?=site_url('sapi/pettycash/f_tgl/YYYY-MM-DD');?></code></li>
                    <li>Link filter berdasar Kode PettyCash <code><?=site_url('sapi/pettycash/f_pcash/KODEPETTYCASH');?></code></li>
                    <li>Link filter berdasar Tanggal dan Kas <code><?=site_url('sapi/pettycash/f_tglkas/YYYY-MM-DD/KODE_KAS');?></code></li>
                </ul>
                <a href ="<?=base_url('sapi/nota');?>" target="_blank"><span class="glyphicon glyphicon-check"></span>  Json Nota</a><br/>
                <ul>
                    <li>Link ambil semua data <code><?=site_url('sapi/nota');?></code></li>
                    <li>Link filter berdasar Tanggal Nota <code><?=site_url('sapi/nota/f_tgl/YYYY-MM-DD');?></code></li>
                    <!--<li>Link filter berdasar KAS <code><?/*=site_url('sapi/nota/f_kas/KODE_KAS');*/?></code></li>
                    <li>Link filter berdasar Tanggal dan Kas <code><?/*=site_url('sapi/nota/f_tglkas/YYYY-MM-DD/KODE_KAS');*/?></code></li>-->
                </ul>
                <a href ="<?=base_url('sapi/pettycash_data');?>" target="_blank"><span class="glyphicon glyphicon-check"></span> Json Petty Cash + Nota</a><br/>
                <ul>
                    <li>Link ambil semua data <code><?=site_url('sapi/pettycash_data');?></code></li>
                    <li>Link filter berdasar Tanggal Nota <code><?=site_url('sapi/pettycash_data/f_tgl/YYYY-MM-DD');?></code></li>
                    <li>Link filter berdasar Kode PettyCash <code><?=site_url('sapi/pettycash_data/f_pcash/KODEPETTYCASH');?></code></li>
                   <!-- <li>Link filter berdasar KAS <code><?/*=site_url('sapi/pettycash_data/f_kas/KODE_KAS');*/?></code></li>
                    <li>Link filter berdasar Tanggal dan Kas <code><?/*=site_url('sapi/pettycash_data/f_tglkas/YYYY-MM-DD/KODE_KAS');*/?></code></li>-->
                </ul>
                <!--<a href ="<?/*=base_url('sapi/per_pettycash');*/?>" target="_blank"><span class="glyphicon glyphicon-check"></span> Json Petty Cash </a><br/>-->

                <a href ="<?=base_url('sapi/nota_account');?>" target="_blank"><span class="glyphicon glyphicon-check"></span> Json Nota Per Account</a><br/>
                <ul>
                    <li>Link ambil semua data <code><?=site_url('sapi/nota_account');?></code></li>
                    <li>Link filter berdasar Account<code><?=site_url('sapi/nota_account/f_acc/KODE_ACCOUNT');?></code></li>
                </ul>
                <a href ="<?=base_url('sapi/account');?>" target="_blank"><span class="glyphicon glyphicon-check"></span> Json Account</a><br/>
            </li>

            <li>Format Tanggal yaitu YYYY-MM-DD, contoh tanggal hari ini <strong><?=date('Y-m-d');?></strong></li>
            <li>KODE_KAS yaitu <strong>1</strong> untuk STMIK dan <strong>2</strong> untuk STIE</li>
            <li>Comma delimiter pad KODE_ACCOUNT harus diraubah menjadi <strong>Titik (.)</strong> sehingga <strong>610,901</strong> menjadi <strong>610.901</strong></li>
        </ul>
    </ol>
</div>
<?php } ?>