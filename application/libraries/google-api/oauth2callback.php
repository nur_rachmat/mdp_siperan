<?php
require_once 'src/Google/autoload.php';

session_start();

$client = new Google_Client();
$client->setAuthConfigFile('client_secrets.json');
$client->setAccessType('offline');
$client->setRedirectUri('http://' . $_SERVER['HTTP_HOST'] . '/google-api/oauth2callback.php');
//$client->addScope(Google_Service_Directory::);
$client->addScope("https://www.googleapis.com/auth/admin.directory.group.member");
$client->addScope("https://www.googleapis.com/auth/admin.directory.group");

if (! isset($_GET['code'])) {
  $auth_url = $client->createAuthUrl();
  header('Location: ' . filter_var($auth_url, FILTER_SANITIZE_URL));
} else {
  $client->authenticate($_GET['code']);
  $_SESSION['access_token'] = $client->getAccessToken();
  $redirect_uri = 'http://' . $_SERVER['HTTP_HOST'] . '/google-api/auth.php';
  header('Location: ' . filter_var($redirect_uri, FILTER_SANITIZE_URL));
}
?>