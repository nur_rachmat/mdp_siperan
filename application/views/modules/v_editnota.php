<form method="post" enctype="multipart/form-data" id="form_nota"  action="<?=site_url('peran/simpan_nota')?>" class="form-horizontal">
    <input type="hidden" name="tgl_lama" value="<?=date("Y-m-d", strtotime($tgl_nota));?>">

    <div class="form-group">
        <label for="inTgl" class="col-sm-3 control-label">Tgl Nota</label>
        <div class="input-group col-sm-4">

            <div class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></div>
            <input type="text" required name="tgl_nota" value="<?=date("d-m-Y", strtotime($tgl_nota));?>"  class="form-control datepicker" id="inTgl" >
        </div>

        <label for="nota_kas" class="col-sm-1 control-label"> Kas</label>
        <div class="input-group col-sm-3">
            <select required class="form-control" name="nota_kas">
                <option value="1" <?=(($nota_kas == 1) ? "selected" : "");?>>STMIK</option>
                <option value="2" <?=(($nota_kas == 2) ? "selected" : "");?>>STIE</option>
            </select>
        </div>
    </div>

    <div class="form-group">
        <label for="inAccount" class="col-sm-3 control-label">No. Ref</label>
        <div class="input-group col-sm-9">

            <input type="text" name="no_ref" value="<?=$dt_edit['no_ref'];?>" required class="form-control" readonly>
        </div>
        <?php
        $ket = strip_tags(htmlspecialchars_decode($dt_edit['keterangan']), "<img>");
        $k_img = preg_replace("/<img[^>]+\>/i", " [ <b>image</b> ] ", $ket);
        $link_more = " [ <a data-toggle='modal' data-target='#modal' href='#' data-href='".site_url('peran/detail_usulan/'.$dt_edit['id'])."' ><i class='glyphicon glyphicon-eye-open'></i> detail</a> ]";
        $ket = substr($k_img, 0, 100).((strlen($k_img) > 100) ? $link_more : "");
        ?>
        <span id="helpket" class="help-block col-sm-offset-3 col-sm-9"><?=$ket;?></span>
        <span id="helpnilai" style="color: #468847 !important;" class="help-block col-sm-offset-3 col-sm-9"><?=number_format($dt_edit['nilai'], 2, ",", ".");?></span>
    </div>

    <div class="form-group">
        <label for="vaccount" class="col-sm-3 control-label">Account</label>
        <div class="col-sm-9">
            <div class="input-group  select2-bootstrap-prepend">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button" data-select2-open="single-prepend-text">
                                        <span class="glyphicon glyphicon-search"></span>
                                    </button>
                                </span>
                <select id="single-prepend-text" name="account" required class="form-control select2-allow-clear select2">
                    <option></option>
                    <optgroup label="Account">
                        <?php
                        if(count($dt_account)){
                            foreach ($dt_account as $acc) {
                                echo "<option value=\"".$acc['kd_account']."\" ".(($acc['kd_account'] == $dt_edit['kd_account']) ? 'selected' : '').">".$acc['account']."</option>";
                            }
                        }
                        ?>
                    </optgroup>
                </select>
                <div class="input-group-btn">
                    <button type="button" class="btn btn-default" aria-label="Help"><span class="glyphicon glyphicon-list-alt"></span></button>
                </div>
            </div>
        </div>
    </div>

    <div class="form-group">
        <label for="inKeterangan" class="col-sm-3 control-label">Keterangan</label>
        <div class="input-group col-sm-9">
            <textarea class="form-control" name="keterangan" rows="3"><?=htmlspecialchars_decode($dt_edit['notes'], ENT_QUOTES);?></textarea>
        </div>
    </div>

    <div class="form-group">
        <label for="inNilai" class="col-sm-3 control-label">Nota</label>
        <div class="input-group col-sm-9">
            <input type="file" name="nota">
        </div>
        <?php if($dt_edit['f_nota'] != '') {?>
            <span class="help-block col-sm-offset-3 col-sm-9">
                                <a href="<?=base_url('uploads/nota/'.$dt_edit['f_nota']);?>" target="_blank">Bukti Nota</a>
                            </span>
        <?php } ?>

        <span class="help-block col-sm-offset-3 col-sm-9">Nota dapat berupa gambar atau file pdf</span>
    </div>

    <div class="form-group">
        <div class="col-sm-offset-3 col-sm-9">
            <button type="submit" name="update" value="Update" data-loading-text="Update Nota..." class="btn btn-warning" autocomplete="off">Update</button>
        </div>
    </div>
</form>