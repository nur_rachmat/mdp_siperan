
<table class="table table-striped " id="foo-modal">
    <thead>
    <tr>
        <th>No. Ref</th>
        <th>Tgl. Nota</th>
        <th>Account</th>
        <th>Nilai</th>
        <th data-breakpoints="xs sm md lg" data-type="html">Keterangan</th>
        <th data-breakpoints="xs sm md lg" data-type="html">Bukti Nota</th>
    </tr>
    </thead>

    <?php
    if(count($dt_nota) > 0){
        echo "<tbody>";
        $total_nilai = 0;
        foreach($dt_nota as $row){
            echo "<tr>";

            echo "<td>".$row['no_ref']."</td>";
            echo "<td>".date("d-M-Y", strtotime($row['tgl_nota']))."</td>";
            echo "<td>".$row['kd_account']. " - ".$row['account']."</td>";

            echo "<td title='".Terbilang($row['nilai'])." Rupiah'>Rp ".number_format($row['nilai'], 2, ",", ".")."</td>";

            echo "<td class=\"det-keet\">".htmlspecialchars_decode($row['keterangan'], ENT_QUOTES)."</td>";
            echo "<td>".(($row['f_nota'] == '') ? 'Tidak Ada' : '<a href="'.base_url('uploads/nota/'.$row['f_nota']).'" target="_blank">Nota</a>')."</td>";
            echo "</tr>";
            $total_nilai = $row['nilai'] + $total_nilai;
        }
        echo "</tbody>";
    }
    ?>
</table>
<?php if(count($dt_nota) > 0) :  ?>
    <h4> Total Nilai : <span class="pull-right"> Rp <?=number_format($total_nilai, 2, ",", ".");?></span></h4>
    <span class="text-muted"><?=Terbilang($total_nilai);?> Rupiah</span>
<?php endif; ?>
<script>
    jQuery(function($){
        $('#foo-modal').footable({
            "useParentWidth": true,
            "paging": {
                "enabled": true
            },
            "filtering": {
                "enabled": true
            },
            "sorting": {
                "enabled": true
            }
        });
    });
</script>
