<div class="col-md-12">
    <table class="table table-striped footable">
        <thead>
        <tr>
            <th>No. Ref</th>
            <th>Account</th>
            <th data-breakpoints="xs sm md lg" data-type="html">Keterangan</th>
            <!--<th data-breakpoints="xs sm md lg" data-type="html">Catatan Nota</th>-->
            <th data-breakpoints="xs sm md lg" data-type="html">Bukti Nota</th>
            <th>Nilai</th>
            <th data-type="html" data-breakpoints="xs" class="text-center">Aksi</th>
        </tr>
        </thead>
        <?php
        if(count($dt_nota) > 0){
            echo "<tbody>";
            $total_nilai = 0;
            foreach($dt_nota as $row){
                echo "<tr>";
                echo "<td>".$row['no_ref']."</td>";
                echo "<td>".$row['kd_account']. " - ".$row['account']."</td>";
                $ket = htmlspecialchars_decode($row['keterangan']);
                //$k_img = preg_replace("/<img[^>]+\>/i", " [ <b>image</b> ] ", $ket);
                //$link_more = " [ <a data-toggle='modal' data-target='#modal' href='#' data-href='".site_url('peran/detail_usulan/'.$row['id'])."' ><i class='glyphicon glyphicon-eye-open'></i> detail</a> ]";
                echo "<td>".$ket."</td>";
                //echo "<td>".$row['notes']."</td>";
                echo "<td>".(($row['f_nota'] == '') ? 'Tidak Ada' : '<a href="'.base_url('uploads/nota/'.$row['f_nota']).'" target="_blank">Nota</a>')."</td>";

                //$act_ubah = " <a href='".site_url('peran/ubah_nota/'.$row['id_nota'].'/'.$row['no_ref'].'/'.$row['nota_kas'].'/'.(str_replace('/','-',$row['tgl_nota'])))."'><button class='btn btn-default btn-sm '><i class='glyphicon glyphicon-edit'></i> Ubah</button></a>";
                //$act_lock = " <a href='".site_url('peran/lock_nota/'.$row['id'])."'><button class='btn btn-warning btn-sm '><i class='glyphicon glyphicon-lock'></i> Kunci</button></a>";
                $act_ubah = " <a href='".site_url('peran/ubah_nota/'.$row['id_nota'].'/'.$row['no_ref'].'/'.$row['nota_kas'].'/'.(str_replace('/','-',$row['tgl_nota'])))."'><button class='btn btn-default btn-sm '><i class='glyphicon glyphicon-edit'></i> Ubah</button></a>";
                $act_hapus = " <a href='".site_url('peran/hapus_nota/'.$row['id_nota'].'/'.$row['no_ref'].'/back')."'><button class='btn btn-danger btn-sm '><i class='glyphicon glyphicon-remove'></i> Hapus</button></a>";
                $act_hapus =($row['lock'] == 'T') ?  $act_hapus : "";
                $act_ubah =($row['lock'] == 'T') ?  $act_ubah : "";
                $act_lock = " <a href='".site_url('peran/lock_nota/'.$row['id_nota'].'/back')."'><button class='btn btn-warning btn-sm '><i class='glyphicon glyphicon-lock'></i> Kunci</button></a>";
                $act_lock =($row['lock'] == 'T') ?  $act_lock : " <a href='".site_url('peran/unlock_nota/'.$row['id_nota'])."'><button class='btn btn-success btn-sm '><i class='glyphicon glyphicon-record'></i> Buka Kunci</button></a>";


                echo "<td title='".Terbilang($row['nilai'])." Rupiah'>Rp ".number_format($row['nilai'], 2, ",", ".")."</td>";
                //echo "<td class='text-center'>" . $act_ubah . "</td>";
                echo "<td class='text-center'>" . $act_lock . $act_ubah . $act_hapus. "</td>";
                echo "</tr>";
                $total_nilai = $row['nilai'] + $total_nilai;
            }
            echo "</tbody>";
        }
        ?>
    </table>
    <?php if(count($dt_nota) > 0) :  ?>
        <h4> Total Nilai : <span class="pull-right"> Rp <?=number_format($total_nilai, 2, ",", ".");?></span></h4>
    <?php endif; ?>
</div>