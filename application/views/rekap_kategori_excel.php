
<?php
if($this->input->post('print')){

    $kode = $this->input->post('kategori', true);
    $periode = $this->input->post('periode', true);
    $kas = $this->input->post('kas', true);
    $filename = $kode."#".$periode.".xls"; // File Name
// Download file
    header("Content-Disposition: attachment; filename=\"$filename\"");
    header("Content-Type: application/vnd.ms-excel");

    $prd = explode("-", $periode);

    if($kode != 'semua'){
        $sql = "SELECT  t_nota.*, t_account.account as account FROM t_nota
            inner join t_account on t_nota.kd_account = t_account.kd_account
            inner join t_pcash on t_nota.pcash = t_pcash.id
            where t_nota.kd_account = ? and (t_nota.pcash is not null or t_nota.pcash != '')
            AND (DATE_FORMAT( t_pcash.tgl_pcash,  '%Y-%m-%d' ) >= DATE_FORMAT( ?,  '%Y-%m-%d' )
            and DATE_FORMAT( t_pcash.tgl_pcash,  '%Y-%m-%d' ) <= DATE_FORMAT( ?,  '%Y-%m-%d' ))
            AND t_pcash.kas_owner = ?
            ORDER BY t_nota.tgl_nota ASC";
        $dt_pettycash = $this->db->query($sql, array($kode, $prd[0], $prd[1], $kas))->result_array();
    }else{
        $dt_account = $this->Siperan_Model->get_dt_account();
        foreach ($dt_account as $acc) {
            $kodek = $acc['kd_account'];
            $sql = "SELECT  t_nota.*, t_account.account as account FROM t_nota
            inner join t_account on t_nota.kd_account = t_account.kd_account
            inner join t_pcash on t_nota.pcash = t_pcash.id
            where t_nota.kd_account = ? and (t_nota.pcash is not null or t_nota.pcash != '')
            AND (DATE_FORMAT( t_pcash.tgl_pcash,  '%Y-%m-%d' ) >= DATE_FORMAT( ?,  '%Y-%m-%d' )
            and DATE_FORMAT( t_pcash.tgl_pcash,  '%Y-%m-%d' ) <= DATE_FORMAT( ?,  '%Y-%m-%d' ))
            AND t_pcash.kas_owner = ?
            ORDER BY t_nota.tgl_nota ASC";
            $dt_pettycash[$kodek] = $this->db->query($sql, array($kodek, $prd[0], $prd[1], $kas))->result_array();
        }
    }


    if($kode != 'semua'){
        if(count($dt_pettycash) > 0){
            $pc = $this->Siperan_Model->get_account($kode);
            echo '<p class="alert alert-success text-center"><strong>Daftar Nota Berdasarkan Account (Kategori) '.$pc.' ('.$kode.')</strong></p>';

        }else{
            $dt_pettycash = array();
        }
    }else{
        foreach ($dt_account as $acc) {
            $kodek = $acc['kd_account'];
            if(count($dt_pettycash[$kodek]) > 0){
                $pc = $this->Siperan_Model->get_account($kodek);
                echo '<p class="alert alert-success text-center"><strong>Daftar Nota Berdasarkan Account (Kategori) '.$pc.' ('.$kodek.')</strong></p>';
                ?>
                <table class="table table-striped footable" id="foo-modal">
                    <thead>
                    <tr>
                        <th>No. Ref</th>
                        <th>Tgl. Nota</th>
                        <th>Petty Cash</th>
                        <th data-breakpoints="xs sm md lg" data-type="html">Keterangan</th>
                        <th data-breakpoints="xs sm md lg" data-type="html">Nota</th>
                        <th>Nominal</th>
                    </tr>
                    </thead>

                    <?php
                    if(count($dt_pettycash[$kodek]) > 0){
                        echo "<tbody>";
                        $total_nilai = 0;
                        foreach($dt_pettycash[$kodek] as $row){
                            echo "<tr>";
                            echo "<td>".$row['no_ref']."</td>";
                            echo "<td>".date("d-M-Y", strtotime($row['tgl_nota']))."</td>";
                            $pc = $this->Siperan_Model->get_pcash($row['pcash']);
                            echo "<td>".$pc['kode_pcash']."</td>";
                            echo "<td class=\"det-keet\">".htmlspecialchars_decode($row['keterangan'], ENT_QUOTES)."</td>";
                            echo "<td>".(($row['f_nota'] == '') ? 'Tidak Ada' : '<a href="'.base_url('uploads/nota/'.$row['f_nota']).'" target="_blank">Nota</a>')."</td>";
                            echo "<td title='".Terbilang($row['nilai'])." Rupiah'>Rp ".number_format($row['nilai'], 2, ",", ".")."</td>";
                            echo "</tr>";
                            $total_nilai = $row['nilai'] + $total_nilai;
                        }
                        echo "</tbody>";
                    }
                    ?>
                </table>
                <?php if(count($dt_pettycash[$kodek]) > 0) :  ?>
                    <h4> Total Nominal : <span class="pull-right"> Rp <?=number_format($total_nilai, 2, ",", ".");?></span></h4>
                    <span class="text-muted" style="color:darkred;"><?=Terbilang($total_nilai);?> Rupiah</span>
                <?php endif; ?>
                <hr/>
                <?php
            }
        }
    }

    if($kode != 'semua'){
?>
        <table class="table table-striped footable" id="foo-modal">
            <thead>
            <tr>
                <th>Tgl. Nota</th>
                <th>Petty Cash</th>
                <th data-breakpoints="xs sm md lg" data-type="html">Keterangan</th>

                <th>Nominal</th>
            </tr>
            </thead>

            <?php
            if(count($dt_pettycash) > 0){
                echo "<tbody>";
                $total_nilai = 0;
                foreach($dt_pettycash as $row){
                    echo "<tr>";
                    echo "<td>".date("d-M-Y", strtotime($row['tgl_nota']))."</td>";
                    $pc = $this->Siperan_Model->get_pcash($row['pcash']);
                    echo "<td>".$pc['kode_pcash']."</td>";
                    echo "<td class=\"det-keet\">".htmlspecialchars_decode($row['keterangan'], ENT_QUOTES)."</td>";

                    echo "<td title='".Terbilang($row['nilai'])." Rupiah'>Rp ".number_format($row['nilai'], 2, ",", ".")."</td>";
                    echo "</tr>";
                    $total_nilai = $row['nilai'] + $total_nilai;
                }
                echo "</tbody>";
            }
            ?>
        </table>
        <?php if(count($dt_pettycash) > 0) :  ?>
            <h4> Total Nominal : <span class="pull-right"> Rp <?=number_format($total_nilai, 2, ",", ".");?></span></h4>
            <span class="text-muted"><?=Terbilang($total_nilai);?> Rupiah</span>
        <?php endif; ?>
    <?php } ?>

<?php } ?>

