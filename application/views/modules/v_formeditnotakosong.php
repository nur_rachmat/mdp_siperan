<form method="post" enctype="multipart/form-data" id="form_nota"  action="<?=site_url('peran/simpan_nota')?>" class="form-horizontal">
    <input type="hidden" name="id_nota" value="<?=$dt_editkosong['id_nota'];?>">
    <div class="form-group has-warning">
        <span style="color: #468847 !important;" class="help-block col-sm-offset-2 col-sm-10">Pilih Petty Cash. Buat Petty cash melalui menu Petty Cash. Petty Cash yang tampil adalah Petty Cash yang belum terkunci</span>
        <label for="inPcash" class="col-sm-2 control-label">Petty Cash</label>
        <div class="input-group col-sm-9">
            <div class="input-group-addon"><i class="glyphicon glyphicon-briefcase"></i></div>
            <select id="inPcash" name="pettycash" required class="form-control select2-allow-clear select2_pcash">
                <optgroup label="Petty Cash">
                    <?php
                    $dt_pcash = $this->Siperan_Model->get_dt_pcash('T');
                    if(count($dt_pcash)){
                        $ko = array('1'=>'STMIK', '2'=>'STIE');
                        foreach ($dt_pcash as $pcash) {
                            if($pcash['lock'] == 'T')
                                echo "<option value=\"".$pcash['id']."\" ".(($dt_editkosong['pcash'] == $pcash['id']) ? 'selected' : '')." >".$pcash['kode_pcash']." - ".$ko[$pcash['kas_owner']]."</option>";
                        }
                    }
                    ?>
                </optgroup>
            </select>
        </div>
    </div>
    <hr style="border-color: #468847;"/>
    <div class="form-group">
        <label for="inTgl" class="col-sm-1 control-label">Tgl Nota</label>
        <div class="input-group col-sm-5">
            <div class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></div>
            <input type="text" required name="tgl_nota" value="<?=date("d-m-Y");?>"  class="form-control datepicker" id="inTgl" >
        </div>

        <label for="nota_kas" class="col-sm-1 control-label"> Kas</label>
        <div class="input-group col-sm-5">
            <!--<input type="hidden" name="nota_kas" readonly  value="<?/*=$dt_editkosong['nota_kas'];*/?>">-->
            <!--<input type="text" name="kas" readonly  value="<?/*=$ko[$dt_editkosong['nota_kas']];*/?>" id="nota_kas" class="form-control " style="background-color: #DFF0D8 !important;" >-->
            <select required class="form-control" name="nota_kas">
                <option value="1" <?=($dt_editkosong['nota_kas'] == 1) ? 'selected' : '';?>>STMIK</option>
                <option value="2" <?=($dt_editkosong['nota_kas'] == 2) ? 'selected' : '';?>>STIE</option>
            </select>
        </div>
    </div>

    <div class="form-group">
        <label for="inAccount" class="col-sm-1 control-label">No. Ref</label>
        <div class="input-group col-sm-5">
            <input type="text" name="no_ref" value="<?=$dt_editkosong['no_ref'];?>" required class="form-control" readonly>
        </div>
        <label for="inAccount" class="col-sm-1 control-label">Nilai Usulan</label>
        <div class="input-group col-sm-5">
            <input type="text" value="<?php $nu = $this->Siperan_Model->get_nilai_usulan($dt_editkosong['no_ref']); echo "Rp ".number_format($nu['nilai'], 2, ",", ".");?>" required class="form-control" readonly>
        </div>


    </div>

    <div class="form-group">
        <label for="vaccount" class="col-sm-1 control-label">Account</label>
        <div class="col-sm-11">
            <div class="input-group  select2-bootstrap-prepend">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button" data-select2-open="single-prepend-text">
                                        <span class="glyphicon glyphicon-search"></span>
                                    </button>
                                </span>
                <select id="single-prepend-text" name="account" required class="form-control select2-allow-clear select2">
                    <option></option>
                    <optgroup label="Account">
                        <?php
                        if(count($dt_account)){
                            foreach ($dt_account as $acc) {
                                echo "<option value=\"".$acc['kd_account']."\" ".(($acc['kd_account'] == $dt_editkosong['kd_account']) ? 'selected' : '').">".$acc['account']."</option>";
                            }
                        }
                        ?>
                    </optgroup>
                </select>
                <div class="input-group-btn">
                    <button type="button" class="btn btn-default" aria-label="Help"><span class="glyphicon glyphicon-list-alt"></span></button>
                </div>
            </div>
        </div>
    </div>

    <div class="form-group">
        <label for="inKeterangan" class="col-sm-1 control-label">Keterangan</label>
        <div class="input-group col-sm-11">
            <textarea class="form-control" name="keterangan" id="defaultketnota" rows="3"><?=strip_tags(htmlspecialchars_decode($dt_editkosong['keterangan']));?></textarea>
        </div>
    </div>

    <div class="form-group">
        <label for="inFile" class="col-sm-1 control-label">Nota</label>
        <div class="input-group col-sm-11">
            <input type="file" id="inFile" name="nota">
            <p class="help-block">File Nota dapat berupa gambar atau file PDF</p>

        </div>
        <?php if($dt_editkosong['f_nota'] != ''){
            echo "<span class=\"help-block col-sm-offset-1 col-sm-11\">".
                anchor('uploads/nota/'.$dt_editkosong['f_nota'], '<i class="fa fa-download"></i> Nota Lama')
                ."<br/>
                                <b>Catatan : </b>Jika anda mengunggah Nota Baru, maka nota lama akan otomatis tertimpa.</span>";

        }?>

    </div>

    <div class="form-group">
        <label for="inNilai" class="col-sm-1 control-label">Nilai</label>
        <div class="input-group col-sm-11">
            <div class="input-group-addon">Rp </div>
            <input type="number" name="nilai" min="1" id="inNilai" value="<?=$dt_editkosong['nilai'];?>" class="form-control" required>
            <div class="input-group-addon">,00</div>
        </div>
        <span id="helpSatuan" class="help-block col-sm-offset-1 col-sm-11"></span>
    </div>



    <div class="form-group">
        <div class="col-sm-offset-1 col-sm-11">
            <button type="submit" name="update" value="Update" data-loading-text="Update Nota..." class="btn btn-success" autocomplete="off">Update</button>

            <a href = "<?=$this->agent->referrer();?>" class="btn btn-warning">Kembali</a>
        </div>
    </div>
</form>