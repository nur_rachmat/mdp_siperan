<div class="alert alert-info" role="alert">
    <i class="icon-info-sign"></i> Cari nota yang telah dibuat berdasarkan Tanggal Nota dan Kas
</div>
<form method="post" action="<?=site_url('peran/nota/created')?>" class="form-horizontal has-success">
    <div class="form-group">
        <label for="inTgl" class="col-sm-1 control-label">Tgl Nota</label>
        <div class="input-group col-sm-4">
            <div class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></div>
            <input type="text" name="tglnota" <?php if($this->input->post('tglnota')){ echo "  value='".date("d-m-Y", strtotime($this->input->post('tglnota')))."'  class=\"form-control datepicker\""; }else{ echo  " class=\"form-control datepicker\" value=\"".date("d-m-Y")."\""; }  ?>  id="inTgl" >
        </div>

        <label for="nota_kas" class="col-sm-1 control-label"> Kas</label>

        <div class="input-group col-sm-4">
            <select class="form-control" id="kas" name="kas">
                <option value="1" <?=(($this->input->post('kas') == 1) ? "selected" : "");?>>STMIK</option>
                <option value="2" <?=(($this->input->post('kas') == 2) ? "selected" : "");?>>STIE</option>
            </select>
        </div>

        <div class="input-group col-sm-2">
            <button type="submit" class="btn btn-info" ><span class="glyphicon glyphicon-search"></span></button>
        </div>

    </div>
</form>

<div class="alert alert-success" role="alert">
    <i class="icon-tasks"></i> Nota yang telah dibuat. Nota masih dapat dirubah sebelum status nota terkunci.
</div>
<?php if($this->input->post()) : ?>
    <table class="table table-striped footable">
        <thead>
        <tr>
            <th>No. Ref</th>
            <th>Tgl. Nota</th>
            <th>Account</th>
            <th data-breakpoints="xs sm md lg" data-type="html">Keterangan</th>
            <th data-breakpoints="xs sm md lg" data-type="html">Petty Cash</th>
            <th data-breakpoints="xs sm md lg" data-type="html">Bukti Nota</th>
            <th>Nilai</th>
            <th data-type="html" data-breakpoints="xs">Aksi</th>
        </tr>
        </thead>
        <?php
        $tgl_nota = $this->input->post('tglnota', TRUE);
        $kas = $this->input->post('kas', TRUE);
        $dt_semuanota = $this->Siperan_Model->get_dt_nota($tgl_nota, $kas);

        if(count($dt_semuanota) > 0){
            echo "<tbody>";
            foreach($dt_semuanota as $row){
                echo "<tr>";
                echo "<td>".$row['no_ref']."</td>";
                echo "<td>".date('d-M-Y', strtotime($row['tgl_nota']))."</td>";

                echo "<td>".$row['kd_account']. " - ".$row['account']."</td>";
                $ket = strip_tags(htmlspecialchars_decode($row['keterangan']), "<img>");
                $k_img = preg_replace("/<img[^>]+\>/i", " [ <b>image</b> ] ", $ket);
                $link_more = " [ <a data-toggle='modal' data-target='#modal' href='#' data-href='".site_url('peran/detail_usulan/'.$row['id_nota'])."' ><i class='glyphicon glyphicon-eye-open'></i> detail</a> ]";
                echo "<td>".strip_tags(htmlspecialchars_decode($row['keterangan']))."</td>";
                echo "<td>".$row['kode_pcash']."</td>";

                echo "<td>".(($row['f_nota'] == '') ? 'Tidak Ada' : '<a href="'.base_url('uploads/nota/'.$row['f_nota']).'" target="_blank">Nota</a>')."</td>";


                $act_ubah = " <a href='".site_url('peran/ubah_nota/'.$row['id_nota'].'/'.$row['no_ref'].'/'.$row['nota_kas'].'/'.(str_replace('/','-',$row['tgl_nota'])))."'><button class='btn btn-default btn-sm '><i class='glyphicon glyphicon-edit'></i> Ubah</button></a>";
                $act_hapus = " <a href='".site_url('peran/hapus_nota/'.$row['id_nota'].'/'.$row['no_ref'])."'><button class='btn btn-danger btn-sm '><i class='glyphicon glyphicon-remove'></i> Hapus</button></a>";
                $act_hapus =($row['lock'] == 'T') ?  $act_hapus : "";
                $act_ubah =($row['lock'] == 'T') ?  $act_ubah : "";
                $act_lock = " <a href='".site_url('peran/lock_nota/'.$row['id_nota'])."'><button class='btn btn-warning btn-sm '><i class='glyphicon glyphicon-lock'></i> Kunci</button></a>";
                $act_lock =($row['lock'] == 'T') ?  $act_lock : " <a href='".site_url('peran/unlock_nota/'.$row['id_nota'])."'><button class='btn btn-success btn-sm '><i class='glyphicon glyphicon-record'></i> Buka Kunci</button></a>";
                echo "<td title='".Terbilang($row['nilai'])." Rupiah'>Rp ".number_format($row['nilai'], 2, ",", ".")."</td>";

                echo "<td>" . $act_lock . $act_ubah . $act_hapus. "</td>";

                echo "</tr>";

            }
            echo "</tbody>";
        }else{
            echo "<div class=\"alert alert-danger\" role=\"alert\">
        <i class=\"icon-tasks\"></i> Belum ada nota untuk tanggal {$tgl_nota}
    </div>";
        }
        ?>
    </table>
<?php endif; ?>