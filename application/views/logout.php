<meta name="google-signin-scope" content="profile email">
<meta name="google-signin-client_id" content="685791212141-4etrat3m7q9kr9hhicigthdp97cuilvs.apps.googleusercontent.com">
<script src="<?php echo base_url();?>data_table/js/jquery.js"></script>
<script src="https://apis.google.com/js/platform.js" async defer></script>

<div class="g-signin2" data-onsuccess="onSignIn" data-onclick="onSubmit"></div>

<script>
    var js_base_url = function(){ return "<?php echo base_url();?>"; }
    $( document ).ready(function() {
        console.log( "loging out!" );
        signOut();
        $(location).attr('href', js_base_url() + '/welcome');
    });
    function signOut() {
        var auth2 = gapi.auth2.getAuthInstance();
        auth2.signOut().then(function () {
            console.log('User signed out.');
        });
    }
</script>