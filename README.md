**Instalasi :**
1. Clone Project ini dengan Git pada web server anda melalui url : https://gitlab.com/nur_rachmat/mdp_siperan.git
2. Project juga dapat di download dalam format .zip atau tarball.
3. Buat database mysql db_siperan
4. Import file db_siperan.sql ke database tersebut

**Konfigurasi :**
1. Buka folder application/config
2. Edit file config.php dengan mengubah parameter base_url sesuai root directory di web server anda
    
        $config['base_url']	= 'http://localhost/siperan/';
        menjadi:
        $config['base_url']	= "http://192.168.35.36/mdp_siperan";
        atau
        $config['base_url']	= "http://siperan.mdp.ac.id";
    
3. Edit file database.php ubah parameter berikut sesuai konfigurasi dan hak akses database mysql anda contoh : 
    
        $db['default']['hostname'] = 'localhost';<br>
        $db['default']['username'] = 'root';<br>
        $db['default']['password'] = 'root';<br>
        $db['default']['database'] = 'db_siperan';<br>
    
**Pengembangan**

SIPERAN dibangun dengan Framework CodeIgniter dengan bahasa pemrograman PHP. 
Apabila ingin melakukan pengembangan dapat lansung dilakukan dengan mudah dengan mengikuti aturan MVC yang sudah ada
Panduan untuk CodeIgniter dapat dibaca di http://www.codeigniter.com/user_guide/

**Penggunaan**

Terdapat 4 level pengguna yang dapat mengakses SIPERAN
1. Ketua
2. Ketua Yayasan
3. BAK (Administrasi Kuangan)
4. Bendahara Yayasan

Berikut User Default ke 4 akses tersebut yang dapat digunakan untuk demo Sistem

_Ketua :_
username : ketua<br>
password : 123456

_Ketua Yayasan :_
username : yayasan<br>
password : 123456

_Bendahara Yayasan :_
username : bendahara<br>
password : 123456

_BAK :_
username : bak<br>
password : 123456

Sistem ini menggunakan Email untuk notifikasi, pastikan konfigurasi SMTP pada server sudah dikonfigurasi dengan baik.<br>
SMTP dapat menggunakan milik Gmail dengan mengubah konfigurasi pada library mailsender.php di folder application/libraries/mailsender.php
