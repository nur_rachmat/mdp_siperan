	<div class="alert alert-success no-print" role="alert">
		<h4><i class="glyphicon glyphicon-lock"></i> Ubah Password</h4>
	</div>

	<div class="row">
		<div class="col-md-12">
			<?php echo $this->session->flashdata("k_passwod");?>
			<form method="post" action="<?=site_url('peran/ubahpassword')?>" class="form-horizontal">
				<div class="form-group">
					<label for="inUser" class="col-sm-3 control-label">Username</label>
					<div class="col-sm-9">
						<input type="text" name="username" id="inUser" class="form-control" readonly value="<?=$this->session->userdata('admin_user')?>">
					</div>
				</div>

				<div class="form-group">
					<label for="inEmail" class="col-sm-3 control-label">Email</label>
					<div class="col-sm-9">
						<input type="email" name="email" id="inEmail" class="form-control" value="<?=$this->session->userdata('admin_email')?>">
					</div>
				</div>

				<div class="form-group has-warning">
					<label for="p1" class="col-sm-3 control-label">Password Lama</label>
					<div class="col-sm-9">
						<input type="password" name="p1" id="p1" class="form-control"  autofocus required>
					</div>
				</div>

				<div class="form-group">
					<label for="p2" class="col-sm-3 control-label">Password Baru</label>
					<div class="col-sm-9">
						<input type="password" name="p2" id="p2" class="form-control"  required>
					</div>
				</div>
				<div class="form-group">
					<label for="p3" class="col-sm-3 control-label">Ulangi Password Baru</label>
					<div class="col-sm-9">
						<input type="password" class="form-control" name="p3" id="p3" required>
					</div>
				</div>

				<div class="form-group">
					<div class="col-sm-offset-3 col-sm-9">
						<button type="submit" class="btn btn-primary">Simpan</button>
						<a href="<?=site_url('peran');?>" class="btn btn-success">Kembali</a>
					</div>
				</div>

			</form>
		</div>
	</div>