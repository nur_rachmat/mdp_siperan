<ul class="nav nav-tabs">
    <li><a tabindex="-1" href="<?php echo site_url('peran/daftar_pettycash'); ?>"><i class="glyphicon glyphicon-briefcase"> </i> Rekap Petty Cash</a></li>
    <li><a tabindex="-1" href="<?php echo site_url('peran/daftar_kategori'); ?>"><i class="glyphicon glyphicon-compressed"> </i> Rekap Account (Kategori)</a></li>

</ul>
<?php if($this->session->flashdata('simpan_ok')){ ?>
    <div class="alert alert-success alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <?=$this->session->flashdata('simpan_ok');?>
    </div>
<?php } ?>

<?php if($active == 'pcash' || $active == 'buat_nota') : ?>
<div class="alert alert-success" role="alert">
    <h4>
        <i class="icon-tasks"></i> Petty Cash
        <span class="label label-danger pull-right"> <?=count($dt_pcash);?></span>
    </h4>
</div>

<form method="post" action="<?=site_url('peran/lock_pcash');?>" >
    <button type="submit" id="btn_lock" disabled class="btn btn-success btn-md"><i class="glyphicon glyphicon-lock"></i> Lock</button>
    <a href="<?=site_url('peran/pcash/buat');?>"><button type="button" class="btn btn-primary btn-md"><i class="glyphicon glyphicon-plus-sign"></i> Buat Petty Cash</button></a>

    <a data-toggle='modal' data-target='#modalrekapkas' href='#' data-href='' class='btn btn-info btn-md'><i class='glyphicon glyphicon-print'></i> Cetak Rekap Kas Keluar</a>
    <div class="claerfix">&nbsp;</div>
    <input type="checkbox" id="check_all_lock"> <label for="check_all">Semua</label>
    <div class="alert alert-info" role="alert">
        <i class="fa fa-info"></i> Daftar Petty Cash yang masih Aktif
    </div>
    <table class="table table-striped footable">
        <thead>
            <tr>
                <th class="text-center" data-type="html"></th>
                <th>Kode</th>
                <th data-type="date">Tanggal</th>
                <th>Nilai</th>
                <th data-breakpoints="xs sm md lg" data-type="html">Keterangan</th>
                <th data-type="html">Kas</th>
                <th data-type="html">Status</th>
                <th data-type="html" data-breakpoints="xs" class="text-center">Aksi</th>
            </tr>
        </thead>

        <?php
        echo "<tbody>";
        if(count($dt_pcash) > 0){
            foreach($dt_pcash as $row){
                echo "<tr>";
                echo "<td class='text-center'><input type='checkbox' name='lock[]' id='lock[]' value='".$row['id']."'></td>";
                echo "<td>".$row['kode_pcash']."</td>";
                echo "<td>".date('d-m-Y', strtotime($row['tgl_pcash']))."</td>";
                echo "<td title='".Terbilang($row['nilai_pcash'])." Rupiah'>Rp ".number_format($row['nilai_pcash'], 2, ",", ".")."</td>";
                echo "<td>".htmlspecialchars_decode($row['keterangan'])."</td>";

                if($row['kas_owner'] === '1'){
                    $kas = "<span class='label label-info lbl-sm'>STMIK</span>";
                }elseif($row['kas_owner'] == '2'){
                    $kas = "<span class='label label-success lbl-sm'>STIE</span>";
                }else{
                    $kas = "<span class='label label-danger lbl-sm'>N/A</span>";
                }
                echo "<td>".$kas."</td>";
                $act_lock = " <a href='".site_url('peran/lock_pcash/'.$row['id'].'/'.$row['kode_pcash'])."' class='btn btn-primary btn-sm'><i class='glyphicon glyphicon-lock'></i> Lock</a>";
                $act_nota = " ";
                $tbh_nota = " ";
                $print_nota = " ";



                if($row['lock'] == 'T'){
                    $status = "<span class='label label-success lbl-sm'><i class='glyphicon glyphicon-check'></i> Open</span> ";
                    $act_nota = " <a data-toggle='modal' data-target='#modal' href='#' data-href='".site_url('peran/detail_nota_pcash/'.$row['id'])."' class='btn btn-success btn-sm'><i class='glyphicon glyphicon-eye-open'></i> Lihat Nota</a> ";
                    $tbh_nota = " <a href='".site_url('peran/nota/tambahnota/'.$row['id'].'/'.$row['kas_owner'])."' class='btn btn-default btn-sm'><i class='glyphicon glyphicon-plus'></i> Tambah Nota</a> ";
                    $print_nota = " <a href='".site_url('peran/cetak/pcash/'.$row['id'].'/'.$row['kas_owner'])."' target='_blank' class='btn btn-default btn-sm'><i class='glyphicon glyphicon-print'></i> Print</a> ";
                }elseif($row['lock'] == 'Y'){
                    $act_lock ="";
                    $status = "<span class='label label-primary lbl-sm'><i class='glyphicon glyphicon-lock'></i> Locked</span>";
                    $act_nota = " <a data-toggle='modal' data-target='#modal' href='#' data-href='".site_url('peran/detail_nota_pcash/'.$row['id'])."' class='btn btn-success btn-sm'><i class='glyphicon glyphicon-eye-open'></i> Lihat Nota</a> ";
                    $print_nota = " <a href='".site_url('peran/cetak/pcash/'.$row['id'].'/'.$row['kas_owner'])."' target='_blank' class='btn btn-default btn-sm'><i class='glyphicon glyphicon-print'></i> Print</a> ";
                }

                echo "<td>" . $status . "</td>";
                echo "<td class='text-center'>" . $act_lock . $act_nota. $tbh_nota. $print_nota."</td>";

                echo "</tr>";

            }
        }
        echo "</tbody>";
        ?>
    </table>
</form>

    <?php if($active == 'buat_nota' && isset($tgl_pcash)) : ?>
    <div class="row">
        <div class="col-md-5">
            <h4>Tambahkan dari Nota</h4>

            <?php if($this->session->flashdata('simpan_ggl')){ ?>
                <div class="alert alert-danger alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <?=$this->session->flashdata('simpan_ggl');?>
                </div>
            <?php } ?>
            <form method="post"  action="<?=site_url('peran/tambah_nota')?>" class="form-horizontal">
                <input type="hidden" name="tgl_lama" value="<?=date("Y-m-d", strtotime($dt_pcash[0]['tgl_pcash']));?>">
                <input type="hidden" name="id" value="<?=$dt_pcash[0]['id'];?>">
                <div class="form-group">
                    <label for="inTglNota" class="col-sm-2 control-label">Tgl.Nota</label>
                    <div class="input-group col-sm-8">
                        <div class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></div>
                        <input type="text" id="inTglNota" name="tgl_nota"  class="form-control datepicker">

                    </div>
                    <div class="input-group col-sm-2">
                        <button type="submit" name="tambah" value="tambah_tgl" class="btn btn-info" ><span class="glyphicon glyphicon-plus-sign"></span></button>
                    </div>
                </div>
                <div class="form-group">
                    <label for="inTglNota" class="col-sm-6 control-label">Atau</label>
                </div>
                <div class="form-group">
                    <label for="inTglNota" class="col-sm-2 control-label">No. Ref</label>
                    <div class="input-group col-sm-8">
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="button" data-select2-open="single-prepend-text">
                                <span class="glyphicon glyphicon-search"></span>
                            </button>
                        </span>
                        <select id="single-prepend-text" name="no_ref" class="form-control select2-allow-clear select2no_ref">
                            <option></option>
                            <optgroup label="No. Ref / Tgl. Nota">
                                <?php
                                if(count($dt_noref)){
                                    foreach ($dt_noref as $acc) {
                                        echo "<option value=\"".$acc['no_ref']."\" >".$acc['no_ref']." / ".date('d-M-Y', strtotime($acc['tgl_nota']))."</option>";
                                    }
                                }
                                ?>
                            </optgroup>
                        </select>
                    </div>
                    <div class="input-group col-sm-2">
                        <button type="submit" name="tambah" value="tambah_noref" class="btn btn-info" ><span class="glyphicon glyphicon-plus-sign"></span></button>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label">Tanggal</label>
                    <div class="input-group col-sm-5">
                        <div class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></div>
                        <input type="text" readonly name="tgl_pcash" value="<?=date("d-M-Y", strtotime($tgl_pcash));?>"  class="form-control " >
                    </div>

                    <label class="col-sm-1 control-label"> Kode</label>
                    <div class="input-group col-sm-4">
                        <input type="text" id="inKdPcash" name="kode_pcash" value="<?=$dt_pcash[0]['kode_pcash'];?>" required class="form-control" readonly>
                    </div>
                </div>

<!--

                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-9">
                        <button type="submit" name="tambah" value="Tambah" data-loading-text="Menambahkan ke Petty Cash..." class="btn btn-primary" autocomplete="off">Tambah</button>
                    </div>
                </div>-->
            </form>
        </div>

        <div class="col-md-7">
            <h4>Nota Petty Cash</h4>
            <table class="table table-striped footable">
                <thead>
                    <tr>
                        <th>No. Ref</th>
                        <th>Tgl. Nota</th>
                        <th>Account</th>
                        <th data-breakpoints="xs sm md lg" data-type="html">Keterangan</th>
                        <th data-breakpoints="xs sm md lg" data-type="html">Catatan Nota</th>
                        <th data-breakpoints="xs sm md lg" data-type="html">Bukti Nota</th>
                        <th>Nilai</th><th data-type="html" data-breakpoints="xs">Aksi</th>
                    </tr>
                </thead>

                <?php
                if(count($dt_nota) > 0){
                    echo "<tbody>";
                    $total_nilai = 0;
                    foreach($dt_nota as $row){
                        echo "<tr>";

                        echo "<td>".$row['no_ref']."</td>";
                        echo "<td>".date("d-M-Y", strtotime($row['tgl_nota']))."</td>";
                        echo "<td>".$row['kd_account']. " - ".$row['account']."</td>";
                        $ket    = strip_tags(htmlspecialchars_decode($row['keterangan']), "<img>");
                        $k_img  = preg_replace("/<img[^>]+\>/i", " [ <b>image</b> ] ", $ket);
                        $link_more = " [ <a data-toggle='modal' data-target='#modal' href='#' data-href='".site_url('peran/detail_usulan/'.$row['id'])."' ><i class='glyphicon glyphicon-eye-open'></i> detail</a> ]";
                        echo "<td>".substr($k_img, 0, 100).((strlen($k_img) > 100) ? $link_more : "")."</td>";
                        echo "<td>".$row['notes']."</td>";
                        echo "<td>".(($row['f_nota'] == '') ? 'Tidak Ada' : '<a href="'.base_url('uploads/nota/'.$row['f_nota']).'" target="_blank">Nota</a>')."</td>";

                        $act_ubah = " <a href='".site_url('peran/batal_nota/'.$row['id'].'/'.$row['no_ref'].'/'.$row['nota_kas'].'/'.(str_replace('/','-',$tgl_nota)))."'><button class='btn btn-info btn-sm '><i class='glyphicon glyphicon-ban-circle'></i> Lepas</button></a>";
                        //$act_lock = " <a href='".site_url('peran/lock_nota/'.$row['id'])."'><button class='btn btn-warning btn-sm '><i class='glyphicon glyphicon-lock'></i> Kunci</button></a>";

                        echo "<td title='".Terbilang($row['nilai'])." Rupiah'>Rp ".number_format($row['nilai'], 2, ",", ".")."</td>";
                        echo "<td class='text-center'>" . $act_ubah . "</td>";
                        echo "</tr>";
                        $total_nilai = $row['nilai'] + $total_nilai;
                    }
                    echo "</tbody>";
                }
                ?>
            </table>
            <?php if(count($dt_nota) > 0) :  ?>
                <h4> Total Nilai : <span class="pull-right"> Rp <?=number_format($total_nilai, 2, ",", ".");?></span></h4>
            <?php endif; ?>
        </div>
    </div>
    <?php endif; ?>


<?php endif; ?>

<?php if($active == 'buat') : ?>
<div class="alert alert-info" role="alert">
    <h4>
        <i class="icon-tasks"></i> Buat Petty Cash
    </h4>
</div>
<?php
    /*$kode =count($dt_pcash);
    $kode =669+$kode;
    if($kode < 10){
        $kode = "000".($kode+1);
    }elseif($kode < 100){
        $kode = "00".($kode+1);
    }else{
        $kode = "0".($kode+1);
    }*/

?>
<form method="post"  action="<?=site_url('peran/simpan_pcash')?>" class="form-horizontal">
    <div class="form-group">
        <label for="inTgl" class="col-sm-3 control-label">Tanggal</label>
        <div class="input-group col-sm-4">
            <div class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></div>
            <input type="text" required name="tgl_pcash" value="<?=date("d-m-Y");?>"  class="form-control datepicker" id="inTgl" >
        </div>

        <label for="nota_kas" class="col-sm-1 control-label"> Kas</label>
        <div class="input-group col-sm-3">
            <select id="nota_kas" required class="form-control" name="pcash_owner">
                <option value="1">STMIK</option>
                <option value="2">STIE</option>
            </select>
        </div>
    </div>
    <div class="form-group has-success">
        <label for="inKode" class="col-sm-3 control-label">Kode</label>
        <div class="input-group col-sm-9">
            <input type="text" name="kode" id="inKode" required class="form-control" value="<?=$kode_stmik;?>">
        </div>
    </div>

    <div class="form-group">
        <label for="inKeterangan" class="col-sm-3 control-label">Keterangan</label>
        <div class="input-group col-sm-9">
            <textarea class="form-control" name="keterangan" rows="3" placeholder="Keterangan lengkap untuk petty cash yang dibuat"></textarea>
        </div>
    </div>


    <div class="form-group">
        <div class="col-sm-offset-3 col-sm-9">
            <button type="submit" name="simpan" value="Simpan" data-loading-text="Simpan Nota..." class="btn btn-primary" autocomplete="off">Simpan</button>
            <a href="<?=site_url('peran/pcash');?>" class="btn btn-info">Kembali</a>
        </div>
    </div>
</form>

<?php endif; ?>


<!-- Modal -->
<div class="modal fade bs-example-modal-lg" id="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Detail Nota Petty Cash</h4>
            </div>
            <div class="modal-body">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="modalrekapkas" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Cetak Rekap Kas Keluar</h4>
            </div>
            <div class="modal-body">
                <form method="post" target="_blank" action="<?=site_url('peran/cetak/rekapkas')?>" class="form-inline no-print">
                    <div class="form-group">
                        <label for="inTgl" class="col-sm-1 control-label text-right">Kas</label>
                        <div class="input-group col-sm-2">
                            <select id="nota_kas" required class="form-control" name="pcash_owner">
                                <option value="1">STMIK</option>
                                <option value="2">STIE</option>
                            </select>
                        </div>
                        <label for="inTgl" class="col-sm-2 control-label text-right">Petty Cash</label>
                        <div class="input-group col-sm-5">
                            <div class="input-group-addon"><i class="glyphicon glyphicon-briefcase"></i></div>
                            <select  id="inPcash" name="pettycash[]" multiple required class="form-control select2-allow-clear select2_pcash" style="width: 100% !important;">
                                <optgroup label="Petty Cash">
                                    <?php
                                    $dt_pcash = $this->Siperan_Model->get_pcash();
                                    if(count($dt_pcash)){
                                        $ko = array('1'=>'STMIK', '2'=>'STIE');
                                        foreach ($dt_pcash as $pcash) {
                                            echo "<option value=\"".$pcash['id']."\" >".$pcash['kode_pcash']." # ".$ko[$pcash['kas_owner']]." # ".date('d-M-Y', strtotime($pcash['tgl_pcash']))."</option>";
                                        }
                                    }
                                    ?>
                                </optgroup>
                            </select>
                        </div>

                        <div class="col-sm-1">
                            <button type="submit" name="cari" value="Cari" class="btn btn-info">Cari</button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>