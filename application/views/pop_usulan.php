<div class="alert alert-success" role="alert">
    <h4>
        <i class="icon-info"></i> Data Usulan yang telah disetujui oleh yayasan dapat diupdate untuk dibuat nota
    </h4>
</div>
<table class="table table-striped" id="foo-modal">
    <thead>
    <tr>
        <th>No. Ref</th>
        <th>Tgl. Usulan</th>
        <th>Account</th>
        <th data-breakpoints="xs sm md lg" data-type="html">Keterangan</th>
        <th>Nilai</th>
        <th data-type="html">Rekening</th>
    </tr>
    </thead>
    <?php
    if(count($dt_noref) > 0){
        echo "<tbody>";
        foreach($dt_noref as $row){
            echo "<tr>";
            echo "<td>".$row['no_ref']."</td>";
            echo "<td>".date('d-M-Y', strtotime($row['tgl_usul']))."</td>";

            echo "<td>".$row['kd_account']. " - ".$row['account']."</td>";
            $ket = strip_tags(htmlspecialchars_decode($row['keterangan']), "<img>");
            $k_img = preg_replace("/<img[^>]+\>/i", " [ <b>image</b> ] ", $ket);
            $link_more = " [ <a data-toggle='modal' data-target='#modal' href='#' data-href='".site_url('peran/detail_usulan/'.$row['id'])."' ><i class='glyphicon glyphicon-eye-open'></i> detail</a> ]";
            echo "<td>".substr($k_img, 0, 100).((strlen($k_img) > 100) ? $link_more : "")."</td>";

            echo "<td title='".Terbilang($row['nilai'])." Rupiah'>Rp ".number_format($row['nilai'], 2, ",", ".")."</td>";
            if($row['rekening'] === '1'){
                $kas = "<span class='label label-info lbl-sm'>STMIK</span>";
            }elseif($row['rekening'] == '2'){
                $kas = "<span class='label label-success lbl-sm'>STIE</span>";
            }else{
                $kas = "<span class='label label-danger lbl-sm'>Bersama</span>";
            }
            echo "<td>". $kas . "</td>";

            echo "</tr>";

        }
        echo "</tbody>";
    }
    ?>
</table>
<script>
    jQuery(function($){
        $('#foo-modal').footable({
            "useParentWidth": true,
            "paging": {
                "enabled": true
            },
            "filtering": {
                "enabled": true
            },
            "sorting": {
                "enabled": true
            }
        });
    });
</script>
