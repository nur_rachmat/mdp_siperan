<?php
$dt = "data_{$p}";
$vard = ${$dt};
?>
<style type="text/css">
    td{padding: 5px;}
    th{padding: 2px; background-color: #999999;}
</style>
<table width="100%">
    <tr>
        <td width="50%">
            <img src="<?=base_url('logo/logo'.$kas.'.png');?>" width="75%">
        </td>
        <td style="text-align: center;">
            <h3>YAYASAN MULTI DATA PALEMBANG<br/>
                REKAP KAS KELUAR</h3>
        </td>
    </tr>

</table>
<table width="100%" border="1" cellspacing="4" cellspacing="4" style="border-collapse: collapse; padding: 5px;">
    <tr height="50">
        <th rowspan="2">No.</th>
        <th rowspan="2">Tanggal Transaksi</th>
        <th rowspan="2">Keterangan</th>
        <th width="15%" colspan="2">Jumlah (Rp)</th>
    </tr>
    <tr height="50">
        <th>Debet</th>
        <th>Kredit</th>
    </tr>
    <?php
    if(count($vard) > 0){
        $i = 1;
        $total_nilai = 0;
        foreach($vard as $row){
            echo "<tr>";
            echo "<td align='center'>".$i++."</td>";
            echo "<td align='center'>".date("d-M-Y", strtotime($row['tgl_pcash']))."</td>";
            echo "<td> Bukti Kas No. : ".$row['kode_pcash']." / ".$kas."</td>";
            echo "<td align='center'></td>";
            echo "<td align='right'>".number_format($row['nilai_pcash'], 0, ",", ".")."</td>";
            echo "</tr>";
            $total_nilai = $row['nilai_pcash'] + $total_nilai;
        }
    }
    ?>
    <tr style="font-weight: bold;">
        <td colspan="2"></td>
        <td align="right">Total </td>
        <td ></td>
        <td align="right"><?=number_format($total_nilai, 0, ",", ".");?></td>
    </tr>
</table>
<p><strong>Terbilang : #<?=Terbilang($total_nilai)." Rupiah";?></strong> #</p>
<table width="100%">
    <tr>
        <td></td>
        <td>Palembang, <?=date("d-M-Y");?></td>

    </tr>
    <tr>
        <td>Diketahui Oleh,</td>
        <td>Dibuat Oleh,</td>
    </tr>
    <tr>
        <td height="50"></td>
        <td></td>
    </tr>
    <tr>



        <td>
            <u>Johannes Petrus, S.Kom., M.T.I</u><br/>
            Ketua</td>

        <td>
            <u><?=$pk2;?></u><br/>
            Pembantu Ketua II</td>
    </tr>

</table>

