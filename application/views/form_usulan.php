<?php if($this->session->flashdata('simpan_ggl')){ ?>
    <div class="alert alert-danger alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <?=$this->session->flashdata('simpan_ggl');?>
    </div>
<?php } ?>

<div class="alert alert-success no-print" role="alert">
    <h4><i class="glyphicon glyphicon-pencil"></i> <?=ucfirst($act);?> Usulan</h4>
</div>

<div class="row">
    <div class="col-md-12">
        <?php echo $this->session->flashdata("e_usulan");?>
        <form method="post" id="form_usulan" action="<?=site_url('peran/simpan_usulan')?>" class="form-horizontal">

            <div class="form-group">
                <label for="inTgl" class="col-sm-2 control-label">Tanggal Usulan</label>
                <div class="input-group col-sm-10">
                    <div class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i> </div>
                    <input type="text" name="tgl_usul" id="inTgl" class="form-control" readonly value="<?=date('Y-m-d');?>">
                </div>
            </div>

            <div class="form-group has-success">
                <label for="inPengusul" class="col-sm-2 control-label">Pengusul</label>
                <div class="input-group col-sm-10">
                    <div class="input-group-addon"><i class="glyphicon glyphicon-user"></i> </div>
                    <input type="text" name="pengusul" id="inPengusul" class="form-control" readonly value="<?=$this->session->userdata('admin_user')?>">
                    <input type="hidden" name="id_pengusul" value="<?=$this->session->userdata('admin_id')?>">
                </div>
            </div>

            <div class="form-group">
                <label for="inKas" class="col-sm-2 control-label">Kas</label>
                <div class="input-group col-sm-10">
                    <label class="radio-inline">
                        <input type="radio" name="kas" id="inKas" value="1" required> STMIK
                    </label>
                    <label class="radio-inline">
                        <input type="radio" name="kas" id="inKas2" value="2" required> STIE
                    </label>
                    <label class="radio-inline">
                        <input type="radio" name="kas" id="inKas3" value="3" required> Bersama
                    </label>
                </div>
            </div>

            <!--<div class="form-group">
                <label for="inAccount" class="col-sm-2 control-label">Account</label>
                <div class="col-sm-10">
                    <div class="input-group  select2-bootstrap-prepend">
						<span class="input-group-btn">
                            <button class="btn btn-default" type="button" data-select2-open="single-prepend-text">
                                <span class="glyphicon glyphicon-search"></span>
                            </button>
						</span>
                        <select id="single-prepend-text" name="account" required class="form-control select2-allow-clear select2">
                            <option></option>
                            <optgroup label="Master Account">
                                <?php
/*                                    if(count($dt_account)){
                                        foreach ($dt_account as $acc) {
                                            echo "<option value=\"".$acc['kd_account']."\">".$acc['account']."</option>";
                                        }
                                    }
                                */?>
                            </optgroup>
                        </select>

                        <div class="input-group-btn">
                            <button type="button" class="btn btn-default" aria-label="Help"><span class="glyphicon glyphicon-list-alt"></span></button>
                        </div>

                        <div class="input-group-btn">
                            <button type="button" class="btn btn-success" aria-label="Help"><span class="glyphicon glyphicon-plus"></span></button>
                        </div>
                    </div>
                </div>
            </div>-->

            <div class="form-group">
                <label for="inNilai" class="col-sm-2 control-label">Nilai</label>
                <div class="input-group col-sm-10">
                    <div class="input-group-addon">Rp </div>
                    <input type="number" name="nilai" min="1" id="inNilai" class="form-control" value="<?=($this->session->flashdata('f_nilai')) ? $this->session->flashdata('f_nilai') : '';?>" required>
                    <div class="input-group-addon">,00</div>
                </div>
                <span id="helpSatuan" class="help-block col-sm-offset-2 col-sm-10"></span>
            </div>


            <div class="form-group">
                <label for="inKeterangan" class="col-md-2 col-sm-2 control-label">Usulan 1</label>
                <div class="input-group col-md-6 col-sm-6">
                    <textarea class="form-control inKeterangan" name="keterangan1" id="su1" required rows="1"><?=($this->session->flashdata('f_keterangan')) ? htmlspecialchars_decode($this->session->flashdata('f_keterangan'), ENT_QUOTES) : '';?></textarea>
                    <input type="hidden" class="inKetHTMLsu1" id="inKetHTMLsu1">

                </div>
                <label for="single-prepend-text" class="col-md-1 col-sm-1 control-label">Account 1</label>
                <div class="col-md-3 col-sm-3">
                    <div class="input-group  select2-bootstrap-prepend">
						<span class="input-group-btn">
                            <button class="btn btn-default" type="button" data-select2-open="single-prepend-text">
                                <span class="glyphicon glyphicon-search"></span>
                            </button>
						</span>
                        <select id="single-prepend-text" name="account1" class="form-control select2-allow-clear select2">
                            <option></option>
                            <optgroup label="Master Account">
                                <?php
                                    if(count($dt_account)){
                                        foreach ($dt_account as $acc) {
                                            echo "<option value=\"".$acc['kd_account']."\">".$acc['account']."</option>";
                                        }
                                    }
                                ?>
                            </optgroup>
                        </select>
                    </div>
                </div>
            </div>
            <?php for($u=2; $u<=10; $u++){ ?>
            <div class="form-group " id="fg_usul_<?=$u?>">
                <label for="inKeterangan" class="col-md-2 col-sm-2 control-label">Usulan <?=$u?></label>
                <div class="input-group col-md-6 col-sm-6">
                    <div id="su<?=$u?>" class="inKeterangan"><?=($this->session->flashdata('f_keterangan'.$u)) ? htmlspecialchars_decode($this->session->flashdata('f_keterangan'.$u), ENT_QUOTES) : '';?></div>
                    <input type="hidden" name="keterangan<?=$u?>" class="inKetHTMLsu<?=$u?>">
                </div>
                <label for="single-prepend-text" class="col-md-1 col-sm-1 control-label">Account <?=$u?></label>
                <div class="col-md-3 col-sm-3">
                    <div class="input-group  select2-bootstrap-prepend">
						<span class="input-group-btn">
                            <button class="btn btn-default" type="button" data-select2-open="single-prepend-text">
                                <span class="glyphicon glyphicon-search"></span>
                            </button>
						</span>
                        <select id="single-prepend-text" name="account<?=$u?>" class="form-control select2-allow-clear select2">
                            <option></option>
                            <optgroup label="Master Account">
                                <?php
                                if(count($dt_account)){
                                    foreach ($dt_account as $acc) {
                                        echo "<option value=\"".$acc['kd_account']."\">".$acc['account']."</option>";
                                    }
                                }
                                ?>
                            </optgroup>
                        </select>
                    </div>
                </div>
            </div>
            <?php } ?>

            <!--<div class="form-group">
                <label for="inKeterangan" class="col-md-2 col-sm-2 col-xs-2 control-label">Usulan 3</label>
                <div class="input-group col-md-6 col-sm-6 col-xs-6">
                    <textarea class="form-control inKeterangan" name="keterangan3" id="inKeterangan" required rows="1"><?/*=($this->session->flashdata('f_keterangan')) ? htmlspecialchars_decode($this->session->flashdata('f_keterangan'), ENT_QUOTES) : '';*/?></textarea>
                    <input type="hidden" id="inKetHTML" class="inKetHTML">
                </div>
                <label for="single-prepend-text" class="col-md-1 col-sm-1 col-xs-1 control-label">Account</label>
                <div class="col-md-3 col-sm-3 col-xs-3">
                    <div class="input-group  select2-bootstrap-prepend">
						<span class="input-group-btn">
                            <button class="btn btn-default" type="button" data-select2-open="single-prepend-text">
                                <span class="glyphicon glyphicon-search"></span>
                            </button>
						</span>
                        <select id="single-prepend-text" name="account" required class="form-control select2-allow-clear select2">
                            <option></option>
                            <optgroup label="Master Account">
                                <?php
/*                                if(count($dt_account)){
                                    foreach ($dt_account as $acc) {
                                        echo "<option value=\"".$acc['kd_account']."\">".$acc['account']."</option>";
                                    }
                                }
                                */?>
                            </optgroup>
                        </select>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label for="inKeterangan" class="col-md-2 col-sm-2 col-xs-2 control-label">Usulan 4</label>
                <div class="input-group col-md-6 col-sm-6 col-xs-6">
                    <textarea class="form-control inKeterangan" name="keterangan4" id="inKeterangan" required rows="1"><?/*=($this->session->flashdata('f_keterangan')) ? htmlspecialchars_decode($this->session->flashdata('f_keterangan'), ENT_QUOTES) : '';*/?></textarea>
                    <input type="hidden" id="inKetHTML" class="inKetHTML">
                </div>
                <label for="single-prepend-text" class="col-md-1 col-sm-1 col-xs-1 control-label">Account</label>
                <div class="col-md-3 col-sm-3 col-xs-3">
                    <div class="input-group  select2-bootstrap-prepend">
						<span class="input-group-btn">
                            <button class="btn btn-default" type="button" data-select2-open="single-prepend-text">
                                <span class="glyphicon glyphicon-search"></span>
                            </button>
						</span>
                        <select id="single-prepend-text" name="account" required class="form-control select2-allow-clear select2">
                            <option></option>
                            <optgroup label="Master Account">
                                <?php
/*                                if(count($dt_account)){
                                    foreach ($dt_account as $acc) {
                                        echo "<option value=\"".$acc['kd_account']."\">".$acc['account']."</option>";
                                    }
                                }
                                */?>
                            </optgroup>
                        </select>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label for="inKeterangan" class="col-md-2 col-sm-2 col-xs-2 control-label">Usulan 5</label>
                <div class="input-group col-md-6 col-sm-6 col-xs-6">
                    <textarea class="form-control inKeterangan" name="keterangan5" id="inKeterangan" required rows="1"><?/*=($this->session->flashdata('f_keterangan')) ? htmlspecialchars_decode($this->session->flashdata('f_keterangan'), ENT_QUOTES) : '';*/?></textarea>
                    <input type="hidden" id="inKetHTML" class="inKetHTML">
                </div>
                <label for="single-prepend-text" class="col-md-1 col-sm-1 col-xs-1 control-label">Account</label>
                <div class="col-md-3 col-sm-3 col-xs-3">
                    <div class="input-group  select2-bootstrap-prepend">
						<span class="input-group-btn">
                            <button class="btn btn-default" type="button" data-select2-open="single-prepend-text">
                                <span class="glyphicon glyphicon-search"></span>
                            </button>
						</span>
                        <select id="single-prepend-text" name="account" required class="form-control select2-allow-clear select2">
                            <option></option>
                            <optgroup label="Master Account">
                                <?php
/*                                if(count($dt_account)){
                                    foreach ($dt_account as $acc) {
                                        echo "<option value=\"".$acc['kd_account']."\">".$acc['account']."</option>";
                                    }
                                }
                                */?>
                            </optgroup>
                        </select>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label for="inKeterangan" class="col-md-2 col-sm-2 col-xs-2 control-label">Usulan 6</label>
                <div class="input-group col-md-6 col-sm-6 col-xs-6">
                    <textarea class="form-control inKeterangan" name="keterangan6" id="inKeterangan" required rows="1"><?/*=($this->session->flashdata('f_keterangan')) ? htmlspecialchars_decode($this->session->flashdata('f_keterangan'), ENT_QUOTES) : '';*/?></textarea>
                    <input type="hidden" id="inKetHTML" class="inKetHTML">
                </div>
                <label for="single-prepend-text" class="col-md-1 col-sm-1 col-xs-1 control-label">Account</label>
                <div class="col-md-3 col-sm-3 col-xs-3">
                    <div class="input-group  select2-bootstrap-prepend">
						<span class="input-group-btn">
                            <button class="btn btn-default" type="button" data-select2-open="single-prepend-text">
                                <span class="glyphicon glyphicon-search"></span>
                            </button>
						</span>
                        <select id="single-prepend-text" name="account" required class="form-control select2-allow-clear select2">
                            <option></option>
                            <optgroup label="Master Account">
                                <?php
/*                                if(count($dt_account)){
                                    foreach ($dt_account as $acc) {
                                        echo "<option value=\"".$acc['kd_account']."\">".$acc['account']."</option>";
                                    }
                                }
                                */?>
                            </optgroup>
                        </select>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label for="inKeterangan" class="col-md-2 col-sm-2 col-xs-2 control-label">Usulan 7</label>
                <div class="input-group col-md-6 col-sm-6 col-xs-6">
                    <textarea class="form-control inKeterangan" name="keterangan7" id="inKeterangan" required rows="1"><?/*=($this->session->flashdata('f_keterangan')) ? htmlspecialchars_decode($this->session->flashdata('f_keterangan'), ENT_QUOTES) : '';*/?></textarea>
                    <input type="hidden" id="inKetHTML" class="inKetHTML">
                </div>
                <label for="single-prepend-text" class="col-md-1 col-sm-1 col-xs-1 control-label">Account</label>
                <div class="col-md-3 col-sm-3 col-xs-3">
                    <div class="input-group  select2-bootstrap-prepend">
						<span class="input-group-btn">
                            <button class="btn btn-default" type="button" data-select2-open="single-prepend-text">
                                <span class="glyphicon glyphicon-search"></span>
                            </button>
						</span>
                        <select id="single-prepend-text" name="account" required class="form-control select2-allow-clear select2">
                            <option></option>
                            <optgroup label="Master Account">
                                <?php
/*                                if(count($dt_account)){
                                    foreach ($dt_account as $acc) {
                                        echo "<option value=\"".$acc['kd_account']."\">".$acc['account']."</option>";
                                    }
                                }
                                */?>
                            </optgroup>
                        </select>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label for="inKeterangan" class="col-md-2 col-sm-2 col-xs-2 control-label">Usulan 8</label>
                <div class="input-group col-md-6 col-sm-6 col-xs-6">
                    <textarea class="form-control inKeterangan" name="keterangan8" id="inKeterangan" required rows="1"><?/*=($this->session->flashdata('f_keterangan')) ? htmlspecialchars_decode($this->session->flashdata('f_keterangan'), ENT_QUOTES) : '';*/?></textarea>
                    <input type="hidden" id="inKetHTML" class="inKetHTML">
                </div>
                <label for="single-prepend-text" class="col-md-1 col-sm-1 col-xs-1 control-label">Account</label>
                <div class="col-md-3 col-sm-3 col-xs-3">
                    <div class="input-group  select2-bootstrap-prepend">
						<span class="input-group-btn">
                            <button class="btn btn-default" type="button" data-select2-open="single-prepend-text">
                                <span class="glyphicon glyphicon-search"></span>
                            </button>
						</span>
                        <select id="single-prepend-text" name="account" required class="form-control select2-allow-clear select2">
                            <option></option>
                            <optgroup label="Master Account">
                                <?php
/*                                if(count($dt_account)){
                                    foreach ($dt_account as $acc) {
                                        echo "<option value=\"".$acc['kd_account']."\">".$acc['account']."</option>";
                                    }
                                }
                                */?>
                            </optgroup>
                        </select>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label for="inKeterangan" class="col-md-2 col-sm-2 col-xs-2 control-label">Usulan 9</label>
                <div class="input-group col-md-6 col-sm-6 col-xs-6">
                    <textarea class="form-control inKeterangan" name="keterangan9" id="inKeterangan" required rows="1"><?/*=($this->session->flashdata('f_keterangan')) ? htmlspecialchars_decode($this->session->flashdata('f_keterangan'), ENT_QUOTES) : '';*/?></textarea>
                    <input type="hidden" id="inKetHTML" class="inKetHTML">
                </div>
                <label for="single-prepend-text" class="col-md-1 col-sm-1 col-xs-1 control-label">Account</label>
                <div class="col-md-3 col-sm-3 col-xs-3">
                    <div class="input-group  select2-bootstrap-prepend">
						<span class="input-group-btn">
                            <button class="btn btn-default" type="button" data-select2-open="single-prepend-text">
                                <span class="glyphicon glyphicon-search"></span>
                            </button>
						</span>
                        <select id="single-prepend-text" name="account" required class="form-control select2-allow-clear select2">
                            <option></option>
                            <optgroup label="Master Account">
                                <?php
/*                                if(count($dt_account)){
                                    foreach ($dt_account as $acc) {
                                        echo "<option value=\"".$acc['kd_account']."\">".$acc['account']."</option>";
                                    }
                                }
                                */?>
                            </optgroup>
                        </select>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label for="inKeterangan" class="col-md-2 col-sm-2 col-xs-2 control-label">Usulan 10</label>
                <div class="input-group col-md-6 col-sm-6 col-xs-6">
                    <textarea class="form-control inKeterangan" name="keterangan10" id="inKeterangan" required rows="1"><?/*=($this->session->flashdata('f_keterangan')) ? htmlspecialchars_decode($this->session->flashdata('f_keterangan'), ENT_QUOTES) : '';*/?></textarea>
                    <input type="hidden" id="inKetHTML" class="inKetHTML">
                </div>
                <label for="single-prepend-text" class="col-md-1 col-sm-1 col-xs-1 control-label">Account</label>
                <div class="col-md-3 col-sm-3 col-xs-3">
                    <div class="input-group  select2-bootstrap-prepend">
						<span class="input-group-btn">
                            <button class="btn btn-default" type="button" data-select2-open="single-prepend-text">
                                <span class="glyphicon glyphicon-search"></span>
                            </button>
						</span>
                        <select id="single-prepend-text" name="account" required class="form-control select2-allow-clear select2">
                            <option></option>
                            <optgroup label="Master Account">
                                <?php
/*                                if(count($dt_account)){
                                    foreach ($dt_account as $acc) {
                                        echo "<option value=\"".$acc['kd_account']."\">".$acc['account']."</option>";
                                    }
                                }
                                */?>
                            </optgroup>
                        </select>
                    </div>
                </div>
            </div>-->

            <div class="form-group">
                <div class="col-sm-offset-3 col-sm-10">
                    <button type="submit" name="<?=$act;?>" value="Ajukan" id="btnAjukan" data-loading-text="Mengirim Usulan..." class="btn btn-primary" autocomplete="off">Ajukan</button>
                    <a href="<?=site_url('peran');?>" class="btn btn-success">Kembali</a>
                </div>
            </div>
        </form>
    </div>
</div>