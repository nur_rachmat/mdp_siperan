<?php
//$ip =  $_SERVER["REMOTE_ADDR"];
//if((substr($ip,0,10)=="192.168.35")or($ip=="116.90.208.212")){
?>
<!doctype html>
<head>
	<meta http-equiv="Content-type" content="text/html; charset=utf-8">
	<title>Schedule</title>
</head>
	<script src="dhtmlxscheduler.js" type="text/javascript" charset="utf-8"></script>
	<link rel="stylesheet" href="dhtmlxscheduler.css" type="text/css" title="no title" charset="utf-8">

	
<style type="text/css" media="screen">
	html, body{
		margin:0px;
		padding:0px;
		height:100%;
		overflow:hidden;
	}	
</style>

<script type="text/javascript" charset="utf-8">
	function init() {
		scheduler.config.xml_date="%Y-%m-%d %H:%i";
		scheduler.config.prevent_cache = true;
		
		scheduler.config.lightbox.sections=[	
			{name:"description", height:130, map_to:"text", type:"textarea" , focus:true},
			{name:"location", height:200, type:"textarea", map_to:"details" },
			{name:"time", height:72, type:"time", map_to:"auto"}
		];
		scheduler.config.first_hour = 4;
		scheduler.config.limit_time_select = true;
		scheduler.locale.labels.section_location="Peralatan yang dibutuhkan";
		scheduler.locale.labels.section_description="Agenda";
		scheduler.config.dblclick_create = false; //disable double click
		scheduler.config.buttons_right=[];
		//scheduler.config.wide_form = true;
		//scheduler.config.details_on_create=true;
		//scheduler.config.details_on_dblclick=true;

		<?php
			$y = date('Y');
			$m = date('m');
			$m = $m-1;
			$d = date('d');
		?>
		
		scheduler.init('scheduler_here',new Date(<?php echo "$y,$m,$d" ?>),"month");
		scheduler.setLoadMode("month")
		scheduler.load("events.php");
		
		var dp = new dataProcessor("events.php");
		dp.init(scheduler);

	}
</script>

<body onload="init();">
	<div id="scheduler_here" class="dhx_cal_container" style='width:100%; height:100%;'>
		<div class="dhx_cal_navline">
			<h3 style=" text-align: center;">Agenda</h3>
			<div class="dhx_cal_prev_button">&nbsp;</div>
			<div class="dhx_cal_next_button">&nbsp;</div>
			<div class="dhx_cal_today_button"></div>
			<div class="dhx_cal_date" style="padding-top:6px"></div>
			<div class="dhx_cal_tab" name="day_tab" style="right:204px;"></div>
			<div class="dhx_cal_tab" name="week_tab" style="right:140px;"></div>
			<div class="dhx_cal_tab" name="month_tab" style="right:76px;"></div>
			<h4 style=" text-align: center;color: red;text-decoration: blink;">(Untuk melihat detail agenda, klik 2 kali di nama agenda)</h4>
		</div>
		<div class="dhx_cal_header">
		</div>
		<div class="dhx_cal_data">
		</div>		
	</div>
</body>

<?php
//}else{
	//$config['base_url']	= "https://simponi.mdp.ac.id";
	//echo "Maaf, bisa diakses dari jaringan lokal. :)";
//}
?>