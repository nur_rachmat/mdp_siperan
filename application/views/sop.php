<h2>Prosedur SIPERAN</h2>

<ul class="nav nav-tabs">
    <li class="active"><a href="#siperan-tab" data-toggle="tab">Persetujuan<i class="fa"></i></a></li>
</ul>


<div class="tab-content">
	<div class="tab-pane active" id="siperan-tab">
		<div class="row">
			<div class="col-md-12">
				<ol>
					<li>Menu usulan hanya diisi oleh bag keuangan kampus</li>
					<li>Menu persetujuan hanya diakses oleh yayasan</li>
					<li>Menu laporan bisa diakses semua beserta bendahara yayasan.</li>
					<li>Pada menu persetujuan hanya menampilkan usulan yang belum disetujui</li>
					<li>Persetujuan dilakukan dengan meng-klik tombol (persetujuan)</li>
					<li>Persetujuan dapat dilakukan satu persatu, beberapa atau seluruh usulan yang status nya belum disetujui</li>
				</ol>
			</div>
	   </div>
	</div>
</div>