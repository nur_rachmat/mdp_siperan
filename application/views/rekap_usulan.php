<div class="alert alert-info no-print" role="alert">
    <h4><i class="glyphicon glyphicon-file"></i> Rekap Data Usulan Keuangan</h4>
</div>

<form method="post" action="<?=site_url('peran/daftar_usulan')?>" class="form-inline no-print">
    <div class="form-group">
        <label for="inTgl" class="col-sm-1 control-label text-right">Kas</label>
        <div class="input-group col-sm-2">
            <select class="form-control" name="kas">
                <option value="1" <?=(($this->input->post('kas') == 1) ? "selected" : "");?>>STMIK</option>
                <option value="2" <?=(($this->input->post('kas') == 2) ? "selected" : "");?>>STIE</option>
                <option value="3" <?=(($this->input->post('kas') == 3) ? "selected" : "");?>>Bersama</option>
                <option value="0" <?=(($this->input->post('kas') == 0) ? "selected" : "");?>>Semua</option>
            </select>
        </div>

        <label for="inTgl" class="col-sm-1 control-label text-right">Dari Tgl</label>
        <div class="input-group col-sm-3">
            <div class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i> </div>
            <input type="text"  value="<?=(($this->input->post()) ? $this->input->post('tgl_dari') : date("d-m-Y"));?>" name="tgl_dari" id="inTgl" class="form-control datepicker">
        </div>
        <label for="inTgls" class="col-sm-1 control-label text-center">s.d</label>
        <div class="input-group col-sm-3">
            <div class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i> </div>
            <input type="text" value="<?=(($this->input->post()) ? $this->input->post('tgl_sampai') : date("d-m-Y"));?>" name="tgl_sampai" id="inTgls" class="form-control datepicker">
        </div>

        <div class="col-sm-1">
            <button type="submit" name="cari" value="Cari" class="btn btn-primary">Cari</button>
        </div>
    </div>
</form>
<div class="clearfix">&nbsp;</div>
<?php
if($this->input->post()){
    $dari = $this->input->post('tgl_dari', true);
    $sampai = $this->input->post('tgl_sampai', true);
    $kas = $this->input->post('kas', true);
    $w_kas = "";
    $s_kas = "";
    if($kas != 0){
        $w_kas = "and t_usulan.rekening = '".$kas."'";
        if($kas == 1){
            $s_kas = "STMIK MDP";
        }elseif($kas == 2){
            $s_kas = "STIE MDP";
        }elseif($kas == 3){
            $s_kas = "Bersama";
        }
    }
    if($dari != $sampai) {
        $query = "SELECT t_usulan.*, t_pengguna.email as user FROM t_usulan inner join t_pengguna on t_usulan.pengusul = t_pengguna.id
                    where (DATE_FORMAT( t_usulan.tgl_usul,  '%Y-%m-%d' ) >= DATE_FORMAT( ?,  '%Y-%m-%d' )
                    and DATE_FORMAT( t_usulan.tgl_usul,  '%Y-%m-%d' ) <= DATE_FORMAT( ?,  '%Y-%m-%d' ))
                    and (t_usulan.status = '1' or t_usulan.status = '5') ".$w_kas." order by t_usulan.tgl_usul asc";
        $res = $this->db->query($query , array(date('Y-m-d', strtotime($dari)), date('Y-m-d', strtotime($sampai))));
    }else{
        $query = "SELECT t_usulan.*, t_pengguna.email as user FROM t_usulan inner join t_pengguna on t_usulan.pengusul = t_pengguna.id
                where DATE_FORMAT( t_usulan.tgl_usul,  '%Y-%m-%d' ) = DATE_FORMAT( ?,  '%Y-%m-%d' )
                and (t_usulan.status = '1' or t_usulan.status = '5') ".$w_kas." order by t_usulan.tgl_usul asc";
        $res = $this->db->query($query , array(date('Y-m-d', strtotime($dari))));
    }

    if($res->num_rows() > 0){
        $dt_usulan = $res->result_array();
        if($dari != $sampai){
            //echo $this->db->last_query();
            echo '<p class="alert alert-success text-center"><strong>Daftar Usulan Keuangan '.$s_kas.' Tanggal '. date('d-m-Y', strtotime($dari)) .' s.d '. date('d-m-Y', strtotime($sampai)) ."</strong></p>";
        }else{
            //echo $this->db->last_query();
            echo '<p class="alert alert-success text-center"><strong>Daftar Usulan Keuangan '.$s_kas.' Tanggal '. date('d-m-Y', strtotime($dari)) ."</strong></p>";
        }

    }else{
       // echo $this->db->last_query();
        $dt_usulan = array();
    }
}
?>
<!--<table class="table table-striped table-bordered" id="foo-rekap">
    <thead>
        <tr>
            <th >No</th>
            <th>Tgl Usul</th>
            <th>Pengusul</th>
            <th data-type="html">Keterangan</th>

            <th>Nilai</th>
        </tr>
    </thead>-->
<table class="table table-striped footable">
    <thead>
        <tr>
            <th>No.</th>
            <th>No. Ref</th>
            <th>Tgl Usul</th>
            <th data-type="html">Pengusul</th>
            <th data-breakpoints="xs sm md lg" data-type="html">Keterangan</th>
            <th>Nilai</th>
        </tr>
    </thead>
    <?php
    $total = 0;
    $i = 0;
    if(count($dt_usulan) > 0){
        echo "<tbody>";
        foreach($dt_usulan as $row){
            echo "<tr>";
            echo "<td>".(++$i)."</td>";
            echo "<td>".$row['no_ref']."</td>";
            echo "<td>".date('d-m-Y', strtotime($row['tgl_usul']))."</td>";
            $user = explode("@", $row['user']);

            echo "<td>".$user[0]."</td>";
            echo "<td class=\"det-keet\">".htmlspecialchars_decode($row['keterangan'], ENT_QUOTES)."</td>";
            echo "<td>Rp ".number_format($row['nilai'], 2, ",", ".")."</td>";

            echo "</tr>";
            $total =  $total + $row['nilai'];

        }
        echo "</tbody>";
    }
    ?>
    <tfoot>

    <tr>
        <td colspan="4" class="text-right">Total Nilai</td>
        <td><strong>Rp <?=number_format($total, 2, ",", ".");?></strong></td>
    </tr>
    <tr>
        <td colspan="4" class="text-right">Terbilang</td>
        <td><strong><?=Terbilang($total);?> Rupiah</strong></td>
    </tr>
    </tfoot>
</table>

<script src="<?php echo base_url(); ?>data_table/js/jquery.js"></script>
<script src="<?php echo base_url(); ?>assets/footable/js/footable.js"></script>
<script>
    jQuery(function($){
        $('#foo-rekap').footable({
            "useParentWidth": true,
            "paging": {
                "enabled": true
            },
            "filtering": {
                "enabled": true
            },
            "sorting": {
                "enabled": true
            }
        });
    });
</script>
