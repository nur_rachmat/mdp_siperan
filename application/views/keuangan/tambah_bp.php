<ul class="nav nav-tabs">

	<li><a href="<?=site_url('keuangan/lihat_bp');?>"><i class="glyphicon glyphicon-blackboard"> </i> Lihat Bon/Pendapatan</a></li>
	<li <?=(isset($active) && $active == 'bon') ? 'class="active"' : '';?>><a href="<?=site_url('keuangan/buat_bon');?>">Buat Bon</a></li>
	<li <?=(isset($active) && $active == 'pendapatan') ? 'class="active"' : '';?>><a href="<?=site_url('keuangan/buat_pendapatan');?>">Buat Pendapatan</a></li>

</ul>
<div class="alert alert-success no-print" role="alert">
	<h4><i class="glyphicon glyphicon-pencil"></i> Buat <?php echo ucfirst($active);?></h4>
</div>

<div class="row">
	<div class="col-md-12">
		<?php echo form_open_multipart('keuangan/buat_'.$active,array("class"=>"form-horizontal")); ?>

		<div class="form-group">
			<label for="kas" class="col-md-2 control-label">Kas</label>
			<div class="col-md-2">
				<select name="kas" class="form-control">
					<?php
					$kas_values = array(
						'1'=>'STMIK',
						'2'=>'STIE',
					);

					foreach($kas_values as $value => $display_text)
					{
						$selected = ($value == $this->input->post('kas')) ? ' selected="selected"' : "";

						echo '<option value="'.$value.'" '.$selected.'>'.$display_text.'</option>';
					}
					?>
				</select>
			</div>

			<label for="unit" class="col-md-1 control-label">Unit</label>
			<div class="col-md-2">
				<select name="unit" class="form-control  select2-allow-clear select2" data-placeholder="Pilih Unit">
					<option value=""></option>
					<?php
					foreach($all_unit as $unit)
					{
						$selected = ($unit['id'] == $this->input->post('unit')) ? ' selected="selected"' : "";

						echo '<option value="'.$unit['id'].'" '.$selected.'>'.$unit['nama_unit'].'</option>';
					}
					?>
				</select>
			</div>

			<label for="tgl_bon" class="col-md-1 control-label"><span class="text-danger">*</span>Tgl Bon</label>
			<div class="col-md-2">
				<input type="text" name="tgl_bon" value="<?php echo $this->input->post('tgl_bon'); ?>" class="form-control datepicker" id="tgl_bon" />
				<span class="text-danger"><?php echo form_error('tgl_bon');?></span>
			</div>


		</div>
		<div class="form-group">
			<label for="kd_account" class="col-md-2 control-label">Kd Account</label>
			<div class="col-md-3">
				<div class="input-group  select2-bootstrap-prepend">
						<span class="input-group-btn">
                            <button class="btn btn-default" type="button" data-select2-open="single-prepend-text">
                                <span class="glyphicon glyphicon-search"></span>
                            </button>
						</span>
					<select id="single-prepend-text" required name="kd_account" class="form-control select2-allow-clear select2">
						<option></option>
						<optgroup label="Master Account">
							<?php
							if(count($dt_account)){
								foreach ($dt_account as $acc) {
									echo "<option value=\"".$acc['kd_account']."\">".$acc['account']."</option>";
								}
							}
							?>
						</optgroup>
					</select>
				</div>
			</div>



			<label for="nilai" class="col-md-1 control-label"><span class="text-danger">*</span>Nilai</label>
			<div class="input-group col-sm-4">
				<div class="input-group-addon">Rp </div>
				<input type="number" id="inNilai" name="nilai" value="<?php echo $this->input->post('nilai'); ?>" class="form-control" />
				<div class="input-group-addon">,00</div>
			</div>
			<span class="text-danger col-sm-offset-5 col-sm-8"><?php echo form_error('nilai');?></span>
			<span id="helpSatuan" class="help-block col-sm-offset-5 col-sm-8"></span>
		</div>

		<div class="form-group">
			<label for="keterangan" class="col-md-2 control-label"><span class="text-danger">*</span>Keterangan</label>
			<div class="col-md-8">
				<textarea name="keterangan" class="form-control" id="keterangan"><?php echo $this->input->post('keterangan'); ?></textarea>
				<span class="text-danger"><?php echo form_error('keterangan');?></span>
			</div>
		</div>
		<div class="form-group">
			<label for="bon" class="col-md-2 control-label">Bon</label>
			<div class="col-md-8">
				<input type="file" name="bon" value="<?php echo $this->input->post('bon'); ?>" class="form-control" id="bon" />
			</div>

			<span class="help-block col-sm-offset-2 col-sm-8">Jika tidak ada dapat diabaikan</span>
		</div>

		<div class="form-group">
			<div class="col-sm-offset-2 col-sm-8">
				<button type="submit" name="" class="btn btn-success">Save and Input</button>
			</div>
		</div>

		<?php echo form_close(); ?>
	</div>
</div>

