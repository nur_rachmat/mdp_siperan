-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 14, 2017 at 11:41 AM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.5.37

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_siperan`
--

-- --------------------------------------------------------

--
-- Table structure for table `gcm_users`
--

CREATE TABLE `gcm_users` (
  `id` int(11) NOT NULL,
  `id_pengguna` varchar(100) DEFAULT NULL,
  `gcm_regid` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gcm_users`
--

INSERT INTO `gcm_users` (`id`, `id_pengguna`, `gcm_regid`, `created_at`) VALUES
(5, 'siddiqachmad@mdp.ac.id', 'APA91bGvErKy-iPpCvRCuOVYMAaw49EnprrunXYl5o1gc_qPINZ_PRFKl0yKBN5ozybOameEbyvcoooxkdwlf4_whU4DQJH5Crhb4kojW1K8PGXmbsvE7UxnjXMFaoKcephcFY80Ex_W', '2016-02-02 21:36:03'),
(6, 'siddiqachmad@mdp.ac.id', 'APA91bGvErKy-iPpCvRCuOVYMAaw49EnprrunXYl5o1gc_qPINZ_PRFKl0yKBN5ozybOameEbyvcoooxkdwlf4_whU4DQJH5Crhb4kojW1K8PGXmbsvE7UxnjXMFaoKcephcFY80Ex_W', '2016-02-15 00:54:54'),
(7, 'siddiqachmad@mdp.ac.id ', 'APA91bHw00Ur69lIeilV5ZQ3-PCcFGIjaA-1vcIG-TVbFLH_41Otw93vPQLVjQtutKIeJNCMHRcUFSvgMU21dAA6sc3xDVksikQIEe88AU1JIfVJHpZhDhodfkV_2EiDfo_RF8rSal5N', '2016-02-27 02:58:36'),
(8, 'yulistia@mdp.ac.id', 'APA91bGtxMce27iynm6d49GTSgCHstevhmzbWiDJlZKWAAlwXpsIljhqxPBvR7_Y4H1fKOEdBJRrCoTBxqSKIopFzh0VbyL2pMBMWdm6aB6m7e-CuUFVOj9AWE9BuvLLngSuGB7xO6iK', '2016-02-29 04:35:25');

-- --------------------------------------------------------

--
-- Table structure for table `t_account`
--

CREATE TABLE `t_account` (
  `kd_account` varchar(10) NOT NULL,
  `account` varchar(150) NOT NULL,
  `status` varchar(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_account`
--

INSERT INTO `t_account` (`kd_account`, `account`, `status`) VALUES
('111,100', 'KAS KANTOR', 'D'),
('111,200', 'KAS KANTIN', 'D'),
('111,300', 'KAS PERANTARA', 'D'),
('112,100', 'BANK CENTRAL ASIA', 'D'),
('112,200', 'BANK PANIN', 'D'),
('112,300', 'BANK CENTRAL ASIA-B', 'D'),
('112,400', 'BANK CENTRAL ASIA-C', 'D'),
('112,500', 'BANK MANDIRI', 'D'),
('113,100', 'BIAYA KULIAH YMH DITERIMA', 'D'),
('114,100', 'PIUTANG LAIN-LAIN', 'D'),
('115,100', 'PERSEDIAAN BAHAN LAB KOMPUTER', 'D'),
('116,100', 'PREMI ASURANSI', 'D'),
('116,200', 'ASTEK', 'D'),
('116,300', 'PROPISI PK BANK', 'D'),
('117,100', 'PPN MASUKAN', 'D'),
('117,200', 'PPH PSL 22', 'D'),
('117,300', 'PPH PSL 23', 'D'),
('117,500', 'PPH PSL 25', 'D'),
('121,100', 'TANAH', 'D'),
('122,100', 'BANGUNAN STMIK MDP', 'D'),
('122,200', 'BANGUNAN KANTIN STMIK MDP', 'D'),
('123,110', 'LAPANGAN BASKET', 'D'),
('123,120', 'LAPANGAN VOLLEY', 'D'),
('124,100', 'INSTALASI LISTRIK', 'D'),
('125,110', 'INVENTARIS STMIK MDP', 'D'),
('125,120', 'INVENTARIS MESS', 'D'),
('125,130', 'INVENTARIS KANTIN', 'D'),
('125,140', 'INVENTARIS PERPUSTAKAAN', 'D'),
('125,150', 'INVENTARIS LAB. KOMPUTER', 'D'),
('125,160', 'INVENTARIS LAB. BAHASA', 'D'),
('125,170', 'INVENTARIS LAB. FISIKA', 'D'),
('126,100', 'KENDARAAN', 'D'),
('130,100', 'AKM. PENYUSUTAN BANGUNAN', 'D'),
('130,200', 'AKM. PENYUSUTAN INVENTARIS', 'D'),
('130,300', 'AKM PENYUSUTAN KENDARAAN', 'D'),
('211,100', 'HUTANG USAHA', 'D'),
('212,100', 'BIAYA YANG MASIH HARUS DIBAYAR', 'D'),
('213,000', 'HUTANG PAJAK', 'D'),
('213,100', 'HUTANG PPH PSL 21', 'D'),
('213,200', 'HUTANG PPH PSL 22', 'D'),
('213,300', 'HUTANG PPH PSL 23', 'D'),
('213,500', 'HUTANG PPH PSL 25', 'D'),
('213,600', 'HUTANG PPH FINAL PSL 4 AYAT 2', 'D'),
('214,100', 'HUTANG LAIN-LAIN', 'D'),
('215,100', 'HASIL DITERIMA DIMUKA', 'D'),
('220,110', 'HUTANG BANK', 'D'),
('310,000', 'MODAL DISETOR', 'D'),
('320,100', 'L/R DITAHAN', 'D'),
('330,000', 'L/R TAHUN BERJALAN', 'D'),
('410,110', 'PENERIMAAN SUMBANGAN PEMBANGUNAN', 'D'),
('410,120', 'PENERIMAAN PENDAFTARAN MAHASISWA', 'D'),
('410,130', 'PENERIMAAN BIAYA KULIAH', 'D'),
('410,140', 'PENERIMAAN BPP, PINDAH PROGRAM', 'D'),
('410,150', 'PENERIMAAN PRATIKUM, KESEHATAN, UJIAN, BKM', 'D'),
('410,160', 'PENERIMAAN KURSUS,PAK', 'D'),
('412,110', 'RETUR PENDIDIKAN', 'D'),
('510,001', 'HONOR DOSEN/INSTRUKTUR', 'D'),
('510,002', 'GAJI KARYAWAN TETAP', 'D'),
('510,003', 'LEMBUR KARYAWAN', 'D'),
('510,004', 'PROMOSI & IKLAN', 'D'),
('510,005', 'PAKET POS/EKSPEDISI', 'D'),
('510,006', 'ALAT TULIS KANTOR & FOTOCOPY', 'D'),
('510,007', 'BENSIN, PARKIR, OLIE', 'D'),
('510,008', 'PERLENGKAPAN PENDIDIKAN', 'D'),
('510,009', 'PERALATAN KANTOR', 'D'),
('510,010', 'KONSUMSI, DAPUR & KEPERLUAN RT', 'D'),
('510,011', 'TELEPON/FAX/INTERNET', 'D'),
('510,012', 'LISTRIK & AIR', 'D'),
('510,013', 'KEAMANAN, KEBERSIHAN & IURAN', 'D'),
('510,014', 'KORAN, MAJALAH, DLL', 'D'),
('510,015', 'MATERAI & PERANGKO', 'D'),
('510,016', 'TRANSPORTASI/PERJALANAN DINAS', 'D'),
('510,017', 'PENDIDIKAN, TRAINING & SEMINAR', 'D'),
('510,018', 'BIAYA PAJAK', 'D'),
('510,019', 'PREMI ASURANSI', 'D'),
('510,020', 'BIAYA SEWA', 'D'),
('510,021', 'BIAYA PERIZINAN', 'D'),
('510,022', 'BIAYA ASTEK', 'D'),
('510,023', 'ENTERTAINMENT & SUMBANGAN', 'D'),
('510,024', 'PAKAIAN SERAGAM', 'D'),
('510,025', 'BEASISWA', 'D'),
('510,026', 'BIAYA PEMELIHARAAN BANGUNAN STMIK', 'D'),
('510,027', 'BIAYA PEMELIHARAAN BANGUNAN KANTIN', 'D'),
('510,028', 'BIAYA PEMELIHARAAN KENDARAAN', 'D'),
('510,029', 'BIAYA PEMELIHARAAN INVENTARIS', 'D'),
('510,030', 'BIAYA PERLENGKAPAN MESS', 'D'),
('510,031', 'BIAYA KEGIATAN MAHASISWA', 'D'),
('510,032', 'BIAYA PENGOBATAN', 'D'),
('520,001', 'PEMAKAIAN BAHAN-LAB KOMPUTER', 'D'),
('520,002', 'PEMAKAIAN BAHAN-LAB BAHASA', 'D'),
('520,003', 'PERAWATAN RUANG KULIAH/LAB/WC', 'D'),
('520,004', 'BIAYA PENYUSUTAN BANGUNAN', 'D'),
('520,005', 'BIAYA PENYUSUTAN KENDARAAN', 'D'),
('520,006', 'BIAYA PENYUSUTAN INVENTARIS', 'D'),
('540,001', 'PROPISI', 'D'),
('540,002', 'BIAYA ADMINISTRASI BANK', 'D'),
('540,003', 'MATERAI', 'D'),
('540,004', 'SELISIH KURS', 'D'),
('540,100', 'BUNGA PINJAMAN', 'D'),
('610,100', 'JASA GIRO', 'D'),
('610,200', 'PENERIMAAN PENJUALAN KUPON KANTIN', 'D'),
('610,201', 'BIAYA OPERASIONAL KANTIN', 'D'),
('610,300', 'PENERIMAAN PENJUALAN BUKU & ATK', 'D'),
('610,301', 'BIAYA PENJUALAN BUKU & ATK', 'D'),
('610,400', 'PENERIMAAN PENJUALAN AKTIVA TETAP', 'D'),
('610,900', 'PENERIMAAN LAIN-LAIN', 'D'),
('610,901', 'BIAYA LAIN-LAIN', 'D'),
('510,033', 'BIAYA HOST TO HOST SYB', 'D');

-- --------------------------------------------------------

--
-- Table structure for table `t_account_lama`
--

CREATE TABLE `t_account_lama` (
  `kd_account` varchar(10) NOT NULL,
  `account` varchar(150) NOT NULL,
  `status` varchar(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_account_lama`
--

INSERT INTO `t_account_lama` (`kd_account`, `account`, `status`) VALUES
('510,004', 'PROMOSI & IKLAN', 'D'),
('510,005', 'PAKET POS/EKSPEDISI', 'D'),
('510,006', 'ALAT TULIS KANTOR & FOTOCOPY', 'D'),
('510,007', 'BENSIN, PARKIR, OLIE', 'D'),
('510,008', 'PERLENGKAPAN PENDIDIKAN', 'D'),
('510,009', 'PERALATAN KANTOR', 'D'),
('510,010', 'KONSUMSI, DAPUR & KEPERLUAN RT', 'D'),
('510,011', 'TELEPON/FAX/INTERNET', 'D'),
('510,012', 'LISTRIK & AIR', 'D'),
('510,013', 'KEAMANAN, KEBERSIHAN & IURAN', 'D'),
('510,014', 'KORAN, MAJALAH, DLL', 'D'),
('510,015', 'MATERAI & PERANGKO', 'D'),
('510,016', 'TRANSPORTASI/PERJALANAN DINAS', 'D'),
('510,017', 'PENDIDIKAN, TRAINING & SEMINAR', 'D'),
('510,018', 'BIAYA PAJAK', 'D'),
('510,019', 'PREMI ASURANSI', 'D'),
('510,020', 'BIAYA SEWA', 'D'),
('510,021', 'BIAYA PERIZINAN', 'D'),
('510,022', 'BIAYA ASTEK', 'D'),
('510,023', 'ENTERTAINMENT & SUMBANGAN', 'D'),
('510,024', 'PAKAIAN SERAGAM', 'D'),
('510,025', 'BEASISWA', 'D'),
('510,026', 'BIAYA PEMELIHARAAN BANGUNAN STMIK', 'D'),
('510,027', 'BIAYA PEMELIHARAAN BANGUNAN KANTIN', 'D'),
('510,028', 'BIAYA PEMELIHARAAN KENDARAAN', 'D'),
('510,029', 'BIAYA PEMELIHARAAN INVENTARIS', 'D'),
('510,030', 'BIAYA PERLENGKAPAN MESS', 'D'),
('510,031', 'BIAYA KEGIATAN MAHASISWA', 'D'),
('510,032', 'BIAYA PENGOBATAN', 'D'),
('520,000', 'BIAYA OPERASIONAL STMIK MDP', 'D'),
('520,001', 'PEMAKAIAN BAHAN-LAB KOMPUTER', 'D'),
('520,002', 'PEMAKAIAN BAHAN-LAB BAHASA', 'D'),
('520,003', 'PERAWATAN RUANG KULIAH/LAB/WC', 'D'),
('520,004', 'BIAYA PENYUSUTAN BANGUNAN', 'D'),
('520,005', 'BIAYA PENYUSUTAN KENDARAAN', 'D'),
('520,006', 'BIAYA PENYUSUTAN INVENTARIS', 'D'),
('540,000', 'BIAYA KEUANGAN & LAIN-LAIN', 'D'),
('540,001', 'PROPISI', 'D'),
('540,002', 'BIAYA ADMINISTRASI BANK', 'D'),
('540,003', 'MATERAI', 'D'),
('540,004', 'SELISIH KURS', 'D'),
('540,100', 'BUNGA PINJAMAN', 'D'),
('610,100', 'JASA GIRO', 'K'),
('610,201', 'BIAYA OPERASIONAL KANTIN', 'D'),
('610,301', 'BIAYA PENJUALAN BUKU & ATK', 'D'),
('610,901', 'BIAYA LAIN-LAIN', 'D');

-- --------------------------------------------------------

--
-- Table structure for table `t_bon`
--

CREATE TABLE `t_bon` (
  `id` int(4) NOT NULL,
  `tgl_bon` date DEFAULT NULL,
  `nilai` varchar(25) DEFAULT NULL,
  `kas` int(1) NOT NULL,
  `unit` int(1) NOT NULL,
  `accounter` int(4) NOT NULL,
  `kd_account` varchar(10) DEFAULT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  `bon` varchar(255) DEFAULT NULL,
  `tgl_buat` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `status` enum('bon','pendapatan') DEFAULT 'bon'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `t_item_bon`
--

CREATE TABLE `t_item_bon` (
  `id` int(4) NOT NULL,
  `id_bon` int(4) NOT NULL,
  `kd_account` varchar(10) NOT NULL,
  `tgl_bon` date DEFAULT NULL,
  `nilai` varchar(25) DEFAULT NULL,
  `kas` int(1) NOT NULL,
  `unit` int(1) NOT NULL,
  `accounter` int(4) NOT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  `bon` varchar(255) DEFAULT NULL,
  `tgl_buat` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `t_nota`
--

CREATE TABLE `t_nota` (
  `id_nota` int(11) NOT NULL,
  `no_ref` varchar(10) NOT NULL,
  `nilai` varchar(10) DEFAULT NULL,
  `keterangan` longtext NOT NULL,
  `tgl_nota` date DEFAULT NULL,
  `f_nota` varchar(150) DEFAULT NULL,
  `kd_account` varchar(10) NOT NULL,
  `pcash` int(6) DEFAULT NULL,
  `lock` varchar(1) DEFAULT 'T',
  `tgl_buatnota` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `nota_kas` int(1) DEFAULT NULL,
  `accounter` int(4) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `t_pcash`
--

CREATE TABLE `t_pcash` (
  `id` int(4) NOT NULL,
  `kode_pcash` varchar(10) NOT NULL,
  `tgl_pcash` date DEFAULT NULL,
  `nilai` varchar(25) DEFAULT NULL,
  `kas_owner` int(1) NOT NULL,
  `accounter` int(4) NOT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  `lock` enum('T','Y') DEFAULT NULL,
  `tgl_buat` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `tgl_lock` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `t_pembiayaan`
--

CREATE TABLE `t_pembiayaan` (
  `id_pembiayaan` int(11) NOT NULL,
  `no_ref` int(11) DEFAULT NULL,
  `keterangan` mediumtext NOT NULL,
  `nilai` varchar(12) NOT NULL,
  `tgl_nota` date NOT NULL,
  `tgl_input` datetime NOT NULL,
  `nota` varchar(255) DEFAULT NULL,
  `accounter` int(4) NOT NULL,
  `kd_account` varchar(10) NOT NULL,
  `kas` int(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `t_pengguna`
--

CREATE TABLE `t_pengguna` (
  `id` int(4) NOT NULL,
  `email` varchar(100) NOT NULL,
  `username` varchar(15) DEFAULT NULL,
  `name` varchar(150) DEFAULT NULL,
  `password` varchar(100) NOT NULL,
  `level` enum('SuperAdmin','Ketua','Administrasi','Yayasan','Bendahara','Accounting') NOT NULL,
  `notifikasi` enum('Y','N') NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `t_pengguna`
--

INSERT INTO `t_pengguna` (`id`, `email`, `username`, `name`, `password`, `level`, `notifikasi`) VALUES
(1, 'rachmat.nur91@staff.mdp.ac.id', 'njr', NULL, '7c4a8d09ca3762af61e59520943dc26494f8941b', 'SuperAdmin', 'Y'),
(3, 'meilyani@mdp.ac.id', 'bak', 'Meilyani', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'Administrasi', 'Y'),
(4, 'alex@mdp.co.id', 'yayasan', 'Alexander Kurniawan', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'Yayasan', 'Y'),
(5, 'neneng@mdp.co.id', 'bendahara', 'Neneng', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'Bendahara', 'Y'),
(6, 'johannes@mdp.ac.id', 'ketua', 'Johannes Petrus', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'Ketua', 'Y');

-- --------------------------------------------------------

--
-- Table structure for table `t_transfer`
--

CREATE TABLE `t_transfer` (
  `id_usulan` int(6) NOT NULL,
  `no_ref` varchar(10) NOT NULL,
  `nama_penerima` varchar(50) NOT NULL,
  `rek_penerima` varchar(15) NOT NULL,
  `pesan` text,
  `tgl_permintaan` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `t_unit`
--

CREATE TABLE `t_unit` (
  `id` tinyint(4) NOT NULL,
  `nama_unit` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `t_usulan`
--

CREATE TABLE `t_usulan` (
  `id` int(6) NOT NULL,
  `tgl_usul` datetime NOT NULL,
  `pengusul` int(4) NOT NULL,
  `nilai` varchar(10) CHARACTER SET latin1 NOT NULL,
  `keterangan` longtext CHARACTER SET latin1 NOT NULL,
  `status` enum('0','1','3','4','5','6') CHARACTER SET latin1 NOT NULL COMMENT '0:Diajukan. 1:Disetujui, 3:Ditolak, 4:Dibatalkan, 5:Minta Dana, 6:Dsetujui Ketua',
  `tgl_rev` datetime DEFAULT NULL,
  `revby` int(4) DEFAULT NULL,
  `no_ref` varchar(10) DEFAULT NULL,
  `kd_account` varchar(10) DEFAULT NULL,
  `tgl_nota` date DEFAULT NULL,
  `nota_kas` int(1) DEFAULT NULL,
  `kd_pcash` varchar(10) DEFAULT NULL,
  `notes` varchar(255) DEFAULT NULL,
  `f_nota` varchar(150) DEFAULT NULL,
  `accounter` int(4) DEFAULT NULL,
  `rekening` int(1) DEFAULT NULL COMMENT '1=STMIK, 2=STIE, 3=Bersama',
  `dana` varchar(10) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `gcm_users`
--
ALTER TABLE `gcm_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_account`
--
ALTER TABLE `t_account`
  ADD UNIQUE KEY `kd_account` (`kd_account`);

--
-- Indexes for table `t_account_lama`
--
ALTER TABLE `t_account_lama`
  ADD UNIQUE KEY `kd_account` (`kd_account`);

--
-- Indexes for table `t_bon`
--
ALTER TABLE `t_bon`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_item_bon`
--
ALTER TABLE `t_item_bon`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_nota`
--
ALTER TABLE `t_nota`
  ADD PRIMARY KEY (`id_nota`);

--
-- Indexes for table `t_pcash`
--
ALTER TABLE `t_pcash`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_pembiayaan`
--
ALTER TABLE `t_pembiayaan`
  ADD PRIMARY KEY (`id_pembiayaan`);

--
-- Indexes for table `t_pengguna`
--
ALTER TABLE `t_pengguna`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_transfer`
--
ALTER TABLE `t_transfer`
  ADD PRIMARY KEY (`id_usulan`),
  ADD UNIQUE KEY `no_ref` (`no_ref`);

--
-- Indexes for table `t_unit`
--
ALTER TABLE `t_unit`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_usulan`
--
ALTER TABLE `t_usulan`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `gcm_users`
--
ALTER TABLE `gcm_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `t_bon`
--
ALTER TABLE `t_bon`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `t_item_bon`
--
ALTER TABLE `t_item_bon`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `t_nota`
--
ALTER TABLE `t_nota`
  MODIFY `id_nota` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `t_pcash`
--
ALTER TABLE `t_pcash`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `t_pembiayaan`
--
ALTER TABLE `t_pembiayaan`
  MODIFY `id_pembiayaan` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `t_pengguna`
--
ALTER TABLE `t_pengguna`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `t_unit`
--
ALTER TABLE `t_unit`
  MODIFY `id` tinyint(4) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `t_usulan`
--
ALTER TABLE `t_usulan`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
