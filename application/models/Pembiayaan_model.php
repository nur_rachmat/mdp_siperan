<?php
/**
 * Created by PhpStorm.
 * User: Njr
 * Date: 7/28/2017
 * Time: 4:24 PM
 */

class Pembiayaan_Model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    /*
     * Get order by order_id
     */
    function get_pembiayaan($pembiayaan_id)
    {
        return $this->db->get_where('t_pembiayaan',array('id_pembiayaan'=>$pembiayaan_id))->row_array();
    }



    /*
     * Get all t_pembiayaan
     */
    function get_all_pembiayaan()
    {
        $this->db->order_by('id_pembiayaan', 'desc');
        return $this->db->get('t_pembiayaan')->result_array();
    }

    function get_all_pembiayaan_where($where)
    {
        return $this->db->get_where('t_pembiayaan',$where)->result_array();
    }

    /*
     * function to add new order
     */
    function add_pembiayaan($params)
    {
        $this->db->insert('t_pembiayaan',$params);
        return $this->db->insert_id();
    }

    /*
     * function to update order
     */
    function update_pembiayaan($pembiayaan_id,$params)
    {
        $this->db->where('id_pembiayaan',$pembiayaan_id);
        return $this->db->update('t_pembiayaan',$params);
    }

    /*
     * function to delete order
     */
    function delete_pembiayaan($pembiayaan_id)
    {
        return $this->db->delete('t_pembiayaan',array('id_pembiayaan'=>$pembiayaan_id));
    }
}