<?php if($this->session->flashdata('simpan_ok')){ ?>
    <div class="alert alert-success alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <?=$this->session->flashdata('simpan_ok');?>
    </div>
<?php } ?>
<ul class="nav nav-tabs">
    <?php
    $level = $this->session->userdata('admin_level');
    if($level != "Ketua") {
    ?>
    <li <?=(isset($active)) ? '' : 'class="active"';?>><a href="<?=site_url('peran/usulan');?>">Usulan Diajukan</a></li>
    <?php } ?>
    <li <?=(isset($active) && $active == 'setuju') ? 'class="active"' : '';?>><a href="<?=site_url('peran/usulan/setuju');?>">Usulan Disetujui</a></li>
    <li <?=(isset($active) && $active == 'tolak') ? 'class="active"' : '';?>><a href="<?=site_url('peran/usulan/tolak');?>">Usulan Tidak Disetujui </a></li>
    <li <?=(isset($active) && $active == 'batal') ? 'class="active"' : '';?>><a href="<?=site_url('peran/usulan/batal');?>">Usulan Dibatalkan </a></li>
    <?php //if($level != "Ketua") { ?>

    <?php //} ?>
</ul>
<div class="claerfix">&nbsp;</div>
<?php if($level != "Ketua") { ?>
<a href="<?=site_url('peran/tambah_usulan');?>"><button type="submit" class="btn btn-primary"><i class="icon-plus icon-white"></i> Ajukan Usulan</button></a>
<?php  } ?>
<div class="claerfix">&nbsp;</div>
<div class="alert alert-success" role="alert">
    <h4>
        <i class="icon-tasks"></i> Usulan Keuangan yang Telah Diajukan
        <span class="label label-danger pull-right"> <?=count($dt_usulan);?> Usulan </span>
    </h4>
</div>
<?php $show = false; if($show ==  true) : ?>

<table class="table table-striped footable">
    <thead>
    <tr>
        <th>No. Ref</th>
        <th data-breakpoints="xs sm" data-type="date">Tgl Usul</th>

        <th>Pengusul</th>
        <?=(isset($active) && $active == 'setuju') ? '<th data-breakpoints="xs">Tgl Acc</th>' : '';?>
        <th>Nilai</th>
        <th data-breakpoints="xs sm" data-type="html">Kas</th>
        <th data-breakpoints="xs sm" data-type="html">Keterangan</th>
        <th data-type="html">Status</th>
        <?php if(!isset($active)){ ?><th data-type="html" data-breakpoints="xs">Aksi</th><?php } ?>
    </tr>
    </thead>
    <?php
    if(count($dt_usulan) > 0){
        echo "<tbody>";
        foreach($dt_usulan as $row){
            echo "<tr>";
            echo "<td>".$row['no_ref']."</td>";
            echo "<td>".date('d-m-Y H:i:s', strtotime($row['tgl_usul']))."</td>";
            $user = explode("@", $row['user']);

            echo "<td>".$user[0]."</td>";
            echo (isset($active) && $active == 'setuju') ? "<td>".date('d-m-Y H:i:s', strtotime($row['tgl_rev']))."</td>" : '';

            if($row['rekening'] === '1'){
                $kas = "<span class='label label-info'>STMIK</span>";
            }elseif($row['rekening'] == '2'){
                $kas = "<span class='label label-success'>STIE</span>";
            }elseif($row['rekening'] == '3'){
                $kas = "<span class='label label-default'>Bersama</span>";
            }else{
                $kas = "<span class='label label-danger'>N/A</span>";
            }
            echo "<td title='".Terbilang($row['nilai'])." Rupiah'>Rp ".number_format($row['nilai'], 2, ",", ".")."</td>";
            //echo "<td>".$row['nilai']."</td>";
            echo "<td>".$kas."</td>";
            $ket = strip_tags(htmlspecialchars_decode($row['keterangan']), "<img>");
            $k_img = preg_replace("/<img[^>]+\>/i", " [ <b>image</b> ] ", $ket);

            $link_img = "<a data-toggle='modal' data-target='#modal' href='#' data-href='".site_url('peran/detail_usulan/'.$row['id'])."' >image</a>";
            //$k_img = str_replace('image', $link_img, $k_img);
            $link_more = " [ <a data-toggle='modal' data-target='#modal' href='#' data-href='".site_url('peran/detail_usulan/'.$row['id'])."' ><i class='glyphicon glyphicon-eye-open'></i> detail</a> ]";
            echo "<td>".substr($k_img, 0, 100).((strlen($k_img) > 75) ? $link_more : "")."</td>";



            $act_batal = " <a href='".site_url('peran/batal_usulan/'.$row['id'])."'><button class='btn btn-warning btn-sm '><i class='glyphicon glyphicon-remove-circle'></i> Batal</button></a>";
            $act_ubah = " <a href='".site_url('peran/ubah_usulan/'.$row['id'])."'><button class='btn btn-default btn-sm '><i class='glyphicon glyphicon-edit'></i> Ubah</button></a>";
            $act_minta = '<div class="btn-group">
                      <button class="btn btn-success btn-sm dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Transfer ke <span class="caret"></span>
                      </button>
                      <ul class="dropdown-menu">
                        <li><a href="'.site_url('peran/minta_dana/'.$row['id'].'/stmik').'">Rek. STMIK</a></li>
                        <li><a href="'.site_url('peran/minta_dana/'.$row['id'].'/stie').'">Rek. STIE</a></li>
                        <li><a href="'.site_url('peran/minta_dana/'.$row['id'].'/tunai').'">Tunai</a></li>
                        <li><a href="#" data-toggle="modal" data-target="#modal-reklain" data-noref="'.$row['no_ref'].'" data-idusulan="'.$row['id'].'">Rekening Lain</a></li>
                      </ul>
                    </div>';
            //$act_minta = " <a href='".site_url('peran/minta_dana/'.$row['id'])."'><button class='btn btn-success btn-sm '><i class='glyphicon glyphicon-transfer'></i> Minta Dana</button></a>";
            if($row['status'] == 0){
                $status = "<span class='label label-info lbl-sm'>Telah Diajukan</span> ";
                $act_batal = $act_batal . " ". $act_ubah;
            }elseif($row['status'] == 1){
                $status = "<span class='label label-success lbl-sm'>Disetujui Yayasan</span>";
                $act_batal = $act_minta;
            }elseif($row['status'] == 3){
                $status = "<span class='label label-danger lbl-sm'>Ditolak</span>";
                $act_batal = '';
            }elseif($row['status'] == 4){
                $status = "<span class='label label-warning lbl-sm'>Dibatalkan</span>";
                $act_batal = '';
            }elseif($row['status'] == 5) {
                $status = "<span class='label label-success lbl-sm'>Disetujui</span>";
                $act_batal = "[ diajukan ke bendahara ]";
            }elseif($row['status'] == 6){
                $status = "<span class='label label-default lbl-sm'>Disetujui Ketua</span>";
                $act_batal = '';
            }else{
                $status = "<span class='label label-default lbl-sm'>Tidak Diketahui</span>";
                $act_batal = '';
            }


            echo "<td >".$status."</td>";
            if(!isset($active)) {
                echo "<td class='text-center'>" . $act_batal . "</td>";
            }
            echo "</tr>";

        }
        echo "</tbody>";
    }

    /*function Terbilang($satuan){
        $huruf = array ("", "Satu", "Dua", "Tiga", "Empat", "Lima", "Enam",
            "Tujuh", "Delapan", "Sembilan", "Sepuluh","Sebelas");
        if ($satuan < 12)
            return " ".$huruf[$satuan];
        elseif ($satuan < 20)
            return Terbilang($satuan - 10)." Belas";
        elseif ($satuan < 100)
            return Terbilang($satuan / 10)." Puluh".Terbilang($satuan % 10);
        elseif ($satuan < 200)
            return "Seratus".Terbilang($satuan - 100);
        elseif ($satuan < 1000)
            return Terbilang($satuan / 100)." Ratus".Terbilang($satuan % 100);
        elseif ($satuan < 2000)
            return "Seribu".Terbilang($satuan - 1000);
        elseif ($satuan < 1000000)
            return Terbilang($satuan / 1000)." Ribu".Terbilang($satuan % 1000);
        elseif ($satuan < 1000000000)
            return Terbilang($satuan / 1000000)." Juta".Terbilang($satuan % 1000000);
        elseif ($satuan >= 1000000000)
            return "Angka terlalu Besar";
    }*/
    ?>
</table>
<?php endif; ?>

<table id="table-usulandiajukan" class="table" data-paging="true"></table>


<!--<table class="table table-striped" id="dataTable">
    <thead>
        <tr>
            <th>Tgl Usul</th>
            <th>Pengusul</th>
            <?/*=(isset($active) && $active == 'setuju') ? '<th>Tgl Acc</th>' : '';*/?>
            <th>Nilai</th>
            <th>Keterangan</th>
            <th>Status</th>
            <?php /*if(!isset($active)){ */?><th>Aksi</th><?php /*} */?>
        </tr>
    </thead>
    <?php
/*    if(count($dt_usulan) > 0){
        echo "<tbody>";
        foreach($dt_usulan as $row){
            echo "<tr>";
            echo "<td>".date('d-m-Y H:i:s', strtotime($row['tgl_usul']))."</td>";
            $user = explode("@", $row['user']);
            echo "<td>".$user[0]."</td>";
            echo (isset($active) && $active == 'setuju') ? "<td>".date('d-m-Y H:i:s', strtotime($row['tgl_rev']))."</td>" : '';

            echo "<td>".$row['nilai']."</td>";
            $ket = strip_tags(htmlspecialchars_decode($row['keterangan']), "<img>");
            $k_img = preg_replace("/<img[^>]+\>/i", " [ <b>image</b> ] ", $ket);
            $link_more = " [ <a data-toggle='modal' data-target='#modal'  href='".site_url('peran/detail_usulan/'.$row['id'])."' ><i class='glyphicon glyphicon-eye-open'></i> detail</a> ]";
            echo "<td>".substr($k_img, 0, 100).((strlen($k_img) > 100) ? $link_more : "")."</td>";



            $act_batal = " <a href='".site_url('peran/batal_usulan/'.$row['id'])."'><button class='btn btn-warning btn-sm '><i class='glyphicon glyphicon-remove-circle'></i> Batal</button></a>";
            $act_ubah = " <a href='".site_url('peran/ubah_usulan/'.$row['id'])."'><button class='btn btn-default btn-sm '><i class='glyphicon glyphicon-edit'></i> Ubah</button></a>";
            $act_minta = " <a href='".site_url('peran/minta_dana/'.$row['id'])."'><button class='btn btn-success btn-sm '><i class='glyphicon glyphicon-transfer'></i> Minta Dana</button></a>";
            if($row['status'] == 0){
                $status = "<span class='label label-info lbl-sm'>Telah Diajukan</span> ";
                $act_batal = $act_batal . " ". $act_ubah;
            }elseif($row['status'] == 1){
                $status = "<span class='label label-success lbl-sm'>Disetujui</span>";
                $act_batal = $act_minta;
            }elseif($row['status'] == 3){
                $status = "<span class='label label-danger lbl-sm'>Ditolak</span>";
                $act_batal = '';
            }elseif($row['status'] == 4){
                $status = "<span class='label label-warning lbl-sm'>Dibatalkan</span>";
                $act_batal = '';
            }elseif($row['status'] == 5){
                $status = "<span class='label label-success lbl-sm'>Disetujui</span>";
                $act_batal = "[ diajukan ke bendahara ]";

            }else{
                $status = "<span class='label label-default lbl-sm'>Tidak Diketahui</span>";
                $act_batal = '';
            }

            echo "<td >".$status."</td>";
            if(!isset($active)) {
                echo "<td class='text-center'>" . $act_batal . "</td>";
            }
            echo "</tr>";

        }
        echo "</tbody>";
    }
    */?>



</table>-->

<!-- Modal -->
<div class="modal fade bs-example-modal-lg" id="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Detail Usulan</h4>
            </div>
            <div class="modal-body">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade bs-example-modal-lg" id="modal-reklain" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Transfer Ke Rekening Lain</h4>
            </div>
            <form class="form-horizontal" method="post" action="">
                <div class="modal-body">
                    <div class="form-group">
                        <label for="recipient-name" class="col-sm-2 control-label text-left">No. Ref / ID:</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" name="ref-no" id="ref-no" readonly>
                        </div>
                        <div class="col-sm-2">
                            <input type="text" class="form-control" name="idusulan" id="idusulan" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="col-sm-2 control-label">Data Penerima</label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" required name="recipient-name" placeholder="Nama Penerima">
                        </div>
                        <div class="col-sm-5">
                         <input type="number" class="form-control" required name="recipient-rek" placeholder="Nomor Rekening ">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="message-text" class="col-sm-2 control-label">Pesan:</label>
                        <div class="col-sm-10">
                            <textarea class="form-control" required name="message-text"></textarea>
                        </div>
                    </div>

                    <p class="bg-warning text-danger text-center">
                        Harap perhatikan kembali Data Penerima dana sebelum mengirimkan permintaan Transfer Rekening.<br/>
                        Pastikan Data yang dimasukkan benar karena proses yang telah dilakukan tidak dapat dibatalkan lagi.
                    </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary" id="btn_transfer">Kirim</button>
                </div>
            </form>
        </div>
    </div>
</div>