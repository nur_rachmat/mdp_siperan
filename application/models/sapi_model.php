<?php
/**
 * Created by PhpStorm.
 * User: Njr
 * Date: 12/12/2015
 * Time: 11:09 AM
 */
class Sapi_Model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }


    function get_dt_account(){
        $Q = "SELECT * FROM t_account order by kd_account ASC";
        $q_acc = $this->db->query($Q);
        return $q_acc->result_array();
    }

    function get_dt_nota($tgl_nota = "", $kas="", $account= ""){
        if($tgl_nota == "" && $kas == "" && $account == "") {
            $sql = "SELECT  t_usulan.*, t_account.account as account FROM t_nota t_usulan inner join t_account on t_usulan.kd_account = t_account.kd_account
            where t_usulan.lock = ? ORDER BY t_usulan.tgl_nota ASC";
            $result = $this->db->query($sql, array('Y'));
        }elseif($tgl_nota == "" && $kas == "" && $account != "") {
            $sql = "SELECT  t_usulan.*, t_account.account as account FROM t_nota t_usulan inner join t_account on t_usulan.kd_account = t_account.kd_account
            where t_usulan.kd_account = ? and t_usulan.lock = ? ORDER BY t_usulan.tgl_nota ASC";
            $result = $this->db->query($sql, array($account, 'Y'));
        }elseif($tgl_nota == "" && $kas != "" && $account == ""){
            $sql = "SELECT  t_usulan.*, t_account.account as account FROM t_nota t_usulan inner join t_account on t_usulan.kd_account = t_account.kd_account
            where t_usulan.pcash = ? and t_usulan.lock = ? ORDER BY t_usulan.tgl_nota ASC";
            $result = $this->db->query($sql, array($kas, 'Y'));
        }elseif($tgl_nota != "" && $kas == "" && $account == ""){
            $tgl_nota = date('Y-m-d', strtotime($tgl_nota));
            $sql = "SELECT  t_usulan.*, t_account.account as account FROM t_nota t_usulan inner join t_account on t_usulan.kd_account = t_account.kd_account
            where t_usulan.tgl_nota = ? and t_usulan.lock = ? ORDER BY t_usulan.tgl_nota ASC";
            $result = $this->db->query($sql, array($tgl_nota, 'Y'));
        }else{
            $tgl_nota = date('Y-m-d', strtotime($tgl_nota));
            $sql = "SELECT  t_usulan.*, t_account.account as account, t_pcash.kode_pcash, t_pcash.keterangan as pettycash  FROM t_nota t_usulan
            inner join t_account on t_usulan.kd_account = t_account.kd_account INNER JOIN t_pcash on t_usulan.pcash = t_pcash.id
            where t_usulan.tgl_nota = ? AND t_usulan.nota_kas = ? AND t_usulan.lock = ? ORDER BY t_usulan.tgl_nota ASC";
            $result = $this->db->query($sql, array($tgl_nota, $kas, 'Y'));
        }
        return $result->result_array();
    }

    function get_dt_pcash($tgl_pcash = "", $kas=""){
        if($tgl_pcash == "" && $kas == "") {
            $sql = "SELECT  t_pcash.* as account FROM t_pcash where t_pcash.lock = ? ORDER BY t_pcash.tgl_pcash ASC";
            $result = $this->db->query($sql, array('Y'));
        }elseif($tgl_pcash != "" && $kas == ""){
            $tgl_pcash =date('Y-m-d', strtotime($tgl_pcash));
            $sql = "SELECT  t_pcash.* as account FROM t_pcash where t_pcash.tgl_pcash = ? AND t_pcash.lock = ? ORDER BY t_pcash.tgl_pcash ASC";
            $result = $this->db->query($sql, array($tgl_pcash, 'Y'));
        }elseif($tgl_pcash == "" && $kas != ""){
            $tgl_pcash =date('Y-m-d', strtotime($tgl_pcash));
            $sql = "SELECT  t_pcash.* as account FROM t_pcash where t_pcash.kas_owner = ? AND t_pcash.lock = ? ORDER BY t_pcash.tgl_pcash ASC";
            $result = $this->db->query($sql, array($kas, 'Y'));
        }else{
            $tgl_pcash =date('Y-m-d', strtotime($tgl_pcash));
            $sql = "SELECT  t_pcash.* as account FROM t_pcash where t_pcash.tgl_pcash = ? AND t_pcash.kas_owner = ? AND t_pcash.lock = ? ORDER BY t_pcash.tgl_pcash ASC";
            $result = $this->db->query($sql, array($tgl_pcash, $kas, 'Y'));
        }
        return $result->result_array();
    }

    function get_dt_pcash_fpcash($kd_pcash){


        $sql = "SELECT  t_pcash.* as account FROM t_pcash where t_pcash.kode_pcash = ? AND t_pcash.lock = ? ORDER BY t_pcash.tgl_pcash ASC";
        $result = $this->db->query($sql, array($kd_pcash, 'Y'));

        return $result->result_array();
    }

    function get_dt_notakosong(){
        $sql = "SELECT  t_usulan.*, t_account.account as account FROM t_nota t_usulan
inner join t_account on t_usulan.kd_account = t_account.kd_account
where t_usulan.tgl_nota IS NULL ORDER BY t_usulan.tgl_buatnota DESC ";

        $result = $this->db->query($sql);
        return $result->result_array();
    }


    function get_nota($tgl_nota, $kas_nota){
        $sql = "SELECT  t_usulan.*, t_account.account as account
FROM t_nota t_usulan inner join t_account on t_usulan.kd_account = t_account.kd_account
where (DATE_FORMAT( t_usulan.tgl_nota,  '%Y-%m-%d' ) = DATE_FORMAT( ?,  '%Y-%m-%d' ))
and t_usulan.nota_kas = ? ORDER BY t_usulan.id_nota ASC";
        $result = $this->db->query($sql, array($tgl_nota, $kas_nota));
        //echo $this->db->last_query();
        return $result->result_array();
    }

    function get_group_account($acc = ""){
        if($acc != "" && strlen(trim($acc)) > 0){
            $sql = "SELECT t_nota.`kd_account`, account, sum(nilai) as nominal, count(id_nota) as jumlah FROM `t_nota`
            inner join t_account on t_nota.kd_account = t_account.kd_account WHERE t_nota.kd_account = ? AND t_nota.lock = 'Y'
            GROUP BY t_nota.kd_account order by t_nota.kd_account asc";
            $result = $this->db->query($sql, array($acc));
        }else{
            $sql = "SELECT t_nota.`kd_account`, account, sum(nilai) as nominal, count(id_nota) as jumlah FROM `t_nota`
            inner join t_account on t_nota.kd_account = t_account.kd_account WHERE t_nota.lock = 'Y'
            GROUP BY t_nota.kd_account order by t_nota.kd_account asc";
            $result = $this->db->query($sql);
        }


        return $result->result_array();
    }

    function get_accounter($id){
        $Q = "SELECT id, username as user, t_pengguna.name, email FROM t_pengguna WHERE id = ?";
        $q_acc = $this->db->query($Q, array($id));
        return $q_acc->row_array();
    }
}