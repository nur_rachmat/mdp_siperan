<div class="alert alert-success no-print" role="alert">
    <h4><i class="glyphicon glyphicon-pencil"></i> Input Pembiayaan</h4>
</div>
<div class="row">
    <div class="col-md-12">
    <?php echo form_open_multipart('keuangan/tambah_pembiayaan',array("class"=>"form-horizontal")); ?>

        <div class="form-group">
            <label for="kd_account" class="col-md-2 control-label">Kd Account</label>
            <div class="col-md-4">
                <div class="input-group  select2-bootstrap-prepend">
						<span class="input-group-btn">
                            <button class="btn btn-default" type="button" data-select2-open="single-prepend-text">
                                <span class="glyphicon glyphicon-search"></span>
                            </button>
						</span>
                    <select id="single-prepend-text" required name="kd_account" class="form-control select2-allow-clear select2">
                        <option></option>
                        <optgroup label="Master Account">
                            <?php
                            if(count($dt_account)){
                                foreach ($dt_account as $acc) {
                                    echo "<option value=\"".$acc['kd_account']."\">".$acc['account']."</option>";
                                }
                            }
                            ?>
                        </optgroup>
                    </select>
                </div>
            </div>

            <label for="kas" class="col-md-1 control-label">Kas</label>
            <div class="col-md-3">
                <select name="kas" class="form-control">
                    <?php
                    $kas_values = array(
                        '1'=>'STMIK',
                        '2'=>'STIE',
                    );

                    foreach($kas_values as $value => $display_text)
                    {
                        $selected = ($value == $this->input->post('kas')) ? ' selected="selected"' : "";

                        echo '<option value="'.$value.'" '.$selected.'>'.$display_text.'</option>';
                    }
                    ?>
                </select>
            </div>
        </div>

        <div class="form-group">
            <label for="nilai" class="col-md-2 control-label"><span class="text-danger">*</span>Nilai</label>
            <div class="input-group col-sm-8">
                <div class="input-group-addon">Rp </div>
                <input type="number" id="inNilai" name="nilai" value="<?php echo $this->input->post('nilai'); ?>" class="form-control" />
                <div class="input-group-addon">,00</div>
            </div>
            <span class="text-danger col-sm-offset-2 col-sm-8"><?php echo form_error('nilai');?></span>
            <span id="helpSatuan" class="help-block col-sm-offset-2 col-sm-8"></span>
        </div>
        <div class="form-group">
            <label for="tgl_nota" class="col-md-2 control-label"><span class="text-danger">*</span>Tgl Nota</label>
            <div class="col-md-3">
                <input type="text" name="tgl_nota" id="inTgl" value="<?php echo $this->input->post('tgl_nota'); ?>" class="form-control datepicker"/>
                <span class="text-danger"><?php echo form_error('tgl_nota');?></span>
            </div>

            <label for="nota" class="col-md-1 control-label">Nota</label>
            <div class="col-md-4">
                <input type="file" name="nota" value="<?php echo $this->input->post('nota'); ?>" class="form-control" id="nota" />

            </div>
            <span class="help-block col-sm-offset-6 col-sm-4">Jika tidak ada dapat diabaikan</span>
        </div>

        <div class="form-group">
            <label for="keterangan" class="col-md-2 control-label"><span class="text-danger">*</span>Keterangan</label>
            <div class="col-md-8">
                <textarea name="keterangan" class="form-control" id="keterangan"><?php echo $this->input->post('keterangan'); ?></textarea>
                <span class="text-danger"><?php echo form_error('keterangan');?></span>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-8">
                <button type="submit" class="btn btn-success">Save</button>
            </div>
        </div>

    <?php echo form_close(); ?>
    </div>
</div>
