<div class="alert alert-success" role="alert">
    <i class="icon-tasks"></i> Tambah Nota ke dalam Petty Cash
</div>
<div class="row">
    <div class="col-md-12">
        <?php if($this->session->flashdata('simpan_ggl')){ ?>
            <div class="alert alert-danger alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <?=$this->session->flashdata('simpan_ggl');?>
            </div>
        <?php } ?>



        <?php
        // Form Buat Nota (berdasarkan nota kosong)
        if($act == '') :
            $this->load->view('modules/v_formtambahnota');
        endif;
        ?>

    </div>

    <?php
    //Tampilkan list nota pada halaman buat nota
    $this->load->view('modules/v_listnota');

    ?>
</div>