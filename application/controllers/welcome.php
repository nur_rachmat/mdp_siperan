<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends CI_Controller {
	public $folder_upload = "assets/uploads";
	 
	public function index()
	{
		/*$this->load->view('header-nologin');
		$this->load->view('laporanagenda');
		$this->load->view('footer');*/
		$this->loginpage();
	}
	
	public function loginpage()
	{
		$this->load->helper(array('form', 'url'));
		//$this->load->view('header-nologin');
		$this->load->view('welcome_message');
	}
	
	public function login()
	{
		$u 		= $this->security->xss_clean($this->input->post('u'));
        $p 		= sha1($this->security->xss_clean($this->input->post('p')));
	 	$sql 	= "SELECT * FROM t_pengguna WHERE username = ? AND password = ?";
		$q_cek	= $this->db->query($sql, array($u, $p));
		$j_cek	= $q_cek->num_rows();
		$d_cek	= $q_cek->row();
		//echo $this->db->last_query();
		
        if($j_cek == 1) {
            $data = array(
                    'admin_user' => $d_cek->username,
                    'admin_email' => $d_cek->email,
                    'admin_id' => $d_cek->id,
					'admin_level' => $d_cek->level,
					'admin_valid' => TRUE,
					'notifikasi' => $d_cek->notifikasi
                    );
            $this->session->set_userdata($data);
			if($d_cek->level == 'Yayasan' || $d_cek->level == 'SuperAdmin')
				redirect('peran/persetujuan', 'refresh');
			else
            	redirect('peran', 'refresh');
        } else {	
			$this->session->set_flashdata("k", "<div id=\"alert\" class=\"alert alert-error\">Username or Password is not valid</div>");
			redirect('welcome/loginpage');
		}
	}

	public function google_login()
	{
		$u 		= $this->security->xss_clean($this->input->post('email'));
		$sql 	= "SELECT * FROM t_pengguna WHERE email = ?";
		$q_cek	= $this->db->query($sql, array($u));
		$j_cek	= $q_cek->num_rows();
		$d_cek	= $q_cek->row();
		//echo $this->db->last_query();

		if($j_cek == 1) {
			$data = array(
				'admin_user' => $d_cek->username,
				'admin_email' => $d_cek->email,
				'admin_id' => $d_cek->id,
				'admin_level' => $d_cek->level,
				'admin_valid' => TRUE,
				'notifikasi' => $d_cek->notifikasi
			);
			$this->session->set_userdata($data);
			//redirect('peran');
			if($d_cek->level == 'Yayasan')
				echo json_encode(array('login'=>true, 'msg'=>'peran/persetujuan'));
			else
				echo json_encode(array('login'=>true, 'msg'=>'peran'));

		} else {
			//$this->session->set_flashdata("k", "<div id=\"alert\" class=\"alert alert-error\">Maaf anda tidak diizinkan mengakses Sistem ini. <br/>" .$u ." tidak terdaftar.</div>");
			echo json_encode(array('login'=>false, 'msg' =>"<div id=\"alert\" class=\"alert alert-error\">Maaf anda tidak diizinkan mengakses Sistem ini. <br/>" .$u ." tidak terdaftar.</div>"));
			//redirect('welcome/loginpage');
		}
	}

	public function logout(){
		$this->load->view('region/header');
		$this->load->view('logout');
		$this->load->view('region/footer');
	}

	function testjson(){
		$this->load->view('testjson');
	}

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */