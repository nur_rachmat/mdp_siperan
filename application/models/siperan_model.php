<?php
/**
 * Created by PhpStorm.
 * User: Njr
 * Date: 12/12/2015
 * Time: 11:09 AM
 */
class Siperan_Model extends CI_Model
{
    var $tbl_pengguna = 't_pengguna';
    var $tbl_usulan = 't_usulan';
    var $tbl_transfer = 't_transfer';
    var $tbl_nota = 't_nota';
    function __construct()
    {
        parent::__construct();
    }

    public function get_usulan($status = null)
    {
        if($status == 'setuju'){
            $status = '1';
        }elseif($status == 'tolak'){
            $status = 3;
        }elseif( $status == 'batal'){
            $status = 4;
        }elseif($status == 'diaju'){
            $level = $this->session->userdata('admin_level');
            if($level == 'Ketua') {
                $status = '0';
            }elseif($level == 'Yayasan'){
                $status = '6';
            }
        }elseif($status == 'transfer'){
            $status = '5';
        }elseif($status == 'setuju_1'){
            $status = '6';
        }

        if(is_null($status)){
            $sql = "SELECT t_usulan.*, t_pengguna.username as user FROM t_usulan inner join t_pengguna on t_usulan.pengusul = t_pengguna.id where t_usulan.status != '4' ORDER BY tgl_usul DESC";

            $result = $this->db->query($sql);

        }elseif($status == '1'){
            $sql = "SELECT  t_usulan.*, t_pengguna.username as user FROM t_usulan inner join t_pengguna on t_usulan.pengusul = t_pengguna.id  where status = ? or status = ?  ORDER BY tgl_usul DESC";
            $result = $this->db->query($sql, array($status, '5'));
        }else{
            $sql = "SELECT  t_usulan.*, t_pengguna.username as user FROM t_usulan inner join t_pengguna on t_usulan.pengusul = t_pengguna.id  where status = ? ORDER BY tgl_usul DESC";
            $result = $this->db->query($sql, array($status));
        }

        if($result->num_rows() > 0){
            return $result->result_array();
        }else{
            return array();
        }
    }

    public function edit_usulan($id)
    {
        $sql = "SELECT * FROM t_usulan where status = '0' and id = ?";
        $result = $this->db->query($sql, array($id));

        if($result->num_rows() > 0){
            return $result->row_array();
        }else{
            return array();
        }
    }

    function simpan_usulan($data = array(), $act, $where = null){
        if($act === 'tambah'){
            return $this->db->insert($this->tbl_usulan, $data);
        }elseif($act === 'ubah'){
            return $this->db->update($this->tbl_usulan, $data , $where);
            //echo $this->db->last_query();
        }else{
            return false;
        }
    }


    function simpan_transfer($data = array()){
        return $this->db->insert($this->tbl_transfer, $data);
    }

    function batal_usulan($where = null)
    {
        $data = array('status' => '4', 'tgl_rev'=> date('Y-m-d H:i:s'), 'revby'=>$this->session->userdata('admin_id'));
        return $this->db->update($this->tbl_usulan, $data, array('id'=>$where));
    }

    function minta_dana($where = null, $dana = '')
    {
        $data = array('status' => '5', 'dana'=>$dana);
        return $this->db->update($this->tbl_usulan, $data, $where);
    }

    function setuju_usulan($where = array())
    {
        $level = $this->session->userdata('admin_level');
        if($level == 'Ketua'){
            $status  = '6';
        }else{
            $status = '1';
        }
        $data = array('status' => $status, 'tgl_rev'=> date('Y-m-d H:i:s'), 'revby'=>$this->session->userdata('admin_id'));
        return $this->db->update($this->tbl_usulan, $data, $where);
    }

    function setuju_usulan_array($where = array())
    {
        $level = $this->session->userdata('admin_level');
        if($level == 'Ketua'){
            $status  = '6';
        }else{
            $status = '1';
        }
        $result = array();
        foreach($where as $val){
            $data = array('status' => $status, 'tgl_rev'=> date('Y-m-d H:i:s'), 'revby'=>$this->session->userdata('admin_id'));
            $result[] = $this->db->update($this->tbl_usulan, $data, $val);
        }
        return $result;
    }

    function tolak_usulan($where = array())
    {
        $data = array('status' => '3', 'tgl_rev'=> date('Y-m-d H:i:s'), 'revby'=>$this->session->userdata('admin_id'));
        return $this->db->update($this->tbl_usulan, $data, $where);
    }

    function get_mail_yay(){
        $sql = "SELECT email FROM t_pengguna WHERE level = 'Yayasan' and notifikasi = 'Y'";
        $res = $this->db->query($sql);
        if($res->num_rows() == 1){
            $row = $res->row_array();
            return $row['email'];
        }elseif($res->num_rows() > 1){
            $mail = array();
            foreach($res->result_array() as $row){
                $mail[] = $row['email'];
            }
            return $mail;
        }else{
            return 0;
        }
    }

    function get_mail_ketua(){
        $sql = "SELECT email FROM t_pengguna WHERE level = 'Ketua' and notifikasi = 'Y'";
        $res = $this->db->query($sql);
        if($res->num_rows() == 1){
            $row = $res->row_array();
            return $row['email'];
        }elseif($res->num_rows() > 1){
            $mail = array();
            foreach($res->result_array() as $row){
                $mail[] = $row['email'];
            }
            return $mail;
        }else{
            return 0;
        }
    }

    function detail_usulan($id)
    {
        $sql = "SELECT t_usulan.*, t_pengguna.username as user FROM t_usulan inner join t_pengguna on t_usulan.pengusul = t_pengguna.id  WHERE t_usulan.id = ? ORDER BY tgl_usul ASC";
        $result = $this->db->query($sql, array($id));
        if($result->num_rows() == 1){
            return  $result->row_array();
        }else{
            return array();
        }
    }

    function get_mail_bak(){
        $sql = "SELECT email FROM t_pengguna WHERE level = 'Administrasi' and notifikasi = 'Y'";
        $res = $this->db->query($sql);
        if($res->num_rows() == 1){
            $row = $res->row_array();
            return $row['email'];
        }elseif($res->num_rows() > 1){
            $mail = array();
            foreach($res->result_array() as $row){
                $mail[] = $row['email'];
            }
            return $mail;
        }else{
            return 0;
        }
    }


    function get_mail_bdh(){
        $sql = "SELECT email FROM t_pengguna WHERE level = 'Bendahara' and notifikasi = 'Y'";
        $res = $this->db->query($sql);
        if($res->num_rows() == 1){
            $row = $res->row_array();
            return $row['email'];
        }elseif($res->num_rows() > 1){
            $mail = array();
            foreach($res->result_array() as $row){
                $mail[] = $row['email'];
            }
            return $mail;
        }else{
            return 0;
        }
    }

    function get_no_ref($rek){
        $kd_ref = array('','SIK', 'SIE', 'MDP');
        $Q = "SELECT * FROM t_usulan WHERE rekening = ?";
        $q_ref = $this->db->query($Q, array($rek));
        $no_ref = $q_ref->num_rows() + 1;
        if($no_ref < 10){
            return $kd_ref[$rek]."000".$no_ref;
        }elseif($no_ref < 100){
            return $kd_ref[$rek]."00".$no_ref;
        }elseif($no_ref < 1000){
            return $kd_ref[$rek]."0".$no_ref;
        }else{
            return $kd_ref[$rek].$no_ref;
        }
    }

    function get_dt_account(){
        $Q = "SELECT * FROM t_account order by kd_account ASC";
        $q_acc = $this->db->query($Q);
        return $q_acc->result_array();
    }

    function get_dt_nota($tgl_nota= "", $kas=""){
        if($tgl_nota == "" && $kas == ""){
            $sql = "SELECT  t_usulan.*, t_account.account as account FROM t_nota t_usulan
inner join t_account on t_usulan.kd_account = t_account.kd_account
where t_usulan.tgl_nota IS NOT NULL ORDER BY t_usulan.tgl_nota ASC";
            $result = $this->db->query($sql);
        }else{
            $tgl_nota =date('Y-m-d', strtotime($tgl_nota));
            $sql = "SELECT  t_usulan.*, t_account.account as account, t_pcash.kode_pcash, t_pcash.keterangan as pettycash  FROM t_nota t_usulan
inner join t_account on t_usulan.kd_account = t_account.kd_account INNER JOIN t_pcash on t_usulan.pcash = t_pcash.id
where t_usulan.tgl_nota = ? AND t_usulan.nota_kas = ? ORDER BY t_usulan.tgl_nota ASC";
            $result = $this->db->query($sql, array($tgl_nota, $kas));
        }

        //$result = $this->db->query($sql);
        //echo $this->db->last_query();
        return $result->result_array();
    }

    function get_dt_notakosong(){
        $sql = "SELECT  t_usulan.*, t_account.account as account FROM t_nota t_usulan
inner join t_account on t_usulan.kd_account = t_account.kd_account
where t_usulan.tgl_nota IS NULL ORDER BY t_usulan.tgl_buatnota DESC ";

        $result = $this->db->query($sql);
        return $result->result_array();
    }

    function get_nilai_usulan($no_ref){
        $sql = "SELECT nilai FROM t_usulan WHERE no_ref = ?";

        $result = $this->db->query($sql, array($no_ref));
        return $result->row_array();
    }

    function edit_nota($id, $no_ref, $tgl_nota, $nota_kas)
    {
        $sql = "SELECT * FROM t_usulan where id = ? and no_ref = ? and tgl_nota = ? and nota_kas = ?";
        $result = $this->db->query($sql, array($id, $no_ref, $tgl_nota, $nota_kas));

        if($result->num_rows() > 0){
            return $result->row_array();
        }else{
            return array();
        }
    }

    function edit_notakosong($id, $no_ref)
    {
        $sql = "SELECT * FROM t_nota where id_nota = ? and no_ref = ?";
        $result = $this->db->query($sql, array($id, $no_ref));

        if($result->num_rows() > 0){
            return $result->row_array();
        }else{
            return array();
        }
    }

    function get_dt_noref($tgl_nota, $kas = 3){
        $sql = "SELECT  t_nota.id_nota, t_usulan.nilai, t_usulan.tgl_usul, t_usulan.no_ref, t_nota.keterangan, t_nota.kd_account, t_account.account as account
FROM t_nota left OUTER join t_account on t_nota.kd_account = t_account.kd_account
LEFT OUTER  JOIN t_usulan on t_usulan.no_ref = t_nota.no_ref
where t_usulan.no_ref is not null AND (t_usulan.rekening = ? OR t_usulan.rekening = '3') AND (t_usulan.status = '1' or t_usulan.status = '5')
AND t_nota.tgl_nota is null ORDER BY t_usulan.tgl_nota DESC,  t_usulan.tgl_usul DESC";
        $result = $this->db->query($sql, array($kas));
        //echo $this->db->last_query();
        return $result->result_array();
    }

    function get_dt_noref2($pcash, $kas = 3){
        $sql = "SELECT  t_nota.id_nota, t_usulan.nilai, t_usulan.tgl_usul, t_usulan.no_ref, t_nota.keterangan, t_nota.kd_account, t_account.account as account
FROM t_nota left OUTER join t_account on t_nota.kd_account = t_account.kd_account
LEFT OUTER  JOIN t_usulan on t_usulan.no_ref = t_nota.no_ref
where t_usulan.no_ref is not null AND (t_usulan.rekening = ? OR t_usulan.rekening = '3') AND (t_usulan.status = '1' or t_usulan.status = '5')
AND (t_nota.pcash is null) ORDER BY t_usulan.tgl_nota DESC,  t_usulan.tgl_usul DESC";
        $result = $this->db->query($sql, array($kas));
        //echo $this->db->last_query();
        return $result->result_array();
    }

    function get_ref_data($id_nota, $no_ref){
        $sql = "SELECT t_nota.id_nota, t_usulan.id, t_usulan.no_ref, t_usulan.tgl_usul, t_usulan.nilai, t_nota.nilai as nilainota, t_usulan.keterangan, t_nota.keterangan as keterangannota,
t_nota.kd_account, t_account.account as account
FROM t_nota left OUTER join t_account on t_nota.kd_account = t_account.kd_account
LEFT OUTER JOIN t_usulan on t_usulan.no_ref = t_nota.no_ref
where t_nota.id_nota = ? and t_nota.no_ref = ? ORDER BY t_usulan.tgl_nota DESC,  t_usulan.tgl_usul ASC";
        $result = $this->db->query($sql, array($id_nota, $no_ref));
        //echo $this->db->last_query();
        return $result->row_array();
    }

    function get_nota($tgl_nota, $kas_nota){
        $sql = "SELECT  t_usulan.*, t_account.account as account, t_pcash.kode_pcash, t_pcash.keterangan as pettycash
FROM t_nota t_usulan inner join t_account on t_usulan.kd_account = t_account.kd_account INNER JOIN t_pcash on t_usulan.pcash = t_pcash.id
where (DATE_FORMAT( t_usulan.tgl_nota,  '%Y-%m-%d' ) = DATE_FORMAT( ?,  '%Y-%m-%d' ))
and t_usulan.nota_kas = ? ORDER BY t_usulan.id_nota ASC";
        $result = $this->db->query($sql, array($tgl_nota, $kas_nota));
        //echo $this->db->last_query();
        return $result->result_array();
    }

    public function simpan_nota($data, $where = array())
    {
        if(count($where) > 0){
            return $this->db->update($this->tbl_nota, $data, $where);
        }else{
            return $this->db->insert($this->tbl_nota, $data);
        }

    }

    public function simpan_pcash($data = array())
    {
        return $this->db->insert('t_pcash', $data);
    }

    public function get_dt_pcash($id = "", $kode_pcash="", $lock ="T")
    {
        if($id == "" && $kode_pcash == "") {
            $Q = "SELECT *, (select sum(nilai) as nilai_pcash FROM t_nota where pcash = id) as nilai_pcash FROM t_pcash order by kode_pcash ASC";
            $q_acc = $this->db->query($Q);
        }elseif($id != ""){
            if($id == "T") {
                $Q = "SELECT *, (select sum(nilai) as nilai_pcash FROM t_nota where pcash = id and nota_kas = kas_owner) as  nilai_pcash FROM t_pcash WHERE t_pcash.lock = ? order by kode_pcash ASC";
                $q_acc = $this->db->query($Q, array('T'));
                //echo $this->db->last_query();
            }elseif($id == "Y"){
                $Q = "SELECT *, (select sum(nilai) as nilai_pcash FROM t_nota where pcash = id and nota_kas = kas_owner) as  nilai_pcash FROM t_pcash WHERE t_pcash.lock = ? order by kode_pcash ASC";
                $q_acc = $this->db->query($Q, array('Y'));
                //echo $this->db->last_query();

            }else{
                $Q = "SELECT *, (select sum(nilai) as nilai_pcash FROM t_nota where pcash = id and nota_kas = kas_owner) as nilai_pcash FROM t_pcash WHERE kas_owner = ? and t_pcash.lock = ? order by kode_pcash ASC";
                $q_acc = $this->db->query($Q, array($id, $lock));
            }

        }else{
            $Q = "SELECT *, (select sum(nilai) as nilai_pcash FROM t_nota where pcash = {$id}) as nilai_pcash FROM t_pcash WHERE id = ? and kode_pcash = ? order by kode_pcash ASC";
            $q_acc = $this->db->query($Q, array($id, $kode_pcash));
        }
       // echo $this->db->last_query();

        return $q_acc->result_array();
    }

    public function get_all_noref(){
        $sql = "SELECT  t_usulan.*, t_account.account as account
FROM t_usulan inner join t_account on t_usulan.kd_account = t_account.kd_account
where t_usulan.tgl_nota is null ORDER BY t_usulan.tgl_usul DESC";
        $result = $this->db->query($sql);
        //echo $this->db->last_query();
        return $result->result_array();
    }

    public function get_nota_pcash($kd_pcash)
    {
        $sql = "SELECT  t_nota.*, t_account.account as account FROM t_nota inner join t_account on t_nota.kd_account = t_account.kd_account  where t_nota.pcash = ? ORDER BY t_nota.tgl_nota ASC";
        $result = $this->db->query($sql, array($kd_pcash));
        return $result->result_array();
    }

    public function get_nota_account($account)
    {
        $sql = "SELECT  t_nota.*, t_account.account as account FROM t_nota inner join t_account on t_nota.kd_account = t_account.kd_account  where t_nota.kd_account = ? and (t_nota.pcash is not null or t_nota.pcash != '') ORDER BY t_nota.tgl_nota ASC";
        $result = $this->db->query($sql, array($account));
        return $result->result_array();
    }

    public function get_pcash($id=null)
    {
        if($id != ""){
            $sql = "SELECT * FROM t_pcash where id = ? ";
            $result = $this->db->query($sql, array($id));
            return $result->row_array();
        }else{
            $sql = "SELECT * FROM t_pcash order by id DESC, tgl_pcash DESC";
            $result = $this->db->query($sql);
            return $result->result_array();

        }

    }

    public function get_apcash($id = array())
    {
        //$Q = "SELECT *, (select sum(nilai) as nilai_pcash FROM t_nota where pcash = id and nota_kas = kas_owner) as  nilai_pcash FROM t_pcash WHERE t_pcash.lock = ? order by kode_pcash ASC";
        $this->db->select("*, (select sum(nilai) as nilai_pcash FROM t_nota where pcash = id and nota_kas = kas_owner) as  nilai_pcash");
        if(count($id) == 1){
            $this->db->where('id', $id[0]);
        }else{
            $this->db->where('id', $id[0]);
            foreach($id as $key=>$val){
                if($key > 0){
                    $this->db->or_where('id', $val);
                }
            }
        }
        $result = $this->db->get('t_pcash');

        return $result->result_array();
    }

    public function get_dt_noref_xpcash($kd_pcash){
        $sql = "SELECT  t_usulan.id, t_usulan.tgl_usul, t_usulan.no_ref, t_usulan.tgl_nota, t_usulan.keterangan, t_usulan.kd_account, t_account.account as account
FROM t_usulan inner join t_account on t_usulan.kd_account = t_account.kd_account
where t_usulan.tgl_nota is not null and (t_usulan.kd_pcash is null or t_usulan.kd_pcash != ?) ORDER BY t_usulan.tgl_nota DESC,  t_usulan.tgl_usul ASC";
        $result = $this->db->query($sql, array($kd_pcash));
        //echo $this->db->last_query();
        return $result->result_array();
    }

    public function get_count_pcash($kas = '1'){
        $Q = "SELECT kode_pcash FROM t_pcash where kas_owner = ? ORDER by id DESC limit 0,1";
        $result = $this->db->query($Q, array($kas));
        return $result->row();
    }

    public function lock_pcash($id, $kd_pcash)
    {
        $n = "SELECT SUM( nilai ) AS nilai FROM  t_nota WHERE pcash =  ?";
        $nr = $this->db->query($n, array($id));
        $rs = $nr->row_array();
        if(count($rs)){
            $nilai = $rs['nilai'];
        }else{
            $nilai = 0;
        }
        //echo $this->db->last_query();
        $this->db->update('t_nota', array('lock'=>'Y'), array('pcash'=>$id));
        return $this->db->update('t_pcash', array('lock'=> 'Y', 'nilai'=>$nilai, 'tgl_lock'=> date('Y-m-d H:i:s')) , array('id'=>$id));
    }

    public function lock_pcash_array($where)
    {
        $result = array();
        foreach($where as $key=>$val){
            $n = "SELECT SUM( t_nota.nilai ) AS nilai FROM  t_nota where pcash = ?";
            $nr = $this->db->query($n, array($val['id']));
            $rs = $nr->row_array();
            if(count($rs)){
                $nilai = $rs['nilai'];
            }else{
                $nilai = 0;
            }
            $data = array('lock' => 'Y', 'nilai'=>$nilai, 'tgl_lock'=> date('Y-m-d H:i:s'));
            $result[] = $this->db->update('t_pcash', $data, $val);
            $resultloxknota[] = $this->db->update('t_nota', array('lock'=>'Y'), array('pcash'=>$val['id']));
            //echo $this->db->last_query();
        }

        return $result;
    }

    public function lock_nota($id)
    {
        return $this->db->update('t_nota', array('lock'=>'Y'), array('id_nota'=>$id));
    }

    public function hapus_nota($id)
    {
        return $this->db->update('t_nota', array('nilai'=>NULL, 'tgl_nota'=>NULL, 'pcash'=>NULL, 'nota_kas'=>NULL ), array('id_nota'=>$id, 'lock'=>'T'));
    }


   


    /*function get_usulan_baru(){
        $sql = "SELECT * FROM t_usulan WHERE status = 0 ORDER BY tgl_usul ASC";
        $result = $this->db->query($sql);
        if($result->num_rows() > 0){
            return $result->result_array();
        }else{
            return array();
        }
    }

    function get_usulan_status($status){
        $sql = "SELECT * FROM t_usulan WHERE status = ? ORDER BY tgl_usul ASC";
        $result = $this->db->query($sql, array($status));
        if($result->num_rows() > 0){
            return $result->result_array();
        }else{
            return array();
        }
    }*/

    public function simpan_batchnota($data)
    {
        return $this->db->insert_batch($this->tbl_nota, $data);
    }
    function get_account($kd){
        $Q = "SELECT account FROM t_account WHERE kd_account = ?";
        $q_acc = $this->db->query($Q, array($kd));
        $res =  $q_acc->row_array();
        if($q_acc->num_rows() > 0){
            return $res['account'];
        }else{
            return "-";
        }
    }

    function get_accounter($id){
        $Q = "SELECT id, username as user, t_pengguna.name, email FROM t_pengguna WHERE id = ?";
        $q_acc = $this->db->query($Q, array($id));
        return $q_acc->row_array();
    }
}