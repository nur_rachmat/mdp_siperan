<?php if($this->session->flashdata('simpan_ok')){ ?>
    <div class="alert alert-success alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <?=$this->session->flashdata('simpan_ok');?>
    </div>
<?php } ?>
<?php $level = $this->session->userdata('admin_level'); ?>
<div class="alert alert-success" role="alert">
    <h4>
        <i class="icon-tasks"></i> Persetujuan Usulan Keuangan <?=$level ;?>
        <span class="label label-danger pull-right"> <?=count($dt_usulan);?> Usulan </span>
    </h4>
</div>

<div class="alert alert-info" role="alert">
    <?php if($level == "Yayasan") {?>
    Usulan yang telah diberikan persetujuan oleh Yayasan akan diteruskan kembali ke Bagian Administrasi Keuangan MDP.
    <?php }elseif($level == "Ketua") { ?>
        Usulan yang telah diberikan persetujuan oleh Ketua AMIK/STMIK/STIE MDP akan diteruskan ke Yayasan MDP untuk mendapatkan Persetujuan.
    <?php } ?>
</div>


<form method="post" action="<?=site_url('peran/setuju_usulan');?>" >
    <button type="submit" id="btn_setuju" disabled class="btn btn-success btn-md"><i class="glyphicon glyphicon-ok"></i> Beri Persetujuan</button></a>
    <div class="claerfix">&nbsp;</div>
    <input type="checkbox" id="check_all"> <label for="check_all">Semua</label>
    <table class="table table-striped footable">
        <thead>
            <tr>
                <th class="text-center" data-type="html"></th>
                <th data-type="date">Tgl Usulan</th>
                <th>Nilai</th>
                <th data-breakpoints="xs sm" data-type="html">Keterangan</th>
                <th>Pengusul</th>
                <th data-breakpoints="md" data-type="html">Status</th>
                <?php if(!isset($active)){ ?><th data-type="html" data-breakpoints="xs">Aksi</th><?php } ?>
            </tr>
        </thead>

        <?php
        echo "<tbody>";
        if(count($dt_usulan) > 0){

            foreach($dt_usulan as $row){
                echo "<tr>";
                echo "<td class='text-center'><input type='checkbox' name='setuju[]' id='setuju[]' value='".$row['id']."'></td>";
                echo "<td>".date('d-m-Y H:i:s', strtotime($row['tgl_usul']))."</td>";
                echo "<td title='".Terbilang($row['nilai'])." Rupiah'>Rp ".number_format($row['nilai'], 2, ",", ".")."</td>";

                $ket = strip_tags(htmlspecialchars_decode($row['keterangan']), "<img>");
                $k_img = preg_replace("/<img[^>]+\>/i", " [ <b>image</b> ] ", $ket);
                $link_more = " [ <a data-toggle='modal' href='#' data-target='#modal' data-href='".site_url('peran/detail_usulan/'.$row['id'])."' ><i class='glyphicon glyphicon-eye-open'></i> detail</a> ]";
                echo "<td>".substr($k_img, 0, 100).((strlen($k_img) > 100) ? $link_more : "")."</td>";
                $user = explode("@", $row['user']);

                echo "<td>".$user[0]."</td>";

                $act_ok = " <a href='".site_url('peran/setuju_usulan/'.$row['id'])."' class='btn btn-success btn-md'><i class='glyphicon glyphicon-ok'></i> Setuju</a>";
                $act_x = " <a href='".site_url('peran/tolak_usulan/'.$row['id'])."' class='btn btn-default btn-sm'><i class='glyphicon glyphicon-remove'></i> Tolak</a>";
                if($row['status'] == 0){
                    $status = "<span class='label label-info lbl-sm'>Telah Diajukan</span> ";
                    $act_ok = $act_ok . " ". $act_x;
                }elseif($row['status'] == 1){
                    $status = "<span class='label label-success lbl-sm'>Disetujui</span>";
                    $act_ok = '';
                }elseif($row['status'] == 3){
                    $status = "<span class='label label-danger lbl-sm'>Ditolak</span>";
                    $act_ok = '';
                }elseif($row['status'] == 4){
                    $status = "<span class='label label-warning lbl-sm'>Dibatalkan</span>";
                    $act_ok = '';
                }elseif($row['status'] == 6){
                    $status = "<span class='label label-warning lbl-sm'>Disetujui Ketua</span>";
                    $act_ok = $act_ok . " ". $act_x;

                }else{
                    $status = "<span class='label label-dan lbl-sm'>Tidak Diketahui</span>";
                    $act_ok = '';
                }

                echo "<td>".$status."</td>";
                if(!isset($active)) {
                    echo "<td class='text-center'>" . $act_ok . "</td>";
                }
                echo "</tr>";

            }

        }
        echo "</tbody>";


        ?>

    </table>
    <?php if(count($dt_usulan) == 0){
        echo "<p class='text-info'>Terimakasih. Tidak ada lagi usulan yang harus diberikan persetujuan.</p>";
    } ?>

    <!--<table class="table table-striped" id="dataTable2">
    <thead>
    <tr>
        <th class="text-center"><input type="checkbox" id="check_all"> <label for="check_all">Semua</label></th>
        <th>Tgl Usul</th>
        <th>Nilai</th>
        <th>Keterangan</th>
        <th>Pengusul</th>
        <?php /*if(!isset($active)){ */?><th>Aksi</th><?php /*} */?>
    </tr>
    </thead>
    <?php
/*    if(count($dt_usulan) > 0){
        echo "<tbody>";
        foreach($dt_usulan as $row){
            echo "<tr>";
            echo "<td class='text-center'><input type='checkbox' name='setuju[]' id='setuju[]' value='".$row['id']."'></td>";
            echo "<td>".date('d-m-Y H:i:s', strtotime($row['tgl_usul']))."</td>";
            echo "<td>".$row['nilai']."</td>";

            $ket = strip_tags(htmlspecialchars_decode($row['keterangan']), "<img>");
            $k_img = preg_replace("/<img[^>]+\>/i", " [ <b>image</b> ] ", $ket);
            $link_more = " [ <a data-toggle='modal' data-target='#modal'  href='".site_url('peran/detail_usulan/'.$row['id'])."' ><i class='glyphicon glyphicon-eye-open'></i> detail</a> ]";
            echo "<td>".substr($k_img, 0, 100).((strlen($k_img) > 100) ? $link_more : "")."</td>";
            $user = explode("@", $row['user']);

            echo "<td>".$user[0]."</td>";

            $act_ok = " <a href='".site_url('peran/setuju_usulan/'.$row['id'])."' class='btn btn-success btn-md'><i class='glyphicon glyphicon-ok'></i> Setuju</a>";
            $act_x = " <a href='".site_url('peran/tolak_usulan/'.$row['id'])."' class='btn btn-default btn-sm'><i class='glyphicon glyphicon-remove'></i> Tolak</a>";
            if($row['status'] == 0){
                $status = "<span class='label label-info lbl-sm'>Telah Diajukan</span> ";
                $act_ok = $act_ok . " ". $act_x;
            }elseif($row['status'] == 1){
                $status = "<span class='label label-success lbl-sm'>Disetujui</span>";
                $act_ok = '';
            }elseif($row['status'] == 3){
                $status = "<span class='label label-danger lbl-sm'>Ditolak</span>";
                $act_ok = '';
            }elseif($row['status'] == 4){
                $status = "<span class='label label-warning lbl-sm'>Dibatalkan</span>";
                $act_ok = '';
            }else{
                $status = "<span class='label label-dan lbl-sm'>Tidak Diketahui</span>";
                $act_ok = '';
            }


            if(!isset($active)) {
                echo "<td class='text-center'>" . $act_ok . "</td>";
            }
            echo "</tr>";

        }
        echo "</tbody>";
    }
    */?>
    </table>-->


</form>
<div class="alert alert-danger" role="alert">
    <p class="bg-danger text-danger">
        Daftar usulan yang tampil adalah usulan yang belum diberikan persetujuan. <br/>
        Persetujuan pertama akan dilakukan oleh Ketua STMIK/STIE MDP dan akan dilanjutkan ke Yayasan
    </p>
</div>



<!-- Modal -->
<div class="modal fade bs-example-modal-lg" id="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Detail Usulan</h4>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>