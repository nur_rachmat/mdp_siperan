<?php if($this->session->flashdata('simpan_ok')){ ?>
	<div class="alert alert-success alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<?=$this->session->flashdata('simpan_ok');?>
	</div>
<?php } ?>
<?php if($this->session->flashdata('simpan_ggl')){ ?>
	<div class="alert alert-danger alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<?=$this->session->flashdata('simpan_ggl');?>
	</div>
<?php } ?>

<ul class="nav nav-tabs">
	<?php if(!is_null($id_bon)) {?>
	<li><a href="<?=site_url('keuangan/input_'.$active.'/'.$id_bon.'#item');?>"><i class="glyphicon glyphicon-list-alt"> </i> Item <?php echo ucfirst($active);?></a></li>
	<?php }?>
	<li <?=(isset($active) && $active == 'bon') ? 'class="active"' : '';?>><a href="<?=site_url('keuangan/input_bon');?>">Input Bon</a></li>
	<li <?=(isset($active) && $active == 'pendapatan') ? 'class="active"' : '';?>><a href="<?=site_url('keuangan/input_pendapatan');?>">Input Pendapatan</a></li>
	<li><a href="<?=site_url('keuangan/lihat_bp');?>"><i class="glyphicon glyphicon-blackboard"> </i>Lihat Bon/Pendapatan</a></li>
</ul>
<div class="alert alert-success no-print" role="alert">
	<h4><i class="glyphicon glyphicon-pencil"></i> Input Item <?php echo ucfirst($active);?></h4>
</div>

<div class="row">
	<div class="col-md-12">
		<?php echo form_open_multipart('keuangan/input_'.$active.'/'.$id_bon,array("class"=>"form-horizontal")); ?>

		<div class="form-group">
			<label for="kd_account" class="col-md-2 control-label">Bon</label>
			<div class="col-md-8">
				<div class="input-group  select2-bootstrap-prepend">
						<span class="input-group-btn">
                            <button class="btn btn-default" type="button" data-select2-open="single-prepend-text">
                                <span class="glyphicon glyphicon-search"></span>
                            </button>
						</span>
					<select id="single-prepend-text" required name="id_bon" class="form-control select2-allow-clear select2">
						<option></option>
						<optgroup label="Master Account">
							<?php
							if(count($all_t_bon)){
								foreach ($all_t_bon as $bon) {
									$selected = ($bon['id'] == $this->input->post('id_bon') || $id_bon == $bon['id']) ? "selected" : "";

									echo "<option value=\"".$bon['id']."\" ".$selected.">".$bon['keterangan']." - Rp ".number_format($bon['nilai'],2, ',', '.')."</option>";
								}
							}
							?>
						</optgroup>
					</select>
				</div>
			</div>

		</div>

		<div class="form-group">
			<label for="kas" class="col-md-2 control-label">Kas</label>
			<div class="col-md-2">
				<select name="kas" <?php echo (isset($id_bon) && is_null($id_bon)) ? "readonly" : "";?> class="form-control">
					<?php
					$kas_values = array(
						'1'=>'STMIK',
						'2'=>'STIE',
					);

					foreach($kas_values as $value => $display_text)
					{
						$selected = ($value == $this->input->post('kas') || $dt_bon['kas'] == $value) ? ' selected="selected"' : "";

						echo '<option value="'.$value.'" '.$selected.'>'.$display_text.'</option>';
					}
					?>
				</select>
			</div>

			<label for="unit" class="col-md-1 control-label">Unit</label>
			<div class="col-md-2">
				<select name="unit" <?php echo (isset($id_bon) && is_null($id_bon)) ? "readonly" : "";?> class="form-control  select2-allow-clear select2" data-placeholder="Pilih Unit">
					<option value=""></option>
					<?php
					foreach($all_unit as $unit)
					{
						$selected = ($unit['id'] == $this->input->post('unit') || $dt_bon['kas'] == $unit['id']) ? ' selected="selected"' : "";

						echo '<option value="'.$unit['id'].'" '.$selected.'>'.$unit['nama_unit'].'</option>';
					}
					?>
				</select>
			</div>

			<label for="tgl_bon" class="col-md-1 control-label"><span class="text-danger">*</span>Tgl Bon</label>
			<div class="col-md-2">
				<input type="text" name="tgl_bon" value="<?php echo $this->input->post('tgl_bon'); ?>" class="form-control datepicker" id="tgl_bon" />
				<span class="text-danger"><?php echo form_error('tgl_bon');?></span>
			</div>


		</div>
		<div class="form-group">
			<label for="kd_account" class="col-md-2 control-label">Kd Account</label>
			<div class="col-md-3">
				<div class="input-group  select2-bootstrap-prepend">
						<span class="input-group-btn">
                            <button class="btn btn-default" type="button" data-select2-open="single-prepend-text">
                                <span class="glyphicon glyphicon-search"></span>
                            </button>
						</span>
					<select id="single-prepend-text" required name="kd_account" class="form-control select2-allow-clear select2">
						<option></option>
						<optgroup label="Master Account">
							<?php
							if(count($dt_account)){
								foreach ($dt_account as $acc) {
									$selected = ($acc['kd_account'] == $this->input->post('kd_account') || $acc['kd_account'] == $dt_bon['kd_account']) ? "selected" : "";
									echo "<option value=\"".$acc['kd_account']."\" ".$selected.">".$acc['account']."</option>";
								}
							}
							?>
						</optgroup>
					</select>
				</div>
			</div>



			<label for="nilai" class="col-md-1 control-label"><span class="text-danger">*</span>Nilai</label>
			<div class="input-group col-sm-4">
				<div class="input-group-addon">Rp </div>
				<input type="number" id="inNilai" name="nilai" value="<?php echo $this->input->post('nilai'); ?>" class="form-control" />
				<div class="input-group-addon">,00</div>
			</div>
			<span class="text-danger col-sm-offset-5 col-sm-8"><?php echo form_error('nilai');?></span>
			<span id="helpSatuan" class="help-block col-sm-offset-5 col-sm-8"></span>
		</div>

		<div class="form-group">
			<label for="keterangan" class="col-md-2 control-label"><span class="text-danger">*</span>Keterangan</label>
			<div class="col-md-8">
				<textarea name="keterangan" class="form-control" id="keterangan"><?php echo $this->input->post('keterangan'); ?></textarea>
				<span class="text-danger"><?php echo form_error('keterangan');?></span>
			</div>
		</div>
		<div class="form-group">
			<label for="bon" class="col-md-2 control-label">Bon</label>
			<div class="col-md-8">
				<input type="file" name="nota" value="<?php echo $this->input->post('bon'); ?>" class="form-control" id="bon" />
			</div>

			<span class="help-block col-sm-offset-2 col-sm-8">Jika tidak ada dapat diabaikan</span>
		</div>

		<div class="form-group">
			<div class="col-sm-offset-2 col-sm-8">
				<button type="submit" name="" class="btn btn-success">Save and Input</button>
			</div>
		</div>

		<?php echo form_close(); ?>
	</div>

	<?php if(count($item_bon) > 0  && !is_null($id_bon)){ ?>
		<hr/>
		<div class="col-md-12" id="item">
			<div class="well">
				<h4><strong><?php echo ucfirst($active);?></strong></h4>
				<div class="row">
					<div class="col-md-1"></div>
					<label for="keterangan" class="col-md-2 control-label"><span class="text-danger">Keterangan</span></label>
					<div class="col-md-8">
						<?php echo $dt_bon['keterangan']; ?>
					</div>
					<div class="col-md-1"></div>
				</div>
                <div class="row">
                    <div class="col-md-1"></div>
                    <label for="keterangan" class="col-md-2 control-label"><span class="text-danger">Nota</span></label>
                    <div class="col-md-8">

                    </div>
                    <div class="col-md-1"></div>
                </div>
				<div class="row">
					<div class="col-md-1"></div>
					<label for="keterangan" class="col-md-2 control-label"><span class="text-danger">Nilai</span></label>
					<div class="col-md-8">
						<?php echo "Rp ".number_format($dt_bon['nilai'], 2, ",", "."); ?>
						<br/>
						<span class="text-danger"><?=Terbilang($dt_bon['nilai']);?> Rupiah</span>
					</div>
					<div class="col-md-1"></div>
				</div>
				<div class="clearfix"></div>
			</div>

			<table class="table table-striped footable">
				<thead>
					<tr>
						<th>No</th>
						<th data-breakpoints="xs sm" data-type="date">Tgl Bon</th>
						<th>Account</th>
						<th>Nilai</th>
						<th data-breakpoints="xs sm" data-type="html">Kas</th>
						<th data-breakpoints="xs sm" data-type="html">Unit</th>
						<th data-breakpoints="xs sm" data-type="html">Keterangan</th>
						<th data-type="html">Aksi</th>
					</tr>
				</thead>
				<?php
				if(count($item_bon) > 0){
					echo "<tbody>";
					$total_nilai = 0;
					foreach($item_bon as $key=>$row){
						$total_nilai = $row['nilai'] + $total_nilai;
						echo "<tr>";
						echo "<td>".(++$key)."</td>";
						echo "<td>".date('d-m-Y', strtotime($row['tgl_bon']))."</td>";
						echo "<td>".$row['kd_account']."</td>";

						if($row['kas'] === '1'){
							$kas = "<span class='label label-info'>STMIK</span>";
						}elseif($row['kas'] == '2'){
							$kas = "<span class='label label-success'>STIE</span>";
						}elseif($row['kas'] == '3'){
							$kas = "<span class='label label-default'>Bersama</span>";
						}else{
							$kas = "<span class='label label-danger'>N/A</span>";
						}

						echo "<td title='".Terbilang($row['nilai'])." Rupiah'>Rp ".number_format($row['nilai'], 2, ",", ".")."</td>";
						echo "<td>".$kas."</td>";
						$unit =  $this->Unit_model->get_unit($row['unit']);
						$_unit = isset($unit['nama_unit']) ? $unit['nama_unit'] : 'N/A' ;
						echo "<td>".$_unit."</td>";

						$ket = str_replace(PHP_EOL,"<br>",$row['keterangan']);
						$ket .= ((strlen($row['bon']) == 0) ? '' : '<br/><a href="'.base_url('uploads/nota_keuangan/'.$row['bon']).'" target="_blank">Nota</a>');
						echo "<td>".$ket."</td>";


						$act_hapus = " <a href='".site_url('keuangan/hapus_item/'.$row['id'])."'><button class='btn btn-warning btn-sm '><i class='glyphicon glyphicon-remove-circle'></i> Hapus</button></a>";
						//$act_ubah = " <a href='".site_url('keuangan/ubah_item/'.$row['id'])."'><button class='btn btn-default btn-sm '><i class='glyphicon glyphicon-edit'></i> Ubah</button></a>";
						$act_ubah = "";
						echo "<td>" . $act_hapus . $act_ubah. "</td>";


						echo "</tr>";

					}
					echo "</tbody>";
				}


				?>



			</table>
			<?php if(count($item_bon) > 0) :  ?>
				<div class="well">
					<div class="row">
						<div class="col-md-1"></div>
						<label for="keterangan" class="col-md-2 control-label"><span class="text-danger">Total Nilai</span></label>
						<div class="col-md-8"><p> Rp <?=number_format($total_nilai, 2, ",", ".");?></p></div>
						<div class="col-md-1"></div>
					</div>
					<div class="row">
						<div class="col-md-1"></div>
						<label for="keterangan" class="col-md-2 control-label"><span class="text-danger">Terbilang</span></label>
						<div class="col-md-8">
							<span style="color:darkred;"><?=Terbilang($total_nilai);?> Rupiah</span>
						</div>
						<div class="col-md-1"></div>
					</div>
					<hr/>
					<div class="row">
						<div class="col-md-1"></div>
						<label for="keterangan" class="col-md-2 control-label"><span class="text-danger">Sisa Dana</span></label>
						<div class="col-md-8"><p> Rp <?=number_format(($dt_bon['nilai'] - $total_nilai), 2, ",", ".");?></p></div>
						<div class="col-md-1"></div>
					</div>
					<div class="row">
						<div class="col-md-1"></div>
						<label for="keterangan" class="col-md-2 control-label"><span class="text-danger">Terbilang</span></label>
						<div class="col-md-8">
							<span style="color:darkred;"><?=Terbilang(($dt_bon['nilai'] - $total_nilai));?> Rupiah</span>
						</div>
						<div class="col-md-1"></div>
					</div>
				</div>

			<?php endif; ?>
		</div>
	<?php } ?>
</div>

