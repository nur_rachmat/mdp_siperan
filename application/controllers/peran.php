<?php
/**
 * Created by PhpStorm.
 * User: Njr
 * Date: 12/11/2015
 * Time: 1:40 PM
 */
if (!defined('BASEPATH')) exit ('No direct script access allowed');
class Peran extends CI_Controller
{
    public $folder_upload = "assets/uploads";
    public $tahun = "";
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('akses_helper');
        $this->load->model('Siperan_Model');
        $this->load->library('mailsender', array('smtp_user'=>'email@gmail.com', 'smtp_pass'=>'passwordgmail'));
        $this->tahun = date('Y');

        if ($this->session->userdata('admin_valid') == FALSE && $this->session->userdata('admin_user') == ""){
            redirect("welcome");
        }
    }

    public function index($act=null)
    {
        $data['act_pro'] = $act;
        $this->load->view('region/header');
        $this->load->view('sop', $data);
        $this->load->view('region/footer');
    }

    public function nota_json($jenis){
        if($jenis == 'columns'){
            $data = array(
                array("name"=>"no_ref","title"=>"No. Ref"),
              array("name"=>"nilai_usulan","title"=>"Nilai Dalam Usulan"),
              array("name"=>"account","title"=>"Account"),
              array("name"=>"keterangan","title"=>"Keterangan", "breakpoints"=>"xs sm md lg"),
              array("name"=>"bukti","title"=>"Bukti Nota", "breakpoints"=>"xs sm md lg","type"=>"html"),
              array("name"=>"nilai","title"=>"Nilai Nota"),
              array("name"=>"tgl_buat","title"=>"Tgl.Buat","type"=>"date","formatString"=>"DD-MM-YYYY"),
              array("name"=>"aksi","title"=>"Aksi","type"=>"html","breakpoints"=>"xs")
            );
            $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));
            //echo json_encode($data);
        }else{
            $data = array();
            $dt_usulan = $this->Siperan_Model->get_dt_notakosong();
            foreach($dt_usulan as $key=>$val){
                $data[$key]['no_ref'] = $val['no_ref'];
                $nu = $this->Siperan_Model->get_nilai_usulan($val['no_ref']);
                $data[$key]['nilai_usulan'] = "Rp ".number_format($nu['nilai'], 2, ",", ".");

                $ket = strip_tags(htmlspecialchars_decode($val['keterangan']), "<img>");
                $k_img = preg_replace("/<img[^>]+\>/i", " [ <b>image</b> ] ", $ket);
                $link_more = " [ <a data-toggle='modal' data-target='#modal' href='#' data-href='".site_url('peran/detail_usulan/'.$val['id_nota'])."' ><i class='glyphicon glyphicon-eye-open'></i> detail</a> ]";
                $data[$key]['keterangan'] = strip_tags(htmlspecialchars_decode($val['keterangan']));
                $data[$key]['bukti'] = (($val['f_nota'] == '') ? 'Tidak Ada' : '<a href="'.base_url('uploads/nota/'.$val['f_nota']).'" target="_blank">Nota</a>');

                $data[$key]['account'] = $val['kd_account']. " - ".$val['account'];

                $act_ubah = " <a href='".site_url('peran/ubah_nota/'.$val['id_nota'].'/'.$val['no_ref'])."'><button class='btn btn-success btn-sm '><i class='glyphicon glyphicon-list-alt'></i> Nota</button></a>";
                $act_ubah =($val['lock'] == 'T') ?  $act_ubah : "";
                //$act_lock = " <a href='".site_url('peran/lock_nota/'.$row['id'])."'><button class='btn btn-warning btn-sm '><i class='glyphicon glyphicon-lock'></i> Kunci</button></a>";
                $act_hapus = " <a href='".site_url('peran/hapus_nota/'.$val['id_nota'].'/'.$val['no_ref'])."'><button class='btn btn-danger btn-sm '><i class='glyphicon glyphicon-remove'></i> Hapus</button></a>";
                $act_hapus =($val['lock'] == 'T') ?  $act_hapus : "";

                $act_lock = " <a href='".site_url('peran/lock_nota/'.$val['id_nota'])."'><button class='btn btn-warning btn-sm '><i class='glyphicon glyphicon-lock'></i> Kunci</button></a>";
                //$act_lock =($val['lock'] == 'T') ?  $act_lock : "";
                $act_lock = "";

                $data[$key]['nilai'] = "Rp ".number_format($val['nilai'], 2, ",", ".");
                $data[$key]['tgl_buat'] = date('d-m-Y', strtotime($val['tgl_buatnota']));
                $data[$key]['aksi'] = $act_ubah . $act_lock;

            }
            //echo json_encode($data);
            $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));
        }

    }

    public function usulandiajukan_json($jenis, $active=null){
        $level = $this->session->userdata('admin_level');
        if(is_null($active) && $level == 'Ketua'){
            $active = 'setuju';
        }
        if($jenis == 'columns'){
            if(isset($active) && $active == 'setuju'){
                $data = array(
                    array("name"=>"no_ref","title"=>"No. Ref"),
                    array("name"=>"tgl_usul","title"=>"Tgl Usul","type"=>"date", "breakpoints"=>"xs sm"),
                    array("name"=>"pengusul","title"=>"Pengusul"),
                    array("name"=>"tgl_rev","title"=>"Tgl Acc","breakpoints"=>"xs","type"=>"date",),
                    array("name"=>"nilai_usulan","title"=>"Nilai"),
                    array("name"=>"kas","title"=>"Kas", "breakpoints"=>"xs sm"),
                    array("name"=>"keterangan","title"=>"Keterangan", "breakpoints"=>"xs sm"),
                    array("name"=>"status","title"=>"Status", "type"=>"html")

                );
            }else{
                $data = array(
                    array("name"=>"no_ref","title"=>"No. Ref"),
                    array("name"=>"tgl_usul","title"=>"Tgl Usul","type"=>"date", "breakpoints"=>"xs sm"),
                    array("name"=>"pengusul","title"=>"Pengusul"),
                    array("name"=>"nilai_usulan","title"=>"Nilai"),
                    array("name"=>"kas","title"=>"Kas", "breakpoints"=>"xs sm"),
                    array("name"=>"keterangan","title"=>"Keterangan", "breakpoints"=>"xs sm"),
                    array("name"=>"status","title"=>"Status", "type"=>"html"),
                    array("name"=>"aksi","title"=>"Aksi","type"=>"html","breakpoints"=>"xs")
                );
            }

            $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));
        }else{
            $data = array();

            if(is_null($active)) {
                $dt_usulan = $this->Siperan_Model->get_usulan();
            }else{
                $dt_usulan = $this->Siperan_Model->get_usulan($active);
            }
            foreach($dt_usulan as $key=>$val){
                $data[$key]['no_ref'] = $val['no_ref'];
                $data[$key]['tgl_usul'] = date('d-M-Y', strtotime($val['tgl_usul']));

                $user = explode("@", $val['user']);
                $data[$key]['pengusul'] = $user[0];
                if(isset($active) && $active == 'setuju') {
                    $data[$key]['tgl_rev'] = date('d-m-Y H:i:s', strtotime($val['tgl_rev']));
                }
                $data[$key]['nilai_usulan'] = "Rp ".number_format($val['nilai'], 2, ",", ".");

                if($val['rekening'] === '1'){
                    $kas = "<span class='label label-info'>STMIK</span>";
                }elseif($val['rekening'] == '2'){
                    $kas = "<span class='label label-success'>STIE</span>";
                }elseif($val['rekening'] == '3'){
                    $kas = "<span class='label label-default'>Bersama</span>";
                }else{
                    $kas = "<span class='label label-danger'>N/A</span>";
                }
                $data[$key]['kas'] = $kas;


                $ket = strip_tags(htmlspecialchars_decode($val['keterangan']), "<img>");
                $k_img = preg_replace("/<img[^>]+\>/i", " [ <b>image</b> ] ", $ket);

                //$link_img = "<a data-toggle='modal' data-target='#modal' href='#' data-href='".site_url('peran/detail_usulan/'.$val['id'])."' >image</a>";
                //$k_img = str_replace('image', $link_img, $k_img);
                $link_more = " [ <a data-toggle='modal' data-target='#modal' href='#' data-href='".site_url('peran/detail_usulan/'.$val['id'])."' ><i class='glyphicon glyphicon-eye-open'></i> detail</a> ]";
                $data[$key]['keterangan'] = substr($k_img, 0, 100).((strlen($k_img) > 75) ? $link_more : "");

                $act_batal = " <a href='".site_url('peran/batal_usulan/'.$val['id'])."'><button class='btn btn-warning btn-sm '><i class='glyphicon glyphicon-remove-circle'></i> Batal</button></a>";
                $act_ubah = " <a href='".site_url('peran/ubah_usulan/'.$val['id'])."'><button class='btn btn-default btn-sm '><i class='glyphicon glyphicon-edit'></i> Ubah</button></a>";
                $act_minta = '<div class="btn-group">
                      <button class="btn btn-success btn-sm dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Transfer ke <span class="caret"></span>
                      </button>
                      <ul class="dropdown-menu">
                        <li><a href="'.site_url('peran/minta_dana/'.$val['id'].'/stmik').'">Rek. STMIK</a></li>
                        <li><a href="'.site_url('peran/minta_dana/'.$val['id'].'/stie').'">Rek. STIE</a></li>
                        <li><a href="'.site_url('peran/minta_dana/'.$val['id'].'/tunai').'">Tunai</a></li>
                        <li><a href="#" data-toggle="modal" data-target="#modal-reklain" data-noref="'.$val['no_ref'].'" data-idusulan="'.$val['id'].'">Rekening Lain</a></li>
                      </ul>
                    </div>';

                if($val['status'] == 0){
                    $status = "<span class='label label-info lbl-sm'>Telah Diajukan</span> ";
                    $act_batal = $act_batal . " ". $act_ubah;
                }elseif($val['status'] == 1){
                    $status = "<span class='label label-success lbl-sm'>Disetujui Yayasan</span>";
                    $act_batal = $act_minta;
                }elseif($val['status'] == 3){
                    $status = "<span class='label label-danger lbl-sm'>Ditolak</span>";
                    $act_batal = '';
                }elseif($val['status'] == 4){
                    $status = "<span class='label label-warning lbl-sm'>Dibatalkan</span>";
                    $act_batal = '';
                }elseif($val['status'] == 5) {
                    $status = "<span class='label label-success lbl-sm'>Disetujui</span>";
                    $act_batal = "<span class='label label-success lbl-sm'>[ diajukan ke bendahara ]</span>";
                }elseif($val['status'] == 6){
                    $status = "<span class='label label-default lbl-sm'>Disetujui Ketua</span>";
                    $act_batal = '';
                }else{
                    $status = "<span class='label label-default lbl-sm'>Tidak Diketahui</span>";
                    $act_batal = '';
                }
                $data[$key]['status'] = $status;
                $data[$key]['aksi'] = $act_batal;

            }

            $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));
        }

    }

    public function usulan($status = null)
    {
        $level = $this->session->userdata('admin_level');
        if(is_null($status) && $level == 'Ketua'){
            $status = 'setuju';
        }
        if(is_null($status)) {
            $data['dt_usulan'] = $this->Siperan_Model->get_usulan();
            //print_r($data['dt_usulan']);
        } else {
            $data['active'] = $status;
            $data['dt_usulan'] = $this->Siperan_Model->get_usulan($status);
        }
        $data['status'] = $status;
        $this->load->view('region/header');
        $this->load->view('usulan', $data);
        $this->load->view('region/footer');
    }

    public function tambah_usulan()
    {
        $data['act'] = 'tambah';
        $data['dt_account'] = $this->Siperan_Model->get_dt_account();

        $this->load->view('region/header');
        $this->load->view('form_usulan', $data);
        $this->load->view('region/footer');
    }

    public function simpan_usulan()
    {
        if($this->input->post('tambah')){
            $tgl_usul   = date('Y-m-d H:i:s');
            $id_pengusul = $this->input->post('id_pengusul', true);
            $nilai      = $this->input->post('nilai', true);
            //$keterangan = htmlspecialchars($this->input->post('keterangan1'), ENT_QUOTES);
            $rek        = $this->input->post('kas');
            //$account        = $this->input->post('account1');
            $no_ref     = $this->Siperan_Model->get_no_ref($rek);


            $ju = $this->input->post('ju');
            $keterangan = "<table border='1' width='100%'><tr><td>No</td><td>Usulan</td><td>Account</td></tr>";
            $di = array();
            $j = 1;
            for($u=1; $u<=10; $u++){
                $ket = $this->input->post('keterangan'.$u);
                $acc = $this->input->post('account'.$u);
                if($ket != '' && $acc != ''){
                    $di[] = array('no_ref'=>$no_ref, 'keterangan'=>htmlspecialchars($ket, ENT_QUOTES), 'kd_account'=>$acc, 'accounter'=>$id_pengusul);
                    $nm_acc = $this->Siperan_Model->get_account($acc);
                    $keterangan .= "<tr><td>{$j}.</td><td>{$ket}</td><td>{$acc} ({$nm_acc})</td></tr>";
                    $j++;
                }
            }
            $keterangan .= "</table>";

            $data = array(
                'tgl_usul' => $tgl_usul,
                'pengusul' => $id_pengusul,
                'nilai' => $nilai,
                'keterangan' => htmlspecialchars($keterangan, ENT_QUOTES),
                'no_ref' => $no_ref,
                'rekening'=> $rek

            );
            /*'kd_account' => $account*/
            $simpan = $this->Siperan_Model->simpan_usulan($data, 'tambah');
            $simpan_nota = $this->Siperan_Model->simpan_batchnota($di);



            if($simpan){
                $this->session->set_flashdata('simpan_ok', 'Usulan keuangan telah diajukan kepada Ketua AMIK/STMIK/STIE MDP. <br/>Respon persetujuan dapat dilihat pada menu Usulan atau Email anda.<br/><br/>Terima kasih');
                $mail['subject']    = "SIPERAN - Usulan Keuangan";
                $mail['body']       = "Yth Ketua AMIK/STMIK/STIE MDP<br/><br/>Ini adalah email notifikasi usulan keuangan dari Administrasi Keuangan MDP.
                    <br/><br/>No.Ref : <b>".$no_ref."</b>
                    <br/>Pengusul : <b>".$this->session->userdata('admin_user')."</b>
                    <br/>Nilai Usulan : <b>Rp ".number_format($nilai, 2, ",", ".")."</b>
                    <br/>Keterangan : ". htmlspecialchars_decode($keterangan) ."
                    <br/><br/>Untuk memberikan persetujuan dari usulan yang diajukan dapat dilakukan melalui <a href='".site_url()."'>SIPERAN</a>
                    <br/><br/>Terimakasih<br/><br/>SIPERAN";
                $mail['mail_from']  =  $this->session->userdata('admin_email');
                if($this->session->userdata('admin_level') == 'SuperAdmin'){
                    $mail['mail_to']    =  $this->session->userdata('admin_email');
                    $mail['mail_cc']    =  "rachmat.nur91@gmail.com";
                }else{
                    $mail['mail_to']    =  $this->Siperan_Model->get_mail_ketua(); //$this->session->userdata('admin_user');
                    $mail['mail_cc']    =  "yulistia@mdp.ac.id, megawati@stie-mdp.ac.id";//$this->session->userdata('admin_email');
                }


                //$mail['mail_to'] = 'rachmat.nur91@staff.mdp.ac.id';
                //$mail['mail_cc'] = 'rachmat.nur91@staff.mdp.ac.id';
                $this->mailsender->set_vars($mail);
                $this->mailsender->send_mail();
                redirect('peran/usulan');
            }else{
                $this->session->set_flashdata('f_nilai', $nilai);
                $this->session->set_flashdata('f_keterangan', $keterangan);
                $this->session->set_flashdata('simpan_ggl', 'Usulan keuangan gagal disimpan dan diajukan kepada Ketua AMIK/STMIK/STIE MDP. Silakan coba lagi atau hubungi UPT SI.<br/><br/>Terima kasih');
                redirect('peran/tambah_usulan');
            }
        }

        if($this->input->post('ubah')){
            $id_pengusul = $this->input->post('id_pengusul', true);
            $id_usulan  = $this->input->post('id_usulan', true);
            $nilai      = $this->input->post('nilai', true);
            $keterangan = htmlspecialchars($this->input->post('keterangan'), ENT_QUOTES);
            $account        = $this->input->post('account');
            $data = array(
                'nilai' => $nilai,
                'keterangan' => $keterangan,
                'kd_account' => $account
            );
            //echo "Keterangan : ".$this->input->post('keterangan');
            //print_r($this->input->post());
            $simpan = $this->Siperan_Model->simpan_usulan($data, 'ubah', array('id'=>$id_usulan));
            if($simpan){
                $this->session->set_flashdata('simpan_ok', 'Usulan keuangan telah diubah. <br/>Respon persetujuan dapat dilihat pada menu Usulan.<br/><br/>Terima kasih');
                redirect('peran/usulan');
            }else{
                $this->session->set_flashdata('simpan_ggl', 'Usulan keuangan gagal diubah. Silakan coba lagi atau hubungi UPT SI.<br/><br/>Terima kasih');
                //echo gagal;
                redirect('peran/ubah_usulan/'.$id_pengusul, 'refresh');
            }
        }

        redirect('peran/usulan');

    }

    public function saveImage()
    {
        $name = $this->_randomString();
        $ext = explode('.',$_FILES['file']['name']);
        $filename = $name.'.'.$ext[1];
        $destination = 'uploads/'.$filename;
        $location =  $_FILES["file"]["tmp_name"];
        move_uploaded_file($location,$destination);
        echo base_url("uploads/".$filename);
        //echo "http://static.adzerk.net/Advertisers/03ad6f74d6ae4bf0bc7044a509c6027d.png";
    }
    private function _randomString() {
        return md5(rand(100, 200));
    }

    public function ubah_usulan($id)
    {
        $data['act'] = 'ubah';
        $data['dt_account'] = $this->Siperan_Model->get_dt_account();
        $data['dt_edit'] = $this->Siperan_Model->edit_usulan($id);
        if(count($data['dt_edit']) > 0){
            $this->load->view('region/header');
            $this->load->view('form_editusulan', $data);
            $this->load->view('region/footer');
        }else{
            redirect('peran/usulan');
        }
    }

    public function batal_usulan($id = null)
    {
        $batal = $this->Siperan_Model->batal_usulan($id);
        if($batal){
            $this->session->set_flashdata('simpan_ok', 'Usulan keuangan telah dibatalkan.<br/><br/>Terima kasih');
        }

        redirect('peran/usulan');
    }

    /*Persetujuan*/
    public function persetujuan($status = null)
    {
        $this->_cek_akses(cek_akses_persetujuan());
        $data['dt_usulan'] = $this->Siperan_Model->get_usulan('diaju');

        $this->load->view('region/header');
        $this->load->view('persetujuan', $data);
        $this->load->view('region/footer');
    }

    public function setuju_usulan($id = null)
    {
        $usulans = array();
        if($this->input->post()){
            $data = array();
            foreach($this->input->post('setuju') as $key => $val){
                $data[] = array('id' => $val);
                $usulans[] = $this->Siperan_Model->detail_usulan($val);
            }
            $setuju = $this->Siperan_Model->setuju_usulan_array($data);
            if($setuju){
                $this->session->set_flashdata('simpan_ok', 'Usulan keuangan telah disetujui pada <strong>'.date('Y-m-d H:i:s').'</strong>.<br/><br/>Terima kasih');
                $this->session->set_flashdata('simpan_dt', $setuju);
            }
        }else{
            $setuju = $this->Siperan_Model->setuju_usulan(array('id'=>$id));
            if($setuju){
                $usulans[] = $this->Siperan_Model->detail_usulan($id);
                $this->session->set_flashdata('simpan_ok', 'Usulan keuangan telah disetujui pada <strong>'.date('Y-m-d H:i:s').'</strong>.<br/><br/>Terima kasih');
            }
        }

        $level = $this->session->userdata('admin_level');

        if($level == 'Yayasan' && $setuju) {
            $mail['subject'] = "SIPERAN - Persetujuan Usulan Keuangan";
            $mail['body'] = "Yth BAK MDP<br/><br/>Ini adalah email notifikasi persetujuan usulan keuangan.
<br/>Beberapa usulan keuangan yang anda ajukan telah disetujui oleh Ketua Yayasan MDP. Berikut usulan yang telah disetujui.<br/><hr/>";
            foreach ($usulans as $key => $val) {
                $mail['body'] .= "No.Ref : " . $val['no_ref'] . "<br/>";
                $mail['body'] .= "Pengusul : " . $val['user'] . "<br/>";
                $mail['body'] .= "Nilai Usulan : Rp " . number_format($val['nilai'], 2, ",", ".") . "<br/>";
                $mail['body'] .= "Keterangan : " . htmlspecialchars_decode($val['keterangan']) . "<br/><hr/>";
            }
            $mail['body'] .= "<br/>Usulan yang telah disetujui Ketua Yayasan, selengkapnya dapat dilihat di <a href='" . site_url() . "'>SIPERAN</a>
<br/><br/>Terimakasih<br/><br/>SIPERAN";
            $mail['mail_from'] = $this->session->userdata('admin_email');
            $mail['mail_to'] = $this->Siperan_Model->get_mail_bak(); //$this->session->userdata('admin_user'); "rachmat.nur91@staff.mdp.ac.id";
            //$mail['mail_to']   = 'rachmat.nur91@staff.mdp.ac.id';
            $mail['mail_cc'] = 'johannes@mdp.ac.id'; //"rachmat.nur91@gmail.com";//
        }

        if($level == 'Ketua' && $setuju) {
            $mail['subject'] = "SIPERAN - Persetujuan Usulan Keuangan";
            $mail['body']     = "Yth Yayasan MDP<br/><br/>Ini adalah email notifikasi usulan keuangan dari Adminstrasi Keuangan MDP yang telah diberi persetujuan oleh Ketua AMIK/STMIK/STIE MDP. <br/><hr/>";
            foreach ($usulans as $key => $val) {
                $mail['body'] .= "No.Ref : " . $val['no_ref'] . "<br/>";
                $mail['body'] .= "Pengusul : " . $val['user'] . "<br/>";
                $mail['body'] .= "Nilai Usulan : Rp " . number_format($val['nilai'], 2, ",", ".") . "<br/>";
                $mail['body'] .= "Keterangan : " . htmlspecialchars_decode($val['keterangan']) . "<br/><hr/>";
            }
            $mail['body'] .= "<br/>Usulan yang telah disetujui Ketua AMIK/STMIK/STIE MDP, selengkapnya dapat dilihat di <a href='" . site_url() . "'>SIPERAN</a> untuk diberikan persetujuan
<br/><br/>Terimakasih<br/><br/>SIPERAN";
            $mail['mail_from'] = $this->session->userdata('admin_email');
            $mail['mail_to']   = $this->Siperan_Model->get_mail_yay(); //$this->session->userdata('admin_user');
            //$mail['mail_to']   = 'rachmat.nur91@staff.mdp.ac.id';
            $mail['mail_cc']   = "johannes@mdp.ac.id, yulistia@mdp.ac.id, megawati@stie-mdp.ac.id";//$this->session->userdata('admin_email');
        }

        $this->mailsender->set_vars($mail);
        //print_r($mail);
        $this->mailsender->send_mail();

        redirect('peran/persetujuan');
    }

    public function tolak_usulan($id = null)
    {
        $tolak = $this->Siperan_Model->tolak_usulan(array('id'=>$id));
        if($tolak){
            $this->session->set_flashdata('simpan_ok', 'Usulan keuangan telah ditolak pada <strong>'.date('Y-m-d H:i:s').'</strong>.<br/><br/>Terima kasih');
        }

        redirect('peran/persetujuan');
    }

    public function minta_dana($id = null, $rek = null)
    {
        $minta = $this->Siperan_Model->minta_dana(array('id'=>$id, 'status' => '1'), $rek);
        if($minta){
            $this->session->set_flashdata('simpan_ok', 'Permintaan transfer telah disampaikan ke Bendahara Yayasan MDP <strong>'.date('Y-m-d H:i:s').'</strong>.<br/><br/>Terima kasih');

            $usulan = $this->Siperan_Model->detail_usulan($id);
            $mail['subject']    = "SIPERAN - Permohonan Transfer Dana";
            $mail['body']       = "Yth Bendahara Yayasan MDP<br/><br/>Ini adalah email notifikasi Permohonan Transfer Dana berdasarkan usulan keuangan yang telah diusulkan
<br/><br/>Pengusul : <b>".$this->session->userdata('admin_user')."</b> (".$this->session->userdata('admin_email').")
<br/>No. Ref : <b>".$usulan['no_ref']."</b>
<br/>Nilai Usulan : <b>Rp ".number_format($usulan['nilai'], 2, ",", ".")."</b>
<br/>Keterangan : ". htmlspecialchars_decode($usulan['keterangan']) ."
<br/><br/>Dan telah disetujui oleh Ketua Yayasan pada :
<br/>Tanggal : <b>".date("d-m-Y H:i:s", strtotime($usulan['tgl_rev']))."</b>";
            if($rek == "tunai"){
                $mail['body'] .="<br/><br/>Dimohon untuk menyiapkan Dana sesuai nilai usulan di atas untuk dapat diambil secara <strong>TUNAI</strong>.";
            }elseif($rek == "reklain"){
                $tgl_usul   = date('Y-m-d H:i:s');
                $no_ref = $this->input->post('ref-no');
                $id_usulan = $this->input->post('idusulan');
                $pesan = htmlspecialchars($this->input->post('message-text'), ENT_QUOTES);
                $nama_penerima = $this->input->post('recipient-name');
                $rek_penerima = $this->input->post('recipient-rek');

                $data = array(
                    'tgl_permintaan' => $tgl_usul,
                    'no_ref' => $no_ref,
                    'id_usulan' => $id_usulan,
                    'nama_penerima' => $nama_penerima,
                    'rek_penerima' => $rek_penerima,
                    'pesan'=> $pesan

                );
                $simpan = $this->Siperan_Model->simpan_transfer($data);

                $mail['body'] .="<br/><br/>Dimohon untuk menransfer Dana sesuai nilai usulan di atas ke rekening berikut :";
                $mail['body'] .="<br/>Nama Penerima : <strong>". $nama_penerima."</strong>";
                $mail['body'] .="<br/>Nomor Rekening : <strong>". $rek_penerima."</strong>";
                $mail['body'] .="<br/>Pesan / Keterangan Tambahan : ". htmlspecialchars_decode($pesan);
            }else{
                $mail['body'] .="<br/><br/>Dimohon untuk menransfer Dana sesuai nilai usulan di atas ke rekening <strong>".strtoupper($rek)."</strong>.";
            }

            $mail['body'] .="<br/><br/>Terimakasih<br/><br/>SIPERAN";
            $mail['mail_from']  =  $this->session->userdata('admin_email');

            $mail['mail_cc']    =  "yulistia@mdp.ac.id, megawati@stie-mdp.ac.id";//$this->session->userdata('admin_email');
            $mail['mail_to']    =  $this->Siperan_Model->get_mail_bdh(); //$this->session->userdata('admin_user');
            //$mail['mail_to']    = "rachmat.nur91@staff.mdp.ac.id";


            $this->mailsender->set_vars($mail);
            $this->mailsender->send_mail();
        }

        redirect('peran/usulan');
    }

    public function detail_usulan($id){
        $usulan = $this->Siperan_Model->detail_usulan($id);
        if($usulan['rekening'] === '1'){
            $kas = "<span class='label label-info'>STMIK</span>";
        }elseif($usulan['rekening'] == '2'){
            $kas = "<span class='label label-success'>STIE</span>";
        }elseif($usulan['rekening'] == '3'){
            $kas = "<span class='label label-default'>Bersama</span>";
        }else{
            $kas = "<span class='label label-danger'>N/A</span>";
        }
        //
        echo '<table class="table table-bordered table-striped table-hover">
                <tr>
                    <td>No. Ref</td>
                    <td> : </td>
                    <td>'.$usulan['no_ref'].' </td>
                </tr>
                <tr>
                    <td>Kas</td>
                    <td> : </td>
                    <td>'.$kas.' </td>
                </tr>
                <tr>
                    <td>Tanggal Usul</td>
                    <td> : </td>
                    <td>'.date("d-m-Y H:i:s", strtotime($usulan['tgl_usul'])).' </td>
                </tr>
                <tr>
                    <td>Pengusul</td>
                    <td> : </td>
                    <td>'.$usulan['user'].' </td>
                </tr>
                <tr valign="top">
                    <td>Nilai</td>
                    <td> : </td>
                    <td>Rp '.number_format($usulan['nilai'], 2, ",", ".").'
                    <p class="help-block">'.$this->Terbilang($usulan['nilai']).' Rupiah</p></td>
                </tr>
                <tr valign="top">
                    <td>Keterangan</td>
                    <td> : </td>
                    <td class="det-keet">'.htmlspecialchars_decode($usulan['keterangan']).' </td>
                </tr>
            </table>';
        //$terbilang = $this->Terbilang(100000);
        //echo "Terbilang ". $this->Terbilang(100000);
    }

    public function Terbilang($satuan)
    {
        $huruf = array ("", "Satu", "Dua", "Tiga", "Empat", "Lima", "Enam", "Tujuh", "Delapan", "Sembilan", "Sepuluh","Sebelas");
        if ($satuan < 12)
            return " ".$huruf[$satuan];
        elseif ($satuan < 20)
            return $this->Terbilang($satuan - 10)." Belas";
        elseif ($satuan < 100)
            return $this->Terbilang($satuan / 10)." Puluh".$this->Terbilang($satuan % 10);
        elseif ($satuan < 200)
            return "Seratus".$this->Terbilang($satuan - 100);
        elseif ($satuan < 1000)
            return $this->Terbilang($satuan / 100)." Ratus".$this->Terbilang($satuan % 100);
        elseif ($satuan < 2000)
            return "Seribu".$this->Terbilang($satuan - 1000);
        elseif ($satuan < 1000000)
            return $this->Terbilang($satuan / 1000)." Ribu".$this->Terbilang($satuan % 1000);
        elseif ($satuan < 1000000000)
            return $this->Terbilang($satuan / 1000000)." Juta".$this->Terbilang($satuan % 1000000);
        elseif ($satuan >= 1000000000)
            return "Angka terlalu Besar";
    }

    /*Transfer Dana*/
    public function transfer($status = null)
    {
        $data['dt_usulan'] = $this->Siperan_Model->get_usulan('transfer');
        $data['active'] = 'transfer';
        $this->load->view('region/header');
        $this->load->view('transfer', $data);
        $this->load->view('region/footer');
    }

    /*Laporan*/
    public function daftar_usulan($status = null)
    {
        $data['dt_usulan'] = $this->Siperan_Model->get_usulan('setuju');

        $this->load->view('region/header');
        $this->load->view('rekap_usulan', $data);
        $this->load->view('region/footer');
    }

    public function daftar_pettycash($status = null)
    {
        $data['dt_usulan'] = array();

        $this->load->view('region/header');
        $this->load->view('rekap_pettycash', $data);
        $this->load->view('region/footer');
    }

    public function daftar_kategori($status = null)
    {
        $data['dt_usulan'] = array();
        if($this->input->post('print')) {
            $this->load->view('rekap_kategori_excel', $data);
        }else{
            $this->load->view('region/header');
            $this->load->view('rekap_kategori', $data);
            $this->load->view('region/footer');
        }

    }

    /* Nota */
    public function nota($aksi = null, $pcash = null, $kas=null)
    {
        $this->_cek_akses(cek_akses_nota());
        //$data['dt_semuanota'] = $this->Siperan_Model->get_dt_nota();
        $data['dt_notakosong'] = $this->Siperan_Model->get_dt_notakosong();
        if($pcash != '' && $kas !=""){
            $data['dt_nota'] = $this->Siperan_Model->get_nota_pcash($pcash);
            $data['kd_pcash'] = $pcash;
            $data['kas'] = $kas;
        }
        $data['active'] = $aksi;
        $this->load->view('region/header');
        $this->load->view('nota', $data);
        $this->load->view('region/footer');
    }

    public function buat_nota()
    {
        $this->_cek_akses(cek_akses_nota());

        if($this->input->post() || $this->session->flashdata('tgl_nota')){
            $data['dt_account'] = $this->Siperan_Model->get_dt_account();
            $tgl_nota = ($this->input->post('tgl_nota', true)) ? $this->input->post('tgl_nota', true) : $this->session->flashdata('tgl_nota');
            $tgl_nota= date("Y-m-d", strtotime($tgl_nota));
            //(DATE_FORMAT( t_usulan.tgl_usul,  '%Y-%m-%d' )
            $kas_nota = ($this->input->post('nota_kas', true)) ? $this->input->post('nota_kas', true) : $this->session->flashdata('nota_kas');
            $data['dt_nota'] = $this->Siperan_Model->get_nota($tgl_nota, $kas_nota);
            $data['dt_noref'] = $this->Siperan_Model->get_dt_noref($tgl_nota, $kas_nota);
            $data['dt_pcash'] = $this->Siperan_Model->get_dt_pcash($kas_nota);
            //print_r($data['dt_noref']);
            $data['act'] = "buat_nota";

            $data['tgl_nota'] = $tgl_nota;
            $data['nota_kas'] = $kas_nota;
            $data['lock'] = true;

            $this->session->set_flashdata('tgl_nota', $tgl_nota);
            $this->session->set_flashdata('nota_kas', $kas_nota);

            if($this->session->flashdata('tgl_nota')){
                $this->session->keep_flashdata('tgl_nota');
            }

            if($this->session->flashdata('nota_kas')){
                $this->session->keep_flashdata('nota_kas');
            }
            $this->load->view('region/header');
            $this->load->view('nota', $data);
            $this->load->view('region/footer');
        }else{
            redirect('peran/nota', 'refresh');
        }

    }

    public function ubah_nota($id, $no_ref, $kas_nota =Null, $tgl_nota=Null)
    {
        $this->_cek_akses(cek_akses_nota());

        $data['dt_edit'] = $this->Siperan_Model->edit_nota($id, $no_ref, str_replace('-','/',$tgl_nota), $kas_nota);
        $data['dt_editkosong'] = $this->Siperan_Model->edit_notakosong($id, $no_ref);
        $data['dt_account'] = $this->Siperan_Model->get_dt_account();


        //print_r($data['dt_edit']);
        if(count($data['dt_edit']) > 0 && $kas_nota == '') {
            $data['dt_nota'] = $this->Siperan_Model->get_nota(str_replace('-', '/', $tgl_nota), $kas_nota);
            $data['act']    = "edit_nota";
            $data['lock']   = true;

            $data['tgl_nota'] = str_replace('-', '/', $tgl_nota);
            $data['nota_kas'] = $kas_nota;

            if ($this->session->flashdata('tgl_nota')) {
                $this->session->keep_flashdata('tgl_nota');
            }

            if ($this->session->flashdata('nota_kas')) {
                $this->session->keep_flashdata('nota_kas');
            }

            $this->load->view('region/header');
            $this->load->view('nota', $data);
            $this->load->view('region/footer');
        }elseif(count($data['dt_editkosong']) > 0) {
            $data['act']    = "edit_notakosong";
            $data['lock']   = true;
            $this->load->view('region/header');
            $this->load->view('nota', $data);
            $this->load->view('region/footer');
        }else{

            redirect('peran/nota', 'refresh');
        }

    }

    public function simpan_nota($back = null)
    {
        /*if($this->input->post('simpan')){
            $no_ref   = $this->input->post('no_ref', true);
            $nota_kas   = $this->input->post('nota_kas', true);
            $tgl_nota   = $this->input->post('tgl_nota', true);
            $tgl_nota   = date("Y-m-d", strtotime($tgl_nota));
            $nilai      = $this->input->post('nilai', true);
            $account      = $this->input->post('account', true);
            $keterangan  = htmlspecialchars($this->input->post('keterangan'), ENT_QUOTES);
            if($_FILES['nota']['name']){
                $nota =  $this->saveNota();
            }else{
                $nota['status'] = true;
                $nota['msg']    = 'nofile';
            }

            if($nota['status'] == true){
                if($nota['msg'] == 'nofile'){
                    $data = array(
                        'no_ref'=>$no_ref,
                        'nilai'=>$nilai,
                        'keterangan' => $keterangan,
                        'tgl_nota' => $tgl_nota,
                        'kd_account' => $account,
                        'nota_kas'=>$nota_kas,
                        'accounter'=>$this->session->userdata('admin_id'));
                }else{
                    $data = array(
                        'no_ref'=>$no_ref,
                        'nilai'=>$nilai,
                        'keterangan' => $keterangan,
                        'tgl_nota' => $tgl_nota,
                        'kd_account' => $account,
                        'nota_kas'=>$nota_kas,
                        'accounter'=>$this->session->userdata('admin_id'),
                        'f_nota' => $nota['msg']);
                }
                $simpan = $this->Siperan_Model->simpan_nota($data);
            }else{
                $simpan = false;
            }

            if($simpan){
                $this->session->set_flashdata('simpan_ok', 'Nota berhasil disimpan. Terimakasih');
            }else{
                $this->session->set_flashdata('simpan_ggl', 'Nota gagal disimpan. <br/><br/>Note : '.$nota['msg'] . $this->db->_error_message());
            }
        }*/

        if($this->input->post('update')){
            $no_ref   = $this->input->post('no_ref', true);
            $id_nota   = $this->input->post('id_nota', true);
            $nota_kas   = $this->input->post('nota_kas', true);
            $tgl_nota_baru  = $this->input->post('tgl_nota', true);
            $tgl_nota_baru  = date("Y-m-d", strtotime($tgl_nota_baru));
            $tgl_nota       = $this->input->post('tgl_lama', true);
            $keterangan     = htmlspecialchars($this->input->post('keterangan'), ENT_QUOTES);
            $kd_account = $this->input->post('account', true);
            $nilai_nota = $this->input->post('nilai', true);
            $petty_cash = $this->input->post('pettycash', true);



            if($_FILES['nota']['name']){
                $nota =  $this->saveNota($petty_cash);
            }else{
                $nota['status'] = true;
                $nota['msg']    = 'nofile';
            }

            if($nota['status'] == true){
                if($nota['msg'] == 'nofile'){
                    $data = array(
                        'tgl_nota' => $tgl_nota_baru,
                        'nota_kas'=>$nota_kas,
                        'keterangan' => $keterangan,
                        'accounter'=>$this->session->userdata('admin_id'),
                        'kd_account'=>$kd_account,
                        'pcash'=>$petty_cash,
                        'nilai'=>$nilai_nota);
                }else{
                    $data = array(
                        'tgl_nota' => $tgl_nota_baru,
                        'nota_kas'=>$nota_kas,
                        'keterangan' => $keterangan,
                        'accounter'=>$this->session->userdata('admin_id'),
                        'kd_account'=>$kd_account,
                        'pcash'=>$petty_cash,
                        'nilai'=>$nilai_nota,
                        'f_nota' => $nota['msg']);
                }
                $simpan = $this->Siperan_Model->simpan_nota($data, array('id_nota'=>$id_nota, 'no_ref'=>$no_ref));
            }else{
                $simpan = false;
            }

            if($simpan){
                $this->session->set_flashdata('simpan_ok', 'Nota berhasil diupdate. Terimakasih');
            }else{
                $this->session->set_flashdata('simpan_ggl', 'Nota gagal diupdate. <br/><br/>Note : '.$nota['msg'] . $this->db->_error_message());
            }

            $this->session->set_flashdata('tgl_nota', $tgl_nota);
            $this->session->set_flashdata('nota_kas', $nota_kas);
            //print_r($data);
           // print_r($this->input->post());
            $this->_go_back($back);
            //redirect('peran/nota/created');
        }else{
            redirect('peran/buat_nota');

        }
    }

    public function saveNota($petty_cash = null)
    {
        /*$allowed = array('jpg', 'png', 'gif', 'pdf');
        $name   = $this->_randomString();
        $ext    = explode('.',$_FILES['nota']['name']);
        $filename       = $name.'.'.$ext[1];
        $destination    = 'uploads/nota/'.$filename;
        $location       = $_FILES["nota"]["tmp_name"];
        if(in_array($ext[1], $allowed)){
            move_uploaded_file($location,$destination);
            return array('status'=>true ,'msg' => $filename);
        }else{
            return array('status'=>false ,'msg' => 'Format file yang anda upload tidak diizinkan');
        }*/


        $path = 'uploads/nota/';
        is_dir($path) || mkdir($path, '0777', true);
        $config['upload_path'] = $path;
        $config['allowed_types'] = 'jpg|png|gif|pdf|doc|docx';
        $config['max_size']	= '102400';
        $config['remove_spaces'] = 'true';

        $this->load->library('upload', $config);

        if ($this->upload->do_upload('nota'))
        {
            $upload_data = $this->upload->data();
            return array('status'=>true ,'msg' => $upload_data['file_name']);
            //$data['msg_failed'] = $this->upload->display_errors();
        }else{
            $gagal= $this->upload->display_errors();
            return array('status'=>false ,'msg' => strip_tags($gagal));
            /*$nota['status'] = true;
            $nota['msg']    = 'nofile';*/
        }

    }

    /* Petty Cash */
    public function pcash($aksi = 'pcash', $id = null, $kode = null)
    {
        $this->_cek_akses(cek_akses_nota());
        $data['dt_pcash'] = $this->Siperan_Model->get_dt_pcash($id, $kode);
        $data['active'] = $aksi;
        if($aksi == 'buat_nota' && !is_null($id)){
            if($data['dt_pcash'][0]['status'] == '0'){
                $data['tgl_pcash'] =  $data['dt_pcash'][0]['tgl_pcash'];
                $data['kode_pcash'] =  $data['dt_pcash'][0]['kode_pcash'];
                $data['dt_nota'] = $this->Siperan_Model->get_nota_pcash($data['dt_pcash'][0]['kode_pcash']);
                $data['dt_noref'] = $this->Siperan_Model->get_dt_noref_xpcash($data['dt_pcash'][0]['kode_pcash']);
            }

        }
        if($aksi == 'buat'){
            $kode_stmik = $this->Siperan_Model->get_count_pcash('1');
            $kode_stie = $this->Siperan_Model->get_count_pcash('2');


            $kode_stmik = substr($kode_stmik->kode_pcash, 5);
            $kode_stie = substr($kode_stie->kode_pcash, 5);
            if($kode_stmik < 10){
                $kode_stmik = "000".($kode_stmik+1);
            }elseif($kode_stmik < 100){
                $kode_stmik = "00".($kode_stmik+1);
            }else{
                $kode_stmik = "0".($kode_stmik+1);
            }

            if($kode_stie < 10){
                $kode_stie = "000".($kode_stie+1);
            }elseif($kode_stie < 100){
                $kode_stie = "00".($kode_stie+1);
            }else{
                $kode_stie = "0".($kode_stie+1);
            }

            $data['kode_stmik'] = date("Ym").$kode_stmik;
            $data['kode_stie'] = date("Ym").$kode_stie;
        }
        $this->load->view('region/header');
        $this->load->view('pettycash', $data);
        $this->load->view('region/footer');
    }

    public function tambah_nota()
    {
        if($this->input->post('tambah')){
            $id_pcash   = $this->input->post('id', true);
            $kode_pcash   = $this->input->post('kode_pcash', true);

            $tgl_nota   = $this->input->post('tgl_nota', true);
            $tgl_nota = date('Y-m-d', strtotime($tgl_nota));
            $no_ref = $this->input->post('no_ref', true);
            $data = array('kd_pcash' => $kode_pcash);

            if($this->input->post('tambah') == 'tambah_tgl'){
                $simpan = $this->Siperan_Model->simpan_nota($data, array('tgl_nota'=>$tgl_nota));
            }elseif($this->input->post('tambah') == 'tambah_noref'){
                $simpan = $this->Siperan_Model->simpan_nota($data, array('no_ref'=>$no_ref));
            }

            if($simpan){
                $this->session->set_flashdata('simpan_ok', 'Nota berhasil ditambahkan ke Petty Cash '.$kode_pcash.'. Terimakasih');
            }else{
                $this->session->set_flashdata('simpan_ggl', 'Nota gagal ditambahkan ke Petty Cash. <br/><br/>Note : '.$this->db->_error_message());
            }
            redirect('peran/pcash/buat_nota/'.$id_pcash.'/'.$kode_pcash, 'refresh');
        }else{
            redirect('peran/pcash', 'redirect');
        }
    }

    public function simpan_pcash()
    {
        if($this->input->post('simpan')){
            $kd_pcash    = $this->input->post('kode', true);
            $tgl_pcash   = $this->input->post('tgl_pcash', true);
            $tgl_pcash   = date("Y-m-d", strtotime($tgl_pcash));

            $keterangan  = htmlspecialchars($this->input->post('keterangan'), ENT_QUOTES);
            $kas_owner   = $this->input->post('pcash_owner', true);


            $data = array(
                'tgl_pcash'     => $tgl_pcash,
                'kode_pcash'    => $kd_pcash,
                'kas_owner'     => $kas_owner,
                'keterangan'    => $keterangan,
                'lock'    => 'T',
                'accounter'     => $this->session->userdata('admin_id'));

            $simpan = $this->Siperan_Model->simpan_pcash($data);


            if($simpan){
                $this->session->set_flashdata('simpan_ok', 'Petty Cash berhasil dibuat. Terimakasih');
            }else{
                $this->session->set_flashdata('simpan_ggl', 'Petty Cash gagal dibuat. <br/><br/>Note : '. $this->db->_error_message());
            }
        }

        redirect('peran/pcash', 'refresh');
    }

    public function lock_pcash($id, $kd_pcash)
    {
        if($this->input->post()){
            $data = array();
            foreach($this->input->post('lock') as $key => $val){
                $data[] = array('id' => $val);
            }
            $lock = $this->Siperan_Model->lock_pcash_array($data);
            if($lock){
                $this->session->set_flashdata('simpan_ok', 'Petty Cash sudah dikunci. Terimakasih');
                $this->session->set_flashdata('simpan_dt', $lock);
            }
        }

        if($id != '' && $kd_pcash != ''){
            $lock = $this->Siperan_Model->lock_pcash($id, $kd_pcash);
            //echo $this->db->last_query();
            if($lock){
                $this->session->set_flashdata('simpan_ok', 'Petty Cash sudah dikunci. Terimakasih');
            }else{
                $this->session->set_flashdata('simpan_ggl', 'Petty Cash gagal dikunci. <br/><br/>Note : '. $this->db->_error_message());
            }
        }
        redirect('peran/pcash','refresh');
    }

    public function lock_nota($id, $back = false)
    {

        if($id != ''){
            $lock = $this->Siperan_Model->lock_nota($id);
            if($lock){
                $this->session->set_flashdata('simpan_ok', 'Nota sudah dikunci. Terimakasih');
            }else{
                $this->session->set_flashdata('simpan_ggl', 'Nota gagal dikunci. <br/><br/>Note : '. $this->db->_error_message());
            }
        }
        if($back == true){
            if ($this->agent->is_referral())
            {
                redirect($this->agent->referrer(), 'refresh');
            }else{
                redirect('peran/nota','refresh');
            }
        }else{
            redirect('peran/nota/created','refresh');
        }
    }

    public function hapus_nota($id, $no_ref, $back = null)
    {

        if($id != '' && $no_ref != ''){
            $lock = $this->Siperan_Model->hapus_nota($id);
            if($lock){
                $this->session->set_flashdata('simpan_ok', 'Nota dengan no.ref '.$no_ref.' berhasil dihapus dari Petty Cash');
            }else{
                $this->session->set_flashdata('simpan_ggl', 'Nota gagal dihapus. <br/><br/>Note : '. $this->db->_error_message());
            }
        }

        if($back == 'back'){
            if ($this->agent->is_referral())
            {
                redirect($this->agent->referrer(), 'refresh');
            }else{
                redirect('peran/nota','refresh');
            }
        }else{
            redirect('peran/nota/created','refresh');
        }
    }

    public function detail_nota_pcash($kd_pcash)
    {
        $data['dt_nota'] = $this->Siperan_Model->get_nota_pcash($kd_pcash);
        $this->load->view('pop_nota_pcash', $data);

    }
    /* END  PETTY CASH*/

    public function cetak($data= 'pcash', $id='', $kas ='', $tgl='')
    {
        $datas['p'] = $data;
        if($data == "pcash"){
            $datas['data_'.$data] = $this->Siperan_Model->get_nota_pcash($id);
            $datas['pcash'] = $this->Siperan_Model->get_pcash($id);
        }

        if($data == 'rekapkas'){
            $kas = $this->input->post('pcash_owner');
            $datas['data_'.$data] = $this->Siperan_Model->get_apcash($_POST['pettycash']);
        }



        $datas['pk2'] = ($kas == '1') ? "Yulistia, S.Kom., M.T.I." : "Megawati, SE., M.Si";
        $datas['kas'] = ($kas == '1') ? "STMIK" : "STIE";
        $this->load->view('print/print_'.$data, $datas);
    }

    public function password()
    {
        $this->load->view('region/header');
        $this->load->view('password');
        $this->load->view('region/footer');
    }

    public function ubahpassword() {
        $id_user		= $this->session->userdata('admin_user');
        $email		    = $this->input->post('email', true);
        $p1				= sha1($this->input->post('p1'));
        $p2				= sha1($this->input->post('p2'));
        $p3				= sha1($this->input->post('p3'));

        $cek_password_lama	= $this->db->query("SELECT password FROM t_pengguna WHERE username = ?", array($id_user))->row();
        if ($cek_password_lama->password != $p1) {
            $this->session->set_flashdata('k_passwod', '<div id="alert" class="alert alert-error">Password Lama tidak sama</div>');
            redirect('peran/password');
        } else if ($p2 != $p3) {
            $this->session->set_flashdata('k_passwod', '<div id="alert" class="alert alert-error">Password Baru 1 dan 2 tidak cocok</div>');
            redirect('peran/password');
        } else {
            $this->db->query("UPDATE t_pengguna SET email = ?,  password = ? WHERE username = ?", array($email,$p3, $id_user));
            $this->session->set_flashdata('k_passwod', '<div id="alert" class="alert alert-success">Password berhasil diperbaharui</div>');
            redirect('peran/password');
        }
        redirect('peran/password');
    }

    public function logout(){
        $this->session->sess_destroy();
        redirect('welcome');
    }

    private function _cek_akses($akses)
    {
        if(!$akses){
            redirect('peran', 'refresh');
        }
    }

    public function ref_data($id_nota, $no_ref)
    {

        $rdata = $this->Siperan_Model->get_ref_data($id_nota, $no_ref);
        $data =  array('result' => false);
        if(count($rdata)){
            $data['result'] = true;
            $rdata['tebilang'] = Terbilang($rdata['nilai']);
            $rdata['nominal'] = number_format($rdata['nilai'], 2, ",", ".");


            $ket = strip_tags(htmlspecialchars_decode($rdata['keterangan']), "<img>");
            $ketnota = strip_tags(htmlspecialchars_decode($rdata['keterangannota']), "<img>");
            $k_img = preg_replace("/<img[^>]+\>/i", " [ <b>image</b> ] ", $ket);
            $k_imgnota = preg_replace("/<img[^>]+\>/i", " [ <b>image</b> ] ", $ketnota);
            $link_more = " [ <a data-toggle='modal' data-target='#modal' href='#' data-href='".site_url('peran/detail_usulan/'.$rdata['id'])."' ><i class='glyphicon glyphicon-eye-open'></i> detail</a> ]";
            $link_morenota = " [ <a data-toggle='modal' data-target='#modal' href='#' data-href='".site_url('peran/detail_nota/'.$rdata['id_nota'])."' ><i class='glyphicon glyphicon-eye-open'></i> detail</a> ]";

            $rdata['keterangan_clean'] = substr($k_img, 0, 100).((strlen($k_img) > 100) ? $link_more : "");
            $rdata['keterangan_dec'] = htmlspecialchars_decode($rdata['keterangan'], ENT_QUOTES);
            $rdata['keterangan_dec'] = strip_tags($rdata['keterangan_dec']);

            $rdata['keterangannota_clean'] = substr($k_img, 0, 100).((strlen($k_imgnota) > 100) ? $link_morenota : "");
            $rdata['keterangannota_dec'] = htmlspecialchars_decode($rdata['keterangannota'], ENT_QUOTES);
            $rdata['keterangannota_dec'] = strip_tags($rdata['keterangannota_dec']);
            $rdata['tgl_usulan'] = date('d-M-Y', strtotime($rdata['tgl_usul']));

        }
        $data['data'] =  $rdata;
        echo json_encode($data);
    }

    public function pop_usulan()
    {
        $data['dt_noref'] = $this->Siperan_Model->get_all_noref();
        $this->load->view('pop_usulan', $data);

    }

    private function _go_back($back)
    {
        if($back == 'back'){
            if ($this->agent->is_referral())
            {
                redirect($this->agent->referrer(), 'refresh');
            }else{
                redirect('peran/nota','refresh');
            }
        }else{
            redirect('peran/nota/created','refresh');
        }
    }


}
?>