<form method="post" enctype="multipart/form-data" id="form_nota"  action="<?=site_url('peran/simpan_nota')?>" class="form-horizontal">
    <input type="hidden" name="nota_kas" value="<?=$nota_kas;?>">
    <input type="hidden" name="tgl_nota" value="<?=$tgl_nota;?>">
    <input type="hidden" name="no_ref" id="no_ref" value="">
    <div class="form-group">
        <label for="inAccount" class="col-sm-1 control-label">Usulan</label>
        <div class="col-sm-11">
            <div class="input-group  select2-bootstrap-prepend">
                <span class="input-group-btn">
                    <button class="btn btn-default" type="button" data-select2-open="single-prepend-text">
                        <span class="glyphicon glyphicon-search"></span>
                    </button>
                </span>
                <select id="single-prepend-text" name="id_nota" required class="form-control select2-allow-clear select2no_ref">
                    <option></option>
                    <optgroup label="Usulan">
                        <?php

                        if(count($dt_noref)){
                            foreach ($dt_noref as $acc) {
                                echo "<option value=\"".$acc['id_nota']."\" title=\"".$acc['no_ref']."\" data-account=\"".$acc['account']."\" data-nilai=\"".$acc['nilai']."\"  data-keterangan=\"".$acc['keterangan']."\" >".date('d-M-Y', strtotime($acc['tgl_usul'])). " # " . $acc['no_ref']. " # " .strip_tags(htmlspecialchars_decode($acc['keterangan'], ENT_QUOTES))."</option>";
                            }
                        }
                        ?>
                    </optgroup>
                </select>
                <div class="input-group-btn">
                    <?php
                    $atts = array(
                        'width'      => '800',
                        'height'     => '600',
                        'scrollbars' => 'yes',
                        'status'     => 'yes',
                        'resizable'  => 'yes',
                        'screenx'    => '0',
                        'screeny'    => '0'
                    );

                    $l_pop = "<button type=\"button\" class=\"btn btn-default\" aria-label=\"Help\"><span class=\"glyphicon glyphicon-list-alt\"></span></button>";

                    echo "<a data-toggle='modal' data-target='#modal_dt_ref' href='#' data-href='".site_url('peran/pop_usulan')."' >";
                    echo $l_pop;
                    echo "</a>";
                    //echo anchor_popup('peran/pop_usulan', $l_pop, $atts);
                    ?>

                </div>
            </div>
        </div>

        <span id="helpket" class="help-block col-sm-offset-1 col-sm-11">Keterangan Usulan</span>
        <span id="helpnilai" style="color: #468847 !important;" class="help-block col-sm-offset-1 col-sm-11">Nilai</span>
    </div>

    <div class="alert alert-info" role="alert">
        <i class="icon-tasks"></i> Buat Nota
    </div>

    <div class="form-group has-warning">
        <span style="color: #468847 !important;" class="help-block col-sm-offset-2 col-sm-10">Pilih Petty Cash. Buat Petty cash melalui menu Petty Cash. Petty Cash yang tampil adalah Petty Cash yang belum terkunci</span>
        <label for="inPcash" class="col-sm-2 control-label">Petty Cash</label>
        <div class="input-group col-sm-9">
            <div class="input-group-addon"><i class="glyphicon glyphicon-briefcase"></i></div>
            <select id="inPcash" name="pettycash" required class="form-control select2-allow-clear select2_pcash">
                <optgroup label="Petty Cash">
                    <?php
                    $dt_pcash = $this->Siperan_Model->get_dt_pcash($nota_kas);
                    print_r($dt_pcash);
                    if(count($dt_pcash)){
                        foreach ($dt_pcash as $pcash) {
                            if($pcash['lock'] == 'T')
                                echo "<option value=\"".$pcash['id']."\" >".$pcash['kode_pcash']." - ".$pcash['keterangan']."</option>";
                        }
                    }
                    ?>
                </optgroup>
            </select>
        </div>
    </div>
    <hr style="border-color: #468847;"/>

    <div class="form-group">
        <label for="inAccount" class="col-sm-1 control-label">Account</label>
        <div class="col-sm-5">
            <div class="input-group select2-bootstrap-prepend">
                <span class="input-group-btn">
                    <button class="btn btn-default" type="button" data-select2-open="single-prepend-text">
                        <span class="glyphicon glyphicon-search"></span>
                    </button>
                </span>
                <select id="single-prepend-text" name="account" required class="form-control select2-allow-clear select2">
                    <option></option>
                    <optgroup label="Master Account">
                        <?php
                        if(count($dt_account)){
                            foreach ($dt_account as $acc) {
                                echo "<option value=\"".$acc['kd_account']."\">".$acc['account']."</option>";
                            }
                        }
                        ?>
                    </optgroup>
                </select>
            </div>
        </div>
        <label for="inNilai" class="col-sm-1 control-label">Nota</label>
        <div class="input-group col-sm-5">
            <input type="file" name="nota">
        </div>
        <span class="help-block col-sm-offset-7 col-sm-5">Nota dapat berupa gambar atau file pdf</span>
    </div>


    <div class="form-group">
        <label for="inKeterangan" class="col-sm-1 control-label">Keterangan</label>
        <div class="input-group col-sm-11">
            <textarea class="form-control" name="keterangan" id="defaultketnota" rows="3"></textarea>
        </div>
    </div>

    <div class="form-group">
        <span style="color: #468847 !important;" class="help-block col-sm-offset-1 col-sm-11">Nilai berikut adalah total nilai dari usulan yang diajukan. Ubah sesuai nilai NOTA</span>
        <label for="inNilai" class="col-sm-1 control-label">Nilai</label>

        <div class="input-group col-sm-11">
            <div class="input-group-addon">Rp </div>
            <input type="number" name="nilai" min="1" id="inNilai" class="form-control" required>
            <div class="input-group-addon">,00</div>
        </div>
        <span id="helpSatuan" class="help-block col-sm-offset-1 col-sm-11"></span>
    </div>

    <div class="form-group">
        <div class="col-sm-offset-1 col-sm-11">
            <button type="submit" name="update" value="Simpan" data-loading-text="Simpan Nota..." class="btn btn-primary" autocomplete="off">Simpan</button>
        </div>
    </div>
</form>


<div class="alert alert-danger" role="alert">
    <span class="pull-left" ><i class="icon-tasks"></i> Nota</span>
    <?php
    $act_lock = " <a href='".site_url('peran/lock_nota/'.$nota_kas.'/'.$tgl_nota)."' class='pull-right'><button class='btn btn-warning btn-sm '><i class='glyphicon glyphicon-lock'></i> Kunci</button></a>";
    echo $act_lock;
    ?>
    <div class="clearfix"></div>
</div>